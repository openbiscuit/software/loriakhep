
#ifndef __TEXT_CAIRO_H
#define __TEXT_CAIRO_H

#include "vec4f.h"
#include <gtkmm.h>
#include "gadjet_cairo.h"

enum TAlign {
  ALIGN_CENTER = 0,
  ALIGN_LEFT,
  ALIGN_RIGHT
};

/**
 * Display a string using Cairo. The string can be inside a rectangle.
 * Depending of the alignment chosent, the position is either:
 * - ALIGN_RIGHT: the bottom left point of the bounding box
 * - ALIGN_LEFT: the bottom right point of the bounding box
 * - ALIGN_CENTER: the bottom middle point of the bounding box
 */ 
class TextCAIRO : public virtual GadjetCAIRO
{
 public:
  /**
   * @name Creation/Destruction
   */
  /**
   * Constructor.
   *
   * @param text String to be displayed
   * @param align where is the bounding box relative to the position
   * @param is_line do we draw the bounding box.
   */
  TextCAIRO( std::string text,
	     TAlign align = ALIGN_CENTER, bool is_line = true);
  /**
   * Destructor.
   */
  ~TextCAIRO();
  //@}
  
  /**
   * @name box
   */
  /**
   * Get the bounding box around the text.
   */
  virtual Cairo::Rectangle get_external_box( Cairo::RefPtr<Cairo::Context> cr);
  /**
   * Set if bow is dranw.
   */
  virtual void set_is_line( bool is_lined );
  /**
   * Is the bow dranw.
   */
  virtual bool get_is_line(void);
  /**
   * Set the width of the line for drawing the box.
   */
  virtual void set_line_width( float line_width );
  /**
   * Get the width of the line for drawing the box.
   */
  virtual float get_line_width(void);
  /**
   * Set the coef by which the bouding box extends the text.
   */ 
  virtual void set_coef_box_extend( float coef_box_extend );
  /**
   * Get the coef by which the bouding box extends the text.
   */ 
  virtual float get_coef_box_extend(void);
  //@}

  /**
   * @name text
   */
  /**
   * Set the text to be displayed.
   */
  virtual void set_text( std::string text );
  /**
   * Set the alignment for the text box.
   */
  virtual void set_align( TAlign align );
  /**
   * Get the alignment for the text box.
   */
  virtual TAlign get_align(void);
  /**
   * Select the font for the text.
   */
  virtual void select_font( std::string family, float size = 0.2,
		    Cairo::FontSlant slant = Cairo::FONT_SLANT_NORMAL,
		    Cairo::FontWeight weight = Cairo::FONT_WEIGHT_NORMAL);
  //@}

  protected:
  /**
   * Render the Text using a Cairo::Context.
   */
  virtual void render( Cairo::RefPtr<Cairo::Context> cr );
  /**
   * Set the font, the text and compute the bounding box.
   */
  virtual void compute_internal_positions( Cairo::RefPtr<Cairo::Context> cr );
  
 protected:
  /** Internal position of the text. */
  Position _pos_start_text;
  /** Coefficient for extending the bounding box. */
  float _coef_box_extend;
  /** Text to be displayed. */
  std::string _text;
  /** The bounding box. */
  Cairo::Rectangle _external_box;
  /** Is the bouding bow to be drawn. */
  bool _is_line;
  /** Width of the bouding box line. */
  float _line_width;
  /** Alignment of text according to position. */
  TAlign _align;
  /** Font family. */
  std::string _fnt_family;
  /** Font slant. */
  Cairo::FontSlant _fnt_slant;
  /** Font weight. */
  Cairo::FontWeight _fnt_weight;
  /** Font size. */
  float _fnt_size;
};

#endif //__TEXT_CAIRO_H
