# Standard things
# saves the variable $(d) that holds the current directory on the stack,
# and sets it to the current directory given in $(dir), 
# which was passed as a parameter by the parent rules.mk

sp 		:= $(sp).x
dirstack_$(sp)	:= $(d)
d		:= $(dir)

# Subdirectories, if any, in random order

#dir	:= $(d)/test
#include		$(dir)/Rules.mk

# Next, we create an immediately evaluated variable $(OBJS_common) 
# that holds all the objects in that directory.
# We also create $(DEPS_common), which lists any automatic dependency files 
# generated later on.
# To the global variable $(CLEAN), we add the files that the rules present here 
# may create, i.e. the ones we want deleted by a make clean command.)

# Local variables

CORE_OBJS_$(d)	:= $(d)/drawingarea_cairo.o \
                   $(d)/gadjet_cairo.o \
                   $(d)/jauge_cairo.o \
                   $(d)/text_cairo.o \
                   $(d)/jaugetext_cairo.o \
                   $(d)/joystick_gtkmm.o \
                   $(d)/overlay_viewer.o

CORE_DEPS_$(d)	:= $(CORE_OBJS_$(d):%=%.d)

CLEAN		:= $(CLEAN) $(CORE_OBJS_$(d)) $(CORE_DEPS_$(d)) \
                   $(d)/libgadget.a  $(d)/libgadget.so

VERYCLEAN	:= $(VERYCLEAN) $(d)/*~

# target local
TGT_LIB		:= $(TGT_LIB) $(d)/libgadget.a $(d)/libgadget.so

# Now we list the dependency relations relevant to the files in this subdirectory.
# Most importantly, while generating the objects listed here, we want to set 
# the target-specific compiler flags $(CF_TGT) to include a flag that adds 
# this directory to the include path, so that local headers may be included 
# using #include <localfile.h>, which is, as said, more portable 
# than local includes when the source directory is not the current directory.)

# Local rules -> create archive 'core.a'
# Local CFLAGS
$(CORE_OBJS_$(d)):	CF_TGT := -I. -I$(d) $(shell pkg-config --cflags gtkmm-2.4)
#$(OBJS_$(d)):	LF_TGT := -lgsl -lgslcblas -lm 
#$(OBJS_$(d)):	LL_TGT := $(S_LL_INET) cpp/core.a dana-cpp/dana.a

$(d)/libgadget.so:	$(CORE_OBJS_$(d))
		$(DYN_LIB)

$(d)/libgadget.a:	$(CORE_OBJS_$(d))
		$(ARCH)

.PHONY : verbose_$(d)
verbose_$(d): $(d)/libgadget.a $(d)/libgadget.so
	@echo "Generating $^"

# As a last step, we restore the value of the current directory variable $(d) 
# by taking it from the current location on the stack, and we "decrement" 
# the stack pointer by stripping away the .x suffix 
# we added at the top of this file.

# Standard things

-include	$(CORE_DEPS_$(d))

d		:= $(dirstack_$(sp))
sp		:= $(basename $(sp))

