
#ifndef __GADJET_CAIRO_H
#define __GADJET_CAIRO_H

#include "vec4f.h"
#include <gtkmm.h>


#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

/** Forward declaration of shared pointer. */
typedef boost::shared_ptr <class GadjetCAIRO> GadjetCAIROPtr;

/**
 * A GadjetCAIRO is the base class for elements that are supposed to
 * be renderer on a Gtk::DrawingArea using the CAIRO drawing library.
 * 
 * A GadjetCAIRO has a Position, an orientation, a scale
 * and foreground/background color.
 */
class GadjetCAIRO 
{
 public:
   /**
   * @name Creation/Destruction
   */
  /**
   * Constructor.
   *
   * @param x position of the Gadjet
   * @param y position of the Gadjet
   * @param angle orientation in radian
   * @param scale of the gadjet
   */
  GadjetCAIRO( float x = 0.0, float y = 0.0,
	       float angle = 0.0 , float scale = 1.0);
  /**
   * Constructor.
   *
   * @param pos position of the Gadjet
   * @param angle orientation in radian
   * @param scale of the gadjet
   */
  GadjetCAIRO( Position &pos,
	       float angle = 0.0, float scale = 1.0);
  /** 
   * Destructor.
   */
  ~GadjetCAIRO();
  //@}

  /**
   * @name Color
   */
  /**
   * Set foreground color in RGBA format.
   */
  virtual void set_fg_color( float red = 0.0, float green = 0.0,
			     float blue = 0.0, float alpha = 1.0);
  /**
   * Set foreground color.
   */
  virtual void set_fg_color( Color &color );
  /**
   * Set background color in RGBA format.
   */
  virtual void set_bg_color( float red = 0.0, float green = 0.0,
			     float blue = 0.0, float alpha = 1.0);
  /**
   * Set background color.
   */
  virtual void set_bg_color( Color &color );
  /**
   * Convert color from RGBA to Color.
   */
  static Color color_from_rgba( float red, float green, float blue,
				float alpha=1.0 );
  //@}

  /**
   * @name Position/Orientation
   */
  /**
   * Set position (only 2D will be used).
   */
  virtual void set_position( Position &pos);
  /**
   * Set position.
   */
  virtual void set_position( float x, float y);
  /**
   * Get x position.
   */
  virtual float get_x_position(void);
  /**
   * Get y position.
   */
  virtual float get_y_position(void);
  /**
   * Orientation is expressed in radian, counterclockwise.
   */
  virtual void set_orientation( float angle );
  /**
   * Orientation is expressed in radian, counterclockwise.
   */
  virtual float get_orientation(void);
  /**
   * Scale is applied along x and y axis.
   */
  virtual void set_scale( float scale );
  /**
   * Scale is applied along x and y axis.
   */
  virtual float get_scale(void);
  /**
   * Convert XY to Position.
   */
  static Position position_from_xy( float x, float y);
  //@}

  /**
   * Render the Gadjet using a Cairo::Context.
   */
  virtual void render( Cairo::RefPtr<Cairo::Context> cr );

 protected:
  /** Position along x and y axis */
  Position _pos;
  /** Orientation as a radian angle, trigonometric. */
  float _angle;
  /** Scale along x and y. */
  float _scale;
  /** Foreground color. */
  Color  _bg_color;
  /** Background color. */
  Color  _fg_color;
};

#endif //__GADJET_CAIRO_H
