
#ifndef __JAUGETEXT_CAIRO_H
#define __JAUGETEXT_CAIRO_H

#include "vec4f.h"
#include <gtkmm.h>
#include "text_cairo.h"
#include "jauge_cairo.h"

#include <boost/format.hpp>

/**
 * A JaugeTextCAIRO display a numerical value as a string inside a rectangle.
 * On peut spécifier le format du chiffre 
 * http://www.boost.org/doc/libs/1_37_0/libs/format/doc/format.html.
 */
class JaugeTextCAIRO : public TextCAIRO, public JaugeCAIRO
{
 public:
  
  /**
   * @name Creation/Destruction
   */
  /**
   * Constructor.
   *
   * @param val initial value to be displayed
   * @param lvl_min lower bound of displayed value
   * @param lvl_max upper bound of displayed value
   * @param align where is the bounding box relative to the position
   * @param is_line do we draw the bounding box.
   */
  JaugeTextCAIRO( float val, float lvl_min=0.0, float lvl_max=10.0,
		 TAlign align = ALIGN_CENTER, bool is_line = true);
  /**
   * Destructor.
   */
  ~JaugeTextCAIRO();
  //@}

  
  /**
   * Set a format for displaying float.
   * Default is "%+5.2f".
   */
  virtual void set_float_format(boost::format& float_format);
  
  
 protected:
  /**
   * Render the Jauge using a TextCAIRO.
   */
  virtual void render( Cairo::RefPtr<Cairo::Context> cr );
  /**
   * Set the font, the text and compute the bounding box.
   * The bouding box is the biggest from the bouding boxes
   * of the upper and lower bounds.
   */
  virtual void compute_internal_positions( Cairo::RefPtr<Cairo::Context> cr );


 protected:
  /** format for displaying float. */ 
  boost::format _float_format;
};

#endif //__JAUGETEXT_CAIRO_H
