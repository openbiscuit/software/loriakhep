
#include "joystick_gtkmm.h"
#include <string.h>
#include <iostream>
#include <iomanip>
#include <math.h>

// ==========================================================================
JoystickGTKMM::JoystickGTKMM(Glib::ustring name) : Gtk::Frame(name)
{
  /* this->set_events( Gdk::EXPOSURE_MASK */
  /* 			| Gdk::LEAVE_NOTIFY_MASK */
  /* 			| Gdk::BUTTON_PRESS_MASK */
  /* 			| Gdk::BUTTON_RELEASE_MASK */
  /* 			| Gdk::POINTER_MOTION_MASK */
  /* 			| Gdk::POINTER_MOTION_HINT_MASK */
  /* 			| Gdk::KEY_PRESS_MASK */
  /* 			| Gdk::SCROLL_MASK); */
  //| Gdk::GDK_CONFIGURE_MASK);
  
  _action = false;
  _x_joy = 0.0;
  _y_joy = 0.0;
  
  _width = 200.0;
  _height = 200.0;

  build();
  set_recenter( true );
  
  _width = (double) this->get_width();
  _height = (double) this->get_height();
}
JoystickGTKMM::~JoystickGTKMM()
{
}
// ==========================================================================
double
JoystickGTKMM::get_x(void) 
{ 
  return _x_joy;
}
double
JoystickGTKMM::get_y(void)
{ 
  return _y_joy;
}
// ==========================================================================
bool
JoystickGTKMM::get_recenter(void)
{
  return _recenter_button.get_active();
}
void
JoystickGTKMM::set_recenter( bool recenter)
{
  _recenter_button.set_active (recenter);
}
// ==========================================================================  
void
JoystickGTKMM::build()
{
  /* Set the frames label */
  //this->set_label("Joystick");
  //this->set_size_request(200, 200);
  
  /* Align the label at the right of the frame */
  //m_Frame.set_label_align(Gtk::ALIGN_RIGHT, Gtk::ALIGN_TOP);
  
  /* Set the style of the frame */
  this->set_shadow_type(Gtk::SHADOW_ETCHED_OUT);
  
  // inside the Frame
  _area.set_size_request( int(_width), int(_height));
  _area.set_events( Gdk::EXPOSURE_MASK
		    | Gdk::LEAVE_NOTIFY_MASK
		    | Gdk::BUTTON_PRESS_MASK
		    | Gdk::BUTTON_RELEASE_MASK
		    | Gdk::POINTER_MOTION_MASK
		    | Gdk::POINTER_MOTION_HINT_MASK
		    | Gdk::KEY_PRESS_MASK
		    | Gdk::SCROLL_MASK);
  _area.signal_expose_event().connect(sigc::mem_fun(*this,&JoystickGTKMM::__on_expose_event));
  _area.signal_configure_event().connect(sigc::mem_fun(*this,&JoystickGTKMM::__on_configure_event));
  _area.signal_button_press_event().connect(sigc::mem_fun(*this,&JoystickGTKMM::__on_button_press_event));
  _area.signal_button_release_event().connect(sigc::mem_fun(*this,&JoystickGTKMM::__on_button_release_event));
  _area.signal_motion_notify_event().connect(sigc::mem_fun(*this,&JoystickGTKMM::__on_motion_notify_event));
  
  _area.show();
  
  _vbox.pack_start( _area , true, true, 0);
  //_vbox.add( _area );

  _label.set_text( get_label_ustring() );
  _label.show();
  
  _hbox.pack_start( _label, true, true, 0);
  //_vbox.add( _label );

  _recenter_button.set_label("Rappel");
  //_recenter_button.set_active( get_recenter() );
  _recenter_button.signal_toggled().connect(sigc::mem_fun(*this,&JoystickGTKMM::__on_toggled_recenter));
  _recenter_button.show();
  _hbox.pack_start( _recenter_button, false, false, 0);
  
  _hbox.show();
  _vbox.pack_start( _hbox, false, false, 0);
  
  _vbox.show();
  
  this->add( _vbox );
  //this->show_all_children();
  this->show(); 
  
}
// ==========================================================================
Glib::ustring
JoystickGTKMM::get_label_ustring()
{
  Glib::ustring res = Glib::ustring("x = ") +
    Glib::ustring::format( std::fixed, std::setprecision(2), get_x()) +
    Glib::ustring(" / y = ") +
    Glib::ustring::format( std::fixed, std::setprecision(2), get_y());
  
  return res;
}
// ==========================================================================
// override
void // bool
JoystickGTKMM::__on_toggled_recenter()// Gtk::ToggleButton* button )
{
  if( _recenter_button.get_active() == true ) {
    //std::cout << "Recenter activated\n";
    this->_x_joy = 0.0;
    this->_y_joy = 0.0;
    // force our program to redraw the entire area
    Glib::RefPtr<Gdk::Window> win = _area.get_window();
    if (win) {
      Gdk::Rectangle r(0, 0, get_allocation().get_width(),
		       get_allocation().get_height());
      win->invalidate_rect(r, false);
    }
  }
  // else {
//     std::cout << "Recenter desactivated\n";
//     //set_recenter(false);
//   }
}

// ==========================================================================
bool 
JoystickGTKMM::__on_expose_event( GdkEventExpose* event)
{
  //std::cout << "__on_expose_event\n";
  // info about the drawing area
  Glib::RefPtr<Gdk::Window> window = _area.get_window();
  if( window ) {
    //std::cout << "  window\n";
    /* Gtk::Allocation allocation = get_allocation(); */
    /* 	const int width = allocation.get_width(); */
    /* 	const int height = allocation.get_height(); */
    /* 	int lesser = MIN(width, height); */
    
    Cairo::RefPtr<Cairo::Context> cr = window->create_cairo_context();
    cr->set_line_width(2);
    
    // clip to exposed area
    cr->rectangle( event->area.x, event->area.y,
		   event->area.width, event->area.height );
    cr->clip();
    
    render_joystick(cr);
    update_label();
  }
  
  return true; // no event propagation
}
// ==========================================================================
bool
JoystickGTKMM::__on_button_press_event ( GdkEventButton* event)
{
  // Button 1
  if( event->button == 1 ) {
    this->_action = true;
    this->_x_joy = clamp((double) event->x / this->_width * 2.0 - 1.0);
    this->_y_joy = clamp(-((double) event->y / this->_height * 2.0 - 1.0));
  }
  // force our program to redraw the entire area
  Glib::RefPtr<Gdk::Window> win = _area.get_window();
  if (win) {
    Gdk::Rectangle r(0, 0, get_allocation().get_width(),
		     get_allocation().get_height());
    win->invalidate_rect(r, false);
  }
  return false; // event propagation
}
// ==========================================================================
bool
JoystickGTKMM::__on_button_release_event ( GdkEventButton* event )
{
  // Button 1
  if( event->button == 1 ) {
    this->_action = false;
    if( _recenter_button.get_active() ) {
      this->_x_joy = 0.0;
      this->_y_joy = 0.0;
    }
  }
  // force our program to redraw the entire area
  Glib::RefPtr<Gdk::Window> win = _area.get_window();
  if (win) {
    Gdk::Rectangle r(0, 0, get_allocation().get_width(),
		     get_allocation().get_height());
    win->invalidate_rect(r, false);
  }
  return false; // event propagation
}
// ==========================================================================
bool
JoystickGTKMM::__on_motion_notify_event ( GdkEventMotion* event)
{
  if( this->_action == true ) {
    //std::cout << "Mouse on " << event->x << ", " << event->y << "\n";
    this->_x_joy = clamp((double) event->x / this->_width * 2.0 - 1.0);
    this->_y_joy = clamp(-((double) event->y / this->_height * 2.0 - 1.0));
  }
  // force our program to redraw the entire area
  Glib::RefPtr<Gdk::Window> win = _area.get_window();
  if (win) {
    Gdk::Rectangle r(0, 0, get_allocation().get_width(),
		     get_allocation().get_height());
    win->invalidate_rect(r, false);
  }
  return false; // event propagation
}
// ==========================================================================
bool
JoystickGTKMM::__on_configure_event (GdkEventConfigure* event)
{
  // new dimension
  Glib::RefPtr<Gdk::Window> win = _area.get_window();
  if (win) {
    this->_width = (double) event->width;
    this->_height= (double) event->height;
    //std::cout << "Dimension = " << this->_width << ", " << this->_height << "\n";
  }
  return false; // event propagation
}
// ==========================================================================
void
JoystickGTKMM::render_joystick(Cairo::RefPtr<Cairo::Context> cr)
{
  // scale to -1,1 square, with y incresing toward the top
  // and translate (0, 0) to be (1.0, 1.0), i.e.
  // the center of the window
  cr->scale(this->_width/2, -this->_height/2);
  cr->translate(1.0, -1.0);
  
  // draw lines
  cr->set_line_width(0.01);
  
  // background is white
  cr->set_source_rgb( 1.0, 1.0, 1.0 );
  cr->paint();
  
  // draw lines
  cr->set_source_rgb( 0.0, 0.0, 0.0 );
  
  cr->move_to(  0.0, -1.0 );
  cr->line_to(  0.0,  1.0 );
  cr->move_to( -1.0,  0.0 );
  cr->line_to(  1.0,  0.0 );
  
  cr->stroke();
  
  // circle
  cr->set_source_rgba(0.8, 0.0, 0.0, 0.6);    // partially translucent
  cr->arc( this->get_x(), this->get_y(), 0.1,
	   0.0, 2.0 * M_PI); // full circle
  cr->fill_preserve();
}
// ==========================================================================
void
JoystickGTKMM::update_label()
{
  _label.set_text( get_label_ustring() );
}
double
JoystickGTKMM::clamp(double val)
{
  if( val > 1.0 ) return 1.0;
  if( val < -1.0) return -1.0;
  return val;
}
// ==========================================================================
