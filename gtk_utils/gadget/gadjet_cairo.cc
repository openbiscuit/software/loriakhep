
#include "gadjet_cairo.h"
#include <iostream>

// ==========================================================================
GadjetCAIRO::GadjetCAIRO( float x, float y,
			  float angle, float scale )
{
  set_position( x, y );
  set_orientation( angle );
  set_scale( scale );
}

GadjetCAIRO::GadjetCAIRO( Position &pos,
			  float angle, float scale )
{
  set_position( pos );
  set_orientation( angle );
  set_scale( scale );
}
GadjetCAIRO::~GadjetCAIRO()
{
}
// ==========================================================================
void
GadjetCAIRO::render( Cairo::RefPtr<Cairo::Context> cr )
{
  std::cout << "Rendering Gadget" << "\n";
  cr->translate( _pos.x, _pos.y );
  cr->rotate( _angle );
  cr->scale( _scale, _scale );
}
// ==========================================================================
void
GadjetCAIRO::set_fg_color( float red, float green, float blue, float alpha )
{
  _fg_color = color_from_rgba( red, green, blue, alpha );
}
void 
GadjetCAIRO::set_fg_color( Color &color )
{
  _fg_color = color;
}
void
GadjetCAIRO::set_bg_color( float red, float green, float blue, float alpha )
{
  _bg_color = color_from_rgba( red, green, blue, alpha );
}
void 
GadjetCAIRO::set_bg_color( Color &color )
{
  _bg_color = color;
}
Color
GadjetCAIRO::color_from_rgba( float red, float green, float blue, float alpha )
{
  Color res;
  res.red = red;
  res.green = green;
  res.blue = blue;
  res.alpha = alpha;

  return res;
}
// ==========================================================================
void
GadjetCAIRO::set_position( Position &pos)
{
  _pos = pos;
}
void
GadjetCAIRO::set_position( float x, float y)
{
  _pos = position_from_xy( x, y);
}
float
GadjetCAIRO::get_x_position(void)
{
  return _pos.x;
}
float
GadjetCAIRO::get_y_position(void)
{
  return _pos.y;
}
Position
GadjetCAIRO::position_from_xy( float x, float y)
{
  Position res;
  res.x = x;
  res.y = y;
  res.z = 0.0;
  res.w = 0.0;

  return res;
}
// ==========================================================================
void
GadjetCAIRO::set_orientation( float angle )
{
  _angle = angle;
}
float
GadjetCAIRO::get_orientation(void)
{
  return _angle;
}
void
GadjetCAIRO::set_scale( float scale )
{
  _scale = scale;
}
float
GadjetCAIRO::get_scale(void)
{
  return _scale;
}
// ==========================================================================
