
#ifndef __JAUGE_CAIRO_H
#define __JAUGE_CAIRO_H

#include "vec4f.h"
#include <gtkmm.h>
#include "gadjet_cairo.h"

/**
 * A JaugeCAIRO is a Gadjet displaying a value as a line of varyying length.
 *
 * The line extends is controled either as a ratio of its maximum length
 * or as a level betweend bounds.<br>
 * We have: level = lvl_min + ratio * (lvl_max - lvl_min)
 */
class JaugeCAIRO : public virtual GadjetCAIRO
{
 public:
  /**
   * @name Creation/Destruction
   */
  /**
   * Constructor.
   *
   * @param lvl_min lower bound of displayed value
   * @param lvl_max upper bound of displayed value
   * @param ratio proportion of the maximum length to display
   * @param length maximum length of the Jauge on screen
   * @param width width of the Jauge on screen
   */
  JaugeCAIRO( float lvl_min=0.0, float lvl_max=1.0, float ratio = 0.5,
	      float length = 1.0, float width = 0.1);
  /** 
   * Destructor.
   */
  ~JaugeCAIRO();
  //@}

  /**
   * @name Value
   */
  /**
   * Set the value of the Jauge as a ratio of its length.
   * Jauge is unchanged if ratio is outside [0, 1].
   *
   * @param ratio 
   * @return level
   */
  virtual float set_ratio( float ratio );
  /**
   * Get the ratio of displayed length.
   */
  virtual float get_ratio(void);
  /**
   * Set the value of the Jauge as a level (between bounds).
   * Jauge is unchanged if level outside bounds.
   *
   * @param level 
   * @return ratio
   */
  virtual float set_level( float level );
  /**
   * Get the level of the Jauge.
   */
   virtual float get_level(void);
   /**
    * Set bounds.
    */
   virtual void set_bounds( float lvl_min, float lvl_max );
   //@}
   
 protected:
   /**
   * Render the Jauge as a background line and a foreground line.
   */
   virtual void render( Cairo::RefPtr<Cairo::Context> cr );

 protected:
   /** Lower bound for value. */
   float _lvl_min;
   /** Upper bound for value. */
   float _lvl_max;
   /** Actual ratio used */
   float _ratio;
   /** Maximum length of Jauge. */
   float _length;
   /** Width of Jauge. */
   float _width; 		   
};


#endif //__JAUGE_CAIRO_H
