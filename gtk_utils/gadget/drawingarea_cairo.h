
#ifndef __DRAWINGAREA_CAIRO_H
#define __DRAWINGAREA_CAIRO_H

#include <gtkmm.h>
#include <iostream>
#include <iomanip>
#include <math.h>
#include <vector>
#include "gadjet_cairo.h"

/**
 * A DrawingAreaCAIOR is a Gtk::Frame that opens on drawing area where "Gadjet"
 * can be added and rendered.
 *
 * The DrawingArea is scaled and transformed so as to display à -1,1 x -1,1
 * area, where y increses when goind upscreen.
 */
class DrawingAreaCAIRO : public Gtk::Frame 
{
 public:
  /**
   * @name Creation/Destruction
   */
  /**
   * Constructor.
   *
   * @param name of the Gtk::Frame label displayed.
   */
  DrawingAreaCAIRO(Glib::ustring name = "Area" );
  /** 
   * Destructor.
   */
  ~DrawingAreaCAIRO();
  //@}

  /**
   * Add a new Gadjet.
   */
  void add_gadjet( GadjetCAIROPtr gadjet );
  /**
   * Force redraw of Area.
   */
  void force_redraw();
 
  /**
   * Signal for forcing render.
   */
  typedef sigc::signal<void> signal_redraw_t;
  signal_redraw_t& signal_redraw() { return _signal_redraw; };
  


 protected:
  /**
   * Build the GUI.
   * Gtk::Frame<br>
   *   + Gtk::VBox<br>
   *     + Gtk::DrawingArea -> draw joystick<br>
   *     + Gtk::Hbox<br>
   *       + Gtk::Label -> display x/y values<br>
   */
  void build();
  /**
   * @name callback
   * Callback functions.
   */
  /** When _area is exposed -> draw joystick. */
  virtual bool __on_expose_event( GdkEventExpose* event);
  /** When _area is clicked -> set _action to true and follow mouse. */
  virtual bool __on_button_press_event ( GdkEventButton* event);
  /** When _area is un_clicked -> set _action to false, [recenter] */
  virtual bool __on_button_release_event ( GdkEventButton* event );
  /** When mouse is moved over _area -> follow ?? */ 
  virtual bool __on_motion_notify_event ( GdkEventMotion* event);
  /** When _area size or position are changed. */
  virtual bool __on_configure_event (GdkEventConfigure* event);
  /** When signal_redraw is received. */
  virtual void __on_redraw_signal(void); 
  //@}

  /**
   * Update the label od _label.
   */
  virtual void update_label();
  /**
   * Ensure that 'val' belongs to [-1,1].
   */
  double clamp(double val);
  
  /**
   * Draw 'light' axes on the area.
   */
  void render_axes(Cairo::RefPtr<Cairo::Context> cr);
  
  /**
   * @name private variables
   */
  /** Actual width of the area. */
  double             _width_area;
  /** Actual height of the area. */
  double             _height_area;
  //@}
  /**
   * @name widget
   * See build().
   */
  Gtk::DrawingArea   _area;
  Gtk::Label         _label;
  Gtk::VBox          _vbox;
  Gtk::HBox          _hbox;
  //@}

  /**
   * Vector of all the Gadjet to draw.
   */
  std::vector<GadjetCAIROPtr> _gadjets_vec;

  /**
   * Signal for redraw.
   */
  signal_redraw_t _signal_redraw;
};

#endif //__DRAWINGAREA_CAIRO_H
