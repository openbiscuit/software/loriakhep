
#include "drawingarea_cairo.h"
#include <string.h>
#include <iostream>
#include <iomanip>
#include <math.h>

#include <boost/foreach.hpp>

// ==========================================================================
DrawingAreaCAIRO::DrawingAreaCAIRO(Glib::ustring name) : Gtk::Frame(name)
{
  _width_area = 200.0;
  _height_area = 200.0;

  build();
  
  _width_area = (double) this->get_width();
  _height_area = (double) this->get_height();
}
DrawingAreaCAIRO::~DrawingAreaCAIRO()
{
}
// ==========================================================================
void
DrawingAreaCAIRO::add_gadjet( GadjetCAIROPtr gadjet )
{
  _gadjets_vec.push_back(gadjet);
}
// ==========================================================================  
void
DrawingAreaCAIRO::build()
{
  /* Set the frames label */
  //this->set_label("Joystick");
  //this->set_size_request(200, 200);
  
  /* Align the label at the right of the frame */
  //m_Frame.set_label_align(Gtk::ALIGN_RIGHT, Gtk::ALIGN_TOP);
  
  /* Set the style of the frame */
  this->set_shadow_type(Gtk::SHADOW_ETCHED_OUT);
  
  // inside the Frame
  _area.set_size_request( int(_width_area), int(_height_area));
  _area.set_events( Gdk::EXPOSURE_MASK
		    | Gdk::LEAVE_NOTIFY_MASK
		    | Gdk::BUTTON_PRESS_MASK
		    | Gdk::BUTTON_RELEASE_MASK
		    | Gdk::POINTER_MOTION_MASK
		    | Gdk::POINTER_MOTION_HINT_MASK
		    | Gdk::KEY_PRESS_MASK
		    | Gdk::SCROLL_MASK);
  _area.signal_expose_event().connect(sigc::mem_fun(*this,&DrawingAreaCAIRO::__on_expose_event));
  _area.signal_configure_event().connect(sigc::mem_fun(*this,&DrawingAreaCAIRO::__on_configure_event));
  //_area.signal_button_press_event().connect(sigc::mem_fun(*this,&DrawingAreaCAIRO::__on_button_press_event));
  //_area.signal_button_release_event().connect(sigc::mem_fun(*this,&DrawingAreaCAIRO::__on_button_release_event));
  //_area.signal_motion_notify_event().connect(sigc::mem_fun(*this,&DrawingAreaCAIRO::__on_motion_notify_event));
  signal_redraw().connect(sigc::mem_fun(*this,&DrawingAreaCAIRO::__on_redraw_signal));
  
  _area.show();
  
  _vbox.pack_start( _area , true, true, 0);
  //_vbox.add( _area );

  _label.set_text( "-" );
  _label.show();
  
  _hbox.pack_start( _label, true, true, 0);
  //_vbox.add( _label );

  //_recenter_button.set_label("Rappel");
  //_recenter_button.set_active( get_recenter() );
  //_recenter_button.signal_toggled().connect(sigc::mem_fun(*this,&DrawingAreaCAIRO::__on_toggled_recenter));
  //_recenter_button.show();
  //_hbox.pack_start( _recenter_button, false, false, 0);
  
  _hbox.show();
  _vbox.pack_start( _hbox, false, false, 0);
  
  _vbox.show();
  
  this->add( _vbox );
  //this->show_all_children();
  this->show(); 
  
}
// ==========================================================================
bool 
DrawingAreaCAIRO::__on_expose_event( GdkEventExpose* event)
{
  std::cout << "__on_expose_event\n";
  // info about the drawing area
  Glib::RefPtr<Gdk::Window> window = _area.get_window();
  if( window ) {
    //std::cout << "  window\n";
    /* Gtk::Allocation allocation = get_allocation(); */
    /* 	const int width = allocation.get_width(); */
    /* 	const int height = allocation.get_height(); */
    /* 	int lesser = MIN(width, height); */
    
    Cairo::RefPtr<Cairo::Context> cr = window->create_cairo_context();
    cr->set_line_width(2);
    
    // clip to exposed area
    cr->rectangle( event->area.x, event->area.y,
		   event->area.width, event->area.height );
    cr->clip();
    
    render_axes(cr);
    update_label();

    BOOST_FOREACH( GadjetCAIROPtr gadjet, _gadjets_vec ) {
      cr->save();
      gadjet->render(cr);
      cr->restore();
    }
  }
  
  return true; // no event propagation
}
// ==========================================================================
bool
DrawingAreaCAIRO::__on_button_press_event ( GdkEventButton* event)
{
  force_redraw();
  return false; // event propagation
}
// ==========================================================================
bool
DrawingAreaCAIRO::__on_button_release_event ( GdkEventButton* event )
{
  return false; // event propagation
}
// ==========================================================================
bool
DrawingAreaCAIRO::__on_motion_notify_event ( GdkEventMotion* event)
{
  return false; // event propagation
}
// ==========================================================================
bool
DrawingAreaCAIRO::__on_configure_event (GdkEventConfigure* event)
{
  // new dimension
  Glib::RefPtr<Gdk::Window> win = _area.get_window();
  if (win) {
    this->_width_area = (double) event->width;
    this->_height_area = (double) event->height;
    //std::cout << "Dimension = " << this->_width << ", " << this->_height << "\n";
  }
  return false; // event propagation
}
// ==========================================================================
void
DrawingAreaCAIRO::__on_redraw_signal(void)
{
  // std::cout << "__on_signal_redraw\n";
//   // info about the drawing area
//   Glib::RefPtr<Gdk::Window> window = _area.get_window();

//   if( window ) {
//     //std::cout << "  window\n";
//     /* Gtk::Allocation allocation = get_allocation(); */
//     /* 	const int width = allocation.get_width(); */
//     /* 	const int height = allocation.get_height(); */
//     /* 	int lesser = MIN(width, height); */
    
//     Cairo::RefPtr<Cairo::Context> cr = window->create_cairo_context();
//     render_axes(cr);
//     update_label();

//     BOOST_FOREACH( GadjetCAIROPtr gadjet, _gadjets_vec ) {
//       cr->save();
//       gadjet->render(cr);
//       cr->restore();
//     }
//   }
  std::cout << "__on_signal_redraw\n";
  force_redraw();
}
// ==========================================================================
void
DrawingAreaCAIRO::render_axes(Cairo::RefPtr<Cairo::Context> cr)
{
  // scale to -1,1 square, with y incresing toward the top
  // and translate (0, 0) to be (1.0, 1.0), i.e.
  // the center of the window
  cr->scale(this->_width_area/2, -this->_height_area/2);
  cr->translate(1.0, -1.0);

  cr->save();  
  // draw lines
  cr->set_line_width(0.005);
  
  // background is white
  cr->set_source_rgb( 1.0, 1.0, 1.0 );
  cr->paint();
  
  // draw lines
  cr->set_source_rgba( 0.0, 0.0, 0.0, 0.3 );
  
  cr->move_to(  0.0, -1.0 );
  cr->line_to(  0.0,  1.0 );
  cr->move_to( -1.0,  0.0 );
  cr->line_to(  1.0,  0.0 );
  
  cr->stroke();
  cr->restore();
}
// ==========================================================================
void
DrawingAreaCAIRO::update_label()
{
}
double
DrawingAreaCAIRO::clamp(double val)
{
  if( val > 1.0 ) return 1.0;
  if( val < -1.0) return -1.0;
  return val;
}
void
DrawingAreaCAIRO::force_redraw()
{
  // // force our program to redraw the entire area
//   Glib::RefPtr<Gdk::Window> win = _area.get_window();
//   if (win) {
//     Gdk::Rectangle r(0, 0, get_allocation().get_width(),
// 		     get_allocation().get_height());
//     std::cout << "Invalidate 0,0 to " << get_allocation().get_width() << ", " << get_allocation().get_height() << "\n";
//     win->invalidate_rect(r, false);
//   } 
  _area.queue_draw();   
}
// ==========================================================================
