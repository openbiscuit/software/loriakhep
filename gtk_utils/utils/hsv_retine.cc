#include "hsv_retine.h"

// ==========================================================================
int HSVRetine::pos_x_def [] = {45, 80, 125, 170, 215, 260, 305, 350};
int HSVRetine::nb_pos_def = 7;

// ==========================================================================
HSVRetine::HSVRetine()
{
  nb_pos = nb_pos_def;
  pos_x = pos_x_def;

  hist = new float* [nb_pos];
  for( int i=0; i<nb_pos; i++) hist[i] = new float[36];
  non_hist = new float[nb_pos];
  seuil_light = 200;
  seuil_dark = 50;
}
HSVRetine::HSVRetine( int _nb_pos, int *_pos)
{
  nb_pos = _nb_pos;
  pos_x = _pos;

  hist = new float* [nb_pos];
  for( int i=0; i<nb_pos; i++) hist[i] = new float[36];
  non_hist = new float[nb_pos];
  seuil_light = 200;
  seuil_dark = 50;
}
HSVRetine::~HSVRetine()
{
  delete [] hist;
  delete [] non_hist;
  delete [] pos_x;
}
// ==========================================================================
float
HSVRetine::hue_from_rgb( int red, int green, int blue,
			 int seuil_light, int seuil_dark)
{
  float hue;
  // trop clair ou trop foncé
  if( ((red > seuil_light) && (green > seuil_light) && (blue > seuil_light)) ||
      ((red < seuil_dark) && (green < seuil_dark) && (blue < seuil_dark)) ) {
    return -1.0;
  }

  if( red > green ) {
    if( green > blue ) {
      // --------------------------------- MAX: r  MIN: b
      hue = fmod(60.0 * (green - blue) / (float) (red - blue) + 360.0, 360.0);
      return hue;
    }
    else if( red > blue ) {
      // --------------------------------- MAX: r  MIN: g/b
      hue = fmod(60.0 * (green - blue) / (float) (red - green) + 360.0, 360.0);
      return hue;
    }
    else {
      // --------------------------------- MAX: b/r  MIN: g
      hue = 60.0 * (red - green) / (float) (blue - green) + 240.0;
     return hue; 
    }
  }
  else if( green > red ) {
    if( red > blue ) {
      // --------------------------------- MAX: g  MIN: b
      hue = 60.0 * (blue - red) / (float) (green - blue) + 120.0;
      return hue;
    }
    else if( green > blue ) {
      // --------------------------------- MAX: g  MIN: r/b
      hue = 60.0 * (blue - red) / (float) (green - red) + 120.0;
      return hue;
    }
    else {
      // --------------------------------- MAX: b/g  MIN: r 
      hue = 60.0 * (red - green) / (float) (blue - red) + 240.0;
      return hue;
    }
  }
  else if( blue > red ) {
    // ----------------------------------- MAX: b  MIN: r=g
    hue = 240.0;
    return hue;
  }
  else {
    // --------------------------------- MAX: r=g  MIN: b
    hue = 60.0;
    return hue;
  }
  return -2.0;
}
// ==========================================================================
int
HSVRetine::overlay_hist( GtkWidget *widget , gpointer data )
{
  GdkColor col_green, col_red, col_blue;
  GdkGC *gc;
  int width, height;
  int i, k;
  
  // drawable
  width = widget->allocation.width;
  height = widget->allocation.height;

  // on va dessiner en vert
  gc = gdk_gc_new ( widget->window );
  col_green.red = 0; col_green.green = 65535; col_green.blue = 0;
  col_red.red = 65535; col_red.green = 0; col_red.blue = 0;
  col_blue.red = 0; col_blue.green = 0; col_blue.blue = 65535;
  gdk_gc_set_rgb_fg_color( gc, &col_green );
  gdk_gc_set_line_attributes( gc, 2 /*width*/, 
			      GDK_LINE_SOLID, GDK_CAP_NOT_LAST, GDK_JOIN_MITER);

  // RED
  // un histogram par position
  for( i=0; i<nb_pos; i++) {
    // points trop clairs ou trop sombres
    gdk_gc_set_rgb_fg_color( gc, &col_red );
    gdk_draw_line( widget->window, gc,
		   pos_x[i], height/2-2,
		   pos_x[i] + (int) (non_hist[i] * (pos_x[i+1]-10-pos_x[i])/height), height/2-2);

    gdk_gc_set_rgb_fg_color( gc, &col_blue );
    gdk_draw_line( widget->window, gc,
		   pos_x[i], height/2,
		   pos_x[i], height/2+35*2);
    gdk_draw_line( widget->window, gc,
		   pos_x[i], height/2,
		   pos_x[i+1]-10 , height/2);
      
    gdk_gc_set_rgb_fg_color( gc, &col_green );
    for( k=0; k<36; k++) { 
      gdk_draw_line( widget->window, gc,
		     pos_x[i], height/2+2*k,
		     pos_x[i]+(int) (hist[i][k] * (pos_x[i+1]-10-pos_x[i])/height), height/2+2*k);
    }
  }
  return TRUE;
}
// ==========================================================================
int
HSVRetine::compute_h_hist( GdkPixbuf *pixbuf )
{
  guchar *pix;
  int height = gdk_pixbuf_get_height( pixbuf );
  guchar *pixels = gdk_pixbuf_get_pixels (pixbuf);
  int n_channels = gdk_pixbuf_get_n_channels (pixbuf);
  int rowstride = gdk_pixbuf_get_rowstride (pixbuf);
  int i, x, y;
  float hue;

  for( i = 0; i < nb_pos; i++ ) {
    x = pos_x[i] * n_channels;
    // clear histories
    non_hist[i]=0.0;
    for( y=0; y<36; y++) hist[i][y] = 0.0;

    for( y=0; y < height; y++ ) {
      pix =  pixels + y * rowstride + x;
      // p[0]:red, p[1]:green, p[2]:blue, p[3]:alpha
      //printf( "x=%d, y=%d, pix=%d\n", x, y, pix[0] );
      /* if( pix[0] > 200 ) { */
      /* 	tmp_level_red += 1.0; */
      /* } */
      /* if( pix[1] > 200 ) { */
      /* 	tmp_level_green += 1.0; */
      /* } */
      /* if( pix[2] > 200 ) { */
      /* 	tmp_level_blue += 1.0; */
      /* } */
      hue = hue_from_rgb( pix[0], pix[1], pix[2], seuil_light, seuil_dark );
      if( hue < -1.5 ) printf( "HUE undefined with %d, %d, %d\n", pix[0], pix[1], pix[2] );
      if( hue > -1.0 ) {
	hist[i][(int) (hue / 10.0)] += 1.0;
      }
      else {
	non_hist[i] += 1.0;
      }
    }
  }   
  return TRUE;
}
// ==========================================================================
void
HSVRetine::print()
{
  
  for( int i = 0; i < nb_pos; i++ ) {
    printf( "hist[%d] :", i);
    for( int y=0; y<36; y++) {
      printf( " %5.3f", hist[i][y]);
    }
    printf( "\n");
  }
}
