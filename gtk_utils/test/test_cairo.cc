//
// gcc test_cairo.cc drawingarea_cairo.cc gadjet_cairo.cc jauge_cairo.cc text_cairo.cc -o test_cairo `pkg-config gtkmm-2.4 --cflags --libs` -lboost_thread
// 

#include "drawingarea_cairo.h"
#include "jauge_cairo.h"
#include "text_cairo.h"
#include "jaugetext_cairo.h"

#include <gtkmm/main.h>
#include <gtkmm/window.h>

#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <iostream>

Gtk::Window * win;
DrawingAreaCAIRO * area;
JaugeCAIRO * jauge;
JaugeCAIRO * jauge2;
TextCAIRO * text1;
JaugeTextCAIRO * jtext1;
bool status_changed = false;

bool
on_idle_cbk()
{
  //std::cout << "Idel\n";
  if( status_changed ) {
    area->signal_redraw().emit();
    status_changed = false;
  }
  return true; //not done
}

void
run_window(Gtk::Window *win)
{
  Gtk::Main::run(*win);
}

//DrawingAreaCAIRO *
void
create_new_area()
{
  
  win = new Gtk::Window();
  win->set_title("Thread" );
  //Glib::signal_idle().connect(on_idle);
  Glib::signal_timeout().connect(&on_idle_cbk, 10);
  area = new DrawingAreaCAIRO("Area");
  jauge = new JaugeCAIRO();
  GadjetCAIROPtr jauge_ptr = GadjetCAIROPtr( jauge );
  jauge->set_position( 0.1, 0.1 );
  //jauge->set_orientation( 0.2 );
  jauge->set_bg_color( 1.0, 0.0, 0.0, 0.2); 
  jauge->set_fg_color( 0.0, 1.0, 0.0, 0.9); 
  jauge->set_ratio( 0.0 );
  area->add_gadjet( jauge_ptr );

  jauge2 = new JaugeCAIRO();
  GadjetCAIROPtr jauge2_ptr = GadjetCAIROPtr( jauge2 );
  jauge2->set_position( 0.3, -0.8 );
  jauge2->set_orientation( 2.0 );
  jauge2->set_bg_color( 1.0, 0.0, 0.0, 0.2); 
  jauge2->set_fg_color( 0.0, 0.0, 1.0, 0.9);
  jauge2->set_scale( 0.5);
  jauge2->set_bounds( 2, 5 );
  jauge2->set_level( 2.3 );
  area->add_gadjet( jauge2_ptr );

  text1 = new TextCAIRO( "BouH" );
  GadjetCAIROPtr text1_ptr = GadjetCAIROPtr( text1 );
  area->add_gadjet( text1_ptr );
  text1->set_is_line( true );
  text1->set_coef_box_extend(1.5);
  text1->set_align( ALIGN_LEFT );

  jtext1 = new JaugeTextCAIRO( 2.0, 0.0, 10.0);
  GadjetCAIROPtr jtext1_ptr = GadjetCAIROPtr( jtext1 );
  area->add_gadjet( jtext1_ptr );
  jtext1->set_position( -0.5f, -0.5f );
  jtext1->set_is_line( true );

  win->add( *area );
  win->show_all_children();
  boost::thread joystick_thread(boost::bind(&run_window, win));
  //Gtk::Main::run(*win);
  //return area;
}


int
main (int argc, char *argv[] )
{

  std::cout << "init_gtk" << "\n";
  Gtk::Main kit(argc, argv);
  
  //boost::thread joystick_thread(create_new_area);
  //area = create_new_area("CAIRO");
  // JaugeCAIRO * jauge = new JaugeCAIRO();
//   GadjetCAIROPtr jauge_ptr = GadjetCAIROPtr( jauge );
//   jauge->set_position( 0.1, 0.1 );
//   //jauge->set_orientation( 0.2 );
//   jauge->set_bg_color( 1.0, 0.0, 0.0, 0.2); 
//   jauge->set_fg_color( 0.0, 1.0, 0.0, 0.9); 
//   jauge->set_ratio( 0.0 );
//   area->add_gadjet( jauge_ptr );

  //boost::thread joystick_thread(boost::bind(&run_window, win));
  //boost::thread joystick_thread(create_new_area);
  create_new_area();

   float ratio = 0;
   while( true ) {
    sleep(1);
    ratio += 0.2;
    if( ratio > 20.0 ) ratio = 0.0;
    jtext1->set_level( ratio );
//     text1->set_orientation( ratio );
//     //jauge->set_ratio( ratio );
//     //jauge2->set_ratio( 1.0 - ratio);
//     //jauge2->set_level( jauge2->get_level() + ratio );
//     status_changed = true;
     std::cout << "ratio = " << ratio << "\n";
     area->signal_redraw().emit();
   }

  return 0;
}
