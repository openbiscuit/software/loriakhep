//
// gcc test_draw.cc joystick_gtkmm.cc -o test_draw `pkg-config gtkmm-2.4 --cflags --libs` -lboost_thread
// 

#include "joystick_gtkmm.h"
//#include "joystick_gtk.h"

//#include <gtkmm/main.h>
//#include <gtkmm/window.h>

#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <iostream>

void
run_window(Gtk::Window *win)
{
  Gtk::Main::run(*win);
}

JoystickGTKMM *
create_new_joystick( std::string name )
{
  
  Gtk::Window * win = new Gtk::Window();
  win->set_title( name );
  JoystickGTKMM * joy = new JoystickGTKMM(name);
  joy->set_recenter( false );
  win->add( *joy );
  win->show_all_children();
  
  boost::thread joystick_thread(boost::bind(&run_window, win));

  return joy;
}


int
main (int argc, char *argv[] )
{

  std::cout << "init_gtk" << "\n";
  Gtk::Main kit(argc, argv);

  JoystickGTKMM * my_joy = create_new_joystick("bouh");

  while( true ) {
    std::cout << "JOYSTICK : " << my_joy->get_x() << "  " << my_joy->get_y() << "\n";
    sleep(1);
  }

  return 0;
}
