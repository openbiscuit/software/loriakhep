# Standard things
# saves the variable $(d) that holds the current directory on the stack,
# and sets it to the current directory given in $(dir), 
# which was passed as a parameter by the parent rules.mk

sp 		:= $(sp).x
dirstack_$(sp)	:= $(d)
d		:= $(dir)

# Subdirectories, if any, in random order

#dir	:= $(d)/test
#include		$(dir)/Rules.mk


# Local rules and target

TGTS_$(d)	:= $(d)/doc_gadget

TGT_DOC		:= $(TGT_DOC) $(TGTS_$(d)) verbose_$(d) 
CLEAN_DOC	:= $(CLEAN_DOC) $(d)/html
VERYCLEAN	:= $(VERYCLEAN) $(d)/*~

#$(TGTS_$(d)):	$(d)/rules.mk 

$(d)/doc_gadget: $(d)/gadget.doxy
		$(GENERATE_DOC)

.PHONY : verbose_$(d)
verbose_$(d): $(TGTS_$(d))
	@echo "Generating $^"

# As a last step, we restore the value of the current directory variable $(d) 
# by taking it from the current location on the stack, and we "decrement" 
# the stack pointer by stripping away the .x suffix 
# we added at the top of this file.

# Standard things

-include	$(DEPS_$(d))

d		:= $(dirstack_$(sp))
sp		:= $(basename $(sp))

