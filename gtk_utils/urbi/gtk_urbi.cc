/**
 * Central GTK Initialisation
 */

#include "gtk_urbi.h"
// ==========================================================================
Gtk::Main* GtkURBI::_kit_gtk = NULL;
// ==========================================================================
GtkURBI::GtkURBI()
{
  if (_kit_gtk == NULL) {
    int argc_dummy = 1;
    char **argv_dummy;
    const char *argv_zero = "gtk_loop";
    argv_dummy = (char **) malloc( argc_dummy * sizeof(char *));
    argv_dummy[0] = (char *) malloc( 20 * sizeof(char));
    strcpy( argv_dummy[0], argv_zero);
    
    _kit_gtk = new Gtk::Main(&argc_dummy, &argv_dummy);
  }
}
GtkURBI::~GtkURBI() //---------------------------------------------
{
  if (_kit_gtk != NULL) {
    delete _kit_gtk;
  }
}
// ==========================================================================
