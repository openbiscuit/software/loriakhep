/**
 * Retine and OverlayViewer running parallel to URBI.
 *
 */

#include "retine_urbi.h"
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <functional>

#define WIN_WIDTH 320
#define WIN_HEIGHT 240

// ==========================================================================
//Gtk::Main* RetineURBI::_kit_gtk = NULL;
// ==========================================================================
RetineURBI::RetineURBI(const std::string &name)
  :urbi::UObject(name)
{
  _gtk_urbi = NULL;
  _gtk_urbi = new GtkURBI();

  UBindFunction(RetineURBI, init);
  UBindFunction(RetineURBI, help);
}
RetineURBI::~RetineURBI() //---------------------------------------------
{
  if (_list_pos_tab != NULL ) {
    delete _list_pos_tab;
  }
  if( _gtk_urbi != NULL) {
    delete _gtk_urbi;
  }
  // vs_free(&_vs)
}
// ==========================================================================
int
RetineURBI::help()
{
  std::cout << "** RetineURBI : virtual eye that detect red in vertcal stripes\n";
  std::cout << "__.init( <str> robot object, [opt]<List(int)> position of captor)\n";
  std::cout << "__.val [R/-]: value of the red_level at various x_positions\n";
  std::cout << "__.range [R/W]: red_level is computed in [0,range] and )35-range,35]";
  std::cout << "__.pos [R/-]: position where red_levels are computed\n";
  std::cout << "__.load [R/W]: are sensor read (1) or not (0)\n"; 
  
  return 0; // URBI OK */
}
// ==========================================================================
int
RetineURBI::init(const std::string &name, urbi::UList list_pos)
{
  // A list of position is provided ??
  if( list_pos.array.size() > 0 ) {
    _list_pos_tab = new int[list_pos.array.size()];
    
    for( unsigned int i=0; i<list_pos.array.size(); i++) {
      _list_pos_tab[i] = (int) *(list_pos.array[i]);
    }
    
    _retine = new HSVRetine(list_pos.array.size(), _list_pos_tab);   
  }
  else {
    // create default HSVRetine
    _retine = new HSVRetine();
  }
  
  _name_overlay = name;

  // Par defaut, pas de debug graphique
  _show.init(__name, "show");
  _show = (int) 0;
  // Initialise néanmoins un viewer
  overlay_viewer_init(&_view, WIN_WIDTH, WIN_HEIGHT, (char *) _name_overlay.c_str());
  overlay_viewer_hide(&_view);
  _view.overlay_cbk = boost::bind( &HSVRetine::overlay_hist, _retine, _1, _2);
  // Need to know when 'loaded' is changed
  UNotifyChange( _show, &RetineURBI::on_show_changed );

  // create red_levels and positions
  for( int i=0; i < _retine->nb_pos; i++) {
    // create and init variable for each position
    urbi::UValue *red_lvl = new urbi::UValue();
    *red_lvl = 0.0;
    _red_lvl_vec.array.push_back( red_lvl );
    // create and init variable for each position
    urbi::UValue *red_pos = new urbi::UValue();
    *red_pos = _retine->pos_x[i];
    _red_pos_vec.array.push_back( red_pos );
  }
  // and now the shared UVar made of all this value
  _red_lvl_val.init(__name, "val");
  _red_lvl_val = _red_lvl_vec;
  _red_pos_val.init(__name, "pos");
  _red_pos_val = _red_pos_vec;


  _red_range.init(__name, "range");
  _red_range = (int) 3;

  // bind to the existing 'load' variable
  // not needed as 'load' is already a slotName of every UObject.
  //_urbi_load.init(__name, "load");
  // but need to know when 'loaded' is changed
  UNotifyChange( load, &RetineURBI::on_load_changed );
  // by default, load is set to 1 
  
  // whenever .val is accessed
  UNotifyAccess ( _red_lvl_val, &RetineURBI::_update_sensor_val );


  UBindFunction(RetineURBI, open_stream);

  return 0; // URBI OK */
}
// ==========================================================================
int
RetineURBI::open_stream(const std::string &name)
{
  std::string command = "rtspsrc location=rtsp://"+name+"/live.sdp latency=0 ! rtpmp4vdepay  ! ffdec_mpeg4";
  std::cout << "Command = " << command << "\n";
  
  _vs = new VideoSource;

  // Récupère l'image avec un type précis
  vs_init(_vs,command.c_str(), WIN_WIDTH, WIN_HEIGHT, "video/x-raw-rgb,depth=32,bpp=32");
  
  _id_wanted=0;
  _id_returned=0;
  vs_play(_vs);
  return 0; // URBI OK */
}
// ==========================================================================
int
RetineURBI::_update_sensor_val(void)
{
  if( (ufloat) load == 1 ) { // load must be 1
    // VS must be open
    if( _vs != NULL ) {
      _buf=vs_get_next_image(_vs,_id_wanted,&_id_returned);
      // this will ensure we don't work twice on the same image
      _id_wanted=_id_returned+1;
      if (_buf) {
	// extract the pixbuffer
	_pixbuf=gst_to_gdkpixbuf(_buf);
	// calcule les histogrammes
	_retine->compute_h_hist( _pixbuf );
	
	// et on dessine si besoin
	if( (ufloat) _show == 1 ) {
	  overlay_viewer_draw_pixbuf(&_view, _pixbuf);
	}
	
	gst_buffer_unref(_buf);
	// compute red level
	//std::cout << "Printing history\n";
	//_retine->print();
	//std::cout << "Range = " << (int) (_red_range) << "\n";
	for( int i = 0; i < _retine->nb_pos; i++) {
	  float total = 0.0;
	  float red = 0.0;
	  for( int level=0; level<36; level++) {
	    total += _retine->hist[i][level];
	    if( level < 3 || level > 33 ) 
	      red += _retine->hist[i][level];
	  }
	  if (total > 0.0) {
	    _red_lvl_vec[i] = (urbi::UValue) (red/total);
	  }
	  else {
	    _red_lvl_vec[i] = (urbi::UValue) 0.0;
	  }
	}
      }
      _red_lvl_val = _red_lvl_vec;
    }
  }

  return 0;  // URBI OK */
}
// ==========================================================================
int
RetineURBI::on_load_changed(urbi::UVar& v)
{
  return 0; // URBI OK */
}
// ==========================================================================
int
RetineURBI::on_show_changed(urbi::UVar& v)
{
  //std::cout << __name << " : _show = " << (ufloat) _show << "\n";
  //std::cout << __name << " : load = " << (ufloat) v << "\n";

  if( (ufloat) _show == 1 ) {
    //std::cout << "should show window\n";
    overlay_viewer_show(&_view);
  }
  else{
    //std::cout << "should hide window\n";
    overlay_viewer_hide(&_view);
  } 
  return 0; // URBI OK */
}
// ==========================================================================  
UStart(RetineURBI);
// ==========================================================================
