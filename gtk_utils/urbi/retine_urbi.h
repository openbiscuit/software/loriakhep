#ifndef __RETINE_URBI_H
#define __RETINE_URBI_H

#include <urbi/uobject.hh>
#include "hsv_retine.h"
#include "overlay_viewer.h"
#include "videosource.h"
#include "gtk_urbi.h"
//#include <gtkmm.h>

/**
Un oeil/rétine virtuelle qui s'appuye sur des taux de rouge dans des
bandes verticales (voir HSVRetine). 
On peut visualiser le résultats dans un OverlayViewer avec GTK/GDK.

\section urbi URBI
A la création de l'uobjet, on peut modifier la position horizontal des
endroits où seront calculés ces taux de rouge (voir RetineURBI::init).
La méthode '\c .help()' affiche les 'slots' de l'UObjet.
L'image de la caméra est affichée ou non suivant la valeurs de l'attribut '\c .show'.
L'attribut '\c .range' définit l'étendue de l'intervale des teintes qui comptent
comme des teintes rouges. Enfin, '\c .val' contient les taux de rouge pour les positions données par '\c .pos'.

Un exemple d'utilisation de cet objet dans URBI serait le suivant
\code
>> var re = uobjects.RetineURBI.new("bouh");
>> re.open_stream("korwlcam2.loria.fr");
>> re.show = 1;
>> re.val;
[00032268] [0.657407, 0.609091, 0.757009, 0.845361, 0.954955, 0, 0]
\endcode
\image html retine_urbi.png [Exemple de visualisation de l'image et des histogrammes]
 */
class RetineURBI : public urbi::UObject
{
 public:
  /**
   * @name Creation/Destruction
   */
  /**
   * At creation, a Gtk::Main will be created if not existing.
   * Then 'init' is binded into URBI.
   */
  RetineURBI(const std::string &name);
  /**
   * Destruction, that tries to clear up every Gtk::Widget.
   */
  ~RetineURBI();
  //@}

  /**
   * Create a OverlayViewer of the given name and shows it in a independant thread.
   *
   * @param name Name of RetineURBI (not used right now)
   * @param list_pos [option] list of int that defines the position where red levels will be computed
   *
   * Ainsi, en urbi on peut créer l'UObjet de plusieurs manières
   * \code
   * // Position des bandes par défaut
   * >> var re = uobject.RetineURBI.new("bouh");
   * // En indiquant la position des bandes
   * >> var re = uobject.RetineURBI.new("bouh", [10, 20, 30, 40, 50] );
   * \endcode
   */
  int init(const std::string &name, urbi::UList list_pos);
  /**
   * Open a given videostream.
   */
  int open_stream(const std::string &name);

  /**
   * Print out information on the module.
   */
  int help(void);

 protected:
  /**
   * Track 'load' changes. 
   */
  int on_load_changed(urbi::UVar& v);
  /**
   * Update the value of the Retine, when called.
   */
  int _update_sensor_val(void);
  /**
   * Track 'show' changes. 
   */
  int on_show_changed(urbi::UVar& v);

 protected:
  /** A list of values for red_lvl */
  urbi::UList _red_lvl_vec;
  /** A var for the red_lvl */
  urbi::UVar _red_lvl_val;
  /** A var for the range of red */
  urbi::UVar _red_range;
  /** A list of values for red_pos */
  urbi::UList _red_pos_vec;
  /** A var for the red_pos */
  urbi::UVar _red_pos_val;

  /** Debug : show/hide (1/0) picture with histograms ? */
  urbi::UVar _show;

  /** var holding the requested positions */
  int *_list_pos_tab;
  
  /** A HSVRetine */
  HSVRetine *_retine;
  /** A OverlayViewer. */
  OverlayViewer _view;
  /** Name displayed in the OverlayViewer. */
  std::string _name_overlay;
  /** Pixbuffer */
  GdkPixbuf *_pixbuf;
  /** @name indexImg */
  unsigned int _id_wanted;
  unsigned int _id_returned;
  //@}
  /** Gstream buffer */
  GstBuffer *_buf;
  /** Videosource */
  VideoSource *_vs;
  
  /** The Gtk package that must be initialized. */
  GtkURBI *_gtk_urbi;
};

#endif //__RETINE_URBI_H
