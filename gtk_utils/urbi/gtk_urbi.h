#ifndef __GTK_URBI_H
#define __GTK_URBI_H

#include <gtkmm.h>

/**
Un singleton pour être sûr que GTK est initialisé correctement,
une et une seule fois, par au moins un des UObject.

Comme on le voit dans les differents UObject, l'utilisation courante
est de rajouter dans les fichier .h
\code
  //The Gtk package that must be initialized.
  GtkURBI *_gtk_urbi;
\endcode
Dans le contructeur de l'UObject
\code
  _gtk_urbi = NULL;
  _gtk_urbi = new GtkURBI();
\endcode
et dans le code du destructeur
\code
  if( _gtk_urbi != NULL) {
    delete _gtk_urbi;
  }
\endcode
 */
class GtkURBI
{
 public:
  /**
   * @name Creation/Destruction
   */
  /**
   * At creation, a Gtk::Main will be created if not existing.
   */
  GtkURBI();
  /**
   * Destruction.
   */
  ~GtkURBI();
  //@}

 protected:
  /** The Gtk package that must be initialized. */
  static Gtk::Main* _kit_gtk;
};

#endif //__GTK_URBI_H
