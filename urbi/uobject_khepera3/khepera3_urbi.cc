#include "khepera3_urbi.h"
#include "utils_urbi.h"

// ==========================================================================
Khepera3::Khepera3( const std::string &name )
  : urbi::UObject(name)
{
  // intialise object variables

  UBindFunction(Khepera3, init);
  UBindFunction(Khepera3, help);
  UBindFunction(Khepera3, get_ptr);
}
Khepera3::~Khepera3()
{
}
// ==========================================================================
int
Khepera3::help()
{
  std::cout << "** Khepera3 : core module for khepera3 driver.\n";
  std::cout << "__.init( <str> ip address, <str> port )\n";
  std::cout << "__.get_ptr() => string wrapping ptr to Khepera3 UObject\n";

  return 0; // URBI OK */
}
// ==========================================================================
int
Khepera3::init()
{
  // init Khepera structures...
  khepera3_init();
  
  // Monitor the status of the load variable
  //UNotifyChange( load, &Khepera3::_on_load_changed );

  return 0; // URBI OK */
}
// ==========================================================================
std::string
Khepera3::get_ptr()
{
  return wrap_pointer( this );
}
// ==========================================================================
//int
//Khepera3::_on_load_changed(urbi::UVar& v)
//{
  // std::cout << __name << ":_on_load_changed with " << members.size() << " children\n";
//   // child object
//   for (urbi::UObjectList::iterator it = members.begin();
//        it != members.end();
//        it++) {
//     std::cout << __name << " found child " << (*it)->__name << " of class " << (*it)->classname << "\n";
//     (*it)->load = (urbi::UVar) v;
//   }
//  return 0; // URBI OK */
//}
// ==========================================================================
UStart(Khepera3);
// ==========================================================================
  
