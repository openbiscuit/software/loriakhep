#include "khepera3_ir_urbi.h"
#include "khepera3_urbi.h"
#include "utils_urbi.h"

// ==========================================================================
IrProxi::IrProxi( const std::string &name )
  : urbi::UObject(name)
{
  _parent = NULL;
  // intialise object variables

  UBindFunction(IrProxi, init);
  UBindFunction(IrProxi, help);
}
IrProxi::~IrProxi()
{
  if( _parent != NULL ) {
    _parent->members.remove( this );
  }
}
// ==========================================================================
int
IrProxi::help()
{
  std::cout << "** IrProxy : get values of the IR proximity sensors \n";
  std::cout << "__.init( <str> robot object)\n";
  std::cout << "__.val [R/-]: value of the sensors\n";
  std::cout << "__.timestamp [R/-]: timestamps of the last reading\n";
  std::cout << "__.load [R/W]: are sensor read (1) or not (0)\n"; 
  
  return 0; // URBI OK */
}
// ==========================================================================
int
IrProxi::init(std::string khepera3_ptr, ufloat freq_update )
{
  _parent = (Khepera3 *) unwrap_pointer( khepera3_ptr );
  _parent->members.push_front( this );

  for( int i = cKhepera3SensorsInfrared_Begin; i < cKhepera3SensorsInfrared_Count; ++i ) {
    // create and init variable for each IR
    urbi::UValue *ir = new urbi::UValue();
    *ir = 0;
    _ir_proxi_vec.array.push_back(ir);
  }
  // bind to the existing 'load' variable
  // not needed as 'load' is already a slotName of every UObject.
  //_urbi_load.init(__name, "load");
  // DEBUG UNotifyChange( load, &IrProxi::on_load_changed );

  // and now the shared UVar made of all this value
  _ir_proxi_val.init(__name, "val");
  
  _ir_proxi_val = _ir_proxi_vec;
  _ir_proxi_timestamp.init(__name, "timestamp");
  _ir_proxi_timestamp = 0;

  // At each beginning of cycle, call 'update_ir_proxi()'
  USetTimer( freq_update, &IrProxi::_update_sensor_val);

  return 0; // URBI OK */
}
// ==========================================================================
int
IrProxi::_update_sensor_val(void)
{
  //if( (ufloat) _ir_proxi_load == 1 ) {
  // 'load' is a slotName of every UObject
  if( (ufloat) load == 1 ) {
    //std::cout << __name << " khepera3_infrared_proximity()\n";
    khepera3_infrared_proximity();
    
    for( int i = cKhepera3SensorsInfrared_Begin; i < cKhepera3SensorsInfrared_Count; ++i ) {
      _ir_proxi_vec[i] = (urbi::UValue) khepera3.infrared_proximity.sensor[i];
    }
    _ir_proxi_val = _ir_proxi_vec;
    _ir_proxi_timestamp = (urbi::UValue) khepera3.infrared_proximity.timestamp;
  }
  return 0;  // URBI OK */
}
// ==========================================================================
UStart(IrProxi);
// ==========================================================================

// ==========================================================================
IrAmb::IrAmb( const std::string &name )
  : urbi::UObject(name)
{
  _parent = NULL;
  // intialise object variables

  UBindFunction(IrAmb, init);
  UBindFunction(IrAmb, help);
}
IrAmb::~IrAmb()
{
  if( _parent != NULL ) {
    _parent->members.remove( this );
  }
}
// ==========================================================================
int
IrAmb::help()
{
  std::cout << "** IrAmb : get values of the IR ambient sensors \n";
  std::cout << "__.init( <str> robot object)\n";
  std::cout << "__.val [R/-]: value of the sensors\n";
  std::cout << "__.timestamp [R/-]: timestamps of the last reading\n";
  std::cout << "__.load [R/W]: are sensor read (1) or not (0)\n"; 
  
  return 0; // URBI OK */
}
// ==========================================================================
int
IrAmb::init(std::string khepera3_ptr )
{
  _parent = (Khepera3 *) unwrap_pointer( khepera3_ptr );
  _parent->members.push_front( this );

  for( int i = cKhepera3SensorsInfrared_Begin; i < cKhepera3SensorsInfrared_Count; ++i ) {
    // create and init variable for each IR
    urbi::UValue *ir = new urbi::UValue();
    *ir = 0;
    _ir_amb_vec.array.push_back(ir);
  }
  // bind to the existing 'load' variable
  // not needed as 'load' is already a slotName of every UObject.
  //_urbi_load.init(__name, "load");
  // DEBUG UNotifyChange( load, &IrAmb::on_load_changed );

  // and now the shared UVar made of all this value
  _ir_amb_val.init(__name, "val");
  
  _ir_amb_val = _ir_amb_vec;
  _ir_amb_timestamp.init(__name, "timestamp");
  _ir_amb_timestamp = 0;

  // Updated at each access of 'val'
  UNotifyAccess( _ir_amb_val, &IrAmb::_update_sensor_val);

  return 0; // URBI OK */
}
// ==========================================================================
int
IrAmb::_update_sensor_val(void)
{
  //if( (ufloat) _ir_amb_load == 1 ) {
  // 'load' is a slotName of every UObject
  if( (ufloat) load == 1 ) {
    //std::cout << __name << " khepera3_infrared_proximity()\n";
    khepera3_infrared_ambient();
    
    for( int i = cKhepera3SensorsInfrared_Begin; i < cKhepera3SensorsInfrared_Count; ++i ) {
      _ir_amb_vec[i] = (urbi::UValue) khepera3.infrared_ambient.sensor[i];
    }
    _ir_amb_val = _ir_amb_vec;
    _ir_amb_timestamp = (urbi::UValue) khepera3.infrared_ambient.timestamp;
  }
  return 0;  // URBI OK */
}
// ==========================================================================
UStart(IrAmb);
// ==========================================================================
  
