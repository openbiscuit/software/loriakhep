#ifndef __KHEPERA3_BATTERY_URBI_H
#define __KHEPERA3_BATTERY_URBI_H

#include <urbi/uobject.hh>
#include "khepera3_urbi.h"

class Battery : public urbi::UObject
{
 public:
  /**
   * @name Creation/Destruction
   */
  /**
   * Then 'init()' is binded into URBI.
   */
  Battery( const std::string &name );
  virtual ~Battery();
  //@}

  /**
   * Init the UObject thanks to a core Khepera3 object.
   *
   * @param khepera3_ptr string with a pointer to a Khepera3 UObject.
   */
  int init( std::string khepera3_ptr );
  /**
   * Print out information on the module.
   */
  int help(void);

 protected:
  int _on_voltage_accessed();
  int _on_current_accessed();
  int _on_avg_current_accessed();
  int _on_remain_abs_accessed();
  int _on_remain_rel_accessed();
  int _on_temperature_accessed();
  
  /** battery voltage (0.1 mV) */
  urbi::UVar _voltage;
  /** battery current (0.1 mA) */
  urbi::UVar _current;
  /** avg current (0.1 mA) */
  urbi::UVar _avg_current;
  /** remain absolute (0.1 mAh)*/
  urbi::UVar _remain_abs;
  /** remain relative (0.0001 %) */
  urbi::UVar _remain_rel;
  /** temperature (0.0001 degrees Celsius) */
  urbi::UVar _temperature;

  /** The parent Khepera3 UObject */
  Khepera3 *_parent;

};

#endif //__KHEPERA3_BATTERY_URBI_H
