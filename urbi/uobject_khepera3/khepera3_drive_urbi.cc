#include "khepera3_drive_urbi.h"
#include "utils_urbi.h"
#include <urbi/ublend-type.hh>
// ==========================================================================
Drive::Drive( const std::string &name )
  : urbi::UObject(name)
{
  // intialise object variables
  UBindFunction(Drive, init);
  UBindFunction(Drive, help);
  UBindFunction(Drive, get_ptr);
}
Drive::~Drive()
{
}
// ==========================================================================
int
Drive::help()
{
  std::cout << "** Drive : 2-wheel drive for the khepera\n";
  std::cout << "__.init( <str> robot object)\n";
  std::cout << "__.status [R/-]: status of the drive mechanism [left,right]\n";
  std::cout << "__.error [R/-]: error of the drive mechanism [left,right]\n";
  std::cout << "__.load [R/W]: is drive enabled or not\n"; 
  std::cout << "__.start() start the drive\n"; 
  std::cout << "__.stop() stop the drive\n";
  std::cout << "__.idle() set the drive idle\n";
  std::cout << "__.get_ptr() => string wrapping ptr to Drive UObject\n";
  
  return 0; // URBI OK */
}
// ==========================================================================
int
Drive::init( std::string khepera3_ptr )
{
  // bind the khepera3
  _parent = (Khepera3 *) unwrap_pointer( khepera3_ptr );
  
  // Monitor the status of the load variable
  //UNotifyChange( load, &Drive::_on_load_changed );

  UBindFunction(Drive, start);
  UBindFunction(Drive, stop);
  UBindFunction(Drive, idle);
  
  // Variables
  // create and init variable for each wheel
  for( int i=0; i< 2; ++i ) {
    urbi::UValue *status = new urbi::UValue();
    *status = 0;
    _status_vec.array.push_back(status);

    urbi::UValue *error = new urbi::UValue();
    *error = 0;
    _error_vec.array.push_back(status);
  }
  
  _status.init(__name, "status");
  _status = _status_vec;
  UNotifyAccess( _status, &Drive::_on_status_accessed );

  _error.init(__name, "error");
  _error =  _error_vec;
  UNotifyAccess( _error, &Drive::_on_error_accessed );



  return 0; // URBI OK */
}
// ==========================================================================
std::string
Drive::get_ptr()
{
  return wrap_pointer( this );
}
// ==========================================================================
void
Drive::start()
{
  khepera3_drive_start();
}
// ==========================================================================
void
Drive::stop()
{
  khepera3_drive_stop();
}
// ==========================================================================
void
Drive::idle()
{
  khepera3_drive_idle();
}
// ==========================================================================
int 
Drive::_on_status_accessed()
{
  khepera3_drive_get_status();

  // update UVar
  //_status_left = (int) _khepera3->khepera3.motor_left.status;
  //_status_right = (int) _khepera3->khepera3.motor_right.status;
  _status_vec[0] = (urbi::UValue) khepera3.motor_left.status;
  _status_vec[1] = (urbi::UValue) khepera3.motor_right.status;
  _status = _status_vec;

  return 0; // URBI OK */
}
// ==========================================================================
int
Drive::_on_error_accessed()
{
  khepera3_drive_get_error();

  // update UVar
  //_error_left = (int) _khepera3->khepera3.motor_left.error;
  //_error_right = (int) _khepera3->khepera3.motor_right.error;
  _error_vec[0] = (urbi::UValue) khepera3.motor_left.error;
  _error_vec[1] = (urbi::UValue) khepera3.motor_right.error;
  _error = _error_vec;

  return 0; // URBI OK */
}
// ==========================================================================
//int
//Drive::_on_load_changed(urbi::UVar& v)
//{
  // std::cout << __name << ":_on_load_changed with " << members.size() << " children\n";
//   // child object
//   for (urbi::UObjectList::iterator it = members.begin();
//        it != members.end();
//        it++) {
//     std::cout << __name << " found child " << (*it)->__name << " of class " << (*it)->classname << "\n";
//   }
//  return 0; // URBI OK */
//}
// ==========================================================================

// ==========================================================================
// ==========================================================================
// ==========================================================================

// ==========================================================================
SpeedWheel::SpeedWheel( const std::string &name )
  : urbi::UObject(name)
{
  // intialise object variables
  UBindFunction(SpeedWheel, init);
  UBindFunction(SpeedWheel, help);
}
SpeedWheel::~SpeedWheel()
{
}
// ==========================================================================
int
SpeedWheel::help()
{
  std::cout << "** SpeedWheel : get/set speed of the Wheels \n";
  std::cout << "__.init( <str> drive object)\n";
  std::cout << "__.left [R/W]: speed of the left wheel\n";
  std::cout << "__.right [R/W]: speed of the right wheel\n";
  std::cout << "__.control [R/W]: type of control (0) open, (1) profile\n";
  std::cout << "__.load [R/W]: are sensor read (1) or not (0)\n"; 
  
  return 0; // URBI OK */
}
// ==========================================================================
int
SpeedWheel::init( std::string drive_ptr, ufloat freq_update, ufloat freq_command )
{
  // local variables
  _freq_update = freq_update;
  _freq_command = freq_command;

  // bind khepera3
  _parent = (Drive *) unwrap_pointer( drive_ptr );
  
  // shared control variable
  _control.init(__name, "control");
  _control = 0;

  // shared speed variables
  _speed_left.init(__name, "left");
  UOwned(_speed_left);
  _speed_left.blend = urbi::UMIX;
  _speed_left = 0;
  UNotifyChange( _speed_left, &SpeedWheel::_on_target_changed );

  _speed_right.init(__name, "right");
  UOwned(_speed_right);
  _speed_right.blend = urbi::UMIX;
  _speed_right = 0;
  UNotifyChange( _speed_right, &SpeedWheel::_on_target_changed );
  
  // a new command has not been issued !
  _command_changed = false;

  // Monitor the status of the load variable
  UNotifyChange( load, &SpeedWheel::_on_load_changed );
  load = 1;

  // At each beginning of cycle, call '_update_speed_val()'
  USetTimer(_freq_update, &SpeedWheel::_update_speed_val);
  
  return 0; // URBI OK */
}
// ==========================================================================
int 
SpeedWheel::_update_speed_val(void)
{
  if( (ufloat) load == 1 ) {
    //std::cout << __name << " khepera3_drive_get_current_speed()\n";
    khepera3_drive_get_current_speed();
    
    _speed_left = (urbi::UValue) khepera3.motor_left.current_speed;
    _speed_right = (urbi::UValue) khepera3.motor_right.current_speed;
  }
  return 0;  // URBI OK */
}
// ==========================================================================
int 
SpeedWheel::_update_speed_target(void)
{
  //std::cout << __name << "::_update_speed_target\n";
  if( _command_changed == true ) {
    //std::cout << "_command_changed was true\n";
    if( (ufloat) _control == 1 ) {
      //std::cout << __name << " khepera3_drive_set_speed_using_profile(... );\n";
      khepera3_drive_set_speed_using_profile( (int) _speed_left, 
					      (int) _speed_right );
    }
    else {
      //std::cout << __name << " khepera3_drive_set_speed(...);\n";
      khepera3_drive_set_speed( (int) _speed_left, 
				(int) _speed_right );
    }
    _command_changed = false;
  }
  return 0;  // URBI OK */
}
int
SpeedWheel::update() // -----------------------------------------------------
{
  _update_speed_target();
  return 0;  // URBI OK */
}
// ==========================================================================
int
SpeedWheel::_on_target_changed()
{
  //std::cout << __name << ":_on_target_changed()\n";
  _command_changed = true;

  return 0;  // URBI OK */
}
// ==========================================================================
int
SpeedWheel::_on_load_changed()
//SpeedWheel::_on_load_changed(urbi::UVar& v)
{
  //std::cout << __name << " : load = " << (ufloat) load << "\n";
  //std::cout << __name << " : load = " << (ufloat) v << "\n";

  if( (ufloat) load == 1 ) {
    //std::cout << __name << " // start update" << "\n";
    // At the end of each cycle, call '_update_speed_target()'
    //USetUpdate(_freq_command);
    USetUpdate( (ufloat) _freq_command );
  }
  else {
    //std::cout << __name << " // stop update" << "\n";
    USetUpdate(-1.0);
  }
  
  return 0;  // URBI OK */
}  
// ==========================================================================

// ==========================================================================
// ==========================================================================
// ==========================================================================

// ==========================================================================
PositionWheel::PositionWheel( const std::string &name )
  : urbi::UObject(name)
{
  // intialise object variables
  UBindFunction(PositionWheel, init);
  UBindFunction(PositionWheel, help);
  UBindFunction(PositionWheel, set_position);
}
PositionWheel::~PositionWheel()
{
}
// ==========================================================================
int
PositionWheel::help()
{
  std::cout << "** PositionWheel : get/set/goto position of the Wheels \n";
  std::cout << "__.init( <str> drive object)\n";
  std::cout << "__.left [R/W]: speed of the left wheel\n";
  std::cout << "__.right [R/W]: speed of the right wheel\n";
  std::cout << "__.set_position( <int>left, <int>right ) : set up new value for current position\n"; 
  std::cout << "__.control [R/W]: type of control (0) open, (1) profile\n";
  std::cout << "__.load [R/W]: are sensor read (1) or not (0)\n"; 
  
  return 0; // URBI OK */

}
// ==========================================================================
int
PositionWheel::init( std::string drive_ptr, ufloat freq_update, ufloat freq_command )
{
  _freq_update = freq_update;
  _freq_command = freq_command;
  // bind khepera3
  _parent = (Drive *) unwrap_pointer( drive_ptr );

  
  // Monitor the status of the load variable
  UNotifyChange( load, &PositionWheel::_on_load_changed );

  // shared control variable
  _control.init(__name, "control");
  _control = 0;

  // shared variables
  _pos_left.init(__name, "left");
   UOwned(_pos_left);
  _pos_left.blend = urbi::UMIX;
  _pos_left = 0;
  UNotifyChange( _pos_left, &PositionWheel::_on_target_changed );

  _pos_right.init(__name, "right");
   UOwned(_pos_right);
  _pos_right.blend = urbi::UMIX;
  _pos_right = 0;
  UNotifyChange( _pos_right, &PositionWheel::_on_target_changed );
  
  // a new command has not been issued !
  _command_changed = false;

  // Monitor the status of the load variable
  UNotifyChange( load, &PositionWheel::_on_load_changed );
  load = 1;
  
  // At each beginning of cycle, call '_update_speed_val()'
  USetTimer(_freq_update, &PositionWheel::_update_position_val);
  
  return 0; // URBI OK */
}
// ==========================================================================
int
PositionWheel::set_position( int pos_left, int pos_right )
{
  khepera3_drive_set_current_position( pos_left, pos_right);

  return 0; // URBI OK */
}
// ==========================================================================
int 
PositionWheel::_update_position_val(void)
{
  if( (ufloat) load == 1 ) {
    //std::cout << __name << " khepera3_drive_get_current_speed()\n";
    khepera3_drive_get_current_position();
    
    _pos_left = (urbi::UValue) khepera3.motor_left.current_position;
    _pos_right = (urbi::UValue) khepera3.motor_right.current_position;
  }
  return 0;  // URBI OK */
}
// ==========================================================================
int 
PositionWheel::_update_position_target(void)
{
  if( _command_changed == true ) {
    if( (ufloat) _control == 1 ) {
      //std::cout << __name << " khepera3_drive_goto_position_using_profile(... );\n";
      khepera3_drive_goto_position_using_profile( (int) _pos_left, 
						  (int) _pos_right );
    }
    else {
      //std::cout << __name << " khepera3_drive_goto_position(...);\n";
      khepera3_drive_goto_position( (int) _pos_left, 
				    (int) _pos_right );
    }
  }
  return 0;  // URBI OK */
}
int
PositionWheel::update() // -----------------------------------------------------
{
  _update_position_target();
  return 0;  // URBI OK */
}
// ==========================================================================
int
PositionWheel::_on_target_changed()
{
  //std::cout << __name << ":_on_target_changed()\n";
  _command_changed = true;

  return 0;  // URBI OK */
}
// ==========================================================================
int
PositionWheel::_on_load_changed()
//PositionWheel::_on_load_changed(urbi::UVar& v)
{
  //std::cout << __name << " : load = " << (ufloat) load << "\n";
  //std::cout << __name << " : load = " << (ufloat) v << "\n";

  if( (ufloat) load == 1 ) {
    // At the end of each cycle, call '_update_position_target()'
    USetUpdate((ufloat)_freq_command);
  }
  else {
    // stop update
    USetUpdate(-1.0);
  }
  
  return 0;  // URBI OK */
}  
// ==========================================================================
UStart(Drive);
UStart(SpeedWheel);
UStart(PositionWheel);
// ==========================================================================
