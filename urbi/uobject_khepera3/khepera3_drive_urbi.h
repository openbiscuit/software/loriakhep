#ifndef __KHEPERA3_DRIVE_URBI_H
#define __KHEPERA3_DRIVE_URBI_H

#include <urbi/uobject.hh>
#include "khepera3_urbi.h"

class Drive : public urbi::UObject
{
  public:
  /**
   * @name Creation/Destruction
   */
  /**
   * Then 'init()' is binded into URBI.
   */
  Drive( const std::string &name );
  virtual ~Drive();
  //@}

   /**
   * Init the UObject thanks to a core Khepera3 object.
   *
   * @param khepera3_ptr string with a pointer to the internal khepera3.
   */
  int init( std::string khepera3_ptr );
  /**
   * Print out information on the module.
   */
  int help(void);
  /**
   * Return a string wrapping of the pointer to the UOBject.
   */
  std::string get_ptr();
  
  /**
   * Puts both motors in normal (control) mode.
   */
  void start(void);
  /**
   * Stops both motors immediately.
   */
  void stop(void);
  /**
   * Puts both motors in idle mode.
   */
  void idle(void);

  /** The parent Khepera3 UObject */
  Khepera3 *_parent;

 protected:
  /**
   * Update on demand
   */
  int _on_status_accessed();
  /**
   * Update on demand
   */
  int _on_error_accessed();
  /**
   * Called when load is changed.
   */
  //int _on_load_changed(urbi::UVar& v);

  urbi::UVar _status;
  urbi::UList _status_vec;
  urbi::UVar _error;
  urbi::UList _error_vec;
  
};
class SpeedWheel : public urbi::UObject
{
  public:
  /**
   * @name Creation/Destruction
   */
  /**
   * Then 'init()' is binded into URBI.
   */
  SpeedWheel( const std::string &name );
  virtual ~SpeedWheel();
  //@}

  /**
   * Init the UObject thanks to a core Khepera3 object.
   *
   * @param drive_ptr string with a pointer to a Drive parent
   */
  int init( std::string drive_ptr, ufloat freq_update, ufloat freq_command );
  /**
   * Print out information on the module.
   */
  int help(void);
  /**
   * Set profile for speed.
   */
  // int set_profile(args);
  /**
   * Overload 'update'
   */
  virtual int update();

  /** The parent Drive UObject */
  Drive *_parent;

 protected:
  /**
   * Update values at the beginning of each URBI cycle.
   */
  int _update_speed_val(void);
  /**
   * If needed, send new target speed values at the end of each URBI cycle.
   */
  int _update_speed_target(void);
  /**
   * Called when a target value is given.
   */
  int _on_target_changed();
  /**
   * Called when load is changed.
   */
  int _on_load_changed();
  //int _on_load_changed(urbi::UVar& v);

  urbi::UVar _speed_left;
  urbi::UVar _speed_right;
  urbi::UVar _control;
  bool _command_changed;

  /** A Khepera3 Drive */
  urbi::UVar _drive;

  /** Update frequence (get) */
  ufloat _freq_update;
  /** Command frequence (set) */
  ufloat _freq_command;
  
};
class PositionWheel : public urbi::UObject
{
  public:
  /**
   * @name Creation/Destruction
   */
  /**
   * Then 'init()' is binded into URBI.
   */
  PositionWheel( const std::string &name );
  virtual ~PositionWheel();
  //@}

  /**
   * Init the UObject thanks to a core Khepera3 object.
   *
   * @param drive_ptr string with a pointer to a Drive parent
   */
  int init( std::string drive_ptr, ufloat freq_update, ufloat freq_command );
  /**
   * Print out information on the module.
   */
  int help(void);
  /**
   * Set profile for position.
   */
  // int set_profile(args);
  /**
   * Set the current position to the given values.
   */
  int set_position( int pos_left, int pos_right );
  /**
   * Overload 'update'
   */
  virtual int update();

  /** The parent Drive UObject */
  Drive *_parent;
 protected:
  /**
   * Update values at the beginning of each URBI cycle.
   */
  int _update_position_val(void);
  /**
   * If needed, send new target position values at the end of each URBI cycle.
   */
  int _update_position_target(void);
  /**
   * Called when a target value is given.
   */
  int _on_target_changed();
  /**
   * Called when load is changed.
   */
  int _on_load_changed();
  //int _on_load_changed(urbi::UVar& v);


  urbi::UVar _pos_left;
  urbi::UVar _pos_right;
  urbi::UVar _control;
  bool _command_changed;

  /** Update frequence (get) */
  ufloat _freq_update;
  /** Command frequence (set) */
  ufloat _freq_command;
};
#endif //__KHEPERA3_DRIVE_URBI_H
