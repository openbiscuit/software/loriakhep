#ifndef __KHEPERA3_IR_URBI_H
#define __KHEPERA3_IR_URBI_H

#include <urbi/uobject.hh>
#include "khepera3_urbi.h"

class IrProxi : public urbi::UObject
{
 public:
  /**
   * @name Creation/Destruction
   */
  /**
   * Then 'init()' is binded into URBI.
   */
  IrProxi( const std::string &name );
  virtual ~IrProxi();
  //@}

  /**
   * Init the UObject thanks to a core Khepera3 object.
   *
   * @param khepera3_ptr string with a pointer to a Khepera3 UObject.
   */
  int init( std::string khepera3_ptr, ufloat freq_update );
  /**
   * Print out information on the module.
   */
  int help(void);

 protected:

  /**
   * Update IR PROXI values at each URBI Cycle.
   */
  int _update_sensor_val(void);

  /** A list of values for ir_proxy */
  urbi::UList _ir_proxi_vec;
  /** A value for the ir_proxi timestamp */
  urbi::UVar _ir_proxi_timestamp;
  /** A var for the ir_proxi distance */
  urbi::UVar _ir_proxi_val;

  /** The parent Khepera3 UObject */
  Khepera3 *_parent;

};
class IrAmb : public urbi::UObject
{
 public:
  /**
   * @name Creation/Destruction
   */
  /**
   * Then 'init()' is binded into URBI.
   */
  IrAmb( const std::string &name );
  virtual ~IrAmb();
  //@}

  /**
   * Init the UObject thanks to a core Khepera3 object.
   *
   * @param khepera3_ptr string with a pointer to a Khepera3 UObject.
   */
  int init( std::string khepera3_ptr );
  /**
   * Print out information on the module.
   */
  int help(void);

 protected:

  /**
   * Update IR AMB values at each URBI Cycle.
   */
  int _update_sensor_val(void);

  /** A list of values for ir_proxy */
  urbi::UList _ir_amb_vec;
  /** A value for the ir_amb timestamp */
  urbi::UVar _ir_amb_timestamp;
  /** A var for the ir_amb distance */
  urbi::UVar _ir_amb_val;

  /** The parent Khepera3 UObject */
  Khepera3 *_parent;

};

#endif //__KHEPERA3_URBI_H
