#ifndef __KHEPERA3_TCP_H
#define __KHEPERA3_TCP_H

#include"khepera3_tcp_headers.h"
#include"nmea.h"

class Khepera3_TCP
{
 public:
  /** Structure for holding data */
  struct sKhepera3_TCP khepera3;
  
 public:
  /** Global init */
  void init();
  /** Initialize connection */
  int tcp_init(char *ip_address, int port);
  
  /** Read IR proxi */
  int infrared_proximity();
  /** Read IR ambient */
  int infrared_ambient();
  /**
   * @name motor
   */
  int drive_start();
  int drive_stop();
  int drive_idle();
  int drive_get_status();
  int drive_get_error();

  void drive_get_current_speed();
  void drive_set_speed(int speed_left, int speed_right);
  void drive_set_speed_using_profile(int speed_left, int speed_right);

  void drive_get_current_position();
  /** set the position of the wheel BUT WITHOUT MOVING the robot (see goto)*/
  void drive_set_current_position(int position_left, int position_right);
  /** goto some position */
  void drive_goto_position(int position_left, int position_right);
  void drive_goto_position_using_profile(int position_left, int position_right);
  //@

 private: // --------- NMEA 
  
  struct sNMEAParser _client_snd_nmea_parser;
  struct sNMEAParser _client_rcv_nmea_parser;
  int _connect_filehandle;
  char _msg_buffer[256];
  
  /** Send a command and hope for an answer to process it */
  int process_send_wait_process(char * msg_buffer);

  /** Open TCO connection */
  int tcp_connect( const char *address, int port, int *connect_filehandle);
  /** Print NMEA structure */
  void print_nmea(struct sNMEAMessage *msg); 
  /** Fill up IR data with answer to send message */
  void parse_ir_proxi(struct sNMEAMessage *msg);
  /** Fill up IR data with answer to send message */
  void parse_ir_amb(struct sNMEAMessage *msg);
  /** Fill up motor data with answer to send message */
  void parse_motor_speed(struct sNMEAMessage *msg);
  /** Fill up motor data with answer to send message */
  void parse_motor_position(struct sNMEAMessage *msg);

}; // Khepera3_TCP

/** how to send message */
void client_nmea_send_hook(int connexion, const char *buffer, int len);
/** If send message has unknown character, what to do */
void client_nmea_unknown_character(char c);

//! Initializes this module.
//extern int khepera3_tcp_init(char *ip_address, int port);
//extern void khepera3_init();

  //! Reads the last infrared proximity values and fills them into the khepera3 structure. The return value indicates success (-1) or failure (0). Transfer on I2C bus: 31 bytes.
//extern int khepera3_infrared_proximity();
#endif // __KHEPERA3_TCP_H
