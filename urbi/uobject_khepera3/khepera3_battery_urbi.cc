#include "khepera3_battery_urbi.h"
#include "utils_urbi.h"
#include <urbi/ublend-type.hh>
// ==========================================================================
Battery::Battery( const std::string &name )
  : urbi::UObject(name)
{
  // intialise object variables
  UBindFunction(Battery, init);
  UBindFunction(Battery, help);
}
Battery::~Battery()
{
}
// ==========================================================================
int
Battery::help()
{
  std::cout << "** Battery : state of the battery of the khepera\n";
  std::cout << "__.init( <str> robot object)\n";
  std::cout << "__.help(); this help message\n";
  std::cout << "__.voltage [R/-]: voltage in 0.1 mV\n";
  std::cout << "__.current [R/-]: current in 0.1 mA\n";
  std::cout << "__.avg_current [R/-]: average current in 0.1 mA\n";
  std::cout << "__.remain_abs [R/-]: absolute remaining current in 0.1 mAh\n";
  std::cout << "__.remain_rel [R/-]: relative remaining current in 0.0001\%\n";
  std::cout << "__.temperature [R/-] : temperature in 0.0001 degrees Celsius\n";
  
  return 0; // URBI OK */
}
// ==========================================================================
int
Battery::init( std::string khepera3_ptr )
{
  // bind the khepera3
  _parent = (Khepera3 *) unwrap_pointer( khepera3_ptr );
  
  // Monitor the status of the load variable
  //UNotifyChange( load, &Drive::_on_load_changed );

  // Variables
  // create and init variable for each wheel
  _voltage.init(__name, "voltage");
  _voltage = 0;
  UNotifyAccess( _voltage, &Battery::_on_voltage_accessed );

  _current.init(__name, "current");
  _current = 0;
  UNotifyAccess( _current, &Battery::_on_current_accessed );

  _avg_current.init(__name, "avg_current");
  _avg_current = 0;
  UNotifyAccess( _avg_current, &Battery::_on_avg_current_accessed );

  _remain_abs.init(__name, "remain_abs");
  _remain_abs = 0;
  UNotifyAccess( _remain_abs, &Battery::_on_remain_abs_accessed );

  _remain_rel.init(__name, "remain_rel");
  _remain_rel = 0;
  UNotifyAccess( _remain_rel, &Battery::_on_remain_rel_accessed );

  _temperature.init(__name, "temperature");
  _temperature = 0;
  UNotifyAccess( _temperature, &Battery::_on_temperature_accessed );

  return 0; // URBI OK */
}
// ==========================================================================
int 
Battery::_on_voltage_accessed()
{
  khepera3_battery_voltage();
  _voltage = (urbi::UValue) khepera3.battery.voltage;

  return 0; // URBI OK */ 
}
int 
Battery::_on_current_accessed()
{
  khepera3_battery_current();
  _current = (urbi::UValue) khepera3.battery.current;

  return 0; // URBI OK */ 
}
int 
Battery::_on_avg_current_accessed()
{
  khepera3_battery_current_average();
  _avg_current = (urbi::UValue) khepera3.battery.current_average;

  return 0; // URBI OK */ 
}
int 
Battery::_on_remain_abs_accessed()
{
  khepera3_battery_capacity_remaining_absolute();
  _remain_abs = (urbi::UValue) khepera3.battery.capacity_remaining_absolute;

  return 0; // URBI OK */ 
}
int 
Battery::_on_remain_rel_accessed()
{
  khepera3_battery_capacity_remaining_relative();
  _remain_rel = (urbi::UValue) khepera3.battery.capacity_remaining_relative;

  return 0; // URBI OK */ 
}
int 
Battery::_on_temperature_accessed()
{
  khepera3_battery_temperature();
  _temperature = (urbi::UValue) khepera3.battery.temperature;

  return 0; // URBI OK */ 
}
// ==========================================================================
UStart(Battery);
// ==========================================================================
