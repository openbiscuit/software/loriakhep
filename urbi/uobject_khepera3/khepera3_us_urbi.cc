
#include "khepera3_us_urbi.h"
#include "utils_urbi.h"

// ==========================================================================
UltraSound::UltraSound( const std::string &name )
  : urbi::UObject(name)
{
  // intialise object variables
  UBindFunction(UltraSound, init);
  UBindFunction(UltraSound, help);
  UBindFunction(UltraSound, get_ptr);
}
UltraSound::~UltraSound()
{
}
// ==========================================================================
int
UltraSound::help()
{
  std::cout << "** UltraSound : \n";
  std::cout << "__.init( <str> robot object, <float> update frequence)\n";
  std::cout << "__.load [R/W]: are ultrasound enabled or not\n"; 
  std::cout << "__.max_echo_number [R/W]: max number of echo\n";
  std::cout << "__.get_ptr() => string wrapping ptr to Drive UObject\n";
  
  return 0; // URBI OK */
}

// ==========================================================================
int
UltraSound::init( std::string khepera3_ptr, ufloat freq_update )
{
  _freq_update = freq_update;
  // bind khepera3
  _parent = (Khepera3 *) unwrap_pointer( khepera3_ptr );


  // index of enabled sensors
  for( int i=cKhepera3SensorsUltrasound_Begin; 
       i<cKhepera3SensorsUltrasound_End;
       ++i ) {
    _us_enabled[i] = 0;
  }

  // Define and initialize every variables.
  for( int us_index=cKhepera3SensorsUltrasound_Begin; 
       us_index<cKhepera3SensorsUltrasound_End;
       ++us_index ) {
    //std::cout << __name << "clearing _distance[" << us_index << "]\n";
    _distance[us_index].array.clear();
    _amplitude[us_index].array.clear();
    _timestamp[us_index].array.clear();
    
    urbi::UValue *dummy_val = new urbi::UValue();
    *dummy_val = (ufloat) -1;
    
    _echo_count[us_index] = (ufloat) -1;
    for( int echo = 0; echo < NB_MAX_ECHO; ++echo ) {
      //std::cout << __name << "setting _distance[" << us_index << "]["<<echo<<"]\n";
      dummy_val = new urbi::UValue();
      *dummy_val = (ufloat) -1;
      _distance[us_index].array.push_back(dummy_val);
      dummy_val = new urbi::UValue();
      *dummy_val = (ufloat) -1;
      _amplitude[us_index].array.push_back(dummy_val);
      dummy_val = new urbi::UValue();
      *dummy_val = (urbi::UValue) -1;
      _timestamp[us_index].array.push_back(dummy_val);
      
    }
  }
  
  // Bind variable
  _max_echo_number.init(__name, "max_echo_number");
  UNotifyChange( _max_echo_number, &UltraSound::_on_max_echo_number_changed );
  _max_echo_number = (urbi::UValue) 3;
  

  // not running by default
  load = 0;
  // At each beginning of cycle, call '_update_sensor_values()'
  USetTimer(_freq_update, &UltraSound::_update_sensor_values);
  
  return 0; // URBI OK */
}
// ==========================================================================
std::string
UltraSound::get_ptr()
{
  return wrap_pointer( this );
}
// ==========================================================================
int
UltraSound::_on_max_echo_number_changed()
{
  
  int nb_max = (int) _max_echo_number;
  
  if( (nb_max >= 0 ) && (nb_max < NB_MAX_ECHO)) {
   
    int load_save = 0;
    if( (ufloat) load == 1) {
      load_save = 1;
      load = (urbi::UValue) 0;
    }

    for( int us_index=cKhepera3SensorsUltrasound_Begin; 
       us_index<cKhepera3SensorsUltrasound_End;
       ++us_index ) {
      _echo_count[us_index] = (urbi::UValue) -1;
      
      //std::cout << __name << " clearing\n";
      _distance[us_index].array.clear();
      _amplitude[us_index].array.clear();
      _timestamp[us_index].array.clear();
      //std::cout << __name << " done\n";
      urbi::UValue *dummy_val = new urbi::UValue();
      *dummy_val = (ufloat) -1;

      // DONE : if no bug then could replace NB_MAX_ECHO by nb_max
      for( int echo = 0; echo < nb_max; ++echo ) {
	//std::cout << __name << "resetting _distance[" << us_index << "]["<<echo<<"]\n";
	//_distance[us_index][echo] = (urbi::UValue) -1;
	//_amplitude[us_index][echo] = (urbi::UValue) -1;
	//_timestamp[us_index][echo] = (urbi::UValue) -1;
	//std::cout << __name << " create dummy\n";
	dummy_val = new urbi::UValue();
	*dummy_val = (urbi::UValue) -1;
	//std::cout << __name << " affect dummy\n";
	_timestamp[us_index].array.push_back(dummy_val);
	//std::cout << __name << " done\n";
	dummy_val = new urbi::UValue();
	*dummy_val = (ufloat) -1;
	_distance[us_index].array.push_back(dummy_val);
	dummy_val = new urbi::UValue();
	*dummy_val = (ufloat) -1;
	_amplitude[us_index].array.push_back(dummy_val);
      }
    }

    int result = khepera3_ultrasound_set_max_echo_number( nb_max );
    if( load_save == 1) {
      load = (urbi::UValue) 1;
    }
    return result;
  }
  return 1;
}
// ==========================================================================
int
UltraSound::_update_sensor_values(void)
{
  // only if load is set
  if( (ufloat) load == 1 ) {
    // for each sensor, if enabled, update values
    for( int us_index=cKhepera3SensorsUltrasound_Begin; 
       us_index<cKhepera3SensorsUltrasound_End;
       ++us_index ) {
      
      if( _us_enabled[us_index] == 1 ) {
	//std::cout << __name << " update sensor[" << us_index << "]\n"; 
	khepera3_ultrasound( (eKhepera3SensorsUltrasound) us_index);
	_echo_count[us_index] = (urbi::UValue) khepera3.ultrasound.sensor[us_index].echos_count;
	
	for( int echo = 0; 
	     echo < khepera3.ultrasound.sensor[us_index].echos_count;
	     ++echo ) {
	  //std::cout << __name << " echo nb " <<  echo << "\n";
	  //std::cout << __name << " d=" << khepera3.ultrasound.sensor[us_index].distance[echo] << "  a=" << khepera3.ultrasound.sensor[us_index].amplitude[echo] << "  t=" << khepera3.ultrasound.sensor[us_index].timestamp[echo] << "\n";
	  _distance[us_index][echo] = (urbi::UValue) khepera3.ultrasound.sensor[us_index].distance[echo];
	  _amplitude[us_index][echo] = (urbi::UValue) khepera3.ultrasound.sensor[us_index].amplitude[echo];
	  _timestamp[us_index][echo] = (urbi::UValue) khepera3.ultrasound.sensor[us_index].timestamp[echo];
	}
      }

    }
    
  }
  return 0;  // URBI OK */
}
// ==========================================================================
int
UltraSound::enable_sensor( int us_index )
{
  _us_mask |= khepera3_ultrasound_getsensorbitbysensor( (eKhepera3SensorsUltrasound) us_index );
  int result =  khepera3_ultrasound_enable( (eKhepera3SensorsUltrasoundBit)_us_mask );
  if( result == -1) {
    _us_enabled[us_index] = 1;
  }
  return result;
}
int
UltraSound::disable_sensor( int us_index )
{
  _us_mask &= (~khepera3_ultrasound_getsensorbitbysensor( (eKhepera3SensorsUltrasound) us_index ));
  int result =  khepera3_ultrasound_enable( (eKhepera3SensorsUltrasoundBit)_us_mask );
  if( result == -1) {
    _us_enabled[us_index] = 0;
  }
  return result;
}
// ==========================================================================
UStart(UltraSound);
// ==========================================================================

// ==========================================================================
USSensor::USSensor( const std::string &name )
  : urbi::UObject(name)
{
  // intialise object variables
  UBindFunction(USSensor, init);
  UBindFunction(USSensor, help);
}
USSensor::~USSensor()
{
}
// ==========================================================================
int
USSensor::help()
{
  std::cout << "** USSensor : get values of one Ultrasound sensor \n";
  std::cout << "__.init( <str> UltraSound ptr, <int> index of the sensor)\n";
  std::cout << "__.echo_count [R/-]: nb of echo received\n";
  std::cout << "__.distance [R/-]: distance of each echo received\n";
  std::cout << "__.timestamp [R/-]: timestamp of each echo received\n";
  std::cout << "__.amplitude [R/-]: amplitude of each echo received\n";
  std::cout << "__.load [R/W]: are sensor read (1) or not (0)\n"; 
  
  return 0; // URBI OK */
}
// ==========================================================================
int
USSensor::init( std::string us_ptr, int index )
{
  _index = index;
  // bind khepera3
  _parent = (UltraSound *) unwrap_pointer( us_ptr );

  // bind variables
  //std::cout << __name << " binding _echo_count\n";
  _echo_count.init(__name, "echo_count");
  //std::cout << __name << " setting _echo_count with _index = " << _index <<"\n";
  _echo_count = (urbi::UValue) _parent->_echo_count[_index];
  UNotifyAccess( _echo_count, &USSensor::_on_echo_count_accessed );
  //std::cout << __name << " binding _distance\n";
  _distance.init(__name, "distance");
  //std::cout << __name << " setting _distance\n";
  _distance = _parent->_distance[_index];
  UNotifyAccess( _distance, &USSensor::_on_distance_accessed );
  _amplitude.init(__name, "amplitude");
  _amplitude = _parent->_amplitude[_index];
  UNotifyAccess( _amplitude, &USSensor::_on_amplitude_accessed );
  _timestamp.init(__name, "timestamp");
  _timestamp = _parent->_timestamp[_index];
  UNotifyAccess( _timestamp, &USSensor::_on_timestamp_accessed );

  // when load is changed
  UNotifyChange( load, &USSensor::_on_load_changed );
  load = 0;

  return 0; // URBI OK */
}
// ==========================================================================
int
USSensor::_on_echo_count_accessed(void)
{
  _echo_count = (urbi::UValue) _parent->_echo_count[_index];
  
  return 0; // URBI OK */
}
int
USSensor::_on_distance_accessed(void)
{
  _distance = (urbi::UList) _parent->_distance[_index];
  
  return 0; // URBI OK */
}
int
USSensor::_on_amplitude_accessed(void)
{
  _amplitude = (urbi::UList) _parent->_amplitude[_index];
  
  return 0; // URBI OK */
}
int
USSensor::_on_timestamp_accessed(void)
{
  _timestamp = (urbi::UList) _parent->_timestamp[_index];
  
  return 0; // URBI OK */
}
// ==========================================================================
/**
 * Called when load is changed.
 */
int 
USSensor::_on_load_changed()
{
  // if load to 1, then enable sensor
   if( (ufloat) load == 1 ) {
     return _parent->enable_sensor( _index );
   }
   else {
     return _parent->disable_sensor( _index );
   }
   
   return 0; // URBI OK */
}
// ==========================================================================
UStart( USSensor );
// ==========================================================================
