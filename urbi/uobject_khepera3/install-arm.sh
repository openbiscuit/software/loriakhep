#! /bin/csh
#
# install everything needed on the khepera
#
set GOSTAI_ARM = kernel-2.0-beta3-137-g28f8880-linux-ARM
set BASE_DIR = /home/dutech/Projets/TryUrbi

# copy gostai to khepera
scp -r $BASE_DIR/$GOSTAI_ARM root@192.168.0.202:/media/ram
# copy libstdc++
scp /usr/local/korebot-oetools-1.0/build/tmp/staging/arm-angstrom-linux-gnueabi/lib/libstdc++.so.6.0.8 root@192.168.0.202:/media/ram
# copy khepera3.u
scp khepera3_class.u  root@192.168.0.202:/home/root
# copy dynmaic lib
scp khepera3_urbi_arm.so root@192.168.0.202:/home/root

