//#ifndef _GNU_SOURCE
//#define _GNU_SOURCE
//#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <fcntl.h>

#include "nmea.h"

#include "khepera3_tcp.h"


//struct sNMEAParser _client_snd_nmea_parser;
//struct sNMEAParser _client_rcv_nmea_parser;
//int _connect_filehandle;
//char _msg_buffer[256];

// ==========================================================================
// == private methods =======================================================
// ==========================================================================
int
Khepera3_TCP::tcp_connect( const char *address, int port, int *connect_filehandle)
{
  struct sockaddr_in serv_address;
  struct hostent *serv_host;
  int connect_res;

  // Don't touch anything if the port is zero
  if (port == 0) {
    return 0;
  }

  // Look for host
  serv_host = NULL;
  serv_host = gethostbyname(address);
  if( serv_host == NULL ) {
    fprintf(stderr, "Unable to get server host.");
    return 0;
  }

  // Create Socket handle
  *connect_filehandle = socket( AF_INET, SOCK_STREAM, 0);
  if (*connect_filehandle < 0) {
    fprintf(stderr, "Unable to create socket: Error %d.\n", *connect_filehandle);
    *connect_filehandle = -1;
    return 0;
  }
  
  // Connect
  memset( &serv_address, 0x00, sizeof(serv_address));
  serv_address.sin_family = serv_host->h_addrtype;
  serv_address.sin_port = htons(port);
  memcpy( &serv_address.sin_addr, serv_host->h_addr, serv_host->h_length);
  connect_res = connect( *connect_filehandle,
			 (struct sockaddr *) &serv_address, sizeof(serv_address));

  if( connect_res < 0 ) {
    fprintf(stderr, "Connect failed: Error %d.\n", connect_res);
    *connect_filehandle = -1;
    return 0;
  }

  // IMPORTANT : set fd as non blocking.
  fcntl(*connect_filehandle, F_SETFL, O_NONBLOCK);
  // Success!
  fprintf(stderr, "Connected on TCP port %d.\n", port);
  return 1;
}
// ==========================================================================
void
Khepera3_TCP::print_nmea(struct sNMEAMessage *msg)
{
  unsigned int i;

  printf( "%s with %d arguments\n", msg->command, msg->argument_count);
  for( i=0; i< msg->argument_count; ++i) {
    printf( "  arg[%2.0d] = %s\n", i, msg->argument[i]);
  }
}

// ==========================================================================
void client_nmea_send_hook(int connect_filehandle, const char *buffer, int len)
{
  int nb_send = 0;
  char *p = (char *) buffer;
  int tosend = len;
  
  //printf("sended %s\n", buffer);fflush(stdout);
  while(tosend > 0)
    {
      nb_send = send(connect_filehandle, p, (int)tosend, MSG_NOSIGNAL);
      //printf("send %d\n", nb_send);fflush(stdout);
      if ( nb_send <= 0 )
        return;
      p += nb_send;
      tosend -= nb_send;
    }
}
// ==========================================================================
void client_nmea_unknown_character(char c)
{
  printf("'%c'(%d) was not recognized\n", c, c);
}
// ==========================================================================
void
Khepera3_TCP::parse_ir_proxi(struct sNMEAMessage *msg)
{
  int i;
  for( i = cKhepera3SensorsInfrared_Begin; i < cKhepera3SensorsInfrared_Count; ++i ) {
    //printf( "parse_ir_proxi [%d] = %ld\n", i, strtol(msg->argument[i], 0, 0));
    khepera3.infrared_proximity.sensor[i] = strtol(msg->argument[i], 0, 0);
  }
  khepera3.infrared_proximity.timestamp = strtol(msg->argument[i], 0, 0);
}
void
Khepera3_TCP::parse_ir_amb(struct sNMEAMessage *msg) // ---------------------
{
  int i;
  for( i = cKhepera3SensorsInfrared_Begin; i < cKhepera3SensorsInfrared_Count; ++i ) {
    //printf( "parse_ir_amb [%d] = %ld\n", i, strtol(msg->argument[i], 0, 0));
    khepera3.infrared_ambient.sensor[i] = strtol(msg->argument[i], 0, 0);
  }
  khepera3.infrared_ambient.timestamp = strtol(msg->argument[i], 0, 0);
}
void
Khepera3_TCP::parse_motor_speed(struct sNMEAMessage *msg) // ----------------
{
  khepera3.motor_left.current_speed = strtol(msg->argument[0], 0, 0);
  khepera3.motor_right.current_speed = strtol(msg->argument[1], 0, 0);
}
void
Khepera3_TCP::parse_motor_position(struct sNMEAMessage *msg) // ----------------
{
  khepera3.motor_left.current_position = strtol(msg->argument[0], 0, 0);
  khepera3.motor_right.current_position = strtol(msg->argument[1], 0, 0);
}

// ==========================================================================
int
Khepera3_TCP::process_send_wait_process(char * msg_buffer)
{
  char buffer[256];
  ssize_t nb_read;
  
  // process 
  if( nmea_parser_process_data( &_client_snd_nmea_parser, msg_buffer, strlen(msg_buffer) ) != -1 ) {
    printf( "Msg not recognized\n");
    return 0;
  }

  // send msg as it is valid
  //printf( "msg recognized\n");
  nmea_parser_send( _connect_filehandle, &_client_snd_nmea_parser, &_client_snd_nmea_parser.message);

  // check for answer
  //printf("**** wait for answer *****\n");
  nb_read = 0;
  while( nb_read <= 0 ){
    nb_read = read( _connect_filehandle, buffer, 256 );
    //printf( "read %d from connection\n", nb_read );
    if( nb_read > 0 ) {
      //printf("Received : %s\n", buffer);
      nmea_parser_process_data( &_client_rcv_nmea_parser, buffer, nb_read );
      //printf("**** Answer ****\n");
      //print_nmea( &(_client_rcv_nmea_parser.message) );

      if(strcmp(_client_rcv_nmea_parser.message.command, "IR_PROXI") == 0) { // $GET_PROXI */
	parse_ir_proxi( &_client_rcv_nmea_parser.message );
      }
      else if(strcmp(_client_rcv_nmea_parser.message.command, "IR_AMB") == 0) { // $GET_AMB */
	parse_ir_amb( &_client_rcv_nmea_parser.message );
      }
      else if(strcmp(_client_rcv_nmea_parser.message.command, "MOTOR_SPEED") == 0) { // GET_SPEED */
	parse_motor_speed( &_client_rcv_nmea_parser.message );
      }
      else if (strcmp(_client_rcv_nmea_parser.message.command, "AKN") == 0) { //$AKN */
	// nothing special
      }
      else if(strcmp(_client_rcv_nmea_parser.message.command, "MOTOR_POS") == 0) { // GET_POS */
	parse_motor_position( &_client_rcv_nmea_parser.message );
      }
      else {
	return 0;
      }
    }
    usleep(20);
  }
  //printf("**** no more answer *****\n");
  return -1;
}

// ==========================================================================
// == public methods ========================================================
// ==========================================================================
//struct sKhepera3_TCP khepera3;
// ==========================================================================
int 
Khepera3_TCP::tcp_init(char *ip_address, int port)
{
  fprintf(stderr, "Trying to connect to %s:%d\n", ip_address, port);
  // connect to the robot
  if( tcp_connect( ip_address, port, &_connect_filehandle) != 1 ) {
    fprintf(stderr, "Erreur dans la connection");
    return -1;
  }
  return 0;
}
// ==========================================================================
//! Initializes this module.
void
Khepera3_TCP::init()
{
  // nmea parser for recieving msg
  nmea_parser_init( & _client_rcv_nmea_parser);

  // nmea parser for sending msg
  nmea_parser_init( &_client_snd_nmea_parser);
  _client_snd_nmea_parser.hook_send = client_nmea_send_hook;
  _client_snd_nmea_parser.hook_process_unrecognized_char = client_nmea_unknown_character;
}
// ==========================================================================
//! Reads the last infrared proximity values and fills them into the khepera3 structure. The return value indicates success (-1) or failure (0). Transfer on I2C bus: 31 bytes.
int
Khepera3_TCP::infrared_proximity()
{
  sprintf( _msg_buffer, "$GET_PROXI\n");
  //printf( "Will send %s\n", _msg_buffer );
  //nmea_parser_process_data( this, &_client_snd_nmea_parser, _msg_buffer, strlen(_msg_buffer) );
  //print_nmea( &(_client_snd_nmea_parser.message) );
  return process_send_wait_process( _msg_buffer );
}
// ==========================================================================
//! Reads the last ambient infrared values and fills them into the khepera3 structure. The return value indicates success (-1) or failure (0). Transfer on I2C bus: 31 bytes.
int
Khepera3_TCP::infrared_ambient()
{
  sprintf( _msg_buffer, "$GET_AMB\n");
  //printf( "Will send %s\n", _msg_buffer );
  //nmea_parser_process_data( this, &_client_snd_nmea_parser, _msg_buffer, strlen(_msg_buffer) );
  //print_nmea( &(_client_snd_nmea_parser.message) );
  return process_send_wait_process( _msg_buffer );
}
// ==========================================================================
//! Puts both motors in normal (control) mode.
int
Khepera3_TCP::drive_start()
{
  sprintf( _msg_buffer, "$START\n");
  return process_send_wait_process( _msg_buffer );
}
// ==========================================================================
//! Stops both motors immediately
int
Khepera3_TCP::drive_stop()
{
  sprintf( _msg_buffer, "$STOP\n");
  return process_send_wait_process( _msg_buffer );
}
// ==========================================================================
//! Puts both motors in idle mode. 
int
Khepera3_TCP::drive_idle()
{
  sprintf( _msg_buffer, "$IDLE\n");
  return process_send_wait_process( _msg_buffer );
}
// ==========================================================================
//! Reads the status register and updates the corresponding field in the motor structure. Transfer on I2C bus: 4 bytes.
// ADAPTED so as to set the status of both motors.
int
Khepera3_TCP::drive_get_status()
{
  //sprintf( _msg_buffer, "$MOTOR_STATUS\n" );
  //process_send_wait_process( _msg_buffer );
  return -1;
}
// ==========================================================================
//! Reads the error register and updates the corresponding field in the motor structure. Transfer on I2C bus: 4 bytes.
int
Khepera3_TCP::drive_get_error()
{
  //sprintf( _msg_buffer, "$MOTOR_STATUS\n" );
  //process_send_wait_process( _msg_buffer );
  return -1;
}
// ==========================================================================
//! Reads the current speed and updates the corresponding value the motor structures. Transfer on I2C bus: 32 bytes.
void
Khepera3_TCP::drive_get_current_speed()
{
  sprintf( _msg_buffer, "$GET_SPEED\n");
  process_send_wait_process( _msg_buffer );
}
// ==========================================================================
//! Sets the motor speeds. Transfer on I2C bus: 24 - 30 bytes.
void
Khepera3_TCP::drive_set_speed(int speed_left, int speed_right)
{
  sprintf( _msg_buffer, "$SET_SPEED,%d,%d\n", speed_left, speed_right);
  process_send_wait_process( _msg_buffer );
}
// ==========================================================================
//! Sets the motor speeds and uses the acceleration profile to reach that speed. Transfer on I2C bus: 24 - 30 bytes.
void
Khepera3_TCP::drive_set_speed_using_profile(int speed_left, int speed_right)
{
  sprintf( _msg_buffer, "$SET_SPEED_CONTROL,%d,%d\n", speed_left, speed_right);
  process_send_wait_process( _msg_buffer );
}
// ==========================================================================
//! Reads the current position and updates the corresponding value the motor structures. Transfer on I2C bus: 32 bytes.
void 
Khepera3_TCP::drive_get_current_position()
{
  sprintf( _msg_buffer, "$GET_POS\n");
  process_send_wait_process( _msg_buffer );
}
// ==========================================================================
//! Sets the current encoder position. Transfer on I2C bus: 24 bytes.
void
Khepera3_TCP::drive_set_current_position(int position_left, int position_right)
{
  sprintf( _msg_buffer, "$SET_POS,%d,%d\n", position_left, position_right);
  process_send_wait_process( _msg_buffer );
}
// ==========================================================================
//! Goes to a specific motor position. Transfer on I2C bus: 24 - 30 bytes.
void
Khepera3_TCP::drive_goto_position(int position_left, int position_right)
{
  sprintf( _msg_buffer, "$GOTO_POS,%d,%d\n", position_left, position_right);
  process_send_wait_process( _msg_buffer );
}
// ==========================================================================
//! Goes to a specific motor position using the acceleration profile. Transfer on I2C bus: 24 - 30 bytes.
void
Khepera3_TCP::drive_goto_position_using_profile(int position_left, int position_right)
{
  sprintf( _msg_buffer, "$GOTO_POS_CONTROL,%d,%d\n", position_left, position_right);
  process_send_wait_process( _msg_buffer );
}
// ==========================================================================
