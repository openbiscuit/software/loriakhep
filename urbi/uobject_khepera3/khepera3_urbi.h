#ifndef __KHEPERA3_URBI_H
#define __KHEPERA3_URBI_H

#include <urbi/uobject.hh>
extern "C"
 {
 #include "khepera3.h"
 }

class Khepera3 : public urbi::UObject
{
 public:
  /**
   * @name Creation/Destruction
   */
  /**
   * At creation, a "Khepera" will be created.
   * => a TCP/IP connction with the khepera.
   * Then 'init()' is binded into URBI.
   */
  Khepera3( const std::string &name );
  virtual ~Khepera3();
  //@}

  /**
   * Init the UObject and a connection to the khepera3.
   *
   */
  int init();
  /**
   * Print out information on the module.
   */
  int help(void);
  /**
   * Return a string wrapping of the pointer to the UOBject.
   */
  std::string get_ptr();

 private:
  /**
   * Called when load is changed.
   */
  //int _on_load_changed(urbi::UVar& v);
  

};
#endif //__KHEPERA3_URBI_H
