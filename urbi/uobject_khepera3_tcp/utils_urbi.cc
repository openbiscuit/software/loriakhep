#include<sstream>
#include "utils_urbi.h"

// ==========================================================================
std::string wrap_pointer(void* p)
{
   std::stringstream res;
   res << p;
   return res.str();
}
// ==========================================================================
void* unwrap_pointer(const std::string& s)
{
   std::stringstream i(s);
   void* res;
   i >> res;
   return res;
}
// ==========================================================================
