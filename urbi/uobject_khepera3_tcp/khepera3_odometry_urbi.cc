#include "khepera3_odometry_urbi.h"
#include "khepera3_urbi.h"
#include "utils_urbi.h"

// ==========================================================================
OdometryTrack::OdometryTrack( const std::string &name )
  : urbi::UObject(name)
{
  _parent = NULL;
  // intialise object variables

  UBindFunction(OdometryTrack, init);
  UBindFunction(OdometryTrack, help);
}
OdometryTrack::~OdometryTrack()
{
  if( _parent != NULL ) {
    _parent->members.remove( this );
  }
}
// ==========================================================================
int
OdometryTrack::help()
{
  std::cout << "** OdometryTrack: get values of the Odometry Track Value \n";
  std::cout << "__.init( <str> robot object)\n";
  std::cout << "__.val [1,2,3]: value of x,y,teta\n";
  std::cout << "__.restart(): reset odom at the current position\n";
  std::cout << "__.load [R/W]: are sensor read (1) or not (0)\n"; 
  
  return 0; // URBI OK */
}
// ==========================================================================
int
OdometryTrack::init(std::string khepera3_ptr, ufloat freq_update )
{
  _parent = (Khepera3 *) unwrap_pointer( khepera3_ptr );
  _parent->members.push_front( this );
  _khepera3 = (Khepera3_TCP *) _parent->_khepera3;

  // Bind function for restarting odometry
  UBindFunction(OdometryTrack, restart);

  // for each of the 3 values tracked by odometry, create a variable
  for( int i = 0; i < 3; ++i ) {
    urbi::UValue *va = new urbi::UValue();
    *va = 0;
    _odometry_track_vec.array.push_back(va);
  }
 
  // and now the shared UVar made of all this value
  _odometry_track_val.init(__name, "val");  
  _odometry_track_val = _odometry_track_vec;

  // need to know when 'loaded' is changed
  UNotifyChange( load, &OdometryTrack::on_load_changed );

  // At each beginning of URBIcycle, call 'update_ir_proxi()'
  USetTimer( freq_update, &OdometryTrack::_update_sensor_val);

  // start odomoetry tracking
  _khepera3->odometry_load=1;
  return 0; // URBI OK */
}
// ==========================================================================
int
OdometryTrack::restart()
{
  // odometry_track_start(&ot);
  _khepera3->restart_odometry();
  return 0; // URBI OK */
}
// ==========================================================================
int
OdometryTrack::_update_sensor_val(void)
{
  //if( (ufloat) _ir_proxi_load == 1 ) {
  // 'load' is a slotName of every UObject

  if( (ufloat) load == 1 ) {
    // odometry_track_step(&ot); // DONE by khepera3_status
    // _khepera3->get_odometry();// DONE by khepera3_status
    // update internal value
    _odometry_track_vec[0] =(urbi::UValue) (_khepera3->khepera3.odometry.x);
    _odometry_track_vec[1]=(urbi::UValue)  (_khepera3->khepera3.odometry.y);
    _odometry_track_vec[2] =(urbi::UValue)  (_khepera3->khepera3.odometry.theta);
    
    _odometry_track_val = _odometry_track_vec;
  	
  }
  return 0;  // URBI OK */
}
// ==========================================================================
int
OdometryTrack::on_load_changed(urbi::UVar& v)
{
  // notify khepera3_status that odometry should be read or not.
  if((ufloat) load ==1)
    _khepera3->odometry_load=1;
  else
    _khepera3->odometry_load = 0;
  
  return 0;  // URBI OK */
}
// ==========================================================================
UStart(OdometryTrack);
// ==========================================================================

