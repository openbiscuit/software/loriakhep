#include "khepera3_urbi.h"
#include "utils_urbi.h"

// ==========================================================================
Khepera3::Khepera3( const std::string &name )
  : urbi::UObject(name)
{
  _khepera3 = NULL;
  
  // intialise object variables

  UBindFunction(Khepera3, init);
  UBindFunction(Khepera3, help);
  UBindFunction(Khepera3, get_ptr);
}
Khepera3::~Khepera3()
{
  if( _khepera3 != NULL ) {
    delete _khepera3;
  }
}
// ==========================================================================
int
Khepera3::help()
{
  std::cout << "** Khepera3 : core module for khepera3 driver.\n";
  std::cout << "__.init( <str> ip address, <str> port )\n";
  std::cout << "__.get_ptr() => string wrapping ptr to Khepera3 UObject\n";

  return 0; // URBI OK */
}
// ==========================================================================
int
Khepera3::init(std::string address,
	       int port)
{
  _khepera3 = new Khepera3_TCP();
  
  // init Khepera structures...
  _khepera3->init();
  
  // Monitor the status of the load variable
  //UNotifyChange( load, &Khepera3::_on_load_changed );

  // init TCP connection with Khepera
  if( _khepera3->tcp_init( (char *) address.c_str(), port ) < 0 ) {
    return 1;
  }

  return 0; // URBI OK */
}
// ==========================================================================
std::string
Khepera3::get_ptr()
{
  return wrap_pointer( this );
}
// ==========================================================================
//int
//Khepera3::_on_load_changed(urbi::UVar& v)
//{
  // std::cout << __name << ":_on_load_changed with " << members.size() << " children\n";
//   // child object
//   for (urbi::UObjectList::iterator it = members.begin();
//        it != members.end();
//        it++) {
//     std::cout << __name << " found child " << (*it)->__name << " of class " << (*it)->classname << "\n";
//     (*it)->load = (urbi::UVar) v;
//   }
//  return 0; // URBI OK */
//}
// ==========================================================================
UStart(Khepera3);
// ==========================================================================
  
