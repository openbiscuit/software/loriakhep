#ifndef __KHEPERA3_US_URBI_H
#define __KHEPERA3_US_URBI_H

#include <urbi/uobject.hh>
#include "khepera3_urbi.h"

#define NB_MAX_ECHO 10

class UltraSound : public urbi::UObject
{
  public:
  /**
   * @name Creation/Destruction
   */
  /**
   * Then 'init()' is binded into URBI.
   */
  UltraSound( const std::string &name );
  virtual ~UltraSound();
  //@}

   /**
   * Init the UObject thanks to a core Khepera3 object.
   *
   * @param khepera3_ptr string with a pointer to the internal khepera3.
   * @param freq_update frequency at which sensors are updated.
   */
  int init( std::string khepera3_ptr, ufloat freq_update=0 );
  /**
   * Print out information on the module.
   */
  int help(void);
  /**
   * Return a string wrapping of the pointer to the UOBject.
   */
  std::string get_ptr();
  
  /**
   * Enable a given sensor and update _us_enabled. 
   */
  int enable_sensor( int us_index );
  /**
   * Disable a given sensor and update _us_enabled. 
   */
  int disable_sensor( int us_index );

  /** The parent Khepera3 UObject */
  Khepera3 *_parent;
  /** The "real" Khepera3 */
  Khepera3_TCP *_khepera3;

 protected:
  /**
   * Sets the maximum number of echos received by the ultrasound sensors.
   * The return value indicates success (-1) or failure (0).
   * Transfer on I2C bus: 8 bytes.
   */
  int _on_max_echo_number_changed();

  int on_load_changed(urbi::UVar& v);

  /**
   * Update the sensor values.
   */
  int _update_sensor_values(void);
  /** Frequence of update */
  ufloat _freq_update;

  /** Mask of active sensor (see khepera3toolbox) */
  unsigned char _us_mask;
  /** For each sensor, 0 means disabled, 1 means enabled. */
  int _us_enabled[cKhepera3SensorsUltrasound_Count];
  
  bool _timer;
  void update_sensor();

 public:
  /** Max number of echo */
  urbi::UVar _max_echo_number;
  /**
   * @name Set of sensor values
   */
  urbi::UValue _echo_count[cKhepera3SensorsUltrasound_Count];
  urbi::UList _distance[cKhepera3SensorsUltrasound_Count];
  urbi::UList _amplitude[cKhepera3SensorsUltrasound_Count];
  urbi::UList _timestamp[cKhepera3SensorsUltrasound_Count];
  //@}
  
};
class USSensor : public urbi::UObject
{
  public:
  /**
   * @name Creation/Destruction
   */
  /**
   * Then 'init()' is binded into URBI.
   */
  USSensor( const std::string &name );
  virtual ~USSensor();
  //@}

  /**
   * Init the UObject thanks to a core Khepera3 object.
   *
   * @param drive_ptr string with a pointer to a Drive parent
   */
  int init( std::string us_ptr, int index );
  /**
   * Print out information on the module.
   */
  int help(void);

  /** The parent Ultrasound UObject */
  UltraSound *_parent;

 protected:
  /**
   * @name Accessing variables
   */
  int _on_echo_count_accessed(void);
  int _on_distance_accessed(void);
  int _on_amplitude_accessed(void);
  int _on_timestamp_accessed(void);
  //@}
  /**
   * Called when load is changed.
   */
  int _on_load_changed();

  /**
   * @name Set of sensor values
   */
  urbi::UVar _echo_count;
  urbi::UVar _distance;
  urbi::UVar _amplitude;
  urbi::UVar _timestamp;
  //@}

  /** Index number */
  int _index;
  
};
#endif //__KHEPERA3_US_URBI_H
