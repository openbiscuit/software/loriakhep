#ifndef __KHEPERA3_URBI_H
#define __KHEPERA3_URBI_H

#include <urbi/uobject.hh>
#include "khepera3_tcp.h"

class Khepera3 : public urbi::UObject
{
 public:
  /**
   * @name Creation/Destruction
   */
  /**
   * At creation, a "Khepera" will be created.
   * => a TCP/IP connction with the khepera.
   * Then 'init()' is binded into URBI.
   */
  Khepera3( const std::string &name );
  virtual ~Khepera3();
  //@}

  /**
   * Init the UObject and a connection to the khepera3.
   *
   * @param address IP address of the Khepera
   * @param port port on which the Khepera is listening
   */
  int init( std::string address = std::string("192.168.0.202"),
	    int port = 3000);
  /**
   * Print out information on the module.
   */
  int help(void);
  /**
   * Return a string wrapping of the pointer to the UOBject.
   */
  std::string get_ptr();

  /**
   * The underlying Khepera3 or Khepera3_TCP
   */
  Khepera3_TCP *_khepera3;

 private:
  /**
   * Called when load is changed.
   */
  //int _on_load_changed(urbi::UVar& v);
  

};
#endif //__KHEPERA3_URBI_H
