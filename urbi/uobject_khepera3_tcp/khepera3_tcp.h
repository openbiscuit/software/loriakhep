#ifndef __KHEPERA3_TCP_H
#define __KHEPERA3_TCP_H

#include"khepera3_tcp_headers.h"
#include"nmea.h"
#include <pthread.h>
using namespace std;


class Khepera3_TCP
{
 public:
  /** Structure for holding data */
  struct sKhepera3_TCP khepera3;
  
 public:
  /** Global init */
  void init();

  void read_loop(void *pdata);

  /** Initialize connection */
  int tcp_init(char *ip_address, int port);
  
    /** Read IR proxi */
    int infrared_proximity();

    int infrared_proximity_lock(){return pthread_mutex_lock(&_infrared_proximity_mutex);}
  
    int infrared_proximity_unlock(){return pthread_mutex_unlock(&_infrared_proximity_mutex);}
    /** Read IR ambient */
    int infrared_ambient();

    int infrared_ambient_lock(){return pthread_mutex_lock(&_infrared_ambiant_mutex);}
    int infrared_ambient_unlock(){return pthread_mutex_unlock(&_infrared_ambiant_mutex);}
  /**
   * @name motor
   */
    int drive_start();
    int drive_stop();
    int drive_idle();

    void drive_set_speed(int speed_left, int speed_right);
    void drive_set_speed_using_profile(int speed_left, int speed_right);



    void drive_get_current_position();
    /** set the position of the wheel BUT WITHOUT MOVING the robot (see goto)*/
    void drive_set_current_position(int position_left, int position_right);
    /** goto some position */
    void drive_goto_position(int position_left, int position_right);
    void drive_goto_position_using_profile(int position_left, int position_right);


    int drive_lock(){return pthread_mutex_lock(&_drive_mutex);}
    int drive_unlock(){return pthread_mutex_unlock(&_drive_mutex);}

  //@
  
  /**
   * @name ultrasound
   */
    int ultrasound_set_max_echo_number(int nb_echo);
    int ultrasound(int us_index);
    int ultrasound_start(int us_index);
    int ultrasound_stop(int us_index);
    int allultrasound();
    int ultrasound_lock(){return pthread_mutex_lock(&_ultrasound_mutex);}
    int ultrasound_unlock(){return pthread_mutex_unlock(&_ultrasound_mutex);}
    int restart_odometry();

    int battery_lock(){return pthread_mutex_lock(&_battery_mutex);}
    int battery_unlock(){return pthread_mutex_unlock(&_battery_mutex);}
    int odometry_lock(){return pthread_mutex_lock(&_odometry_mutex);}
    int odometry_unlock(){return pthread_mutex_unlock(&_odometry_mutex);}
    int send_lock(){/*printf("lock\n");*/return pthread_mutex_lock(&_send_mutex);}
    int send_unlock(){/*printf("unlock\n");*/return pthread_mutex_unlock(&_send_mutex);}
  //@
    int ir_proxi_load;
    int drive_load;
    int ir_ambient_load;
    int ultrasound_load;
    int odometry_load;
    int battery_load;
    private: // --------- NMEA 
  
      struct sNMEAParser _client_snd_nmea_parser;
      struct sNMEAParser _client_rcv_nmea_parser;
  //@
  /**
       * @name battery
   */

      pthread_mutex_t _infrared_proximity_mutex;
      pthread_mutex_t _infrared_ambiant_mutex;
      pthread_mutex_t _drive_mutex;
      pthread_mutex_t _ultrasound_mutex;
      pthread_mutex_t _odometry_mutex;
      pthread_mutex_t _battery_mutex;
      pthread_mutex_t _send_mutex;
      void get_all();
      int battery_voltage();
      int battery_current();
      int battery_current_average();
      int battery_capacity_remaining_absolute();
      int battery_capacity_remaining_relative();
      int battery_temperature();
      int get_odometry();
      int drive_get_status();
      int drive_get_error();

      void drive_get_current_speed();
      int _ultrasound_set_max_echo_number(int nb_echo);
      int _ultrasound_start(int us_index);
      int _ultrasound_stop(int us_index);
      void _drive_set_speed(int speed_left, int speed_right);
      void _drive_set_speed_using_profile(int speed_left, int speed_right);
      void _drive_set_current_position(int position_left, int position_right);
      void _drive_goto_position(int position_left, int position_right);
      void _drive_goto_position_using_profile(int position_left, int position_right);
      int _drive_start();
      int _drive_stop();
      int _drive_idle();
      int _drive_start_flag;
      int _drive_stop_flag;
      int _drive_idle_flag;
      int _nb_echo;
      int _us_index;
      int _ultrasound_start_flag[5];
      int _ultrasound_stop_flag[5];
      int _ultrasound_set_max_echo_number_flag;
      int _drive_goto_position_flag;
      int _drive_goto_position_using_profile_flag;
      int _position_left;
      int _position_right;
      int _drive_set_speed_flag;
      int _drive_set_speed_using_profile_flag;
      int _drive_set_current_position_flag;
      int _speed_left;
      int _speed_right;
      int _ultrasound_flag[5];
      int _connect_filehandle;
      char _msg_buffer[256];
      pthread_t thread_reader;
      /** Send a command and hope for an answer to process it */
      int process_send_wait_process(char * msg_buffer);

      /** Open TCO connection */
      int tcp_connect( const char *address, int port, int *connect_filehandle);
      /** Print NMEA structure */
      void print_nmea(struct sNMEAMessage *msg); 
      /** Fill up IR data with answer to send message */
      void parse_ir_proxi(struct sNMEAMessage *msg);
      /** Fill up IR data with answer to send message */
      void parse_ir_amb(struct sNMEAMessage *msg);
      void parse_all(struct sNMEAMessage *msg);
      /** Fill up US data with answer to send message */
      void parse_us(struct sNMEAMessage* msg);
      /** Fill up BATTERY data with answer to send message */
      void parse_battery(struct sNMEAMessage* msg);
      /** Fill up motor data with answer to send message */
      void parse_motor_speed(struct sNMEAMessage *msg);
      /** Fill up motor data with answer to send message */
      void parse_motor_position(struct sNMEAMessage *msg);
      void parse_odometry(struct sNMEAMessage *msg);
      
}; // Khepera3_TCP

/** how to send message */
void client_nmea_send_hook(int connexion, const char *buffer, int len);
/** If send message has unknown character, what to do */
void client_nmea_unknown_character(char c);

//! Initializes this module.
//extern int khepera3_tcp_init(char *ip_address, int port);
//extern void khepera3_init();

  //! Reads the last infrared proximity values and fills them into the khepera3 structure. The return value indicates success (-1) or failure (0). Transfer on I2C bus: 31 bytes.
//extern int khepera3_infrared_proximity();
#endif // __KHEPERA3_TCP_H
