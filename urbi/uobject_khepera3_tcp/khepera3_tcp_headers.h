#ifndef __KHEPERA3_TCP_HEADERS_H
#define __KHEPERA3_TCP_HEADERS_H


//! Infrared sensors
enum eKhepera3SensorsInfrared {
	cKhepera3SensorsInfrared_BackLeft = 0,
	cKhepera3SensorsInfrared_Left = 1,
	cKhepera3SensorsInfrared_FrontSideLeft = 2,
	cKhepera3SensorsInfrared_FrontLeft = 3,
	cKhepera3SensorsInfrared_FrontRight = 4,
	cKhepera3SensorsInfrared_FrontSideRight = 5,
	cKhepera3SensorsInfrared_Right = 6,
	cKhepera3SensorsInfrared_BackRight = 7,
	cKhepera3SensorsInfrared_Back = 8,
	cKhepera3SensorsInfrared_FloorRight = 9,
	cKhepera3SensorsInfrared_FloorLeft = 10,

	cKhepera3SensorsInfrared_Begin = 0,
	cKhepera3SensorsInfrared_End = 10,
	cKhepera3SensorsInfrared_Count = 11,
	cKhepera3SensorsInfrared_RingBegin = 0,
	cKhepera3SensorsInfrared_RingEnd = 9,
	cKhepera3SensorsInfrared_RingCount = 9,
	cKhepera3SensorsInfrared_FloorBegin = 9,
	cKhepera3SensorsInfrared_FloorEnd = 10,
	cKhepera3SensorsInfrared_FloorCount = 2
};

//! Ultrasound sensors
enum eKhepera3SensorsUltrasound {
	cKhepera3SensorsUltrasound_Left = 0,
	cKhepera3SensorsUltrasound_FrontLeft = 1,
	cKhepera3SensorsUltrasound_Front = 2,
	cKhepera3SensorsUltrasound_FrontRight = 3,
	cKhepera3SensorsUltrasound_Right = 4,

	cKhepera3SensorsUltrasound_Begin = 0,
	cKhepera3SensorsUltrasound_End = 5,
	cKhepera3SensorsUltrasound_Count = 5
};

//! Ultrasound sensor bit masks
enum eKhepera3SensorsUltrasoundBit {
	cKhepera3SensorsUltrasoundBit_Left = 1,
	cKhepera3SensorsUltrasoundBit_FrontLeft = 2,
	cKhepera3SensorsUltrasoundBit_Front = 4,
	cKhepera3SensorsUltrasoundBit_FrontRight = 8,
	cKhepera3SensorsUltrasoundBit_Right = 16,

	cKhepera3SensorsUltrasoundBit_None = 0,
	cKhepera3SensorsUltrasoundBit_All = 31
};


//! One measurement of all infrared sensors
struct sKhepera3SensorsInfrared {
  int sensor[11];		//!< Sensor values
  int timestamp;		//!< Timestamp at which the values were read
};

struct sKhepera3Odometry {
  double x;		
  double y;		
  double theta;
};

//! One measurement of one ultrasound sensor
struct sKhepera3SensorsUltrasoundSensor {
	int echos_count;		//!< Number of received echos
	int distance[10];		//!< Received distance in increments (cm at the moment, multiply with distance_per_increment to get meters)
	int amplitude[10];		//!< Received amplitude
	int timestamp[10];		//!< Timestamp at which the echo was received
};

//! The ultrasound sensors
struct sKhepera3SensorsUltrasound {
	struct sKhepera3SensorsUltrasoundSensor sensor[5];	//!< The 5 ultrasound sensors
	float distance_per_increment;						//!< Conversion factor to convert from distance values to meters
};
struct sKhepera3SensorsBattery {
	int voltage;	
	int current;	
	int current_average;
	int capacity_remaining_absolute;	
	int capacity_remaining_relative;
	int temperature;
};

//! Motor status flags
enum eKhepera3MotorStatusFlags {
	cKhepera3MotorStatusFlags_Moving = (1 << 0),					//!< Movement detected
	cKhepera3MotorStatusFlags_Direction = (1 << 1),					//!< Direction (0 = negative, 1 = positive)
	cKhepera3MotorStatusFlags_OnSetpoint = (1 << 2),				//!< On setpoint
	cKhepera3MotorStatusFlags_NearSetpoint = (1 << 3),				//!< Near setpoint
	cKhepera3MotorStatusFlags_CommandSaturated = (1 << 4),			//!< Command saturated
	cKhepera3MotorStatusFlags_AntiResetWindup = (1 << 5),			//!< Antireset windup active
	cKhepera3MotorStatusFlags_SoftwareCurrentControl = (1 << 6),	//!< Software current control active
	cKhepera3MotorStatusFlags_SoftStop = (1 << 7),					//!< Softstop active
};

//! Motor error flags
enum eKhepera3MotorErrorFlags {
	cKhepera3MotorErrorFlags_SampleTimeTooSmall = (1 << 0),	//!< Sample time too small
	cKhepera3MotorErrorFlags_WatchdogOverflow = (1 << 1),	//!< Watchdog timer overflow
	cKhepera3MotorErrorFlags_BrownOut = (1 << 2),			//!< Brown-out
	cKhepera3MotorErrorFlags_SoftwareStop = (1 << 3),		//!< Software stopped motor (if softstop enabled)
	cKhepera3MotorErrorFlags_MotorBlocked = (1 << 4),		//!< Motor blocked (if motor blocking enabled)
	cKhepera3MotorErrorFlags_PositionOutOfRange = (1 << 5),	//!< Position out of range
	cKhepera3MotorErrorFlags_SpeedOutOfRange = (1 << 6),	//!< Speed out of range
	cKhepera3MotorErrorFlags_TorqueOutOfRange = (1 << 7),	//!< Torque out of range
};

//! Motor control types
enum eKhepera3MotorControlType {
	cKhepera3MotorControlType_Unknown				= -1,	//!< Unknown control type
	cKhepera3MotorControlType_OpenLoop				= 0,	//!< Open loop
	cKhepera3MotorControlType_Position				= 1,	//!< Position
	cKhepera3MotorControlType_PositionUsingProfile	= 2,	//!< Position using the acceleration profile
	cKhepera3MotorControlType_Speed					= 3,	//!< Speed
	cKhepera3MotorControlType_SpeedUsingProfile		= 4,	//!< Speed using the acceleration profile
	cKhepera3MotorControlType_Torque				= 5,	//!< Torque (= current)
	//cKhepera3MotorControlType_ZeroFriction			= 6		//!< Zero friction (not implemented)
};

//! One motor
struct sKhepera3Motor {
	int i2c_address;								//!< I2C bus address
	int direction;									//!< -1 to invert the direction, otherwise +1
	unsigned int firmware_version;					//!< Firmware version of the motor controller (updated by khepera3_motor_firmware_version, example: 104 = 0x68 => version 6, revision 8)
	int current_speed;								//!< Current speed (updated by khepera3_motor_get_current_speed)
	int current_position;							//!< Current position (updated by khepera3_motor_get_current_position)
	int current_torque;								//!< Current torque (updated by khepera3_motor_get_current_torque)
	enum eKhepera3MotorStatusFlags status;			//!< Status (updated by khepera3_motor_get_status)
	enum eKhepera3MotorErrorFlags error;			//!< Error (updated by khepera3_motor_get_error)
	enum eKhepera3MotorControlType control_type;	//!< (private) Current control type (updated and used by all methods setting/getting the control type)
};

//! The TCP Khepera 3 robot
struct sKhepera3_TCP {
  struct sKhepera3SensorsInfrared infrared_proximity;		//!< Infrared sensors in proximity mode
  struct sKhepera3SensorsInfrared infrared_ambient;		//!< Infrared sensors in ambient light mode
  struct sKhepera3SensorsUltrasound ultrasound;			//!< Ultrasound sensors
  struct sKhepera3SensorsBattery battery;
  struct sKhepera3Motor motor_left;						//!< Left motor
  struct sKhepera3Motor motor_right;	
  struct sKhepera3Odometry odometry;				
};

#endif // __KHEPERA3_TCP_HEADERS_H
