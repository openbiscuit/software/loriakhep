#ifndef __UTILS_URBI_H
#define __UTILS_URBI_H

#include <strings.h>

/**
 * Wrap a pointer to a string for passing in URBI.
 */
std::string wrap_pointer(void* p);
/**
 * Unwrap a string to a (void *) pointer for passing in URBI.
 */
void* unwrap_pointer(const std::string& s);

#endif // __UTILS_URBI_H
