#ifndef __KHEPERA3_ODOMETRY_URBI_H
#define __KHEPERA3_ODOMETRY_URBI_H

#include <urbi/uobject.hh>
#include "khepera3_urbi.h"

#include "utils_urbi.h"

class OdometryTrack : public urbi::UObject
{
 public:
  /**
   * @name Creation/Destruction
   */
  /**
   * Then 'init()' is binded into URBI.
   */
  OdometryTrack( const std::string &name );
  virtual ~OdometryTrack();
  //@}

  /**
   * Init the UObject thanks to a core Khepera3 object.
   *
   * @param khepera3_ptr string with a pointer to a Khepera3 UObject.
   */
  int init( std::string khepera3_ptr, ufloat freq_update );
  /**
   * Print out information on the module.
   */
  int help(void);
  int restart(void);


 protected:
  /**
   * Update OdometryTrack values at each URBI Cycle.
   */
  int _update_sensor_val(void);
  /**
   * Track 'load' changes. 
   */
  int on_load_changed(urbi::UVar& v);

  /** A list of values for ir_proxy */
  urbi::UList _odometry_track_vec;
  /** A var for the ir_proxi distance */
  urbi::UVar _odometry_track_val;

  /** The parent Khepera3 UObject */
  Khepera3 *_parent;
  Khepera3_TCP *_khepera3;
};


#endif //__KHEPERA3_ODOMETRY_URBI_H
