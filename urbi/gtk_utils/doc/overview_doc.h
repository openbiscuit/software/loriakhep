/**
\mainpage Explications sur le "package" GADGET.

\section intro Introduction

Ce package offre des objets URBI (des uobject) qui allient URBI et GTK.

Au 09/03/2010, trois UObject sont ainsi disponibles:
- RetineURBI: en utilisant une webcam, met à disposition des "taux de rouge" en différentes positions de l'image. On peut visualiser l'image et les histogrammes de teintes de couleurs sur l'écran.
- OverlayURBI: affiche une image issue d'une caméra ou d'un fichier dans une fenêtre. Peut éventuellement "dessiner" sur cette image (comme le montre RetineURBI).
- JoystickURBI: affiche un joystick virtuel que l'on peut "commander" à la souris et qui fourni une position en 'x' et en 'y' dans URBI.

Ces objets s'appuient parfois sur des 'gadgets' utilisant GTK+Cairo (les noms sont de la forme 'TrucCAIRO') et quelques utilitaires dans le répertoire 'utils'.

\section compil Un mot sur la compilation

Pour compiler, il faut être dans le répertoire racine (gtk_utils) et faire:
\verbatim gtkutils> make \endverbatim
Ceci compile, de manière incrémentale, tous les fichiers sources pour en faire les exécutables ou les bibliothèques nécessaires (\c .so, .a).

\verbatim gtkutils> make doc \endverbatim compile la présente documentation qui se trouve ensuite dans \c gtk_utils/doc/html .

\verbatim gtkutils> make x \endverbatim lance URBI avec les modules nécessaires à faire fonctionner RetineURBI.

Ces Makefile sont inspirés des réflexions de Peter Miller sur les make récursifs ("Recursive Make Considered Harmful", http://www.canb.auug.org.au/~millerp) et de Emile van Bergen (http://www.xs4all.nl/~evbergen/nonrecursive-make.html).

En gros:
- \c gtk_utils/Makefile décrit les commandes utiles
- \c gtk_utils/rules.mk décrit les 'targets' de haut niveau
- dans les sous-répertoires, les \c rules.mk décrivent les 'target' et 'règles' locales.
- dans \c gtk_utils/build, on trouve les outils de compilations adaptés qui facilitent la recherche de dépendance entre les fichiers source.

\todo Documenter un peu mieux le répertoire "gadget" 
*/
