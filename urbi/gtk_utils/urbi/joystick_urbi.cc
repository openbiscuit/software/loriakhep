/**
 * Virtual Joystick for URBI.
 *
 * QUESTION: qui est responsable du 'delete UVar' qu'il faudrait faire?
 */

#include "joystick_urbi.h"
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include "joystick_gtkmm.h"
#include <functional>

// ==========================================================================
//Gtk::Main* JoystickURBI::_kit_gtk = NULL;
// ==========================================================================
JoystickURBI::JoystickURBI(const std::string &name)
  :urbi::UObject(name)
{
  _gtk_urbi == NULL;
  _gtk = new GtkURBI();

  _setup_complete = false;
  _var_x_axis = NULL;
  _var_y_axis = NULL;
  _joy = NULL;
  _win = NULL;

  UBindFunction(JoystickURBI, init);
  UBindFunction(JoystickURBI, hide);
  UBindFunction(JoystickURBI, show);
  
  
}
JoystickURBI::~JoystickURBI() //---------------------------------------------
{
  //std::cout << "Deleting"  <<"\n";
  if( _win != NULL ) {
    _win->hide();
    delete _win;
  }
  if( _joy != NULL ) {
    delete _joy;
  }
  if( _gtk_urbi != NULL) {
    delete _gtk_urbi;
  }
}
// ==========================================================================
int
JoystickURBI::init(const std::string &name)
{
  _name_joy = name;
  create_joystick();
  //boost::thread joystick_thread(&JoystickURBI::create_joystick);
  boost::thread joystick_thread(boost::bind(&JoystickURBI::run_joystick,this));
  //boost::thread joystick_thread(boost::bind(&run_window, win));
  
  USetTimer(1.0, &JoystickURBI::frequent);

  return 0;
}
// ==========================================================================
int
JoystickURBI::frequent(void)
{
  
  // create and bind variables if needed
  if( _setup_complete == false ) {
    //std::cout << "_setup_complete is false" << "\n";
    // for each dimension
    _var_x_axis = new urbi::UVar();
    //std::cout << "var_x_axis = new urbi::UVar();" << "\n";
    _var_x_axis->init(__name, "x_axis");
    //std::cout << "var_x_axis->init(" << __name<<", \"x_axis\")" << "\n";
    _var_y_axis = new urbi::UVar();
    //std::cout << "var_y_axis = new urbi::UVar();" << "\n";
    _var_y_axis->init(__name, "y_axis");
    //std::cout << "var_x_axis->init(" << __name<<", \"y_axis\")" << "\n";

    _setup_complete = true;
    //std::cout << "_setup_complete = true" << "\n";
  }

  // update val
  *_var_x_axis = _joy->get_x();
  //std::cout << "var_x_axis = get_x()" << "\n";
  *_var_y_axis = _joy->get_y();
  //std::cout << "var_y_axis = get_y()" << "\n";

  return 0;
}
// ==========================================================================
void
JoystickURBI::hide()
{
  _win->hide();
}
void
JoystickURBI::show()
{
  _win->show();
  boost::thread joystick_thread(boost::bind(&JoystickURBI::run_joystick,this));
}
// ==========================================================================
void
JoystickURBI::create_joystick()
{
  
  _win = new Gtk::Window();
  _win->set_title( _name_joy );
  _joy = new JoystickGTKMM(_name_joy);
  _joy->set_recenter( true );
  _win->add( *_joy );
  _win->show_all_children();
 }
void
JoystickURBI::run_joystick()
{
  Gtk::Main::run(*_win);
}
// ==========================================================================  
UStart(JoystickURBI);
// ==========================================================================
