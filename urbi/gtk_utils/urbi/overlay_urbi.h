#ifndef __OVERLAY_URBI_H
#define __OVERLAY_URBI_H

#include <urbi/uobject.hh>
#include "overlay_viewer.h"
#include "videosource.h"
#include "gtk_urbi.h"

//#include <gtkmm.h>

/**
 * Allow the creation of GTK based overlayed pixture in a pixbuff.
 *
 */
class OverlayURBI : public urbi::UObject
{
 public:
  /**
   * @name Creation/Destruction
   */
  /**
   * At creation, a Gtk::Main will be created if not existing.
   * Then 'init' is binded into URBI.
   */
  OverlayURBI(const std::string &name);
  /**
   * Destruction, that tries to clear up every Gtk::Widget.
   */
  ~OverlayURBI();
  //@}

  /**
   * Create a OverlayViewer of the given name and shows it in a independant thread.
   */
  int init(const std::string &name);

 protected:
  /**
   * Create the OverlayViewer and open a stream.
   */
  void test_stream(ufloat freq_update);
  /**
   * Update the image in the Overlay.
   */
  int _update_img_stream(void);

  /**
   * Create the OverlayViewer and its Gtk::Window, with a single jpg file.
   */
  void test_still();
  /**
   * Called in a independant thread, this shows up the OverlayViewer.
   */
  void run_overlay();

 protected:
  /** A OverlayViewer. */
  OverlayViewer _view;
  /** Name displayed in the OverlayViewer. */
  std::string _name_overlay;
  /** Pixbuffer */
  GdkPixbuf *_pixbuf;
  /** @name indexImg */
  unsigned int _id_wanted;
  unsigned int _id_returned;
  //@}
  /** Gstream buffer */
  GstBuffer *_buf;
  /** Videosource */
  VideoSource _vs;
  
  /** The Gtk package that must be initialized. */
  GtkURBI *_gtk_urbi;
};

#endif //__OVERLAY_URBI_H
