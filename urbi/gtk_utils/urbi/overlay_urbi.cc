/**
 * Overlay Viewer running parallel to URBI
 *
 */

#include "overlay_urbi.h"
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include "overlay_viewer.h"
#include <functional>

// ==========================================================================
//Gtk::Main* OverlayURBI::_kit_gtk = NULL;
// ==========================================================================
OverlayURBI::OverlayURBI(const std::string &name)
  :urbi::UObject(name)
{
  _gtk_urbi = NULL;
  _gtk_urbi = new GtkURBI();

  // _setup_complete = false;
  // _var_x_axis = NULL;
  // _var_y_axis = NULL;
  // _joy = NULL;
  // _win = NULL;

  UBindFunction(OverlayURBI, init);
  // UBindFunction(OverlayURBI, hide);
  // UBindFunction(OverlayURBI, show);
  
  
}
OverlayURBI::~OverlayURBI() //---------------------------------------------
{
  if( _gtk_urbi != NULL) {
    delete _gtk_urbi;
  }
  // vs_free(&_vs)
  //std::cout << "Deleting"  <<"\n";
  // if( _win != NULL ) {
  //   _win->hide();
  //   delete _win;
  // }
  // if( _joy != NULL ) {
  //   delete _joy;
  // }
}
// ==========================================================================
int
OverlayURBI::init(const std::string &name)
{
  _name_overlay = name;

  UBindFunction(OverlayURBI, test_stream);
  UBindFunction(OverlayURBI, test_still);
  //create_overlay();
  //boost::thread joystick_thread(&OverlayURBI::create_joystick);
  //boost::thread joystick_thread(boost::bind(&OverlayURBI::run_overlay,this));
  //boost::thread joystick_thread(boost::bind(&run_window, win));
  
  //USetTimer(1.0, &OverlayURBI::frequent);

  return 0; // URBI OK */
}
// ==========================================================================
// void
// OverlayURBI::hide()
// {
//   //_win->hide();
// }
// void
// OverlayURBI::show()
// {
//   //_win->show();
//   boost::thread joystick_thread(boost::bind(&OverlayURBI::run_overlay,this));
// }
// ==========================================================================
void
OverlayURBI::test_stream(ufloat freq_update)
{
  int width=320;
  int height=240;

  // Initialiser le simple_viewer de libvideosource
  overlay_viewer_init(&_view,width,height, (char *) _name_overlay.c_str());
  
  // Récupère l'image avec un type précis
  vs_init(&_vs,"rtspsrc location=rtsp://korwlcam2.loria.fr/live.sdp latency=0 ! rtpmp4vdepay  ! ffdec_mpeg4", 320, 240, "video/x-raw-rgb,depth=32,bpp=32");
  
  _id_wanted=0;
  _id_returned=0;
  vs_play(&_vs);

  // update image
  USetTimer( freq_update, &OverlayURBI::_update_img_stream);
  //return 0; // URBI OK */
}
// ==========================================================================
int
OverlayURBI::_update_img_stream(void)
{
  _buf=vs_get_next_image(&_vs,_id_wanted,&_id_returned);
  // this will ensure we don't work twice on the same image
  _id_wanted=_id_returned+1;
  if (_buf) {
      // extract the pixbuffer
      _pixbuf=gst_to_gdkpixbuf(_buf);
      // et on dessine
      overlay_viewer_draw_pixbuf(&_view, _pixbuf);

      gst_buffer_unref(_buf);
  }
  return 0;  // URBI OK */
}
// ==========================================================================
void
OverlayURBI::test_still()
{
  GdkPixbuf *pixbuf;
  int height;
  int width;
  // load image
  if( _gtk_urbi != NULL ) {
    GError *err=NULL;
    pixbuf = gdk_pixbuf_new_from_file( "ressource/img_mire2_rgb.jpg", &err);
    height = gdk_pixbuf_get_height( pixbuf );
    width = gdk_pixbuf_get_width( pixbuf );
    if( pixbuf == NULL ) {
      g_warning("%s\n", err->message);
      g_error_free(err);
      //return FALSE;
    }
    // Initialiser le simple_viewer de libvideosource
    overlay_viewer_init(&_view, width, height, (char *) _name_overlay.c_str());
    // no overlay drawing
    _view.overlay_cbk = NULL;
    
    overlay_viewer_draw_pixbuf(&_view, pixbuf);

    boost::thread joystick_thread(boost::bind(&OverlayURBI::run_overlay,this));
  }
  // TODO : could also be done like for Joystick
  //
  // _win = new Gtk::Window();
  // _win->set_title( _name_joy );
  // _joy = new JoystickGTKMM(_name_joy);
  // _joy->set_recenter( true );
  // _win->add( *_joy );
  // _win->show_all_children();
 }
void
OverlayURBI::run_overlay()
{
  //Gtk::Main::run(*_win);
  gtk_main();
}
// ==========================================================================  
UStart(OverlayURBI);
// ==========================================================================
