# Standard things
# saves the variable $(d) that holds the current directory on the stack,
# and sets it to the current directory given in $(dir), 
# which was passed as a parameter by the parent rules.mk

sp 		:= $(sp).x
dirstack_$(sp)	:= $(d)
d		:= $(dir)

# Subdirectories, if any, in random order

#dir	:= $(d)/test
#include		$(dir)/Rules.mk

# Next, we create an immediately evaluated variable $(OBJS_common) 
# that holds all the objects in that directory.
# We also create $(DEPS_common), which lists any automatic dependency files 
# generated later on.
# To the global variable $(CLEAN), we add the files that the rules present here 
# may create, i.e. the ones we want deleted by a make clean command.)

# Local variables
URBI_ROOT 	:= /usr/local/gostai
GDK_FLAGS	:= $(shell pkg-config --cflags gdk-pixbuf-2.0)
GDK_LIBS	:= $(shell pkg-config --libs gdk-pixbuf-2.0)

GTK_FLAGS	:= $(shell pkg-config --cflags gtk+-2.0) $(shell pkg-config gtkmm-2.4 --cflags)
GTK_LIBS	:= $(shell pkg-config --libs gtk+-2.0)  $(shell pkg-config gtkmm-2.4 --libs)

OPENCV_FLAGS	:= `pkg-config --cflags opencv`
OPENCV_LIBS	:= `pkg-config --libs opencv`

GST_FLAGS	:= `pkg-config --cflags gstreamer-0.10` `pkg-config --cflags gstreamer-base-0.10 gstreamer-plugins-base-0.10`
GST_LIBS	:=`pkg-config --libs gstreamer-0.10` `pkg-config --libs gstreamer-base-0.10` -lgstapp-0.10 

VID_FLAGS	:= -I../video/libvideosource
VID_LIBS	:= -L../video/libvideosource -Wl,-rpath,../video/libvideosource -lvideosource

UTILS_FLAGS	:= -Iutils
#UTILS_LIBS	:= -Lutils, -Wl,-rpath,utils -lutils

CORE_OBJS_$(d)	:= $(d)/gtk_urbi.o \
                   $(d)/overlay_urbi.o $(d)/retine_urbi.o \
		   utils/libutils.so 
CORE_DEPS_$(d)	:= $(CORE_OBJS_$(d):%=%.d)

TGTS_$(d)	:= $(d)/gadget_urbi.so
DEPS_$(d)	:= $(TGTS_$(d):%=%.d)

CLEAN		:= $(CLEAN) $(CORE_OBJS_$(d)) $(CORE_DEPS_$(d)) \
                   $(TGTS_$(d)) $(DEPS_$(d))
VERYCLEAN	:= $(VERYCLEAN) $(d)/*~

# target local
TGT_LIB		:= $(TGT_LIB) verbose_$(d) $(TGTS_$(d))

# Now we list the dependency relations relevant to the files in this subdirectory.
# Most importantly, while generating the objects listed here, we want to set 
# the target-specific compiler flags $(CF_TGT) to include a flag that adds 
# this directory to the include path, so that local headers may be included 
# using #include <localfile.h>, which is, as said, more portable 
# than local includes when the source directory is not the current directory.)

# Local rules -> create archive 'core.a'
# Local CFLAGS
# Force remake if rules are changed
#$(CORE_OBJS_$(d)):	$(d)/rules.mk
$(CORE_OBJS_$(d)):	CF_TGT := -I$(d) -Igadget -I${URBI_ROOT}/include \
				$(GDK_FLAGS) $(GTK_FLAGS) \
                                $(UTILS_FLAGS) \
				$(VID_FLAGS) $(GST_FLAGS) $(OPENCV_FLAGS) \
				-Wno-deprecated

# Force remake if rules are changed
#$(TGTS_$(d)): $(d)/rules.mk
#$(TGTS_$(d)):	LF_TGT := 

$(TGTS_$(d)):	LL_TGT := $(GDK_LIBS) $(GTK_LIBS) \
			  $(UTILS_LIBS) \
	                  $(VID_LIBS) $(GST_LIBS) $(OPENCV_LIBS) \
	                  -lgthread-2.0 -lboost_thread

#$(CORE_OBJS_$(d)):	CF_TGT := -I. -I$(d) $(shell pkg-config --cflags gtkmm-2.4)
#$(OBJS_$(d)):	LF_TGT := -lgsl -lgslcblas -lm 
#$(OBJS_$(d)):	LL_TGT := $(S_LL_INET) cpp/core.a dana-cpp/dana.a

$(d)/gadget_urbi.so:	$(CORE_OBJS_$(d)) gadget/overlay_viewer.o
			$(DYN_LIB)

.PHONY : verbose_$(d)
verbose_$(d): $(d)/gadget_urbi.so
	@echo "**** Generating $^"

# As a last step, we restore the value of the current directory variable $(d) 
# by taking it from the current location on the stack, and we "decrement" 
# the stack pointer by stripping away the .x suffix 
# we added at the top of this file.

# Standard things

-include	$(CORE_DEPS_$(d))

d		:= $(dirstack_$(sp))
sp		:= $(basename $(sp))

