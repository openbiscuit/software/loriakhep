#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gtk/gtk.h>

#include <string.h>
#include "overlay_viewer.h"


GThread *th_video;
static void quit(GtkWidget *widget,gpointer data )
{
    gtk_main_quit ();
}

static gboolean expose_event_callback (GtkWidget *widget, GdkEventExpose *event, gpointer data)
{
    OverlayViewer *v=(OverlayViewer*)data;
    gdk_draw_arc(widget->window,
                widget->style->fg_gc[GTK_WIDGET_STATE (widget)],
                TRUE,
                0, 0, widget->allocation.width, widget->allocation.height,
                0, 64 * 360); 

    if (v->buf!=NULL) {
      gdk_draw_pixbuf(widget->window,widget->style->fg_gc[GTK_WIDGET_STATE (widget)],
		      v->buf,
		      0,0,
		      0,0,-1,-1,
		      GDK_RGB_DITHER_NONE,0,0);
      if (v->overlay_cbk != NULL) {
	v->overlay_cbk( widget, NULL );
      }
    }
  return TRUE;
}

static gpointer simple_video_thread( gpointer data )
{
  gdk_threads_enter();
  //gtk_init(NULL,NULL);
  
  gtk_main();
  gdk_threads_leave ();
  return NULL;
}
  
static gpointer video_thread(gpointer data)
{
    printf("Avant Thread\n");
    gdk_threads_enter();
    OverlayViewer *v=(OverlayViewer*)data;

    gtk_init(NULL,NULL);
    v->window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    printf("Avant Nom\n");
    gtk_window_set_title( (GtkWindow *) v->window, v->name);

    g_signal_connect (G_OBJECT(v->window), "delete_event",
		      G_CALLBACK(quit), NULL);

    gtk_container_set_border_width(GTK_CONTAINER(v->window),10);

    v->drawing_area=gtk_drawing_area_new();
    gtk_widget_set_size_request(v->drawing_area,v->w,v->h);
    g_signal_connect(G_OBJECT(v->drawing_area),"expose_event",
                    G_CALLBACK(expose_event_callback),data);

    gtk_container_add(GTK_CONTAINER(v->window),v->drawing_area);
    gtk_widget_show(v->drawing_area);
    //gtk_widget_show(v->window);
    gtk_main();
    gdk_threads_leave ();
    return NULL;
}

void add_new_overlay_viewer( OverlayViewer *v ,int w, int h, char *name)
{
  v->drawing_area=NULL;
  v->window=NULL;
  v->buf=NULL;
  v->w=w;
  v->h=h;
  v->overlay_cbk = NULL;
  printf("Avant strcpy\n");
  strcpy( v->name, name );
    
  gdk_threads_enter();
  v->window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  printf("Avant Nom\n");
  gtk_window_set_title( (GtkWindow *) v->window, v->name);
  
  g_signal_connect (G_OBJECT(v->window), "delete_event",
		    G_CALLBACK(quit), NULL);
  
  gtk_container_set_border_width(GTK_CONTAINER(v->window),10);
  
  v->drawing_area=gtk_drawing_area_new();
  gtk_widget_set_size_request(v->drawing_area,v->w,v->h);
  g_signal_connect(G_OBJECT(v->drawing_area),"expose_event",
		   G_CALLBACK(expose_event_callback),v);
  
  gtk_container_add(GTK_CONTAINER(v->window),v->drawing_area);
  gtk_widget_show(v->drawing_area);
  gtk_widget_show(v->window);
  gdk_threads_leave ();
}

void init_overlay_thread()
{
  GError *err=NULL;
  if (!g_thread_supported()) g_thread_init(NULL);
  gdk_threads_init();
  th_video=g_thread_create(simple_video_thread,NULL,TRUE,&err);
}

void overlay_viewer_init(OverlayViewer *v,int w,int h, char *name)
{
    GError *err=NULL;
    if (!g_thread_supported()) g_thread_init(NULL);
    gdk_threads_init();

    v->drawing_area=NULL;
    v->window=NULL;
    v->buf=NULL;
    v->w=w;
    v->h=h;
    v->overlay_cbk = NULL;
    printf("Avant strcpy\n");
    strcpy( v->name, name );
     printf("Après strcpy\n");
    v->th_video=g_thread_create(video_thread,v,TRUE,&err);
    printf("AprèsThreadCreate\n");
}

void overlay_viewer_draw_pixbuf(OverlayViewer *v,GdkPixbuf *buf)
{
    GError *err=NULL;
    
    gdk_threads_enter();
    if (v->buf!=NULL)
        gdk_pixbuf_unref(v->buf);
    gdk_pixbuf_ref(buf);
    v->buf=buf;
    if (v->drawing_area!=NULL)
        gdk_window_invalidate_rect(v->drawing_area->window,NULL,FALSE); 
    gdk_threads_leave();
}

void overlay_viewer_hide(OverlayViewer *v)
{
  gdk_threads_enter();
  gtk_widget_hide(v->window);
  gdk_threads_leave();
}
void overlay_viewer_show(OverlayViewer *v)
{
  gdk_threads_enter();
  gtk_widget_show(v->window);
  gdk_threads_leave();
}

