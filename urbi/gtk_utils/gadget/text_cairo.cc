
#include "text_cairo.h"
#include <iostream>

// ==========================================================================
TextCAIRO::TextCAIRO( std::string text,
		      TAlign align, bool is_line )
  : GadjetCAIRO(),
    _text(text),
    _is_line(is_line),
    _align(align)
{
  select_font( std::string("serif"), 0.2f );
  _external_box.x = 0.0;
  _external_box.y = 0.0;
  _external_box.width = 0.0;
  _external_box.height = 0.0;
  _line_width = 0.01;
  _coef_box_extend = 1.2f;
}
TextCAIRO::~TextCAIRO() // --------------------------------------------------
{
}
// ==========================================================================
void
TextCAIRO::render( Cairo::RefPtr<Cairo::Context> cr )
{
  std::cout << "Rendering TextCairo : " << _text << "\n";
  // parent
  GadjetCAIRO::render(cr);
  // save context
  cr->save();
  
  compute_internal_positions(cr);
  
  // line around ?
  if( _is_line ) {
    cr->set_line_width( _line_width );
    cr->rectangle( _external_box.x, _external_box.y,
		   _external_box.width, _external_box.height );
    cr->stroke();
  }

  cr->translate( _pos_start_text.x, _pos_start_text.y );
  cr->scale( 1.0, -1.0 );
  cr->show_text( _text);
  cr->restore();
}

Cairo::Rectangle
TextCAIRO::get_external_box( Cairo::RefPtr<Cairo::Context> cr )
{
  cr->save();
  compute_internal_positions(cr);
  cr->restore();

  return _external_box;
}
// ==========================================================================    
void
TextCAIRO::set_is_line( bool is_line )
{
  _is_line = is_line;
}
bool
TextCAIRO::get_is_line(void) // ---------------------------------------------
{
  return _is_line;
}
void
TextCAIRO::set_line_width( float line_width ) // ----------------------------
{
  _line_width = line_width;
}
float
TextCAIRO::get_line_width(void) // ------------------------------------------
{
  return _line_width;
}
void 
TextCAIRO::set_coef_box_extend( float coef_box_extend )
{
  _coef_box_extend = coef_box_extend;
}
float
TextCAIRO::get_coef_box_extend(void)
{
  return _coef_box_extend;
}
// ==========================================================================
void
TextCAIRO::set_text( std::string text )
{
  _text = text;
}
void
TextCAIRO::select_font( std::string family, // ------------------------------
			float size,
			Cairo::FontSlant slant,
			Cairo::FontWeight weight )
{
  _fnt_family = family;
  _fnt_slant = slant;
  _fnt_weight = weight;
  _fnt_size = size;
}
void
TextCAIRO::set_align( TAlign align ) // -------------------------------------
{
  _align = align;
}
TAlign
TextCAIRO::get_align(void) // -----------------------------------------------
{
  return _align;
}
// ==========================================================================
void
TextCAIRO::compute_internal_positions( Cairo::RefPtr<Cairo::Context> cr )
{
  Cairo::TextExtents extents;
  cr->select_font_face( _fnt_family, _fnt_slant, _fnt_weight);
  cr->set_font_size( _fnt_size );
  cr->get_text_extents ( _text, extents);

  std::cout << "Extends x=" << extents.x_bearing << ", y=" << extents.y_bearing << ", w=" << extents.width << ", h=" << extents.height << "\n";
  
  // external box is bearing box scaled by _coef_box_extend.
  switch( _align ) {
  case ALIGN_CENTER:
    _external_box.x = - extents.width/2.0 * _coef_box_extend;
    _external_box.y = 0.0;
    _external_box.width = extents.width * _coef_box_extend;
    _external_box.height = extents.height * _coef_box_extend;
    _pos_start_text = position_from_xy( - extents.width/2.0 + extents.x_bearing,
					(_external_box.height + extents.y_bearing)/2.0);
    break;
    
  case ALIGN_LEFT:
    _external_box.x = 0.0;
    _external_box.y = 0.0;
    _external_box.width = extents.width * _coef_box_extend;
    _external_box.height = extents.height * _coef_box_extend;
    _pos_start_text = position_from_xy( (_external_box.width - extents.width)/2.0 + extents.x_bearing,
					(_external_box.height + extents.y_bearing)/2.0);
    break;
    
  case ALIGN_RIGHT:
    _external_box.x = 0.0 - extents.width * _coef_box_extend;
    _external_box.y = 0.0;
    _external_box.width = extents.width * _coef_box_extend;
    _external_box.height = extents.height * _coef_box_extend;
    _pos_start_text = position_from_xy( (_external_box.width - extents.width)/2.0 + extents.x_bearing - _external_box.width,
					(_external_box.height + extents.y_bearing)/2.0);
    break;
  }
  std::cout << "External x=" << _external_box.x << ", y=" << _external_box.y << ", w=" << _external_box.width << ", h=" << _external_box.height << "\n";
  // starting point for text
  std::cout << "Start x=" << _pos_start_text.x << ", y=" << _pos_start_text.y << "\n";
}
// ==========================================================================
