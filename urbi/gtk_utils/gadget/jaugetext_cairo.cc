#include "jaugetext_cairo.h"
#include <iostream>
#include <math.h>

// ========================================================================
JaugeTextCAIRO::JaugeTextCAIRO( float val, float lvl_min, float lvl_max,
				TAlign align, bool is_line )
: TextCAIRO( "", align, is_line),
  JaugeCAIRO( lvl_min, lvl_max ),
  _float_format("%+5.2f")
{
  set_level( val );
}
JaugeTextCAIRO::~JaugeTextCAIRO() // --------------------------------------
{
}
// ========================================================================
void
JaugeTextCAIRO::render( Cairo::RefPtr<Cairo::Context> cr )
{
  // parent
  GadjetCAIRO::render(cr);
  // save context
  cr->save();
  
  compute_internal_positions(cr);

  // line around ?
  if( _is_line ) {
    cr->set_line_width( _line_width );
    cr->rectangle( _external_box.x, _external_box.y,
		   _external_box.width, _external_box.height );
    cr->stroke();
  }

  cr->translate( _pos_start_text.x, _pos_start_text.y );
  cr->scale( 1.0, -1.0 );
  _float_format % (_lvl_min + _ratio * (_lvl_max - _lvl_min));
  std::cout << "Rendering JaugeTextCairo : " << _float_format.str() << "\n";
  
  cr->show_text( _float_format.str() );
  cr->restore();
}
// ========================================================================
void
JaugeTextCAIRO::compute_internal_positions( Cairo::RefPtr<Cairo::Context> cr )
{
  Cairo::TextExtents extents_min, extents_max;
  cr->select_font_face( _fnt_family, _fnt_slant, _fnt_weight);
  cr->set_font_size( _fnt_size );

  // size for the smallest
  _float_format % _lvl_min;
  cr->get_text_extents ( _float_format.str(), extents_min );
  // size for the biggest
  _float_format % _lvl_max;
  cr->get_text_extents ( _float_format.str(), extents_max );

  double width = fmax( extents_min.width, extents_max.width );
  double height = fmax( extents_min.height, extents_max.height );
  
  // external box is bearing box scaled by _coef_box_extend.
  switch( _align ) {
  case ALIGN_CENTER:
    _external_box.x = -width/2.0 * _coef_box_extend;
    _external_box.y = 0.0;
    _external_box.width = width * _coef_box_extend;
    _external_box.height = height * _coef_box_extend;
    _pos_start_text = position_from_xy( - width/2.0,
					(_external_box.height + extents_max.y_bearing)/2.0)
      ;
    break;
  
  case ALIGN_LEFT:
    _external_box.x = 0.0;
    _external_box.y = 0.0;
    _external_box.width = width * _coef_box_extend;
    _external_box.height = height * _coef_box_extend;
    _pos_start_text = position_from_xy( (_external_box.width - width)/2.0 + extents_max.x_bearing,
					(_external_box.height + extents_max.y_bearing)/2.0);
    break;
    
  case ALIGN_RIGHT:
    _external_box.x = 0.0 - width * _coef_box_extend;
    _external_box.y = 0.0;
    _external_box.width = width * _coef_box_extend;
    _external_box.height = height * _coef_box_extend;
    _pos_start_text = position_from_xy( (_external_box.width - width)/2.0 + extents_max.x_bearing - _external_box.width,
					(_external_box.height + extents_max.y_bearing)/2.0);
    break;
  }

}
// ========================================================================
void
JaugeTextCAIRO::set_float_format(boost::format& float_format)
{
  _float_format = float_format;
}
// ========================================================================
