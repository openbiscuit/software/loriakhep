
#ifndef __JOYSTICK_GTKMM_H
#define __JOYSTICK_GTKMM_H

#include <gtkmm.h>
#include <iostream>
#include <iomanip>
#include <math.h>

/**
 * A JoystickGTKMM is a Gtk::Frame that opens on a "joystick" that can
 * be rencentred (or not) as soon as it is released.
 * The Joystick has two dimension : x and y that are readable and take their
 * value in [-1,1].<br>
 * As indicated by the state of the Gtk::CheckButton 'Rappel', the joystick
 * is either recentred or not when released. The initial state of that
 * checkbutton can be set or read by set/get_recenter.
 */
class JoystickGTKMM : public Gtk::Frame 
{
 public:
  /**
   * @name Creation/Destruction
   */
  /**
   * Constructor.
   *
   * @param name of the Gtk::Frame label displayed.
   */
  JoystickGTKMM(Glib::ustring name = "Joystick" );
  /** 
   * Destructor.
   */
  ~JoystickGTKMM();
  //@}

  /**
   * Get the x value of the Joystick : READ-ONLY.
   */
  double get_x(void);
  /**
   * Get the y value of the Joystick : READ-ONLY.
   */
  double get_y(void);
  
  /**
   * @name recenter
   */
  /**
   * Get the value of the '_recenter' parameter.
   */
  bool get_recenter(void);
  /**
   * Set the value of the '_recenter' parameter. 
   */
  void set_recenter(bool recenter);

  
 protected:
  /**
   * Build the GUI.
   * Gtk::Frame<br>
   *   + Gtk::VBox<br>
   *     + Gtk::DrawingArea -> draw joystick<br>
   *     + Gtk::Hbox<br>
   *       + Gtk::Label -> display x/y values<br>
   *       + Gtk::Checkbutton -> display _recenter value<br>
   */
  void build();
  /**
   * From the actual x/y value, build up the label for displaying their value.
   */
  Glib::ustring get_label_ustring();

  /**
   * @name callback
   * Callback functions.
   */
  /** When _area is exposed -> draw joystick. */
  virtual bool __on_expose_event( GdkEventExpose* event);
  /** When _area is clicked -> set _action to true and follow mouse. */
  virtual bool __on_button_press_event ( GdkEventButton* event);
  /** When _area is un_clicked -> set _action to false, [recenter] */
  virtual bool __on_button_release_event ( GdkEventButton* event );
  /** When mouse is moved over _area -> follow ?? */ 
  virtual bool __on_motion_notify_event ( GdkEventMotion* event);
  /** When _area size or position are changed. */
  virtual bool __on_configure_event (GdkEventConfigure* event);
  /** When the state of _recenter_button is changed -> [recenter] */
  virtual void __on_toggled_recenter();// Gtk::ToggleButton* button );
  //@}

  /**
   * Draw the joystick as a red circle and 2 axes.
   */
  void render_joystick(Cairo::RefPtr<Cairo::Context> cr);
  /**
   * Update the label od _label.
   */
  void update_label();
  /**
   * Ensure that 'val' belongs to [-1,1].
   */
  double clamp(double val);

  /**
   * @name private variables
   */
  /** Are we following the mouse ? */
  bool               _action;
  /** Actual x position of the joystick. */
  double             _x_joy;
  /** Actual y position of the joystick. */
  double             _y_joy;
  /** Actual width of the joystick area. */
  double             _width;
  /** Actual height of the joystick area. */
  double             _height;
  //@}
  /**
   * @name widget
   * See build().
   */
  Gtk::DrawingArea   _area;
  Gtk::Label         _label;
  Gtk::CheckButton   _recenter_button;
  Gtk::VBox          _vbox;
  Gtk::HBox          _hbox;
  //@}
};

#endif //__JOYSTICK_GTKMM_H
