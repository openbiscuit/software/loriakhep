#include "jauge_cairo.h"
#include <iostream>
// ==========================================================================
JaugeCAIRO::JaugeCAIRO( float lvl_min, float lvl_max, float ratio,
			 float length, float width)
  : GadjetCAIRO()
{
  set_bounds( lvl_min, lvl_max );
  set_ratio( ratio );
  _length = length;
  _width = width;
}
JaugeCAIRO::~JaugeCAIRO() // ------------------------------------------------
{
}
// ==========================================================================
void
JaugeCAIRO::render( Cairo::RefPtr<Cairo::Context> cr )
{
  std::cout << "JaugeCAIRO::render()\n";
  // parent
  GadjetCAIRO::render(cr);
  // save context
  cr->save();
  
  cr->set_line_width(_width);
  // path for bg line
  cr->set_source_rgba( _bg_color.red,
		       _bg_color.green,
		       _bg_color.blue,
		       _bg_color.alpha );
  cr->move_to(  0.0, 0.0 );
  cr->line_to(  _length, 0.0);
  cr->stroke();
  // path for fg line
  cr->set_source_rgba( _fg_color.red,
		       _fg_color.green,
		       _fg_color.blue,
		       _fg_color.alpha );
  cr->move_to(  0.0, 0.0 );
  cr->line_to(  _ratio * _length, 0.0);
  // draw
  cr->stroke();
  
  // Restore context
  cr->restore();
}
// ==========================================================================
float
JaugeCAIRO::set_ratio( float ratio )
{
  if( (ratio <= 1.0 ) and (ratio >= 0.0)) {
    _ratio = ratio;
  }
  return get_level();
}
float
JaugeCAIRO::get_ratio(void) // ----------------------------------------------
{
  return _ratio;
}
float
JaugeCAIRO::set_level( float level ) // ------------------------------------
{
  if( (level <= _lvl_max) and (level >= _lvl_min)) {
    _ratio = (level - _lvl_min) / (_lvl_max - _lvl_min);
  }
  return _ratio;
}
float
JaugeCAIRO::get_level(void) // ----------------------------------------------
{
  return _lvl_min + _ratio * (_lvl_max - _lvl_min);
}
// ==========================================================================
void
JaugeCAIRO::set_bounds( float lvl_min, float lvl_max )
{
  if( lvl_min < lvl_max ) {
    _lvl_min = lvl_min;
    _lvl_max = lvl_max;
  }
}
// ==========================================================================
