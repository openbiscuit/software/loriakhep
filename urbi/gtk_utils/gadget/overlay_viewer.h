#ifndef OVERLAY_VIEWER_H
#define OVERLAY_VIEWER_H

#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gtk/gtk.h>
#include <boost/function.hpp>
/*
  TODO : clean up
  TODO (??) : adapt to GtkMM ??
 */

/** \file overlay_viewer.h
 * Visualisation d'une image (GTK) avec la possibilité de "dessiner dessus.
 *
 * La fenetre (Window) est lancée dans un thread independant.
 *
 * Un exemple typique d'utilisation est le suivant:
 * \code
 * // Définition
 * Overlayviewer _view;
 * // Création et initialisation
 * overlay_viewer_init(&_view, WIN_WIDTH, WIN_HEIGHT, "bouh");
 * // Ajout d'un callback appelé pour dessiner sur l'image
 * _view.overlay_cbk = the_call_back_function;
 * // ou, dans le cas d'un membre d'un objet _truc
 *_view.overlay_cbk = boost::bind( &TrucObjet::call_bakc_function, _truc);
 * // Une image a afficher
 * overlay_viewer_draw_pixbuf(&_view, _pixbuf);
 * \endcode
 */

/** \struct OverlayViewer
 * Visualisation d'une image (GTK) avec la possibilité de "dessiner dessus.
 * Structure à initialiser, voir overlay_viewer.h
 *
 * Un exemple typique d'utilisation est le suivant:
 * \code
 * // Définition
 * Overlayviewer _view;
 * // Création et initialisation
 * overlay_viewer_init(&_view, WIN_WIDTH, WIN_HEIGHT, "bouh");
 * // Ajout d'un callback appelé pour dessiner sur l'image
 * _view.overlay_cbk = the_call_back_function;
 * // ou, dans le cas d'un membre d'un objet _truc
 *_view.overlay_cbk = boost::bind( &TrucObjet::call_bakc_function, _truc);
 * // Une image a afficher
 * overlay_viewer_draw_pixbuf(&_view, _pixbuf);
 * \endcode
 *
 * *ATTENTION*: the struct members should NEVER be accessed directly
 *  (may cause the video thread in the background to crash).
 */
typedef struct{
    GtkWidget *window;
    GtkWidget *drawing_area;
    GThread *th_video;
    GdkPixbuf *buf; /* current buffer to draw (cached in case of slow refresh) */
    int w,h;
    boost::function<int (GtkWidget *widget,gpointer data)> overlay_cbk;
    //int (*overlay_cbk)(GtkWidget *widget,gpointer data);
    char name[50];
} OverlayViewer;

extern GThread *th_video;
/* initialize GTK_Thread */
void init_overlay_thread();
/* add a new overlay_viewer */
void add_new_overlay_viewer( OverlayViewer *v ,int w, int h, char *name);

/**
 * Initialize a OverlayViewer of size w*h, with the given name.
 */
void overlay_viewer_init(OverlayViewer *v,int w,int h, char *name);

/** 
 * Queue a new pixmap to draw.
 */
void overlay_viewer_draw_pixbuf(OverlayViewer *v,GdkPixbuf *buf);


/** Show/Hide the overlay_viewer */
void overlay_viewer_hide(OverlayViewer *v);
/** Show/Hide the overlay_viewer */
void overlay_viewer_show(OverlayViewer *v);

#endif
