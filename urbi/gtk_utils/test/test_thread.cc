
// gcc test_thread.cc joystick_gtkmm.cc -o test_thread `pkg-config gtkmm-2.4 --cflags --libs` -lboost_thread

#include <unistd.h>
#include <vector>

#include <boost/foreach.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <iostream>
#include <string.h>

#include "joystick_gtkmm.h"

std::vector<JoystickGTKMM *> vec_joy;
  
void
run_gtk_loop()
{
  
  std::cout << "run_gtk_loop: Init gtk with dummy variables" << "\n";
  int argc_dummy = 1;
  char **argv_dummy;
  const char *argv_zero = "gtk_loop";
  argv_dummy = (char **) malloc( argc_dummy * sizeof(char *));
  argv_dummy[0] = (char *) malloc( 20 * sizeof(char));
  strcpy( argv_dummy[0], argv_zero);
  Gtk::Main kit(&argc_dummy, &argv_dummy);
  
  //std::cout << "run_gtk_loop: Main::run without widget" << "\n";
  //Gtk::Main::run();
}

void
run_window(Gtk::Window *win)
{
  // std::cout << "run_gtk_loop: Init gtk with dummy variables" << "\n";
//   int argc_dummy = 1;
//   char **argv_dummy;
//   char *argv_zero = "gtk_loop";
//   argv_dummy = (char **) malloc( argc_dummy * sizeof(char *));
//   argv_dummy[0] = argv_zero;
//   Gtk::Main kit(&argc_dummy, &argv_dummy);
  
  Gtk::Main::run(*win);
}

void
display_joy()
{
  while(true) {
    std::cout << "**** JoystickGTKMM *** (" << vec_joy.size() << ")\n";
    BOOST_FOREACH(JoystickGTKMM *j, vec_joy) {
      std::cout << j->get_x() << " + " << j->get_y() << "\n";
    } 
    sleep(2);
  }
}


JoystickGTKMM *
create_new_joystick( std::string name )
{
  
  Gtk::Window * win = new Gtk::Window();
  win->set_title( name );
  JoystickGTKMM * joy = new JoystickGTKMM(name);
  joy->set_recenter( false );
  win->add( *joy );
  win->show_all_children();
  
  boost::thread joystick_thread(boost::bind(&run_window, win));

  return joy;
}

int 
main( int argc, char *argv[] )
{

  //boost::thread joystick_thread(run_gtk_loop);
  //run_gtk_loop();
  std::cout << "run_gtk_loop: Init gtk with dummy variables" << "\n";
  int argc_dummy = 1;
  char **argv_dummy;
  const char *argv_zero = "gtk_loop";
  argv_dummy = (char **) malloc( argc_dummy * sizeof(char *));
  argv_dummy[0] = (char *) malloc( 20 * sizeof(char));
  strcpy( argv_dummy[0], argv_zero);
  Gtk::Main kit(&argc_dummy, &argv_dummy);
  boost::thread display_thread(display_joy);

  std::string rep = "";
  while (true) {
    std::cout << "New Joystick y/n ? ";
    std::cin >> rep;
    if( rep != "n" ) {
      JoystickGTKMM * joy = create_new_joystick(rep);
      vec_joy.push_back(joy);
      std::cout << "Vec is of sise " << vec_joy.size() << "\n";
    }
  }

  return 0;
}
  
