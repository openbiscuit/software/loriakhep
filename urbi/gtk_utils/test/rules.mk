# Standard things
# saves the variable $(d) that holds the current directory on the stack,
# and sets it to the current directory given in $(dir), 
# which was passed as a parameter by the parent rules.mk

sp 		:= $(sp).x
dirstack_$(sp)	:= $(d)
d		:= $(dir)

# Subdirectories, if any, in random order

#dir	:= $(d)/test
#include		$(dir)/Rules.mk


# Local rules and target

TGTS_$(d)	:= $(d)/test_cairo   \
                   $(d)/test_draw \
                   $(d)/test_thread

DEPS_$(d)	:= $(TGTS_$(d):%=%.d)

TGT_BIN		:= $(TGT_BIN) verbose_$(d) $(TGTS_$(d))
CLEAN		:= $(CLEAN) $(TGTS_$(d)) $(DEPS_$(d))
VERYCLEAN	:= $(VERYCLEAN) $(d)/*~

$(TGTS_$(d)):	$(d)/rules.mk 

$(TGTS_$(d)):	CF_TGT := -Igadget -I. -DRADDB=\"$(DIR_ETC)\" \
                          $(shell pkg-config gtkmm-2.4 --cflags)
$(TGTS_$(d)):	LF_TGT := $(shell pkg-config gtkmm-2.4 --libs) \
                          -lboost_thread
#                         -L./dana -ldana		
$(TGTS_$(d)):	LL_TGT := $(S_LL_INET) gadget/libgadget.a

$(d)/test_cairo: $(d)/test_cairo.cc gadget/libgadget.a
		$(COMPLINK)
$(d)/test_draw:	$(d)/test_draw.cc gadget/libgadget.a
		$(COMPLINK)
$(d)/test_thread: $(d)/test_thread.cc gadget/libgadget.a
		$(COMPLINK)

.PHONY : verbose_$(d)
verbose_$(d): $(TGTS_$(d))
	@echo "Generating $^"

# As a last step, we restore the value of the current directory variable $(d) 
# by taking it from the current location on the stack, and we "decrement" 
# the stack pointer by stripping away the .x suffix 
# we added at the top of this file.

# Standard things

-include	$(DEPS_$(d))

d		:= $(dirstack_$(sp))
sp		:= $(basename $(sp))
