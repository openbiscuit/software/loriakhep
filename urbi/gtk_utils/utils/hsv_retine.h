#ifndef __HSVRETINE_H
#define __HSVRETINE_H

#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <math.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

/**
Détection du taux de rouge sur plusieurs bandes verticales dans une image.

A chaque position de l'image spécifiée par un des éléments de HSVRetine::pos_x,
les teintes des pixels sur une ligne verticale sont rangé dans un histogramme
à 36 'cases', en fonction de leur teinte qui varie entre 0° (rouge) et 359° (rouge).
Ces histogrammes sont stockés dans HSVRetine::hist. Pour être considéré comme un 
pixel de couleur, la luminosité d'un pixel doit être comprise dans l'intervale
[HSVRetine::seuil_dark, HSVRetine::seuil_blanc], voir .hue_from_rgb().

 */
class HSVRetine
{
public:
  /**
   * @name Creation/Destruction
   */
  /**
   * Construction avec bandes par défaut. 
   */
  HSVRetine();
  /**
   * Construction avec les positions des bandes   
   */
  HSVRetine( int nb_pos, int *pos);
  /**
   * Destruction.
   */
  ~HSVRetine();
  //@}

  /**
   * Calcule les histogrammes de teintes pour toutes les positions
   * indiquées dans pos_x.
   */
  int compute_h_hist( GdkPixbuf *pixbuf );
  /**
   * Dessine sur un GtkDrawable les histogrammes de teintes.
   */
  int overlay_hist( GtkWidget *widget , gpointer data );
  /**
   * Print les histogrammes.
   */
  void print();
   
protected:
  /** 
   * Calcule la teinte (Hue) à partir de RGB pour les points qui ne sont 
   * ni trop noir (rgb < seuil_dark) ni trop blanc (rgb > seuil_blanc). 
   */
  float hue_from_rgb( int red, int green, int blue,
		      int seuil_light, int seuil_dark);
 public:
  /** @name vert_strip */
  /**
   * Position des bandes verticales.
   * 
   * Si il y a plus de 'nb_pos' position, la 'nb_pos+1'-ème sert pour l'affichage.
   */
  /** Par defaut */
  static int pos_x_def[];
  /** Nombre de bandes verticales. */
  static int nb_pos_def;
  /** Actuel */
  int *pos_x;
  int nb_pos;
  //@}

  /** @name hist */
  /**
   * Un histogramme de 36 positions de teinte par bande verticale.
   */
  float **hist;
  /**
   * Un histogramme de blanc/noir par bande verticale.
   */
  float *non_hist;
  //@}

  /** @name param */
  /** seuil pour 'trop blanc' */
  int seuil_light;
  /** seuil pour 'trop noir' */
  int seuil_dark;
  //@}
};
#endif //__HSVRETINE_H
