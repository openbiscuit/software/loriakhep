Gadget 
======

12/03/2010

Ce package offre des objets URBI (des uobject) qui allient URBI et GTK.

Une documentation html assez complète se trouve dans l'archive gtk_utils_doc.tgz. 
Elle contient des exemples d'utilisation. On ouvre cette archive avec 
>> tar xzvf gtk_utils_doc.tgz
ce qui crée le répertoire 'html'. L'entrée de la doc est 'html/index.html'

Au besoin, 'make doc' à la racine de ce 'package' (gtk_utils) devrait compiler
cette documentation  dans le répertoire doc(si vous avez les bon outils).

Nécessite:
- doxygen et dot pour générer les doc
- g++
- make
- gtkmm, urbi

Hiérarchie de fichier
./README.txt : ce fichier
./Makefile et ./rules.mk : Makfile de haut niveau
+ gadget => des widget en gtk
+ urbi   => des UObjets URBI utilisant les gagdet
+ utils  => fonction ou classes utiles pour les UObjets
+ test   => quelques petites applications pour tester les 'gadget'
+ doc    => la doc et de quoi la générer

