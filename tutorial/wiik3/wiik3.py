#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import sys
import cwiid
sys.path.append("../../tcp_connection/python/")
from tcp_client import TcpClient

class Wiimote:
    "helper class for cwiid"
    def __init__(self):
        print "opening wiimote, press 1+2"
        self.rpt_mode=0
        self.rpt_mode^=cwiid.RPT_ACC # capture accel events
        self.rpt_mode^=cwiid.RPT_BTN # capture buttons events
        self.wiimote=cwiid.Wiimote()
        self.wiimote.rpt_mode=self.rpt_mode

    def __del__(self):
        print "closing wiimote"
        self.wiimote.close()
        
    def get_acc(self):
        return self.wiimote.state["acc"]
    
    def get_btn(self):
        return self.wiimote.state["buttons"]


if __name__=="__main__":
    wii=Wiimote()
    k3=TcpClient("localhost",3000)

    print "let's go !"
    k3.set_speed(0,0)
    
    while True:
        # TODO: stop if IR front
        acc=wii.get_acc()
        #print acc
        accx=-(acc[1]-128)/2
        accy=(acc[0]-128)/4
        back=False
        if accx<0: 
                accx=-accx
                back=True
        if accx>10: accx=10
        
        if accy>accx:
            accy=accx
        if accy<-accx:
            accy=-accx
            
        #print accx,accy
        btn=wii.get_btn()
        if btn>4:
            break
        if btn&4==4:
            #print "move !",accx+accy,accx-accy
            if back:
                k3.set_peed(-1500*(accx+accy),-1500*(accx-accy))
            else:
                k3.set_speed(1500*(accx+accy),1500*(accx-accy))
        else:
            k3.set_speed(0,0)
        time.sleep(0.1)
    k3.set_speed(0,0)
