#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#include "khepera3.h"
#include "odometry_track.h"

struct sOdometryTrack ot;
int left,right;

void sigint_cb(int sig) {
    printf("\nInterrupted..\n");
    khepera3_drive_set_speed(0,0);
    exit(sig);
}

void init(void)
{
    khepera3_init();
    khepera3_drive_start(); 
    odometry_track_start(&ot);
    if (ot.configuration.is_default) {
            printf("WARNING: Odometry configuration file (/etc/khepera/odometry) not found. Using default values.\n");
    }
}

void run(void)
{
    signal(SIGINT,sigint_cb);
    khepera3_drive_set_speed(left,right);
    while(1)
    {
        odometry_track_step(&ot);
        printf("$POSITION,%f,%f,%f\n",ot.result.x, ot.result.y, ot.result.theta);
        usleep(20000);
    }

}

int main(int argc, char**argv)
{
    if(argc!=3)
    {
        printf("Usage: %s speed_left speed_right\n",argv[0]);
        exit(-1);
    }
    left=atoi(argv[1]);
    right=atoi(argv[2]);
    init();
    run();
    return 0;
}
