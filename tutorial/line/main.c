#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "khepera3.h"

#define IR_GAIN 4
#define SPEED_FW 5000

void init(void)
{
    khepera3_init();
    khepera3_drive_start(); 
}

void run(void)
{
    int ir_left,ir_right,ir_diff;
    int speed_left,speed_right;
    int dist_front;
    int i;
    
    while(1)
    {
        khepera3_infrared_proximity();
        ir_left=khepera3.infrared_proximity.sensor[cKhepera3SensorsInfrared_FloorLeft];
        ir_right=khepera3.infrared_proximity.sensor[cKhepera3SensorsInfrared_FloorRight];
        ir_diff=(ir_left-ir_right)*IR_GAIN;

        khepera3_ultrasound(cKhepera3SensorsUltrasound_Front);

        struct sKhepera3SensorsUltrasoundSensor *front_us_sensor=&khepera3.ultrasound.sensor[cKhepera3SensorsUltrasound_Front];

        // default behavior: follow line
        speed_left=SPEED_FW+ir_diff;
        speed_right=SPEED_FW-ir_diff;

        if (front_us_sensor->echos_count>0)
            {
                dist_front=front_us_sensor->distance[0];
                if (dist_front<30)
                    {
                        // pause if obstacle in sight
                        speed_left=0;
                        speed_right=0;
                    }
            }
        // test arrêt d'urgence
        
        for (i=0;i<9;i++)
            if (khepera3.infrared_proximity.sensor[i]>800)
                {
                    khepera3_drive_stop();
                    exit(0);
                }

        khepera3_drive_set_speed(speed_left,speed_right);
        usleep(20000);
    }

}

int main()
{
    init();
    run();
    return 0;
}

