#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#include "khepera3.h"

#define IR_GAIN 4
#define SPEED_FW 5000

void sigint_cb(int sig) {
        printf("\nInterrupted..\n");
        khepera3_drive_set_speed(0,0);
        exit(sig);
}

void init(void)
{
    khepera3_init();
    khepera3_drive_start(); 
}

void run(void)
{
    int ir_left,ir_right,ir_diff,ir_mean;
    int speed_left,speed_right;
    int speed_fw;

    signal(SIGINT,sigint_cb);

    while(1)
    {
        khepera3_infrared_proximity();
        ir_left=khepera3.infrared_proximity.sensor[3];
        ir_right=khepera3.infrared_proximity.sensor[4];
        ir_diff=(ir_left-ir_right)*IR_GAIN;
        ir_mean=(ir_left+ir_right)/20;
        if (ir_mean>200)
            ir_mean=200;
        
        speed_fw=(200-ir_mean)*(200-ir_mean)*(200-ir_mean)/400;
        if (speed_fw<1000) speed_fw=0;
        printf("%d\n",speed_fw);
        // default behavior: follow line
        speed_left=speed_fw-ir_diff;
        speed_right=speed_fw+ir_diff;

        // test arrêt d'urgence
        
//         for (i=0;i<9;i++)
//             if (khepera3.infrared_proximity.sensor[i]>3900)
//                 {
//                     khepera3_drive_stop();
//                     exit(0);
//                 }

        khepera3_drive_set_speed(speed_left,speed_right);
        usleep(20000);
    }

}

int main()
{
    init();
    run();
    return 0;
}

