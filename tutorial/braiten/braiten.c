#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>

#include "khepera3.h"

/*  ce programme est un extrait du khepera3_test de k-team, 
    adapté à la khepera3 toolbox et sauvagement débogué et reparamétré.
    Il reste quelques bêtises dans le code, mais c'est ce qui a été
    utilisé lors des premières démos (donc ça fonctionne).
*/

void init(void)
{
    khepera3_init();
    khepera3_drive_start(); 
}

/*void sigint_cb(int sig) 
{
    printf("\nInterrupted..\n");
    khepera3_drive_set_speed(0,0);
    exit(sig);
}
*/

void escape(void)
{
    int lspeed;
    int rspeed;
    /* We're trapped ! Let's find an opening */
    printf("\nescape..\n");

    lspeed=50*256;
    rspeed=-50*256;
    khepera3_drive_set_speed(lspeed,rspeed);
    /* Turn while something in front */
    do{
        usleep(50000);
        khepera3_infrared_proximity();
    } while ((khepera3.infrared_proximity.sensor[3]>50) || (khepera3.infrared_proximity.sensor[4]>50));
}

/* Adapted from k-team example */
void braitenberg()
{
    static const int Connections_A[9] = { 2,  2,  2, -2, -22, -10, -8, 2, 4}; 
    static const int Connections_B[9] = { 2, -8, -10, -22,  -2,  2,  2, 2, 4};
  
  static const int GAIN=8;
  static const int fwSpeed=25600;
  
  int i;
  int ir[9];
  int lspeed,rspeed;
  
  int immobility = 0;
  unsigned int prevpos_left, pos_left, prevpos_right,  pos_right;
  
  //signal(SIGINT,sigint_cb);
  
  printf("\nbraitenberg..\n");
  khepera3_drive_get_current_position();
  prevpos_left=khepera3.motor_left.direction*khepera3.motor_left.current_position;
  prevpos_right=khepera3.motor_right.direction*khepera3.motor_right.current_position;
  
    while(1)
    {
        khepera3_infrared_proximity();
        /* copy and truncate IR */
        /* TODO: remove high and low bits instead */
        for (i=0;i<9;i++)
        {
            ir[i]=khepera3.infrared_proximity.sensor[i];
//	    printf("%d ", ir[i]);
            if (ir[i]>1000) // higher values changes too fast, and we're close to obstacle already
                ir[i]=1000-40;
            else if (ir[i]<40) // seems to help when ambiant light do funny things
                ir[i]=0;
            else
                ir[i]=ir[i]-40;
        }
        
        lspeed=0;rspeed=0;
        for (i = 0; i < 9; i++)
        {
            lspeed+=Connections_A[i]*ir[i];
            rspeed+=Connections_B[i]*ir[i];
        }
	
	printf("\n speed = (%d, %d)\n",lspeed,rspeed);
        lspeed=lspeed*GAIN+fwSpeed;
        rspeed=rspeed*GAIN+fwSpeed;
//	printf("%d %d\n",lspeed,rspeed);
        khepera3_drive_set_speed(lspeed,rspeed);
        
        khepera3_drive_get_current_position();
        pos_left=khepera3.motor_left.direction*khepera3.motor_left.current_position;
        pos_right=khepera3.motor_right.direction*khepera3.motor_right.current_position;
        
        if ((abs(pos_left-prevpos_left)<700) && (abs(pos_right-prevpos_right)<700))
        {
            /* we don't seem to be moving a lot */
            immobility++;
	    printf("\n immobility = %d\n",immobility);
            if(immobility>5) /* we haven't been moving a lot for a while ! */
            {
                escape();
                immobility=0; /* We're free ! */
                prevpos_left = pos_left;
                prevpos_right = pos_right;
            }
        }
        else
        {
            immobility=0;
            prevpos_left=pos_left;
            prevpos_right=pos_right;
        }
        usleep(50000);
    }
}

int main()
{
    init();
    //local_remote_usage_init();
    braitenberg();
    return 0; // never reached
}
