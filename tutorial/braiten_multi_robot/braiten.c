#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include "remote_variable_server.h"
#include "khepera3.h"
#include "../../tcp_connection/remote_serveur.h"
/*  ce programme est un extrait du khepera3_test de k-team,
    adapté à la khepera3 toolbox et sauvagement débogué et reparamétré.
    Il reste quelques bêtises dans le code, mais c'est ce qui a été
    utilisé lors des premières démos (donc ça fonctionne).
*/
/* code repris pour faire un exemple de communication multirobot*/

void init(void)
{
    khepera3_init();
    khepera3_ultrasound_enable(cKhepera3SensorsUltrasoundBit_None);
    khepera3_drive_start();

}

void sigint_cb(int sig)
{
    // printf("\nInterrupted..\n");
    khepera3_drive_set_speed(0,0);
    exit(sig);
}

static void
sig_tstp(int signo) /* signal handler for SIGTSTP */
{
    sigset_t    mask;

    /* ... move cursor to lower left corner, reset tty mode ... */

    /*
     * Unblock SIGTSTP, since it's blocked while we're handling it.
     */
    khepera3_drive_stop();
    sigemptyset(&mask);
    sigaddset(&mask, SIGTSTP);
    sigprocmask(SIG_UNBLOCK, &mask, NULL);

    signal(SIGTSTP, SIG_DFL);   /* reset disposition to default */

    kill(getpid(), SIGTSTP);    /* and send the signal to ourself */

    /* we won't return from the kill until we're continued */

    signal(SIGTSTP, sig_tstp);  /* reestablish signal handler */
    khepera3_drive_start();
    /* ... reset tty mode, redraw screen ... */
}
void escape(void)
{
    int lspeed;
    int rspeed;
    /* We're trapped ! Let's find an opening */

    lspeed=50*256;
    rspeed=-50*256;
    khepera3_drive_set_speed(lspeed,rspeed);
    /* Turn while something in front */
    do {
        usleep(50000);
        khepera3_infrared_proximity();
    } while ((khepera3.infrared_proximity.sensor[3]>50) || (khepera3.infrared_proximity.sensor[4]>50));
}

/* Adapted from k-team example */

void braitenberg()
{
    static const int Connections_A[9] = { 2,  2,  2, -2, -22, -10, -8, 2, 4};
    static const int Connections_B[9] = { 2, -8, -10, -22,  -2,  2,  2, 2, 4};

    static const int GAIN=8;
    static const int fwSpeed=25600/6;

    int i;
    int ir[9];
    int j=0;
    int lspeed,rspeed;

    int immobility = 0;
    unsigned int prevpos_left, pos_left, prevpos_right,  pos_right;
    remoteserver *s;
    client_remote_var *cli;
    signal(SIGINT,sigint_cb);
    signal(SIGKILL,sigint_cb);
    signal(SIGTSTP, sig_tstp);
    khepera3_drive_get_current_position();
    prevpos_left=khepera3.motor_left.direction*khepera3.motor_left.current_position;
    prevpos_right=khepera3.motor_right.direction*khepera3.motor_right.current_position;
   // s = new_remoteserver("localhost",3000,0);
	cli=new_client_remote_var("localhost",3003);
	client_launch_thread(cli);
	client_wait_ready(cli);
   // open_connection(s);
    //while (!is_open(s));
remote_server_variable_set_int_var(cli,"block",0);
    //send_command(s,"$SET_VAR,0,block,0\n");
    while (1)
    {
        khepera3_infrared_proximity();
        /* copy and truncate IR */
        /* TODO: remove high and low bits instead */
        for (i=0;i<9;i++)
        {
            ir[i]=khepera3.infrared_proximity.sensor[i];
            if (ir[i]>1700)
            {
                remote_server_variable_set_int_var(cli,"block",1);
		remote_variable_server_broadcast(cli,"block");
//                send_command(s,"$BROADCAST,block\n");

            }

            if (ir[i]>1000) // higher values changes too fast, and we're close to obstacle already
                ir[i]=1000-40;
            else if (ir[i]<40) // seems to help when ambiant light do funny things
                ir[i]=0;
            else
                ir[i]=ir[i]-40;
        }

        lspeed=0;
        rspeed=0;
        for (i = 0; i < 9; i++)
        {
            lspeed+=Connections_A[i]*ir[i];
            rspeed+=Connections_B[i]*ir[i];
        }

        lspeed=lspeed*GAIN+fwSpeed;
        rspeed=rspeed*GAIN+fwSpeed;
        if (j++==10)
        {
            j=0;
            if ((remote_server_variable_get_int_var(cli,"block")==1))
            {
                lspeed=0;
                rspeed=0;
                khepera3_drive_set_speed(lspeed,rspeed);

                exit(0);

            }
        }
        khepera3_drive_set_speed(lspeed,rspeed);


        usleep(20000);
    }
}

int main()
{
    init();
    braitenberg();
    return 0; // never reached
}
