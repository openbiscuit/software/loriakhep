#include <math.h>
#include "khepera3.h"
#include "odometry_track.h"
#include "odometry_goto.h"

struct sOdometryTrack ot;
struct sOdometryGoto og;

void init()
{
    khepera3_init();
    odometry_track_init();
    odometry_goto_init();

    odometry_track_start(&ot);
    odometry_goto_start(&og, &ot);
    khepera3_drive_start();
}

void run_goto_position(float x, float y) {

	odometry_goto_set_goal(&og, x, y);

	// Move until we have reached the target position
	while (og.result.atgoal == 0) 
        {
		odometry_track_step(og.track);
		odometry_goto_step(&og);
		khepera3_drive_set_speed(og.result.speed_left, og.result.speed_right);
	}
}

void run_goto_heading(float goal_theta) {
	float diff_theta;

	// Move until we have reached the target position
	while (1) {

		odometry_track_step(og.track);

		// Calculate the current heading error
		diff_theta = goal_theta - og.track->result.theta;
		while (diff_theta > M_PI) {
			diff_theta -= 2 * M_PI;
		}
		while (diff_theta < -M_PI) {
			diff_theta += 2 * M_PI;
		}

		// Termination condition
		if (fabs(diff_theta) < 0.01) {
			break;
		}

		// Set speed
		khepera3_drive_set_speed_differential_bounded(og.configuration.speed_max, 0, 0, diff_theta * 8., 1);
	}
	// Stop the motors
	khepera3_drive_set_speed(0, 0);
}

int main(int argc, char *argv[]) 
{
    init();
    run_goto_heading(1.57);
    run_goto_position(0.5,0.5);
    
    return 0;
}
