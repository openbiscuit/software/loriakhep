#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#include "khepera3.h"

#define IR_GAIN 4
#define SPEED_FW 5000

void sigint_cb(int sig) {
        printf("\nInterrupted..\n");
        khepera3_drive_set_speed(0,0);
        exit(sig);
}

void init(void)
{
    khepera3_init();
    khepera3_drive_start(); 
}

void goto_sync(int left,int right)
{
    int moving=1;
    khepera3_drive_goto_position_using_profile(left,right);
    while (moving)
    {
        khepera3_drive_get_current_position();
        if ((abs(khepera3.motor_left.direction*khepera3.motor_left.current_position-left)<100)
             &&
             (abs(khepera3.motor_right.direction*khepera3.motor_right.current_position-right)<100))
            moving=0;
        usleep(20000);
    }
}

void scan_obstacle(void)
{
    int ir_left,ir_right,ir_diff,ir_mean;
    int speed_left,speed_right;
    int speed_fw;
    int dist_front;
    int i;
    

    khepera3_drive_set_current_position(0,0);
    
    for (i=0;i<100;i++)
    {
        khepera3_infrared_proximity();
        ir_left=khepera3.infrared_proximity.sensor[3];
        ir_right=khepera3.infrared_proximity.sensor[4];
        printf("%f %d %d\n",i*10.0*0.047,ir_left,ir_right);
        goto_sync(-i*100,-i*100);
        usleep(200000);
    }
}

int main()
{
    init();
    scan_obstacle();    
    khepera3_drive_set_speed(0,0);
    return 0;
}
