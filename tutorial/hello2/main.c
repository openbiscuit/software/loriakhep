
 #include "khepera3.h"
 #include <stdio.h>

 int main()
 {
    khepera3_init();
    khepera3_drive_start();
    khepera3_dspic_firmware_version();
    printf("Hello world, I'm a khepera, ready to go\n");
    printf(" Firmware version: %d.%d\n",khepera3.dspic.firmware_version, khepera3.dspic.firmware_revision);
    return 0;
 }
