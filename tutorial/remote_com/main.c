#include <stdio.h>
#include <stdlib.h>
#include "../../tcp_connection/remote_serveur.h"


int main(int argc,char *argv[])
{
	remoteserver *s;
	int intarray[10];
	double doublearray[10];
	if(argc<2)
	{printf("usage : ./remote_com [X] \n X=0 for broacaster behaviour, X=1 for reader behaviour");
	exit(-1);}
	else
	{
		if(atoi(argv[1])==0)
		{
			s = new_remoteserver("localhost",3000,0);
			open_connection(s);
			while(!is_open(s))usleep(50000);
			int i,j;
			i=0;
			while(1)
			{
				remote_set_int_var(s,"INTVAR",i);
				remote_broadcast(s,"INTVAR");
				remote_set_double_var(s,"DOUBLEVAR",(double)i);
				remote_broadcast(s,"DOUBLEVAR");
				for(j=0;j<10;j++)
				{
					intarray[j]=i+j;
					doublearray[j]=(double)(i+j) + (0.1)*((double) j);
				}
				remote_set_int_array(s,"INTARRAY",10,intarray);
				remote_broadcast(s,"INTARRAY");
				remote_set_double_array(s,"DOUBLEARRAY",10,doublearray);
			
			
			
				remote_broadcast(s,"DOUBLEARRAY");
				i++;
				usleep(100000);
			}
		}
		else
		{
			s = new_remoteserver("localhost",3001,0);
			open_connection(s);
			while(!is_open(s))usleep(50000);
			int i,j;
			i=0;
			while(1)
			{
				int *arrayi;
				double *arrayd;
				int sizearrayint;
				int sizearraydouble;
				printf("intvar : %d\n doublevar : %f\n",remote_server_get_int_var(s,"INTVAR"),remote_server_get_double_var(s,"DOUBLEVAR"));
				remote_server_get_double_array(s,"DOUBLEARRAY",&sizearraydouble,&arrayd);
				remote_server_get_int_array(s,"INTARRAY",&sizearrayint,&arrayi);
				printf("arrayint: ");
				for(j=0;j<sizearrayint;j++)
				{
					printf("%d\t",arrayi[j]);
				}
				printf("\n");
				printf("arraydouble: ");
				for(j=0;j<sizearraydouble;j++)
				{
					printf("%f\t",arrayd[j]);
				}
				printf("\n");


				usleep(100000);
			}
			
		}
	}
}