#ifndef GST_GDK_UTILS_H
#define GST_GDK_UTILS_H

#include <gst/gst.h>
#include <gst/base/gstbasesink.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

GdkPixbuf *gst_to_gdkpixbuf(GstBuffer *buffer);

#endif


