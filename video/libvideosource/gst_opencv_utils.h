#ifndef GST_OPENCV_UTILS_H
#define GST_OPENCV_UTILS_H

#include <cv.h>
#include <gst/gst.h>
#include <gst/base/gstbasesink.h>

IplImage *gst_to_ipl(GstBuffer *buffer);
#endif


