#ifndef SIMPLE_VIEWER_H
#define SIMPLE_VIEWER_H

#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gtk/gtk.h>

/* the struct members should NEVER be accessed directly
   (may cause the video thread in the background to crash)
*/
typedef struct{
    GtkWidget *window;
    GtkWidget *drawing_area;
    GThread *th_video;
    GdkPixbuf *buf; /* current buffer to draw (cached in case of slow refresh) */
    int w,h;
} SimpleViewer;

/* initialize a SimpleViewer of size w*h */
void simple_viewer_init(SimpleViewer *v,int w,int h);

/* Queue a new pixmap to draw */
void simple_viewer_draw_pixbuf(SimpleViewer *v,GdkPixbuf *buf);

#endif
