#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gtk/gtk.h>

#include "simple_viewer.h"

static void quit(GtkWidget *widget,gpointer data )
{
    gtk_main_quit ();
}

static gboolean expose_event_callback (GtkWidget *widget, GdkEventExpose *event, gpointer data)
{
    SimpleViewer *v=(SimpleViewer*)data;
    gdk_draw_arc(widget->window,
                widget->style->fg_gc[GTK_WIDGET_STATE (widget)],
                TRUE,
                0, 0, widget->allocation.width, widget->allocation.height,
                0, 64 * 360); 

    if (v->buf!=NULL)
        gdk_draw_pixbuf(widget->window,widget->style->fg_gc[GTK_WIDGET_STATE (widget)],
                        v->buf,
                        0,0,
                        0,0,-1,-1,
                        GDK_RGB_DITHER_NONE,0,0);
  return TRUE;
}

static gpointer video_thread(gpointer data)
{
    
    gdk_threads_enter();
    SimpleViewer *v=(SimpleViewer*)data;

    gtk_init(NULL,NULL);
    v->window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
    g_signal_connect (G_OBJECT(v->window), "delete_event",
		      G_CALLBACK(quit), NULL);

    gtk_container_set_border_width(GTK_CONTAINER(v->window),10);

    v->drawing_area=gtk_drawing_area_new();
    gtk_widget_set_size_request(v->drawing_area,v->w,v->h);
    g_signal_connect(G_OBJECT(v->drawing_area),"expose_event",
                    G_CALLBACK(expose_event_callback),data);

    gtk_container_add(GTK_CONTAINER(v->window),v->drawing_area);
    gtk_widget_show(v->drawing_area);
    gtk_widget_show(v->window);
    gtk_main();
    gdk_threads_leave ();
    return NULL;
}

void simple_viewer_init(SimpleViewer *v,int w,int h)
{
    GError *err=NULL;
    if (!g_thread_supported()) g_thread_init(NULL);
    gdk_threads_init();

    v->drawing_area=NULL;
    v->window=NULL;
    v->buf=NULL;
    v->w=w;
    v->h=h;
    v->th_video=g_thread_create(video_thread,v,TRUE,&err);
}

void simple_viewer_draw_pixbuf(SimpleViewer *v,GdkPixbuf *buf)
{
    GError *err=NULL;
    
    gdk_threads_enter();
    if (v->buf!=NULL)
        gdk_pixbuf_unref(v->buf);
    gdk_pixbuf_ref(buf);
    v->buf=buf;
    if (v->drawing_area!=NULL)
        gdk_window_invalidate_rect(v->drawing_area->window,NULL,FALSE); 
    gdk_threads_leave();
}

