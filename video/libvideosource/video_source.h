#ifndef VIDEO_SOURCE_H
#define VIDEO_SOURCE_H

#include <gst/gst.h>

typedef struct {
    const char * pipeline_src;  /* user defined  */
    gchar * pipeline;           /* whole pipeline */
    int w;                  /* width */
    int h;                  /* height */
    const gchar * mode;           /* things like video/x-raw-rgb */
    GThread *th_video;
    GstElement *sink; /* the end of the pipe (where we get images from) */
    GstElement *pipe; /* the pipeline */
    GMainLoop *loop;  /* Glib mainloop */
    gboolean ready;

    GCond* cond; /* used for simple sync things between the threads */
                 /* e.g. waiting for a new image to appear,... */
    GMutex* mutex;

    unsigned int buffer_id; /* warning, can overflow after 3 years (at 25 fps)
                            // or earlier (for bigger fps) obviously
                            // could use gst timestamps instead, but unclear
                            // and presumably src-dependant (time, index, frame nb ???...)
                            */
} VideoSource;

GstBuffer * vs_get_image(VideoSource *vs);
GstBuffer * vs_get_next_image(VideoSource *vs,unsigned int id,unsigned int *returned_id);
void vs_init(VideoSource *vs, const char * pipeline,int w,int h, const char * mode);
/* TODO: change name ? does not free.. */
void vs_free(VideoSource *vs);
void vs_play(VideoSource *vs);
void vs_pause(VideoSource *vs);

#endif
