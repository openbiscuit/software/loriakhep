#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <gst/gst.h>
#include <gst/base/gstbasesink.h>

#include "video_source.h"
/* TODO: utiliser appsink
// TODO: refcount (valgrind)
// TODO: err checking
// TODO: ajouter push_message pour quitter, pause,...
// TODO: tester avec cam, ...
// TODO: capture gray, rgb, pixbuf...
// TODO: gst to gdkpixbuf
// TODO: gst to RGB
*/

static gboolean my_bus_callback (GstBus *bus,GstMessage *message,gpointer data)
{
    VideoSource *vs=(VideoSource*)data;
    
    switch (GST_MESSAGE_TYPE (message)) {
        case GST_MESSAGE_STATE_CHANGED: {
            GstState oldstate;
            GstState newstate;
            GstState pending;
            gst_message_parse_state_changed(message,&oldstate,&newstate,&pending);
            /* looking for sink to be playing (probably useless...) */
//             if ((newstate==4)&&(message->src==(GstObject*)vs->sink))
//                 {
//                     vs->ready=TRUE;
//                 }
            break;
        }
#ifdef DEBUG
        case GST_MESSAGE_WARNING: {
            GError *err=NULL;
            gchar *debug;
        
            gst_message_parse_warning (message, &err, &debug);
            g_print ("Warning: %s %s\n", err->message,debug);
             g_error_free (err);
            g_free (debug);
            break;
        }
#endif
        case GST_MESSAGE_ERROR: {
            GError *err=NULL;
            gchar *debug;
        
            gst_message_parse_error (message, &err, &debug);
            g_print ("Error: %s\n", err->message);
            g_error_free (err);
            g_free (debug);
        
            g_main_loop_quit (vs->loop);
        break;
        }
        case GST_MESSAGE_EOS:
            /* end-of-stream */
            g_main_loop_quit(vs->loop);
        break;
        default:
        /* unhandled message */
        break;
    }
    
    /* we want to be notified again the next time there is a message
    * on the bus, so returning TRUE (FALSE means we want to stop watching
    * for messages on the bus and our callback should not be called again)
    */
    return TRUE;
}

static void cb_handoff(GstElement *fakesink,GstBuffer *buffer,GstPad *pad,gpointer data)
{
  VideoSource *vs=(VideoSource*)data;
  g_mutex_lock(vs->mutex);
/* TODO: debug, test if truly a new one ? (compare pointers ? :-( ) */
  vs->buffer_id++;
  g_cond_signal(vs->cond);
  g_mutex_unlock(vs->mutex);
} 

static void error(GError *err)
{
    fprintf(stderr," *** VideoSource Error \n");
    if (err!=NULL)
    {
        fprintf (stderr, "GError says: %s\n", err->message);
        g_error_free(err);
    }
    exit(EXIT_FAILURE);
}

static gpointer video_thread(gpointer data)
/* Main video thread. 
   data: the VideoSource that knows everything
*/
{
  VideoSource *vs=(VideoSource*)data;
  GError *err=NULL;  
  GstBus *bus;

  g_mutex_lock(vs->mutex);
  gst_init(NULL,NULL);
  
  vs->pipe=gst_parse_launch(vs->pipeline, &err);
  if (vs->pipe==NULL)
      error(err);
   
  vs->sink=gst_bin_get_by_name(GST_BIN(vs->pipe),"sink");
  if (vs->sink==NULL)
      error(err);
  bus=gst_pipeline_get_bus(GST_PIPELINE (vs->pipe));
  gst_bus_add_watch(bus,my_bus_callback,vs);
  gst_object_unref(bus);

  /* callbacks
  // we want to get notified when a new buffer is received
  */
  g_object_set(vs->sink,"signal-handoffs",TRUE,NULL);
  g_signal_connect(vs->sink,"handoff",G_CALLBACK(&cb_handoff),vs);

  vs->loop=g_main_loop_new(NULL,FALSE);
  /* signal the other thread */
  vs->ready=TRUE;
  g_cond_signal(vs->cond);
  g_mutex_unlock(vs->mutex);
  g_main_loop_run(vs->loop);
  return NULL;
}

GstBuffer * vs_get_image(VideoSource *vs)
/* Returns: the last buffer. Can be NULL, can return the same buffer multiple times
// The call MUST unref() the buffer after use.
// Non blocking, should return quickly.
*/
{
    return gst_base_sink_get_last_buffer(GST_BASE_SINK(vs->sink));
}

GstBuffer * vs_get_next_image(VideoSource *vs,unsigned int id, unsigned int *returned_id)
/* Returns: the next available buffer > id
// The call MUST unref() the buffer after use.
// May block until the buffer comes up.
*/
{
    GstBuffer *buf;
    g_mutex_lock(vs->mutex);
    while (vs->buffer_id<id)
      g_cond_wait(vs->cond,vs->mutex);
    buf=gst_base_sink_get_last_buffer(GST_BASE_SINK(vs->sink));
    if (returned_id) 
        *returned_id=vs->buffer_id;
    g_mutex_unlock(vs->mutex);

    return buf;
}

void vs_init(VideoSource *vs, const char * pipeline_src,int w,int h,const char * mode)
{
    GError *err=NULL;
    gchar *s;
    if (!g_thread_supported()) g_thread_init(NULL);
    vs->ready=FALSE;
    vs->cond=g_cond_new();
    vs->mutex=g_mutex_new();
    vs->buffer_id=0;
    vs->pipeline_src=pipeline_src;
    vs->w=w;
    vs->h=h;
    if (mode==NULL)
        vs->mode="video/x-raw-rgb";
    else
        vs->mode=mode;
/*    s=g_strdup_printf(" ! decodebin ! tee name=T ! ffmpegcolorspace ! videoscale ! %s,width=%d,height=%d ! fakesink name=sink T. ! queue ! ffmpegcolorspace ! videoscale ! video/x-raw-yuv,width=%d,height=%d ! xvimagesink sync=false",vs->mode,vs->w,vs->h,vs->w,vs->h);*/
    s=g_strdup_printf(" ! decodebin ! ffmpegcolorspace ! videoscale ! %s,width=%d,height=%d ! fakesink name=sink",vs->mode,vs->w,vs->h);
    
    vs->pipeline=g_strconcat(pipeline_src,s,NULL);
    free(s);
    vs->th_video=g_thread_create(video_thread,vs,TRUE,&err);
    
    /* wait for every object to be created in the other thread
    // could be done differently by i'm lazy
    // we could also create everything here, and only run the loop
    // in the other thread - but we'll need cond&mutex later so...
    */
    g_mutex_lock(vs->mutex);
    while (vs->ready==FALSE)
        g_cond_wait(vs->cond,vs->mutex);
    g_mutex_unlock(vs->mutex);
}

void vs_free(VideoSource *vs)
{
    // TODO: check if main_loop_quit is thread safe here
    g_main_loop_quit(vs->loop);
    g_thread_join(vs->th_video);
    free(vs->pipeline);
}

void vs_play(VideoSource *vs)
{
    gst_element_set_state (vs->pipe, GST_STATE_PLAYING);
}

void vs_pause(VideoSource *vs)
{
    gst_element_set_state (vs->pipe, GST_STATE_PAUSED);
}
