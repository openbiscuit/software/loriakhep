#include "videosource.h"

int main(int argc, char **argv)
{
    GstBuffer *buf;
    VideoSource vs;
    unsigned int id_wanted;
    unsigned int id_returned;

    // Pour utiliser un fichier mpeg en entrée:
    // vs_init(&vs,"filesrc location=\"test.mpg\"",320,240,NULL);
    // Pour du video4linux v2
    //  vs_init(&vs,"v4l2src",640,480,NULL);
    // Pour une caméra des kheperas:
    vs_init(&vs,"rtspsrc location=rtsp://korwlcam2.loria.fr/live.sdp latency=0 ! rtpmp4vdepay  ! ffdec_mpeg4",640,480,NULL);
    
    vs_play(&vs);
    id_wanted=0;
 
    while(1)
    {
        // on return, buf will be a gstreamer buffer (or NULL)
        // id_returned is the frame counter corresponding to that buffer
        // with id_returned>=id_wanted
        // This function blocks if the currently available buffer has id <id_wanted
        buf=vs_get_next_image(&vs,id_wanted,&id_returned);
        // this will ensure we don't work twice on the same image
        id_wanted=id_returned+1;
        printf("Got image from video thread %p %d %lld id: %d\n",(void*)buf, buf==NULL?0:buf->size, buf==NULL?0:buf->timestamp,id_returned);
	if (buf) gst_buffer_unref(buf);
    }
    vs_free(&vs);
    return EXIT_SUCCESS;
}
