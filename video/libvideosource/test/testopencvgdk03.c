#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <gst/gst.h>

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#include <time.h>
#include <ctype.h>

#include <cv.h>
#include "videosource.h"

/*
 Exemple de détection de visage (interaction gstreamer, opencv et affichage avec gdk.
*/

/* Une partie du code ci-dessous est extrait des exemples d'opencv, ne pas diffuser sans
   vérifier la licence !
*/

// Create a string that contains the exact cascade name
const char* cascade_name =
    "haarcascade_frontalface_alt.xml";

// Function to detect and draw any faces that is present in an image
void detect_and_draw( IplImage* img )
{
    printf("Detecting...\n");
    // Create memory for calculations
    static CvMemStorage* storage = 0;

    // Create a new Haar classifier
    static CvHaarClassifierCascade* cascade = 0;

    int scale = 1;

    // Create a new image based on the input image
    IplImage* temp = cvCreateImage( cvSize(img->width/scale,img->height/scale), 8, 3 );

    // Create two points to represent the face locations
    CvPoint pt1, pt2;
    int i;

    // Load the HaarClassifierCascade
    cascade = (CvHaarClassifierCascade*)cvLoad( cascade_name, 0, 0, 0 );
    
    // Check whether the cascade has loaded successfully. Else report and error and quit
    if( !cascade )
    {
        fprintf( stderr, "ERROR: Could not load classifier cascade\n" );
        return;
    }
    
    // Allocate the memory storage
    storage = cvCreateMemStorage(0);

    // Clear the memory storage which was used before
    cvClearMemStorage( storage );

    // Find whether the cascade is loaded, to find the faces. If yes, then:
    if( cascade )
    {

        // There can be more than one face in an image. So create a growable sequence of faces.
        // Detect the objects and store them in the sequence
        CvSeq* faces = cvHaarDetectObjects( img, cascade, storage,
                                            1.1, 2, CV_HAAR_DO_CANNY_PRUNING,
                                            cvSize(40, 40) );

        // Loop the number of faces found.
        for( i = 0; i < (faces ? faces->total : 0); i++ )
        {
           // Create a new rectangle for drawing the face
            CvRect* r = (CvRect*)cvGetSeqElem( faces, i );

            // Find the dimensions of the face,and scale it if necessary
            pt1.x = r->x*scale;
            pt2.x = (r->x+r->width)*scale;
            pt1.y = r->y*scale;
            pt2.y = (r->y+r->height)*scale;

            // Draw the rectangle in the input image
            cvRectangle( img, pt1, pt2, CV_RGB(255,0,0), 3, 8, 0 );
        }
    }

    // Release the temp image created.
    cvReleaseImage( &temp );
}


int main(int argc, char **argv)
{
    GstBuffer *buf;
    VideoSource vs;
    SimpleViewer view;
    IplImage * frame;
    GdkPixbuf * pixbuf;
    GError *error;
    int i;
    unsigned int id_wanted;
    unsigned int id_returned;

    simple_viewer_init(&view,640,480);

    // Pour du video4linux v2
    // On force ici un type d'image précis (récupéré dans le GstBuffer ci dessous) pour faciliter les conversions
    // vers les images de chez openCV
    vs_init(&vs,"v4l2src ! video/x-raw-yuv,framerate=25/1,width=640,height=480 ",640,480,"video/x-raw-rgb,depth=32,bpp=32");
    
    vs_play(&vs);
    id_wanted=0;
    frame=NULL;
    while(1)
    {
        // on return, buf will be a gstreamer buffer (or NULL)
        // id_returned is the frame counter corresponding to that buffer
        // with id_returned>=id_wanted
        // This function blocks if the currently available buffer has id <id_wanted
        buf=vs_get_next_image(&vs,id_wanted,&id_returned);
        // this will ensure we don't work twice on the same image
        id_wanted=id_returned+1;
        printf("Got image from video thread %p %d %lld id: %d\n",(void*)buf, buf==NULL?0:buf->size, buf==NULL?0:buf->timestamp,id_returned);
        if (buf)
        {
             frame=gst_to_ipl(buf); // frame is now an OpenCV image
             detect_and_draw(frame); // face detection (from opencv demo)
             pixbuf=ipl_to_gdkpixbuf(frame); // Pixbuf is a GdkPixpuf image
             simple_viewer_draw_pixbuf(&view,pixbuf); // ... that one can draw
            /* cleanup */
             gdk_pixbuf_unref(pixbuf);
             gst_buffer_unref(buf);
             cvReleaseImage(&frame);
        }
    }
    vs_free(&vs);
    return EXIT_SUCCESS;
}
