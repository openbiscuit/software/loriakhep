#include "videosource.h"

/*
  same as test01, + show how to plug a complex pipeline (inc. tee and secondary display)
*/
int main(int argc, char **argv)
{
    GstBuffer *buf;
    VideoSource vs;
    unsigned int id_wanted;
    unsigned int id_returned;

    vs_init(&vs,"v4l2src ! tee name=T ! queue ! ffmpegcolorspace ! videoscale ! video/x-raw-yuv,width=320,height=240 ! xvimagesink sync=false T. ! queue ",640,480,NULL);
    vs_play(&vs);
    id_wanted=0;
 
    while(1)
    {
        // on return, buf will be a gstreamer buffer (or NULL)
        // id_returned is the frame counter corresponding to that buffer
        // with id_returned>=id_wanted
        // This function blocks if the currently available buffer has id <id_wanted
        buf=vs_get_next_image(&vs,id_wanted,&id_returned);
        // this will ensure we don't work twice on the same image
        id_wanted=id_returned+1;
        printf("Got image from video thread %p %d %lld id: %d\n",(void*)buf, buf==NULL?0:buf->size, buf==NULL?0:buf->timestamp,id_returned);
	if (buf) gst_buffer_unref(buf);
    }
    vs_free(&vs);
    return EXIT_SUCCESS;
}
