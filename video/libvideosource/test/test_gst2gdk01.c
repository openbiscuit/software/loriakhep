#include "videosource.h"

int main(int argc, char **argv)
{
    GstBuffer *buf;
    VideoSource vs;
    unsigned int id_wanted;
    unsigned int id_returned;
    GdkPixbuf * pixbuf;
    GError *err=NULL;
    vs_init(&vs,"v4l2src",640,480,"video/x-raw-rgb,depth=32,bpp=32");
    vs_play(&vs);
    id_wanted=0;
    do
    {
        buf=vs_get_next_image(&vs,id_wanted,&id_returned);
        id_wanted=id_returned+1;
        printf("Got image from video thread %p %d %lld id: %d\n",(void*)buf, buf==NULL?0:buf->size, buf==NULL?0:buf->timestamp,id_returned);
	
    } while (buf==NULL);
    pixbuf=gst_to_gdkpixbuf(buf);
    gdk_pixbuf_save(pixbuf,"test.jpg","jpeg", &err, NULL);

    gdk_pixbuf_unref(pixbuf);
    gst_buffer_unref(buf);
    vs_free(&vs);
    return EXIT_SUCCESS;
}
