#ifndef GDK_OPENCV_UTILS_H
#define GDK_OPENCV_UTILS_H

#include <cv.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

GdkPixbuf *ipl_to_gdkpixbuf(IplImage *frame);

#endif
