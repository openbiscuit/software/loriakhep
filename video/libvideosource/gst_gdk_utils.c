#include <gst/gst.h>
#include <gst/base/gstbasesink.h>
#include <unistd.h>
#include "gst_gdk_utils.h"

GdkPixbuf * gst_to_gdkpixbuf(GstBuffer *buffer)
{
	GdkPixbuf *buf;
	GstCaps* caps = gst_buffer_get_caps(buffer);
	
	GstStructure* structure = gst_caps_get_structure(caps, 0);
	gint bpp, endianness, redmask, greenmask, bluemask,depth;
	gint height, width;

	if(!gst_structure_get_int(structure, "bpp", &bpp) ||
	   !gst_structure_get_int(structure, "depth", &depth) ||
	   !gst_structure_get_int(structure, "endianness", &endianness) ||
	   !gst_structure_get_int(structure, "red_mask", &redmask) ||
	   !gst_structure_get_int(structure, "green_mask", &greenmask) ||
	   !gst_structure_get_int(structure, "blue_mask", &bluemask)) {
		printf("missing essential information in buffer caps, %s\n", gst_caps_to_string(caps));
		return NULL;
	}

	if ((bpp!=32)||(depth!=32))
	{
	    printf(" *** Can't convert buffer to gdkbuffer: bpp!=32 or depth!=32\n");
	    return 0;
	}
	
        if(!gst_structure_get_int(structure, "width", &width) ||
           !gst_structure_get_int(structure, "height", &height))
	  {
	    printf(" *** Can't convert buffer to gdkbuffer: w=0 or h=0\n");
	    return NULL;
	  }
	gst_caps_unref(caps);

        buf=gdk_pixbuf_new(GDK_COLORSPACE_RGB,FALSE,8,width,height);

// In the following we assume:
// - IplImage uses BGR, 24 bpp
// - GST is 32 bpp, endianess=1234
	unsigned char * restrict data = GST_BUFFER_DATA(buffer);
	unsigned nbyte = 4; // bpp>>3;
	guchar * dest_pixels=gdk_pixbuf_get_pixels(buf);
        int dest_rowstride=gdk_pixbuf_get_rowstride(buf);
	for(int r=0;r<height;r++) 
	  {
	    guchar* restrict dest_line=dest_pixels+dest_rowstride*r;
	    for(int c=0;c<width;c++) 
	      {
		dest_line[0]=data[0+1];
		dest_line[1]=data[1+1];
		dest_line[2]=data[2+1];
		dest_line+=3;
		data+=nbyte;
	      }
	}
	return buf;
}
