#ifndef VIDEOSOURCE_H
#define VIDEOSOURCE_H

#include "video_source.h"
#include "gst_opencv_utils.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "video_sink.h"
#include "gdk_opencv_utils.h"
#include "simple_viewer.h"
#include "gst_gdk_utils.h"

#ifdef __cplusplus
}
#endif

#endif
