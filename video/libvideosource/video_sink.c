#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <gst/gst.h>
#include <gst/base/gstbasesink.h>
#include <gst/app/gstappsrc.h>
#include "video_sink.h"

static gpointer video_thread(gpointer data)
/* Main video thread. 
   data: the VideoSink that knows everything
*/
{
  VideoSink *vs=(VideoSink*)data;
  GError *err=NULL;
  GstCaps *caps;

  g_mutex_lock(vs->mutex);
  gst_init(NULL,NULL);
  
  vs->pipe=gst_parse_launch(vs->pipeline, &err);
  vs->appsrc=gst_bin_get_by_name(GST_BIN(vs->pipe),"appsrc");
  
  caps=gst_caps_new_simple("video/x-raw-rgb",
                            "width",G_TYPE_INT,320,
                            "height",G_TYPE_INT,240,
                            NULL);
  gst_app_src_set_caps(GST_APP_SRC(vs->appsrc),caps);
/* TODO unref caps */

  gst_caps_unref(caps);
  vs->loop=g_main_loop_new(NULL,FALSE);
  /* signal the other thread */
  vs->ready=TRUE;
  g_cond_signal(vs->cond);
  g_mutex_unlock(vs->mutex);
  g_main_loop_run(vs->loop);
  return NULL;
}

void video_sink_init(VideoSink *vs, const char * pipeline_src,int w,int h,const char * mode)
{
    GError *err=NULL;
    gchar *s;
    if (!g_thread_supported()) g_thread_init(NULL);
    vs->ready=FALSE;
    vs->cond=g_cond_new();
    vs->mutex=g_mutex_new();
    vs->pipeline_src=pipeline_src;
    vs->w=w;
    vs->h=h;
    if (mode==NULL)
        vs->mode="video/x-raw-rgb";
    else
        vs->mode=mode;
    s=g_strdup_printf("appsrc name=\"appsrc\" ! queue ! ffmpegcolorspace ! videoscale ! %s,width=%d,height=%d ! xvimagesink sync=false",vs->mode,vs->w,vs->h);
    vs->pipeline=g_strconcat(s,pipeline_src,NULL);
    free(s);
    vs->th_video=g_thread_create(video_thread,vs,TRUE,&err);
    printf("%s\n",vs->pipeline);
    /* wait for every object to be created in the other thread
    // could be done differently by i'm lazy
    // we could also create everything here, and only run the loop
    // in the other thread - but we'll need cond&mutex later so...
    */
    g_mutex_lock(vs->mutex);
    while (vs->ready==FALSE)
        g_cond_wait(vs->cond,vs->mutex);
    g_mutex_unlock(vs->mutex);
    printf("Ready.\n");
}

void video_sink_free(VideoSink *vs)
{
    g_thread_join(vs->th_video);
    free(vs->pipeline);
}

void video_sink_play(VideoSink *vs)
{

    gst_element_set_state(vs->pipe,GST_STATE_PLAYING);
    printf("Playing now\n");
}

void video_sink_pause(VideoSink *vs)
{
    gst_element_set_state(vs->pipe,GST_STATE_PAUSED);
}

void video_sink_push(VideoSink * vs)
{
    GstBuffer *buf;
    int r;
/* TODO: also try pad_alloc_buffer: optimized ? */
    buf=gst_buffer_new_and_alloc(320*240*32);
    gst_app_src_push_buffer(GST_APP_SRC(vs->appsrc),buf);
    
}
