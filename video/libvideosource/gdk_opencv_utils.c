#include <cv.h>
#include <cv.hpp>
#include <cvaux.hpp>

#include <gdk-pixbuf/gdk-pixbuf.h>

#include "gdk_opencv_utils.h"

GdkPixbuf *ipl_to_gdkpixbuf(IplImage *frame)
{
	GdkPixbuf *buf;
        int i,j;
        int dest_rowstride, src_rowstride;
        guchar *dest_pixels;
        guchar *src_pixels;
        int w,h;

        buf=gdk_pixbuf_new(GDK_COLORSPACE_RGB,FALSE,8,frame->width,frame->height);


    /* Stupid BGR-only opencv, stupid RGB-only gdk-pixbuf... */
        w=gdk_pixbuf_get_width(buf);
        h=gdk_pixbuf_get_height(buf);
        dest_pixels=gdk_pixbuf_get_pixels(buf);
        dest_rowstride=gdk_pixbuf_get_rowstride(buf);
        src_pixels=frame->imageData;
        src_rowstride=frame->widthStep;

	if (frame->nChannels==3)
	{
        for (j=0;j<h;j++)
            {
            gchar* dest_line=dest_pixels+dest_rowstride*j;
            gchar* src_line=src_pixels+src_rowstride*j;
            for (i=0;i<w;i++)
                {
                    dest_line[0]=src_line[2];
                    dest_line[1]=src_line[1];
                    dest_line[2]=src_line[0];
                    dest_line+=3;
                    src_line+=3;
                }
            }
	} else if (frame->nChannels==1)
	{
	for (j=0;j<h;j++)
            {
            gchar* dest_line=dest_pixels+dest_rowstride*j;
            gchar* src_line=src_pixels+src_rowstride*j;
            for (i=0;i<w;i++)
                {
                    dest_line[0]=src_line[0];
                    dest_line[1]=src_line[0];
                    dest_line[2]=src_line[0];
                    dest_line+=3;
                    src_line+=1;
                }
            }
	} else 
	{
	   printf("Cannot convert OpenCV image: frame.nChannels=%d\n (should be 1 or 3)\n",frame->nChannels);
	   exit(EXIT_FAILURE);
	}

	return buf;
}
