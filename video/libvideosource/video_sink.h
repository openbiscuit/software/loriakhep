#ifndef VIDEO_SINK_H
#define VIDEO_SINK_H

typedef struct {
    const char * pipeline_src;  /* user defined  */
    gchar * pipeline;           /* whole pipeline */
    int w;                  /* width */
    int h;                  /* height */
    const gchar * mode;           /* things like video/x-raw-rgb */
    GThread *th_video;
    GstElement *appsrc; 
    GstElement *pipe; /* the pipeline */
    GMainLoop *loop;  /* Glib mainloop */
    gboolean ready;

    GCond* cond; /* used for simple sync things between the threads */
                 /* e.g. waiting for a new image to appear,... */
    GMutex* mutex;
} VideoSink;

void video_sink_init(VideoSink *vs, const char * pipeline,int w,int h, const char * mode);
/* TODO: change name ? does not free.. */
void video_sink_free(VideoSink *vs);
void video_sink_play(VideoSink *vs);
void video_sink_pause(VideoSink *vs);
void video_sink_push(VideoSink * vs);

#endif
