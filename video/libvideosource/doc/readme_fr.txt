libvideosource: Exemple d'utilisation de gstreamer comme source de données
video (caméras, webcams, fichiers video, streaming...)
+ exemples de conversion de et vers gdk, opencv.

Utilisation:
1) #inclure videsource.h, lier avec libvideosource.so (ou .a)

2) Construire une structure VideoSource

3) Initialiser la structure avec vs_init(source,largeur, hauteur,mode)
où:
- source est une source gstreamer (par exemple v4l2src)
- largeur, hauteur sont les dimensions des images à récupérer
- mode décrit le type de buffer souhaité (par exemple video/x-raw-rgb)
Si mode est NULL, un buffer RGB par défaut sera récupéré, à parser avec les
 fonctions de gstreamer, on ne sait pas s'il est 24 ou 32 bits, etc.

4) lancer l'acquisition: vs_play

5) récupérer des images avec vs_get_next_image(&vs,id_wanted,&id_returned);
où:
- id_wanted est l'id *minimal* souhaité
- id_returned l'adresse de l'id obtenu au final (id_returned>=id_wanted)
Par exemple pour récupérer une image sur 2:
 première image: id_wanted=0, id_returned est >=0
 2eme image: id_wanted=id_returned+2

6) convertir le buffer en image GDK, Opencv, ... ou travailler avec

7) libérer le buffer: gst_buffer_unref(buf);

8) boucler...

9) bien nettoyer en fin de prog avec vs_free(&vs);

Voir les exemples dans test/ 

