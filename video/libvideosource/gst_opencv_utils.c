// Next function originally from opencv library (svn):
/*M///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//  By downloading, copying, installing or using the software you agree to this license.
//  If you do not agree to this license, do not download, install,
//  copy or use the software.
//
//
//                        Intel License Agreement
//                For Open Source Computer Vision Library
//
// Copyright (C) 2008, Nils Hasler, all rights reserved.
// Third party copyrights are property of their respective owners.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//   * Redistribution's of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.
//
//   * Redistribution's in binary form must reproduce the above copyright notice,
//     this list of conditions and the following disclaimer in the documentation
//     and/or other materials provided with the distribution.
//
//   * The name of Intel Corporation may not be used to endorse or promote products
//     derived from this software without specific prior written permission.
//
// This software is provided by the copyright holders and contributors "as is" and
// any express or implied warranties, including, but not limited to, the implied
// warranties of merchantability and fitness for a particular purpose are disclaimed.
// In no event shall the Intel Corporation or contributors be liable for any direct,
// indirect, incidental, special, exemplary, or consequential damages
// (including, but not limited to, procurement of substitute goods or services;
// loss of use, data, or profits; or business interruption) however caused
// and on any theory of liability, whether in contract, strict liability,
// or tort (including negligence or otherwise) arising in any way out of
// the use of this software, even if advised of the possibility of such damage.
//
//M*/

// Modified by: Olivier Rochel INRIA Nancy Grand-Est

#include <cv.h>
#include <cv.hpp>
#include <cvaux.hpp>
#include <gst/gst.h>
#include <gst/base/gstbasesink.h>
#include <unistd.h>

IplImage *gst_to_ipl(GstBuffer *buffer)
{
	GstCaps* caps = gst_buffer_get_caps(buffer);
	assert(gst_caps_get_size(caps) == 1);
	GstStructure* structure = gst_caps_get_structure(caps, 0);

	gint bpp, endianness, redmask, greenmask, bluemask,depth;

	if(!gst_structure_get_int(structure, "bpp", &bpp) ||
	   !gst_structure_get_int(structure, "depth", &depth) ||
	   !gst_structure_get_int(structure, "endianness", &endianness) ||
	   !gst_structure_get_int(structure, "red_mask", &redmask) ||
	   !gst_structure_get_int(structure, "green_mask", &greenmask) ||
	   !gst_structure_get_int(structure, "blue_mask", &bluemask)) {
		printf("missing essential information in buffer caps, %s\n", gst_caps_to_string(caps));
		return 0;
	}
// 	printf("buffer has %d bpp, endianness %d, rgb %x %x %x, %s\n", bpp, endianness, redmask, greenmask, bluemask, gst_caps_to_string(caps));

	if(!redmask || !greenmask || !bluemask)
		return 0;
	if ((bpp!=32)||(depth!=32))
	{
// can only handle 32bits data (because of stupid endianess conversion below)
	    printf(" Can't convert buffer to opencv image: bpp!=32 or depth!=32\n");
	    return 0;
	}
	gint height, width;
        if(!gst_structure_get_int(structure, "width", &width) ||
            !gst_structure_get_int(structure, "height", &height))
		return 0;

        IplImage *frame=NULL;
        frame=cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 3);
	
	gst_caps_unref(caps);
// In the following we assume:
// - IplImage uses BGR, 24 bpp
// - GST is 32 bpp, endianess=1234
	unsigned char * restrict data = GST_BUFFER_DATA(buffer);
// 	unsigned char * restrict dest = frame->imageData;
	unsigned nbyte = 4; // bpp>>3;
// 	unsigned redshift, blueshift, greenshift;
// 	unsigned mask = redmask;
// 	for(redshift = 0, mask = redmask; (mask & 1) == 0; mask >>= 1, redshift++);
// 	for(greenshift = 0, mask = greenmask; (mask & 1) == 0; mask >>= 1, greenshift++);
// 	for(blueshift = 0, mask = bluemask; (mask & 1) == 0; mask >>= 1, blueshift++);

	for(int r = 0; r < frame->height; r++) {
		unsigned char * restrict dest=frame->imageData+r*frame->widthStep;
		for(int c = 0; c < frame->width; c++) {
			dest[0]=data[3];
			dest[1]=data[2];
			dest[2]=data[1];
// 			unsigned int pixel=(*data<<24)+
// 			   (data[1]<<16)+
// 			   (data[2]<<8)+
// 			   data[3];
// 			
// 			dest[at+2] = (pixel & redmask) >> (redshift);
// 			dest[at+1] = (pixel & greenmask) >> (greenshift);
// 			dest[at] = (pixel & bluemask) >> (blueshift);
			dest+=3;
			data+=nbyte;
		}
	}
	return frame;
}
