#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <gst/gst.h>

#include <cv.h>
#include "cv.hpp"
#include "cvaux.hpp"
#include "highgui.h"

extern "C" {
#include "videosource.h"
}

#include <math.h>
/* TODO: CvTracking : public CvCamShiftTracker */

#include "cv_tracking.h"

CvTracking::CvTracking(int width,int height)
{
    s_min=128;
    s_max=255;
    v_min=64;
    v_max=160;
    w=width;
    h=height;
    tx=w/2;
    ty=h/2;
    ta=0;

    // initial searching window. Default: everything
    start_window.x=0;
    start_window.y=0;
    start_window.width=width;
    start_window.height=height;

    // what to take from the target image
    // default: everything (width and height will be set when known)
    target_window.x=0;
    target_window.y=0;
    target_window.width=-1;
    target_window.height=-1;
}

static void error(const char* msg)
{
    fprintf(stderr," *** libtracking error\n %s\n",msg);
    exit(EXIT_FAILURE);
}

void CvTracking::set_hist_limits(int smin, int smax, int vmin, int vmax)
{
    s_min=smin;
    s_max=smax;
    v_min=vmin;
    v_max=vmax;
}

void CvTracking::set_target(const char* image_name)
{
    int err;
    IplImage *target=cvLoadImage(image_name); // should contain a coloured something
    if (target==NULL)
        error("Can't find target image file");
    if (target_window.width==-1)
        target_window.width=target->width;
    if (target_window.height==-1)
        target_window.height=target->height;

    int dims[] = { 20 };
    cv_tracker.set_hist_dims(1,dims);
    cv_tracker.set_hist_bin_range(0,0,255);
    cv_tracker.set_threshold(0);

    cv_tracker.set_min_ch_val(1,s_min);
    cv_tracker.set_max_ch_val(1,s_max);

    cv_tracker.set_min_ch_val(2,v_min);
    cv_tracker.set_max_ch_val(2,v_max);

    cv_tracker.reset_histogram();
    
    err=cv_tracker.set_window(target_window);
    err=cv_tracker.update_histogram(target);
    if(err==0)
    {
      fprintf(stderr,"Tracker: update_histogram failed. Check min/max S and V\n");
      exit(EXIT_FAILURE);
    }
    err=cv_tracker.set_window(start_window);
}

void CvTracking::set_target(const char* image_name,int x,int y,int w,int h)
{
    set_target(image_name);
    target_window.x=x;
    target_window.y=y;
    target_window.width=w;
    target_window.height=h;
}

void CvTracking::track(GstBuffer *buf)
{
    IplImage * frame=NULL;
    frame=gst_to_ipl(buf);
    cv_tracker.track_object(frame);

    CvRect rect=cv_tracker.get_window();
    tx=rect.x+rect.width/2;
    ty=rect.y+rect.height/2;
    ta=cv_tracker.get_orientation();

    cvReleaseImage(&frame);
}

GdkPixbuf* CvTracking::track_and_draw(GstBuffer *gstbuf)
{
    GdkPixbuf * pixbuf;
    IplImage * frame;

    frame=gst_to_ipl(gstbuf);
    cv_tracker.track_object(frame);

    CvRect rect=cv_tracker.get_window();
    tx=rect.x+rect.width/2;
    ty=rect.y+rect.height/2;
    ta=cv_tracker.get_orientation();

    int length = (int)cv_tracker.get_length();
    int width  = (int)cv_tracker.get_width();
    float cs = (float)cos(ta/180.0*M_PI );
    float sn = (float)sin(ta/180.0*M_PI );

    CvPoint p1 = {(int)(tx + length * cs / 2), (int)(ty + length * sn / 2)};
    CvPoint p2 = {(int)(tx - length * cs / 2), (int)(ty - length * sn / 2)};
    CvPoint p3 = {(int)(tx + width * sn / 2), (int)(ty - width * cs / 2)};
    CvPoint p4 = {(int)(tx - width * sn / 2), (int)(ty + width * cs / 2)};
    cvLine(frame, p1, p2, CV_RGB(255,255,255) );
    cvLine(frame, p4, p3, CV_RGB(255,255,255) );
    CvPoint p5={rect.x,rect.y};
    CvPoint p6={rect.x+rect.width,rect.y+rect.height};
    cvRectangle(frame,p5,p6,CV_RGB(255,0,0),3,8,0);
    pixbuf=ipl_to_gdkpixbuf(frame);
    cvReleaseImage(&frame);
    return pixbuf;
}

int CvTracking::get_tx(void) const
{
    return tx;
}

int CvTracking::get_ty(void) const
{
    return ty;
}

double CvTracking::get_ta(void) const
{
    return ta;
}
