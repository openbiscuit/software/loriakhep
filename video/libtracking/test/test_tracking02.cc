#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/time.h>
#include <gst/gst.h>


#include "cv_tracking.h"

extern "C" {
#include "videosource.h"
}

static double t0;
void timer_start(void)
{
    struct timeval tv;
    gettimeofday(&tv,NULL);
    t0=(double)tv.tv_sec+((double)tv.tv_usec)/1e6;
}

double timer_get(void)
{
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return (double)tv.tv_sec+((double)tv.tv_usec)/1e6-t0;
}

int main(int argc, char **argv)
{
    GstBuffer *buf;
    VideoSource vs;
    SimpleViewer view;
    
    GdkPixbuf * pixbuf;
    GError *error;
    int i;
    unsigned int id;
    const int WIDTH=640;
    const int HEIGHT=480;

    // Create tracker
    CvTracking track(WIDTH,HEIGHT);
    track.set_target("pomme.jpg",100,50,320-100,240-50);

    // Main loop & visual stuff
    simple_viewer_init(&view,WIDTH,HEIGHT);
    vs_init(&vs,"v4l2src device=/dev/video1",WIDTH,HEIGHT,NULL);
    //     vs_init(&vs,"filesrc location=\"test.mpg\"",320,240,NULL);
    vs_play(&vs);
    id=1;
    while(1)
    {
        timer_start();
        buf=vs_get_next_image(&vs,id,&id);
        id+=1;
        if (buf)
        {
            pixbuf=track.track_and_draw(buf);
            printf("Target at %d,%d (%f)\n",track.get_tx(),track.get_ty(),track.get_ta());
            simple_viewer_draw_pixbuf(&view,pixbuf);
            /* cleanup */
            gdk_pixbuf_unref(pixbuf);
            gst_buffer_unref(buf);
        }
        printf("%f\n",timer_get());
    }
    vs_free(&vs);
    return EXIT_SUCCESS;
}
