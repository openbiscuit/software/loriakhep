#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <gst/gst.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include <cv.h>
#include "cv.hpp"
#include "cvaux.hpp"
#include <math.h>

/* TODO: CvTracking : public CvCamShiftTracker */

class CvTracking {
protected:
    int s_min,s_max;
    int v_min,v_max;
    int w,h;
    CvCamShiftTracker cv_tracker;
    int tx,ty;
    double ta;

    CvRect start_window;
    CvRect target_window;
public:
    CvTracking(int width,int height);
    void set_target(const char* image_name);
    void set_target(const char* image_name,int x,int y,int w,int h);

    void track(GstBuffer *buf);
    GdkPixbuf* track_and_draw(GstBuffer *buf);
    int get_tx(void) const;
    int get_ty(void) const;
    double get_ta(void) const;
    void set_start(int x,int y,int w,int h);
    void set_hist_limits(int smin, int smax, int vmin, int vmax);

};
