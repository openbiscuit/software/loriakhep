ifdef DESTDIR
ROOT=${DESTDIR}/usr/local/khepera3
else
ROOT=/usr/local/khepera3
endif

.PHONY: video

all: k3tb programs video

k3tb: k3tb_modules k3tb_programs

k3tb_modules:
	cd khepera3toolbox/Modules/ && ../Scripts/k3-make-all

k3tb_programs: k3tb_modules
	cd khepera3toolbox/Programs/ && ../Scripts/k3-make-all

programs: k3tb_modules
	make -C programs

video:
	make -C video/libvideosource
	make -C video/libtracking

install: all
# USE WITH CAUTION !
	install -d ${ROOT}/lib
	install -d ${ROOT}/include
	install -d ${ROOT}/bin
	# install khepera3toolbox first (copy)
	# no K3_ROOT to changes, they're all relative
	cp -rfv khepera3toolbox/ ${ROOT}/khepera3toolbox/
	# get rid of stupid .svn files, if any
	find ${ROOT}/khepera3toolbox/ -depth -name ".svn" -exec rm -rf {} \;
	# install programs
	make -C programs install ROOT=${ROOT}
	# install video
	install video/libvideosource/libvideosource.a ${ROOT}/lib
	install video/libvideosource/libvideosource.so ${ROOT}/lib
	install video/libtracking/libtracking.so ${ROOT}/lib

	install -d ${ROOT}/include/libvideosource/
	install -d ${ROOT}/include/libtracking/
	install video/libvideosource/*h ${ROOT}/include/libvideosource/
	install video/libtracking/*h ${ROOT}/include/libtracking/

clean:
	cd khepera3toolbox/Modules/ && ../Scripts/k3-make-all clean
	cd khepera3toolbox/Programs/ && ../Scripts/k3-make-all clean
	make -C programs clean
	make -C video/libvideosource allclean
	make -C video/libtracking allclean

	rm -rvf debian/loriakhep.debhelper.log
	rm -rvf debian/files
	rm -rvf debian/loriakhep.substvars
	rm -rvf debian/loriakhep
