#include <EEPROM.h>

void writeeprom(int n,int cleargain,int redgain,int greengain,int bluegain)
{
    uint8_t hi,lo;
    hi = cleargain >> 8;
    lo = cleargain;


    EEPROM.write(n*8,lo);
    EEPROM.write(n*8+1,hi);
    hi = redgain >> 8;
    lo = redgain;
    EEPROM.write(n*8+2,lo);
    EEPROM.write(n*8+3,hi);
    hi = greengain >> 8;
    lo = greengain;
    EEPROM.write(n*8+4,lo);
    EEPROM.write(n*8+5,hi);
    hi = bluegain >> 8;
    lo = bluegain;
    EEPROM.write(n*8+6,lo);
    EEPROM.write(n*8+7,hi);



}
void readeprom(int n,int *cleargain,int *redgain,int *greengain,int *bluegain)
{

    *cleargain= EEPROM.read(n*8) + ((int)EEPROM.read(n*8+1) << 8);
    *redgain =EEPROM.read(n*8+2) + ((int)EEPROM.read(n*8+3) << 8);
    *greengain=EEPROM.read(n*8+4) + ((int)EEPROM.read(n*8+5) << 8);
    *bluegain =EEPROM.read(n*8+6) + ((int)EEPROM.read(n*8+7) << 8);
    /*  Serial.println("Save Gain");
     Serial.print(*cleargain);
     Serial.print(" ");
       Serial.print(*redgain);
     Serial.print(" ");
       Serial.print(*greengain);
     Serial.print(" ");
       Serial.print(*bluegain);
     Serial.println();*/

}

void writeeprom3(int n,int cleargain,int redgain,int greengain,int bluegain)
{
    int offset=128;
    uint8_t hi,lo;
    hi = cleargain >> 8;
    lo = cleargain;


    EEPROM.write(offset+n*8,lo);
    EEPROM.write(offset+n*8+1,hi);
    hi = redgain >> 8;
    lo = redgain;
    EEPROM.write(offset+n*8+2,lo);
    EEPROM.write(offset+n*8+3,hi);
    hi = greengain >> 8;
    lo = greengain;
    EEPROM.write(offset+n*8+4,lo);
    EEPROM.write(offset+n*8+5,hi);
    hi = bluegain >> 8;
    lo = bluegain;
    EEPROM.write(offset+n*8+6,lo);
    EEPROM.write(offset+n*8+7,hi);



}
void readeprom3(int n,int *cleargain,int *redgain,int *greengain,int *bluegain)
{
    int offset=128;
    *cleargain= EEPROM.read(offset+n*8) + ((int)EEPROM.read(offset+n*8+1) << 8);
    *redgain =EEPROM.read(offset+n*8+2) + ((int)EEPROM.read(offset+n*8+3) << 8);
    *greengain=EEPROM.read(offset+n*8+4) + ((int)EEPROM.read(offset+n*8+5) << 8);
    *bluegain =EEPROM.read(offset+n*8+6) + ((int)EEPROM.read(offset+n*8+7) << 8);
    /*  Serial.println("Save Gain");
     Serial.print(*cleargain);
     Serial.print(" ");
       Serial.print(*redgain);
     Serial.print(" ");
       Serial.print(*greengain);
     Serial.print(" ");
       Serial.print(*bluegain);
     Serial.println();*/

}
void writeeprom2(int n,int cleargain,int redgain,int greengain,int bluegain)
{
    int offset = 64;
    uint8_t hi,lo;
    lo = cleargain;


    EEPROM.write(offset+n*4,lo);
    lo = redgain;
    EEPROM.write(offset+n*4+1,lo);
    lo = greengain;
    EEPROM.write(offset+n*4+2,lo);
    lo = bluegain;
    EEPROM.write(offset+n*4+3,lo);



}
void readeprom2(int n,int *cleargain,int *redgain,int *greengain,int *bluegain)
{
    int offset = 64;
    *cleargain= EEPROM.read(offset+n*4) ;
    *redgain =EEPROM.read(offset+n*4+1);
    *greengain=EEPROM.read(offset+n*4+2);
    *bluegain =EEPROM.read(offset+n*4+3);
    /*  Serial.println("Save Gain");
     Serial.print(*cleargain);
     Serial.print(" ");
       Serial.print(*redgain);
     Serial.print(" ");
       Serial.print(*greengain);
     Serial.print(" ");
       Serial.print(*bluegain);
     Serial.println();*/

}


/*void writeCalibration() {
  uint8_t hi = calibrationDarkness >> 8;
  uint8_t lo = calibrationDarkness;
  EEPROM.write(0, lo);
  EEPROM.write(1,hi);
  EEPROM.write(2,calibrationDarkness);
  EEPROM.write(3,calibrationRed);
  EEPROM.write(4,calibrationGreen);
  EEPROM.write(5,calibrationBlue);
  Serial.println("Calibration stored");
}*/
