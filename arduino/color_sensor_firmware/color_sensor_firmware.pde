

#include "pins.h"
#include "adjd.h"
#include "EEPROM.h"
#include "string.h"
#include <Servo.h>

Servo myservo;  // create servo object to control a servo

int posservo = 0;

#define valmax 1000
#define NLEC 1
int ClearGain[8];
int RedGain[8];
int GreenGain[8];
int BlueGain[8];
int ClearCap[8];
int RedCap[8];
int GreenCap[8];
int BlueCap[8];
int ClearOffset[8];
int RedOffset[8];
int GreenOffset[8];
int BlueOffset[8];

char buffer[128];
int inc=0;
float redFactor=1;
float blueFactor=1;
float greenFactor=1;
unsigned char sensor[8];
int sensorport[8];
int nsensor;
//initial darkLevel;
int calibrationDarkness = 0;
byte calibrationRed = 5;
byte calibrationGreen = 5;
byte calibrationBlue = 5;

int ax=0;

void setup(void)
{

    DDRD = DDRD | B00001100;

    Serial.begin(115200);
    /* pinMode(redPin, OUTPUT);
     pinMode(greenPin, OUTPUT);
     pinMode(bluePin, OUTPUT);*/
    pinMode(ledPin, OUTPUT);
    digitalWrite(ledPin, LOW);
    //myservo.attach(irPin1);
    //myservo.writeMicroseconds(3000);
    pinMode(irPin1, OUTPUT);
    digitalWrite(irPin1,HIGH);
    pinMode(irPin2, OUTPUT);
    digitalWrite(irPin2, HIGH);
    ledd1=1;
    ledd2=1;
    setupADJD();


    /*  if (digitalRead(clearCalPin)) {
     Serial.println("Omitting stored calibration");
     }
     else {
     readCalibration();
     }*/

    // sensor gain setting (Avago app note 5330)
    // CAPs are 4bit (higher value will result in lower output)
    /* set_register(REG_CAP_RED, 0x01);
     set_register(REG_CAP_GREEN, 0x01);
     set_register(REG_CAP_BLUE, 0x01);
     set_register(REG_CAP_CLEAR, 0x01);

     //calibrate led

     int ledGain = getColorGain();
     set_gain(REG_INT_RED_LO,ledGain);
     set_gain(REG_INT_GREEN_LO,ledGain);
     set_gain(REG_INT_BLUE_LO,ledGain);*/
    /*performMeasurement();
    /* int red=get_readout(REG_DATA_RED_LO);
     int green=get_readout(REG_DATA_GREEN_LO);
     int blue=get_readout(REG_DATA_BLUE_LO);

    /* int m=2000; //bigger anyway
     m=min(m,red);
     m=min(m,green);
     m=min(m,blue);
     redFactor=((float)m*255.0)/(1000*(float)red);
     greenFactor=((float)m*255.0)/(1000*(float)green);
     blueFactor=((float)m*255.0)/(1000*(float)blue);
     */
    nsensor=7;
    ////sensor 1 to 5
    sensor[0]=0b00000010;//d9 sensor 1
    sensorport[0]=PB;
    sensor[1]=0b00000001;//d8 sensor 2
    sensorport[1]=PB;
    sensor[2]=0b00100000;//d5 sensor 3
    sensorport[2]=PD;
    sensor[3]=0b10000000;//d7 sensor 4
    sensorport[3]=PD;
    sensor[4]=0b00010000;//d4 sensor 5
    sensorport[4]=PD;
    sensor[5]=0b00001000;//d3 sensor 6
    sensorport[5]=PD;

    sensor[6]=0b01000000;//d6 sensor 7
    sensorport[6]=PD;




    ///sensor 6 7

    //   cli();
    int i;
    for (i=0; i<nsensor; i++)
    {

        unsubscribeallcap();
        subscribecap(sensor[i],sensorport[i]);

        /* RedGain[i]=getRedGain();
         GreenGain[i]=getGreenGain();
         BlueGain[i]=getBlueGain();
         ClearGain[i]= getClearGain();*/
        /*set_gain(REG_INT_RED_LO,RedGain[i]);
         set_gain(REG_INT_GREEN_LO,GreenGain[i]);
         set_gain(REG_INT_BLUE_LO,BlueGain[i]);

         set_gain(REG_INT_CLEAR_LO,ClearGain[i]);*/
        readeprom(i,&(ClearGain[i]),&(RedGain[i]),&(GreenGain[i]),&(BlueGain[i]));
        readeprom2(i,&(ClearCap[i]),&(RedCap[i]),&(GreenCap[i]),&(BlueCap[i]));
        readeprom3(i,&(ClearOffset[i]),&(RedOffset[i]),&(GreenOffset[i]),&(BlueOffset[i]));
        /* RedGain[i]/=5;
         RedGain[i]*=3;
         GreenGain[i]/=5;
        GreenGain[i]*=3;*/
        set_register2(REG_CAP_RED,RedCap[i]);
        set_register2(REG_CAP_GREEN,GreenCap[i]);
        set_register2(REG_CAP_BLUE,BlueCap[i]);
        set_register2(REG_CAP_CLEAR,ClearCap[i]);

        set_gain(REG_INT_RED_LO,(RedGain[i]));
        set_gain(REG_INT_GREEN_LO,(GreenGain[i]));
        set_gain(REG_INT_BLUE_LO,(BlueGain[i]));

        set_gain(REG_INT_CLEAR_LO,(ClearGain[i]));

    }

    unsubscribeallcap();
    for (i =0; i<nsensor; i++)
    {
        subscribecap(sensor[i],sensorport[i]);
    }

}

void loop() {
    int i;
    char tmp[256];
    /*  int clearGain = getClearGain();
     set_gain(REG_INT_CLEAR_LO,clearGain);
     int colorGain = getColorGain();
     set_gain(REG_INT_RED_LO,colorGain);
     set_gain(REG_INT_GREEN_LO,colorGain);
     set_gain(REG_INT_BLUE_LO,colorGain);*/
    int ok=0;
    // ReadAndSend();
    //Serial.flush();

    while (Serial.available())
    {
        char c = Serial.read();


        buffer[inc]=c;
        int numer,numer2;
        int value;
        int j;
        //Serial.println(c);
        if (c=='\n' || c=='t')
        {
            buffer[inc+1]='\0';
            if (strlen(buffer)<32)
            {

                ///process
                /* char tmp[128];
                 sprintf(tmp,"process :%s",buffer);
                 Serial.print(tmp);
                 */
                switch (buffer[0])  {

                    ///
                case 'o':
                    numer = buffer[3] - '0';

                    for (j=5; j<=strlen(buffer); j++)
                    {
                        tmp[j-5]=buffer[j];


                    }


                    numer2 =atoi(tmp);

                    if (numer>=0 && numer < nsensor && numer2 >=0 && numer2< 4096)
                    {

                        switch (buffer[1])
                        {
                        case 'c':
                            ClearOffset[numer]= numer2;

                            break;
                        case 'r':
                            RedOffset[numer]= numer2;

                            break;
                        case 'g':
                            GreenOffset[numer]= numer2;

                            break;
                        case 'b':
                            BlueOffset[numer]= numer2;


                            break;




                        }

                        writeeprom3(numer,ClearOffset[numer],RedOffset[numer],GreenOffset[numer],BlueOffset[numer]);

                    }

                    break;

                case 'u':


                    Serial.print(nsensor);
                    for (i=0; i<nsensor; i++)
                    {
                        Serial.print(" ");
                        Serial.print(i);
                        Serial.print(" ");
                        Serial.print(ClearOffset[i]);
                        Serial.print(" ");
                        Serial.print(RedOffset[i]);
                        Serial.print(" ");
                        Serial.print(GreenOffset[i]);
                        Serial.print(" ");
                        Serial.print(BlueOffset[i]);


                    }
                    Serial.println();

                    break;
                case 'f':

                    Serial.print(nsensor);
                    for (i=0; i<nsensor; i++)
                    {
                        Serial.print(" ");
                        Serial.print(i);
                        Serial.print(" ");
                        Serial.print(ClearCap[i]);
                        Serial.print(" ");
                        Serial.print(RedCap[i]);
                        Serial.print(" ");
                        Serial.print(GreenCap[i]);
                        Serial.print(" ");
                        Serial.print(BlueCap[i]);


                    }
                    Serial.println();

                    break;


                case 'r':

                    Serial.print(nsensor);
                    for (i=0; i<nsensor; i++)
                    {
                        Serial.print(" ");
                        Serial.print(i);
                        Serial.print(" ");
                        Serial.print(ClearGain[i]);
                        Serial.print(" ");
                        Serial.print(RedGain[i]);
                        Serial.print(" ");
                        Serial.print(GreenGain[i]);
                        Serial.print(" ");
                        Serial.print(BlueGain[i]);


                    }
                    Serial.println();

                    break;

                case 'm':
                    numer = buffer[3] - '0';

                    for (j=5; j<=strlen(buffer); j++)
                    {
                        tmp[j-5]=buffer[j];


                    }


                    numer2 =atoi(tmp);

                    if (numer>=0 && numer < nsensor && numer2 >=0 && numer2< 4096)
                    {
                        unsubscribeallcap();
                        subscribecap(sensor[numer],sensorport[numer]);
                        switch (buffer[1])
                        {
                        case 'c':
                            ClearGain[numer]= numer2;
                            set_gain(REG_INT_CLEAR_LO,ClearGain[numer]);
                            break;
                        case 'r':
                            RedGain[numer]= numer2;
                            set_gain(REG_INT_RED_LO,RedGain[numer]);
                            break;
                        case 'g':
                            GreenGain[numer]= numer2;
                            set_gain(REG_INT_GREEN_LO,GreenGain[numer]);
                            break;
                        case 'b':
                            BlueGain[numer]= numer2;
                            set_gain(REG_INT_BLUE_LO,BlueGain[numer]);

                            break;




                        }

                        writeeprom(numer,ClearGain[numer],RedGain[numer],GreenGain[numer],BlueGain[numer]);
                        unsubscribeallcap();
                        for (i =0; i<nsensor; i++)
                        {
                            subscribecap(sensor[i],sensorport[i]);


                        }
                    }

                    break;
                case 'e':
                    numer = buffer[3] - '0';

                    for (j=5; j<=strlen(buffer); j++)
                    {
                        tmp[j-5]=buffer[j];


                    }


                    numer2 =atoi(tmp);

                    if (numer>=0 && numer < nsensor && numer2 >=0 && numer2< 16)
                    {
                        unsubscribeallcap();
                        subscribecap(sensor[numer],sensorport[numer]);
                        switch (buffer[1])
                        {
                        case 'c':
                            ClearCap[numer]= numer2;
                            set_register2(REG_CAP_CLEAR, numer2);
                            break;
                        case 'r':
                            RedCap[numer]= numer2;
                            set_register2(REG_CAP_RED, numer2);
                            break;
                        case 'g':
                            GreenCap[numer]= numer2;
                            set_register2(REG_CAP_GREEN, numer2);
                            break;
                        case 'b':
                            BlueCap[numer]= numer2;
                            set_register2(REG_CAP_BLUE, numer2);

                            break;




                        }

                        writeeprom2(numer,ClearCap[numer],RedCap[numer],GreenCap[numer],BlueCap[numer]);
                        unsubscribeallcap();
                        for (i =0; i<nsensor; i++)
                        {
                            subscribecap(sensor[i],sensorport[i]);


                        }
                    }
                    break;
                case 'i':
                    if (buffer[1]=='1')
                    {

                        if (buffer[2]=='o')
                        {
                            ledd1=1;
                            digitalWrite(irPin1, HIGH);

                        }
                        else
                        {
                            ledd1=0;
                            digitalWrite(irPin1, LOW);
                        }
                    }
                    else
                    {
                        if (buffer[2]=='o')
                        {
                            ledd2=1;
                            digitalWrite(irPin2, HIGH);

                        }
                        else
                        {
                            ledd2=0;
                            digitalWrite(irPin2, LOW);
                        }

                    }

                    break;
                case 'l':
                    if (buffer[1]=='o')
                    {
                        digitalWrite(ledPin, HIGH);
                    }
                    else
                    {
                        digitalWrite(ledPin, LOW);
                    }
                    break;
                case 'g':
                    ///get

                    ReadAndSend();

                    //Serial.println("Red: Green: Blue:\n");
                    break;
                case 'c':
                    ///calibrate
                    for (i=0; i<nsensor; i++)
                    {
                        unsubscribeallcap();
                        subscribecap(sensor[i],sensorport[i]);

                        switch (buffer[1]) {
                        case 'r':
                            ///red

                            RedGain[i]=getRedGain();
                            set_gain(REG_INT_RED_LO,RedGain[i]);
                            /* set_gain(REG_INT_RED_LO,RedGain);
                             set_gain(REG_INT_GREEN_LO,GreenGain);
                             set_gain(REG_INT_BLUE_LO,BlueGain);

                             set_gain(REG_INT_CLEAR_LO,ClearGain);*/

                            //Serial.println("Red Calibrate\n");
                            break;

                        case 'b':
                            ///green

                            BlueGain[i]=getBlueGain();
                            set_gain(REG_INT_BLUE_LO,BlueGain[i]);

                            //Serial.println("Blue Calibrate\n");
                            break;

                        case 'g':
                            //blue

                            GreenGain[i]=getGreenGain();
                            set_gain(REG_INT_GREEN_LO,GreenGain[i]);

                            //Serial.println("Green Calibrate\n");
                            break;

                        case 'c':
                            ClearGain[i]= getClearGain();
                            set_gain(REG_INT_CLEAR_LO,ClearGain[i]);
                            //clear
                            break;


                        }



                    }

                    unsubscribeallcap();
                    for (i =0; i<nsensor; i++)
                    {
                        subscribecap(sensor[i],sensorport[i]);
                        writeeprom(i,ClearGain[i],RedGain[i],GreenGain[i],BlueGain[i]);

                    }




                    break;

                }


            }
            inc=0;
        }
        else
        {

            inc++;
        }

    }
    if (inc>100)
        inc=0;
    //Serial.println();

    //delay(1000);
}


int ReadAndSend()
{
    int oldcc[8];
    int oldred[8];
    int oldgreen[8];
    int oldblue[8];
    int ccmoy[8][8];
    int redmoy[8][8];
    int greenmoy[8][8];
    int bluemoy[8][8];
    int cc_[8];
    int red_[8];
    int green_[8];
    int blue_[8];
    int n;
    for (int i =0; i<nsensor; i++)
    {
        unsubscribeallcap();
        subscribecap(sensor[i],sensorport[i]);
        set_gain(REG_INT_CLEAR_LO,ClearGain[i]);
        set_gain(REG_INT_RED_LO,RedGain[i]);
        set_gain(REG_INT_GREEN_LO,GreenGain[i]);
        set_gain(REG_INT_BLUE_LO,BlueGain[i]);
        set_register2(REG_CAP_RED,RedCap[i]);
        set_register2(REG_CAP_GREEN,GreenCap[i]);
        set_register2(REG_CAP_BLUE,BlueCap[i]);
        set_register2(REG_CAP_CLEAR,ClearCap[i]);

    }
    unsubscribeallcap();
    for (int i =0; i<nsensor; i++)
    {
        subscribecap(sensor[i],sensorport[i]);
    }
    int cc = 0;
    int red=0;
    int green=0;
    int blue=0;
    for (int i=0; i<1 ; i ++) {

        for (int j=0; j<8; j++)
        {

            digitalWrite(irPin1,LOW);
            digitalWrite(irPin2,LOW);
            performMeasurement();
            if (ledd1)
                digitalWrite(irPin1,HIGH);
            if (ledd2)
                digitalWrite(irPin2,HIGH);
            n=get_readout(REG_DATA_CLEAR_LO,/*cc_*/ccmoy[j]);
            get_readout(REG_DATA_RED_LO,/*red_*/redmoy[j]);
            get_readout(REG_DATA_GREEN_LO,/*green_*/greenmoy[j]);
            get_readout(REG_DATA_BLUE_LO,/*blue_*/bluemoy[j]);
        }


        for (int z=0; z<nsensor; z++)
        {

            cc_[z]=/*(ccmoy[0][z] + ccmoy[1][z]+ccmoy[2][z]+ccmoy[3][z]) /NLEC;*/0;
            red_[z]=/*(redmoy[0][z] + redmoy[1][z]+redmoy[2][z]+redmoy[3][z]) /NLEC;*/0;
            green_[z]=/*(greenmoy[0][z] + greenmoy[1][z]+greenmoy[2][z]+greenmoy[3][z]) /NLEC;*/0;
            blue_[z]=/*(bluemoy[0][z] + bluemoy[1][z]+bluemoy[2][z]+bluemoy[3][z]) /NLEC;*/0;
            for (int zz=0; zz<NLEC; zz++)
            {
                cc_[z]+=ccmoy[zz][z];
                red_[z]+=redmoy[zz][z];
                green_[z]+=greenmoy[zz][z];
                blue_[z]+=bluemoy[zz][z];
            }
            cc_[z]/=NLEC;
            red_[z]/=NLEC;
            green_[z]/=NLEC;
            blue_[z]/=NLEC;

        }
    }
    Serial.print(nsensor);


    for (int i=0; i<nsensor; i++)
    {

        /*if(cc_[i]>0){
         cc=cc_[i];
         oldcc[i]=cc_[i];
         }
         else
         cc=oldcc[i];

         if(red_[i]>0){
         red=red_[i];
         oldred[i]=red_[i];
         }
         else
         red=oldred[i];
         if(green_[i]>0){
         green=green_[i];
         oldgreen[i]=green_[i];
         }
         else
         green=oldgreen[i];
         if(blue_[i]>0){
         blue=blue_[i];
         oldblue[i]=blue_[i];
         }
         else
         blue=oldblue[i];
         */
        cc=cc_[i];

        red=1024-(cc_[i]-red_[i]);

        green=1024-(cc_[i]-green_[i]);

        blue=1024-(cc_[i]-blue_[i]);

        Serial.print(" ");
        Serial.print(i);
        Serial.print(" ");
        Serial.print(cc+ClearOffset[i]);
        Serial.print(" ");
        Serial.print(red+RedOffset[i]);
        Serial.print(" ");
        Serial.print(green+GreenOffset[i]);
        Serial.print(" ");
        Serial.print(blue+BlueOffset[i]);
    }

    /* cc=cc_[1];
     red=red_[1];
     green=green_[1];
     blue=blue_[1];
     Serial.print(" ");
     Serial.print(cc);
     Serial.print(" ");
     Serial.print(red);
     Serial.print(" ");
     Serial.print(green);
     Serial.print(" ");
     Serial.print(blue);*/
    Serial.print("\n");
    Serial.flush();

    //Serial.println();
}
int getClearGain() {
    int gainFound = 0;
    int upperBox=4096;
    int lowerBox = 0;
    int half;
    int r[8];
    while (!gainFound) {
        half = ((upperBox-lowerBox)/2)+lowerBox;
        //no further halfing possbile
        if (half==lowerBox) {
            gainFound=1;
        }
        else {
            set_gain(REG_INT_CLEAR_LO,half);
            digitalWrite(irPin1,LOW);
            digitalWrite(irPin2,LOW);
            performMeasurement();
            if (ledd1)
                digitalWrite(irPin1,HIGH);
            if (ledd2)
                digitalWrite(irPin2,HIGH);
            get_readout(REG_DATA_CLEAR_LO,r);
            int halfValue =r[0];

            if (halfValue>valmax) {
                upperBox=half;
            }
            else if (halfValue<valmax) {
                lowerBox=half;
            }
            else {
                gainFound=1;
            }
        }
    }
    return half;
}

int getColorGain() {
    int gainFound = 0;
    int upperBox=4096;
    int lowerBox = 0;
    int half;
    int r[8];
    while (!gainFound) {
        half = ((upperBox-lowerBox)/2)+lowerBox;
        //no further halfing possbile
        if (half==lowerBox) {
            gainFound=1;
        }
        else {
            set_gain(REG_INT_RED_LO,half);
            set_gain(REG_INT_GREEN_LO,half);
            set_gain(REG_INT_BLUE_LO,half);
            digitalWrite(irPin1,LOW);
            digitalWrite(irPin2,LOW);
            performMeasurement();
            if (ledd1)
                digitalWrite(irPin1,HIGH);
            if (ledd2)
                digitalWrite(irPin2,HIGH);
            int halfValue = 0;
            get_readout(REG_DATA_RED_LO,r);
            halfValue=max(halfValue,r[0]);
            get_readout(REG_DATA_GREEN_LO,r);
            halfValue=max(halfValue,r[0]);
            get_readout(REG_DATA_BLUE_LO,r);
            halfValue=max(halfValue,r[0]);


            /*
            Serial.print("[");
             Serial.print(lowerBox);
             Serial.print(",");
             Serial.print(half);
             Serial.print(",");
             Serial.print(upperBox);
             Serial.print("] -> ");
             Serial.println(halfValue);
             */
            if (halfValue>valmax) {
                upperBox=half;
            }
            else if (halfValue<valmax) {
                lowerBox=half;
            }
            else {
                gainFound=1;
            }
        }
    }
    return half;
}
int getRedGain() {
    int gainFound = 0;
    int upperBox=4096;
    int lowerBox = 0;
    int half;
    int r[8];
    while (!gainFound) {
        half = ((upperBox-lowerBox)/2)+lowerBox;
        //no further halfing possbile
        if (half==lowerBox) {
            gainFound=1;
        }
        else {

            set_gain(REG_INT_RED_LO,half);
            //Serial.println("perfomMeasurement");

            digitalWrite(irPin1,LOW);
            digitalWrite(irPin2,LOW);
            performMeasurement();
            if (ledd1)
                digitalWrite(irPin1,HIGH);
            if (ledd2)
                digitalWrite(irPin2,HIGH);

            get_readout(REG_DATA_RED_LO,r);

            int halfValue = r[0];

            /*
            Serial.print("[");
             Serial.print(lowerBox);
             Serial.print(",");
             Serial.print(half);
             Serial.print(",");
             Serial.print(upperBox);
             Serial.print("] -> ");
             Serial.println(halfValue);
             */

            if (halfValue>valmax) {
                upperBox=half;
            }
            else if (halfValue<valmax) {
                lowerBox=half;
            }
            else {
                gainFound=1;
            }
        }
    }
    return half;
}
int getGreenGain() {
    int gainFound = 0;
    int upperBox=4096;
    int lowerBox = 0;
    int half;
    int r[8];
    while (!gainFound) {
        half = ((upperBox-lowerBox)/2)+lowerBox;
        //no further halfing possbile
        if (half==lowerBox) {
            gainFound=1;
        }
        else {
            set_gain(REG_INT_GREEN_LO,half);
            digitalWrite(irPin1,LOW);
            digitalWrite(irPin2,LOW);
            performMeasurement();
            if (ledd1)
                digitalWrite(irPin1,HIGH);
            if (ledd2)
                digitalWrite(irPin2,HIGH);
            get_readout(REG_DATA_GREEN_LO,r);
            int halfValue = r[0];

            /*
            Serial.print("[");
             Serial.print(lowerBox);
             Serial.print(",");
             Serial.print(half);
             Serial.print(",");
             Serial.print(upperBox);
             Serial.print("] -> ");
             Serial.println(halfValue);
             */

            if (halfValue>valmax) {
                upperBox=half;
            }
            else if (halfValue<valmax) {
                lowerBox=half;
            }
            else {
                gainFound=1;
            }
        }
    }
    return half;
}
int getBlueGain() {
    int gainFound = 0;
    int upperBox=4096;
    int lowerBox = 0;
    int half;
    int r[8];
    while (!gainFound) {
        half = ((upperBox-lowerBox)/2)+lowerBox;
        //no further halfing possbile
        if (half==lowerBox) {
            gainFound=1;
        }
        else {
            set_gain(REG_INT_BLUE_LO,half);
            digitalWrite(irPin1,LOW);
            digitalWrite(irPin2,LOW);
            performMeasurement();
            if (ledd1)
                digitalWrite(irPin1,HIGH);
            if (ledd2)
                digitalWrite(irPin2,HIGH);
            get_readout(REG_DATA_BLUE_LO,r);
            int halfValue = r[0];
            /*
            Serial.print("[");
             Serial.print(lowerBox);
             Serial.print(",");
             Serial.print(half);
             Serial.print(",");
             Serial.print(upperBox);
             Serial.print("] -> ");
             Serial.println(halfValue);
             */

            if (halfValue>valmax) {
                upperBox=half;
            }
            else if (halfValue<valmax) {
                lowerBox=half;
            }
            else {
                gainFound=1;
            }
        }
    }
    return half;
}





