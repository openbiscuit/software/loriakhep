#include <Wire.h>
#include <util/delay.h>
#define set_sda_high() PORTD|=broadcastmask;PORTB|=broadcastmaskB // set data bit to 1
#define set_sda_low()  PORTD&=notbroadcastmask;PORTB&=notbroadcastmaskB
#define set_clock_high() PORTD|=0b00000100
#define set_clock_low() PORTD&=0b11111011
#define i2cdelay 4
#define loopdelay 1
#define write_sda() DDRD = DDRD | broadcastmask; DDRB = DDRB | broadcastmaskB // this is safer as it sets pins 2 to 7 as outputs
#define write_clock() DDRD = DDRD | 0b00000100
#define read_sda() DDRD = DDRD & notbroadcastmask  ;DDRB = DDRB & notbroadcastmaskB  // this is safer as it sets pins 2 to 7 as outputs
#define ACK     1
#define NO_ACK  0
#define DEVICE_WRITE    0xE8 //Default ADJD-S371 I2C address - write

#define DEVICE_READ     0xE9 //Default ADJD-S371 I2C address - read

#define MAXCAP 8

int ncap=0;
unsigned char i2cmask[8];
unsigned char i2cmaskB[8];
int portcap[8];
unsigned char noti2cmask[8];
unsigned char noti2cmaskB[8];
unsigned char broadcastmask;
unsigned char notbroadcastmask;
unsigned char broadcastmaskB;
unsigned char notbroadcastmaskB;
void majbroadcastmask()
{
    int i;
    broadcastmask=0;
    broadcastmaskB=0;
    for (i=0; i<ncap; i++)
    {
        if (portcap[i]==PD)
        {
            broadcastmask|=i2cmask[i];
        }
        else
        {


            broadcastmaskB|=i2cmaskB[i];
        }

    }
//  Serial.println((int)broadcastmaskB);
    notbroadcastmask=broadcastmask^0b11111111;
    notbroadcastmaskB=broadcastmaskB^0b11111111;
}
void unsubscribeallcap()
{
    ncap=0;
    majbroadcastmask();

}
int subscribecap(unsigned char capmask,int port)
{
    if (ncap<MAXCAP)
    {
        portcap[ncap]=port;
        if (portcap[ncap]==PD)
        {
            i2cmask[ncap] = capmask;
            noti2cmask[ncap]=capmask^0b11111111; //reverse bit
        }
        else
        {
            i2cmaskB[ncap] = capmask;
            noti2cmaskB[ncap]=capmask^0b11111111; //reverse bit
        }
        ncap++;
        majbroadcastmask();
        return 1;
    }
    return 0;

}
void unsubscribecap(unsigned char capmask,int port)
{
    int i,j;

    for (i=0; i<ncap; i++)
    {

        if ((portcap[i]==port) && ((port==PD && i2cmask[i]==capmask) || (port==PB && i2cmaskB[i]==capmask)))
        {///unsubscribe this sensor
            for (i=i; i<ncap-1; i++)
            {
                i2cmask[i]=i2cmask[i+1];
                noti2cmask[i]=noti2cmask[i+1];
                i2cmaskB[i]=i2cmaskB[i+1];
                noti2cmaskB[i]=noti2cmaskB[i+1];
                portcap[i]=portcap[i+1];
                ncap--;
                majbroadcastmask();
                return ;
            }



        }

    }

}
void delay_loop(int n_loop)
{
    int i=0;
    while (i<n_loop)
        i++;

}
void i2c_ack_polling(unsigned  device_address)

{

    while (1)

    {

        i2c_start();

        if (i2c_send_byte(device_address) == 1) break;

    }

    i2c_stop();

}
int i2c_read_byte(unsigned char *res)
{
    int i,j;
    unsigned char read_byte;
    unsigned char read_byteB;
    for (i=0; i<ncap; i++)
        res[i]=0;

    set_clock_low();



    read_sda();


    for (j = 0 ; j < 8 ; j++)

    {

        set_clock_low();

        _delay_us(i2cdelay);



        set_clock_high();

        _delay_us(i2cdelay);

        read_byte=PIND;
        read_byteB=PINB;
        for (i=0; i<ncap; i++)
        {
            res[i] <<=1;
            if (portcap[i]==PD)
            {
                if (read_byte&i2cmask[i])
                    res[i]|=0b00000001;
            }
            else
            {

                if (read_byteB&i2cmaskB[i])
                    res[i]|=0b00000001;
//Serial.println("res");

            }


        }
        int nz=res[0];
//  Serial.println(nz);
        /*      in_byte <<= 1;

              if(PIND&i2cmask1)
              in_byte|=0b00000001;

              in_byte2<<=1;
              if(PIND&i2cmask2)
              in_byte|=0b00000001;
        */
    }


    return(ncap);

}
void i2c_start()
{
    write_sda();
    set_sda_high();
    _delay_us(i2cdelay);
    set_clock_high();
    _delay_us(i2cdelay);
    set_sda_low();
    _delay_us(i2cdelay);


}
void i2c_stop()
{
    set_clock_low();
    _delay_us(i2cdelay);
    write_sda();
    set_sda_low();
    _delay_us(i2cdelay);
    set_clock_high();
    _delay_us(i2cdelay);
    set_sda_high();
    _delay_us(i2cdelay);

}
int i2c_send_byte(unsigned char bt)
{
    char to_send = bt;
    int i;
    write_sda();

    for (i=0; i<8; i++)
    {

        set_clock_low();
        _delay_us(i2cdelay);
        if ((to_send&0b10000000))
        {
            set_sda_high();


        }
        else
        {
            set_sda_low();
        }
        to_send <<= 1;
        _delay_us(5);
        set_clock_high();
        _delay_us(i2cdelay);

        ///remplacer ce qui precede par un carry out si possible


    }

    ///sended
    ///reack ack;

    set_clock_low();
    _delay_us(i2cdelay);
//  set_sda_low();
    read_sda();
    // pinMode(3, INPUT);

    set_clock_high();
    _delay_us(i2cdelay);
    for (i=0; i<512; i++)
    {//Serial.println(PIND,BIN);
        if ((!(PIND&broadcastmask)) && (!(PINB&broadcastmaskB)))break;
    }
    set_clock_low();
    _delay_us(i2cdelay);

    if (i==512) return 0;
    return 1;
    ///clock



}
void setupADJD() {
    write_clock();
// Wire.begin();
}

void performMeasurement() {
    unsigned char r[8],n,i;
    int nz;
    set_register2(0x00,0x01); // start sensing
    unsigned char ok=1;
    do {
        //Serial.println("do");
        ok=1;

        n=read_register2(0x00,r);


        for (i=0; i<n; i++)
        {
            if (r[i]!=0)
            {
                ok=0;
            }

        }
        // ok=1;
    }
    while (/*read_register2(0x00) != 0*/!ok) ;
    //Serial.println("UN DO");

}
int get_readout(int readRegister,int *res) {
    int i;
    unsigned char r1[8],r2[8];
    read_register2(readRegister,r1);
    read_register2(readRegister+1,r2);
    for (i=0; i<ncap; i++)
    {

        res[i]=(r1[i]+ (r2[i]<<8));
        //res[i]&=0b00001111111111;
        res[i]&=0b00001111111111;
    }
    return ncap;
}

void set_gain(int gainRegister, int gain) {
    if (gain <4096) {
        uint8_t hi = gain >> 8;
        uint8_t lo = gain;

        set_register2(gainRegister, lo);
        set_register2(gainRegister+1, hi);
    }

}

/*void set_register(unsigned char r, unsigned char v)
{
  Wire.beginTransmission(I2C_ADDRESS);
  Wire.send(r);
  Wire.send(v);
  Wire.endTransmission();
}

unsigned char read_register(unsigned char r)
{
  unsigned char v;
  Wire.beginTransmission(I2C_ADDRESS);
  Wire.send(r);  // register to read
  Wire.endTransmission();

  Wire.requestFrom(I2C_ADDRESS, 1); // read a byte
  while(!Wire.available()) {
    // waiting
  }
  v = Wire.receive();
  return v;
}
*/
int read_register2(unsigned char register_name,unsigned char *res)

{

    unsigned char in_byte;

    // Serial.println("read");

    i2c_ack_polling(DEVICE_WRITE);


    i2c_start();

    i2c_send_byte(DEVICE_WRITE);

    i2c_send_byte(register_name); //Write register address

    //i2c_stop();



    i2c_start(); //Repeat start (SR)

    i2c_send_byte(DEVICE_READ); //Now ask the IC to report on the last command

    i2c_read_byte(res);

    i2c_stop();

    //  Serial.println("end read");

    return(ncap);

}



//Write to a register in LIS

void set_register2(unsigned char register_name, unsigned char register_value)

{

    i2c_ack_polling(DEVICE_WRITE);


    i2c_start();

    i2c_send_byte(DEVICE_WRITE);

    i2c_send_byte(register_name); //Write register address

    i2c_send_byte(register_value); //Write data

    i2c_stop();



    //Return nothing

}
