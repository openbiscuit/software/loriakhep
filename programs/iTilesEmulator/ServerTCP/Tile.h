#include <iostream>
using namespace std;
#include <queue>
#include <map>
#include <time.h>

static unsigned long int worldStateCounter;

class Tile
{
	private :
	
		unsigned short x;
		unsigned short y;
		bool enabled;
		bool reserved;
		string currentID;
		float coef_phero_;
	    double cf_;
	public :
	
		string tileID;
	
		Tile * N;
		Tile * S;
		Tile * W;
		Tile * E;
		
		Tile * TileTab[4];
	
		queue<string> queue_in;
		queue<string> queue_out;
		pthread_mutex_t mutex_in;
		pthread_mutex_t mutex_out;
		
		map<string,string> memory;
		int pheroCounter;
		int currentI;
		int currentP;
		int currentA;
		int last_hops;
		string last_message;
		int last_counter;
	
		Tile();
		Tile(unsigned short a, unsigned short b,double _coef_phero = 0.999999);
		~Tile();
		void SetXY(unsigned short a, unsigned short b);
		unsigned short GetX() const;
		unsigned short GetY() const;
		void SetEnabled(bool b);
		bool GetEnabled() const;
		void BreakLink(string dir);
		void SetReserved(bool b);
		bool GetReserved() const;
		void SetCurrentID(string ID);
		string GetCurrentID() const;
		friend ostream & operator <<(ostream &, const Tile &);
		void Main();
		void Environment();
};

Tile::Tile(){}

Tile::Tile(unsigned short a, unsigned short b,double _coef_phero)
{
	cf_=0.0;
	coef_phero_=_coef_phero;
	x = a;
	y = b;
	enabled = true;
	reserved = false;
	N = NULL;
	S = NULL;
	W = NULL;
	E = NULL;
	TileTab[0] = NULL;
	TileTab[1] = NULL;
	TileTab[2] = NULL;
	TileTab[3] = NULL;
	currentID = "";
	ostringstream id;
	id << GetX() << GetY();
	tileID = id.str();
	pthread_mutex_init(&mutex_in, NULL);
	pthread_mutex_init(&mutex_out, NULL);
	
	//EVAW
		time_t t = time(NULL);
		struct tm * ts = localtime(&t);
		ostringstream os;
		os << ts->tm_hour << ":" << ts->tm_min << ":" << ts->tm_sec;
		string date = os.str();
		memory.insert(pair<string,string>("Date",date));
	//EVAP
		string phero = "0";
		memory.insert(pair<string,string>("Phero",phero));
		cf_=0.0;
		pheroCounter = 0;
	//Spreading
		ostringstream os2;
		os2 << worldStateCounter;
		memory.insert(pair<string,string>(os2.str(),""));
		last_hops = 0;
		last_message = "";
		last_counter = 0;
	//Sat-Alt
		currentI = 0;
		currentP = 0;
		currentA = 0;
}

Tile::~Tile(){}

void Tile::SetXY(unsigned short a, unsigned short b)
{
	x = a;
	y = b;
}

unsigned short Tile::GetX() const
{
	return x;
}

unsigned short Tile::GetY() const
{
	return y;
}

void Tile::SetEnabled(bool b)
{
	enabled = b;
}

bool Tile::GetEnabled() const
{
	return enabled;
}

void Tile::BreakLink(string dir)
{
	string dir_tab = "NESW";
	TileTab[dir_tab.find_first_of(dir)] = NULL;
}

void Tile::SetReserved(bool b)
{
	reserved = b;
}

bool Tile::GetReserved() const
{
	return reserved;
}

void Tile::SetCurrentID(string ID)
{
	currentID = ID;
}

string Tile::GetCurrentID() const
{
	return currentID;
}

extern ostream & operator <<(ostream & s, const Tile & t)
{
	s << "[" << t.x << "," << t.y << "]";
	return s;
}

void Tile::Main()
{
	string dir_tab = "NESW";
	string dir_tab_op = "SWNE";
		
	pthread_mutex_lock(&mutex_in);
	
		//syntax message : id#descriptor(argument)
		string message = queue_in.front();
		string id = message.substr(0, message.find_first_of('#'));
		string request = message.substr(message.find_first_of('#') + 1);
		string descriptor = request.substr(0, request.find_first_of('('));
		string arg = request.substr(request.find_first_of('(') + 1, request.find_first_of(')') - request.find_first_of('(') - 1);
		//cout << "Tile : " << *this << " IN => [ID = " << id << " | Descriptor = " << descriptor << " | Argument = " << arg << " ]" << endl;
		queue_in.pop();
	
	pthread_mutex_unlock(&mutex_in);
	
	if(descriptor == "reserv_T")
	{
		ostringstream oss;
		string access;
		if(reserved == false)
		{
			reserved = true;
			access = "granted";
		}
		else
		{
			access = "denied";
		}	
		oss << "Tile_" << dir_tab_op[dir_tab.find_first_of(id[5])] << "#access_T("<< access << ")";
		pthread_mutex_lock(&TileTab[dir_tab.find_first_of(id[5])]->mutex_in);
			TileTab[dir_tab.find_first_of(id[5])]->queue_in.push(oss.str());
		pthread_mutex_unlock(&TileTab[dir_tab.find_first_of(id[5])]->mutex_in);
	}
	else if(descriptor == "reserv_A")
	{
		currentID = id;
		ostringstream oss;
		if(TileTab[dir_tab.find_first_of(arg[0])] != NULL && TileTab[dir_tab.find_first_of(arg[0])]->GetEnabled() == true)
		{
			oss << "Tile_" << dir_tab_op[dir_tab.find_first_of(arg[0])] << "#reserv_T(" << dir_tab_op[dir_tab.find_first_of(arg[0])] << ")"; 
			pthread_mutex_lock(&TileTab[dir_tab.find_first_of(arg[0])]->mutex_in);
				TileTab[dir_tab.find_first_of(arg[0])]->queue_in.push(oss.str());
			pthread_mutex_unlock(&TileTab[dir_tab.find_first_of(arg[0])]->mutex_in);
		}
		else
		{
			oss << "Tile_" << arg[0] << "#access_T(noTile)";
			pthread_mutex_lock(&mutex_in);
				queue_in.push(oss.str());
			pthread_mutex_unlock(&mutex_in);
		}
	}
	else if(descriptor == "access_T")
	{
		string answer = currentID;
		answer += "#access_A(";
		if(arg == "granted")
		{
			answer += arg;
		}
		else
		{
			answer += "denied";
		}
		answer += ")";
		pthread_mutex_lock(&mutex_out);
			queue_out.push(answer);
		pthread_mutex_unlock(&mutex_out);
	}
	else if(descriptor == "leaving_A")
	{		
		TileTab[dir_tab.find_first_of(arg[0])]->currentID = "";
		TileTab[dir_tab.find_first_of(arg[0])]->reserved = false;
		TileTab[dir_tab.find_first_of(arg[0])]->currentA = 0;
		TileTab[dir_tab.find_first_of(arg[0])]->currentP = 0;
		TileTab[dir_tab.find_first_of(arg[0])]->currentI = 0;
	}
	else if(descriptor == "arriving_A")
	{
		currentID = id;
	}
	else if(descriptor == "check_ID")
	{
		string answer = id;
		answer += "#tile_ID(";
		answer += tileID;
		answer += ")";
		pthread_mutex_lock(&mutex_out);
			queue_out.push(answer);
		pthread_mutex_unlock(&mutex_out);
	}
	else if(descriptor == "check_Date")
	{
		currentID = id;
		string answer = currentID;
		answer += "#date(";
		answer += arg;
		answer += ":";
		if(TileTab[dir_tab.find_first_of(arg[0])] != NULL 
		&& TileTab[dir_tab.find_first_of(arg[0])]->GetEnabled() == true 
		&& TileTab[dir_tab.find_first_of(arg[0])]->GetReserved() == false)
		{
			answer += TileTab[dir_tab.find_first_of(arg[0])]->memory.find("Date")->second;
		}
		else
		{
			answer += "null";
		}
		answer += ")";
		pthread_mutex_lock(&mutex_out);
			queue_out.push(answer);
		pthread_mutex_unlock(&mutex_out);
	}
	else if(descriptor == "update_Date")
	{
		string dir = arg.substr(arg.find_first_of('(') + 1, arg.find_first_of(':'));
		string date = arg.substr(arg.find_first_of(':') + 1, arg.find_first_of(')'));
		TileTab[dir_tab_op.find_first_of(dir[0])]->memory.find("Date")->second = date;
	}
	else if(descriptor == "check_Phero")
	{
		currentID = id;
		string answer = currentID;
		answer += "#phero(";
		answer += arg;
		answer += ":";
		if(TileTab[dir_tab.find_first_of(arg[0])] != NULL 
		&& TileTab[dir_tab.find_first_of(arg[0])]->GetEnabled() == true 
		&& TileTab[dir_tab.find_first_of(arg[0])]->GetReserved() == false)
		{
			answer += TileTab[dir_tab.find_first_of(arg[0])]->memory.find("Phero")->second;
		}
		else
		{
			answer += "null";
		}
		answer += ")";
		pthread_mutex_lock(&mutex_out);
			queue_out.push(answer);
		pthread_mutex_unlock(&mutex_out);
	}
	else if(descriptor == "update_Phero")
	{
		string dir = arg.substr(arg.find_first_of('(') + 1, arg.find_first_of(':'));
		string phero = arg.substr(arg.find_first_of(':') + 1, arg.find_first_of(')'));
		TileTab[dir_tab_op.find_first_of(dir[0])]->memory.find("Phero")->second = phero;
		cf_=atof(phero.c_str());
	}
	else if(descriptor == "spread_This")
	{
		string source = arg.substr(arg.find_first_of('(') + 1, arg.find_first_of(':'));
		string message = arg.substr(arg.find_first_of(':') + 1, arg.find_first_of(')'));
		string counter = arg.substr(arg.find_first_of('#') + 1, arg.find_first_of('>') - arg.find_first_of('#') - 1);
		string dest = arg.substr(arg.find_first_of('>') + 1, arg.find_first_of('$') - arg.find_first_of('>') - 1);
		string path = arg.substr(arg.find_first_of('$') + 1, arg.find_first_of(')') - arg.find_first_of('$') - 1);
		string hops = arg.substr(arg.find_first_of('!') + 1, arg.find_first_of('#') - arg.find_first_of('!') - 1);
		message = message.substr(0, message.find_first_of('!'));
		int hops_i = atoi(hops.c_str());
		

		bool message_found = false;
		if(worldStateCounter - last_counter > 30) //mise à jour obligatoire
		{
			last_message = message;
			last_hops = hops_i;
			last_counter = worldStateCounter;
			message_found = true;
		}
		else if((last_message != message) || ((last_message == message) && (hops_i > last_hops))) //mise à jour si nécessaire
		{
			last_message = message;
			last_hops = hops_i;
			last_counter = worldStateCounter;
			message_found = true;
		}

		if(message_found)
		{	
			string dest_bot = "";
			dest_bot = (dest == "ALL" && currentID != "" ? currentID : dest);
			
			if(dest_bot == currentID)
			{
				ostringstream oss;
				oss << dest_bot << "#request(" << tileID << ":" << message << ":" << dest << ":" << path << source << ")";
				pthread_mutex_lock(&mutex_out);
					queue_out.push(oss.str());
				pthread_mutex_unlock(&mutex_out);
			}		
			hops_i--;
			if(hops_i >= 0)
			{
				for(int i = 0; i < 4; i++)
				{
					if(TileTab[i] != NULL && TileTab[i]->GetEnabled() && i != dir_tab.find_first_of(source[0]))
					{
						ostringstream oss;
						oss << id 
						<< "#spread_This(" 
						<< dir_tab_op[i] 
						<< ":" << message 
						<< "!" << hops_i 
						<< "#" << worldStateCounter 
						<< ">" << dest 
						<< "$" << path << source << ")";
						pthread_mutex_lock(&TileTab[i]->mutex_in);
							TileTab[i]->queue_in.push(oss.str());
						pthread_mutex_unlock(&TileTab[i]->mutex_in);
					}
				}
			}
		}
	}
	else if(descriptor == "state_I")
	{
		currentP = atoi(arg.substr(arg.find_first_of('(') + 1, arg.find_first_of(':')).c_str());
		currentI = atoi(arg.substr(arg.find_first_of(':') + 1, arg.find_first_of('!')).c_str());
		currentA = atoi(arg.substr(arg.find_first_of('!') + 1, arg.find_first_of(')')).c_str());
	}
	else if(descriptor == "perception_A")
	{
		int max_signal = 0;
		char dir = '$';
				
		for(int i = 0; i < 4; i++)
		{
			ostringstream os;
			os << currentID << "#description_A(" << dir_tab[i];
			if(TileTab[i] == NULL || TileTab[i]->GetEnabled() == false)
			{
				os << ":null)";
			}
			else if(TileTab[i]->GetCurrentID() != "")
			{
				if(TileTab[i]->currentI <= max_signal)
				{
					max_signal = TileTab[i]->currentI;
					dir = dir_tab[i];
				}
				os << ":agent)";
			}
			else
			{
				os << ":free)";
			}
			pthread_mutex_lock(&mutex_out);
				queue_out.push(os.str());
			pthread_mutex_unlock(&mutex_out);
		}
		ostringstream os;
		os << currentID << "#max_signal(" << dir << ":" << max_signal << ")";
		pthread_mutex_lock(&mutex_out);
			queue_out.push(os.str());
		pthread_mutex_unlock(&mutex_out);
	}
}

void Tile::Environment()
{
	//EVAP
	if(pheroCounter == 1000)
	{
		double coef_phero = coef_phero_;
		
		double current_phero = /*atof((memory.find("Phero")->second).c_str());*/cf_;

		current_phero = current_phero * coef_phero;
		ostringstream os;
		os << current_phero;
		memory.find("Phero")->second = os.str();
		cf_=current_phero;
		pheroCounter = 0;
	}
	else
	{
		pheroCounter++;
	}
}

