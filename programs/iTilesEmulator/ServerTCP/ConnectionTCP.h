#include <iostream>
using namespace std;
#include <string>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
using boost::asio::ip::tcp;

#include "World.h"

class ConnectionTCP : public boost::enable_shared_from_this<ConnectionTCP>
{
	public:
		
		//Pointeur partagé
		typedef boost::shared_ptr<ConnectionTCP> pointer;

		//Création d'un nouvel objet ConnectionTCP
		static pointer create(boost::asio::io_service& ios, World * w, int i, int j)
		{
			return pointer(new ConnectionTCP(ios, w, i, j));
		}
		
		//Fonction qui retourne la socket courante
		tcp::socket& socket()
		{
			return m_socket;
		}

		//Fonction executée lors de la création de l'objet ConnectionTCP :
		// - à la fin de l'envoi du message de bienvenue, la fonction "handle_start" est executée
		void start()
		{
			world->IncNAgents();
			//cout << world->GetNAgents() << endl;
			x = 0;
			y = 0;
			time_t t = time(NULL);
			struct tm * ts = localtime(&t);
			m_message = "Connected to Server [" + ((m_socket.local_endpoint()).address()).to_string() + "] @";
			ostringstream os;
			os << ts->tm_hour << ":" << ts->tm_min << ":" << ts->tm_sec;
			a = (a > 5 ? 1 : a);
			os << "#" << a;
			m_message += os.str();
			boost::asio::async_write(m_socket, boost::asio::buffer(m_message), 
									 boost::bind(&ConnectionTCP::handle_start, 
									 shared_from_this(), 
									 boost::asio::placeholders::error));
		}
	
		//Fonction d'envoi de messages vers le client :
		// - à la fin de l'envoi du message, la fonction "handle_write" est executée
		void write()
		{
			m_message.clear();
			cout << "> ";
			cin >> m_message;
			
			if(strlen(m_message.c_str()) < 63)
			{
				for(int i = strlen(m_message.c_str()); i < 64; i++)
				{
					m_message += ' ';
				}
				m_message[64] = '\0';
			}
			boost::asio::async_write(m_socket, 
									 boost::asio::buffer(m_message), 
									 boost::bind(&ConnectionTCP::handle_write, 
									 shared_from_this(), 
									 boost::asio::placeholders::error));
		}
		
		//Fonction de réception des messages provenant du client : 
		// - à la fin de l'envoi du message, la fonction "handle_write" est executée
		void read()
		{
			boost::asio::async_read(m_socket, 
									boost::asio::buffer(m_buffer), 
									boost::bind(&ConnectionTCP::handle_read, 
									shared_from_this(), 
									boost::asio::placeholders::error, 
									boost::asio::placeholders::bytes_transferred));
			
		}
		
		//Renvoie le point de connexion du client
		tcp::endpoint who()
		{
			return m_socket.remote_endpoint();
		}
		
		void send_answer(string s)
		{
			m_message = s;
			string arg = s.substr(s.find_first_of('(') + 1, s.find_first_of(')') - s.find_first_of('(') - 1);
			if(arg == "granted")
			{
				string s_buffer;
				int j = 0;
				while(m_buffer[j] != '\0')
				{
					s_buffer += m_buffer[j];
					j++;
				}
				string dir_s = s_buffer.substr(s_buffer.find_first_of('(') + 1, s_buffer.find_first_of(')') - s_buffer.find_first_of('(') - 1);
				switch(dir_to_int(dir_s))
				{
					case 1: y--; break;
					case 2: x++; break;
					case 3: y++; break;
					case 4: x--; break;
					default: break;
				}
			}
			if(strlen(m_message.c_str()) < 63)
			{
				for(int i = strlen(m_message.c_str()); i < 64; i++)
				{
					m_message += ' ';
				}
				m_message[64] = '\0';
			}
			boost::asio::async_write(m_socket, 
								 	boost::asio::buffer(m_message), 
								 	boost::bind(&ConnectionTCP::function_null, 
								 	shared_from_this(), 
								 	boost::asio::placeholders::error));
		}
		
		//Fonction qui ne fait absolument rien
		void function_null(const boost::system::error_code& error){}
		
		//Conversion direction(N,E,S,W) en int
		int dir_to_int(string s)
		{
			if(s == "N")
			{
				return 1;
			}
			else if (s == "E")
			{
				return 2;
			}
			else if(s == "S")
			{
				return 3;
			}
			else if (s == "W")
			{
				return 4;
			}
		}

	private:	
	
		//Socket serveur
		tcp::socket m_socket;
		//Buffers d'entrée/sortie
		string m_message;
		char m_buffer[64];
		//Environnement
		World * world;
		//Position courante
		int x;
		int y;
		//Indice algo
		int a;
		//Nb agents requis
		int nb_a;
		
		
		//Constructeur de l'objet ConnectionTCP
		ConnectionTCP(boost::asio::io_service& io_service, World * w, int i, int j) : m_socket(io_service)
		{
			world = w;
			a = i;
			nb_a = j;
		}

		//Fonction qui lance les threads d'écriture et/ou de lecture
		void handle_start(const boost::system::error_code& error)
		{
			if (!error)
			{
				boost::asio::async_read(m_socket, 
									boost::asio::buffer(m_buffer), 
									boost::bind(&ConnectionTCP::init_pos, 
									shared_from_this(), 
									boost::asio::placeholders::error, 
									boost::asio::placeholders::bytes_transferred));
									
				//boost::thread thread_in(boost::bind(&ConnectionTCP::read, shared_from_this()));
				//boost::thread thread_out(boost::bind(&ConnectionTCP::write, shared_from_this()));
			}
			else 
			{
				cout << error.message() << endl;
			}
		}

		//Fonction qui relance la fonction "write"
		void handle_write(const boost::system::error_code& error)
		{
			if (!error)
			{
				write();
			}
			else
			{
				cout << "Client [" << who() << "] disconnected\n";
				close();
			}
		}
		
		//Fonction de transmission du message du robot vers la dalle
		void handle_read(const boost::system::error_code& error, size_t number_bytes_read)
		{
			if(!error)
			{
				cout << endl << "Client [" << who() << "] : ";
				cout.write(&m_buffer[0], number_bytes_read);
				cout << endl;
				
				string mes_to_tile = who().address().to_string();
				mes_to_tile += ':';
				ostringstream port;
				port << who().port();
				mes_to_tile += port.str();
				mes_to_tile += '#';
				mes_to_tile += m_buffer;
					
				pthread_mutex_lock(&world->GetTile(x,y)->mutex_in);
					world->GetTile(x,y)->queue_in.push(mes_to_tile);
				pthread_mutex_unlock(&world->GetTile(x,y)->mutex_in);

				read();
			}
			else
			{
				cout << "Client [" << who() << "] disconnected\n";
				close();
			}
		}
		
		//Fonction d'initialisation de la position du robot sur la dalle
		void init_pos(const boost::system::error_code& error, size_t number_bytes_read)
		{
			if(!error)
			{
				cout << "Client [" << who() << "] : ";
				cout.write(&m_buffer[0], number_bytes_read);
				cout << endl;
				
				string buffer_string = m_buffer;
				string x_string = buffer_string.substr(0, buffer_string.find_first_of(','));
				string y_string = buffer_string.substr(buffer_string.find_first_of(',') + 1, buffer_string.length() - 1);
				x = atoi(x_string.c_str());
				y = atoi(y_string.c_str());
				
				world->GetTile(x,y)->SetReserved(true);
				ostringstream id;
				id << who().address().to_string() << ":" << who().port();
				world->GetTile(x,y)->SetCurrentID(id.str());
				while(world->GetNAgents() < nb_a)
				{
					usleep(100);
				}
				
				read();
			}
			else
			{
				cout << "Client [" << who() << "] disconnected\n";
				close();
			}
		}
		
		//Fermeture de la socket courante
		void close()
		{
			world->GetTile(x,y)->SetCurrentID("");
			world->GetTile(x,y)->SetReserved(false);
			m_socket.close();
			world->DecNAgents();
		}
};
