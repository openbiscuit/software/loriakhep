#include <iostream>
#include <sstream>
using namespace std;
#include <pthread.h>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
using boost::asio::ip::tcp;

#include "ConnectionTCP.h"

class ServerTCP
{		
	private:
		
		tcp::acceptor m_acceptor;
		int nb_connec;
		ConnectionTCP::pointer connec_tab[20];
		World * world;
		int algo;
		int agents_required;
		
		//Fonction d'écoute de nouvelles connexions
		//Si une nouvelle connexion arrive :
		// - on crée un nouvelle instance de l'objet ConnectionTCP
		// - on execute la fonction "handle_accept"
		void start_accept(World * w, int i, int j)
		{
			ConnectionTCP::pointer new_connection = ConnectionTCP::create(m_acceptor.io_service(), w, i, j);
			m_acceptor.async_accept(new_connection->socket(), boost::bind(&ServerTCP::handle_accept, this, new_connection, w, i, j, boost::asio::placeholders::error));
		}
		
		//Si la connexion a réussi :
		// - On commence le traitement de la connexion (cf ConnectionTCP.h)
		// - On relance la fonction start_accept() pour gérer les nouvelles connexions  
		void handle_accept(ConnectionTCP::pointer new_connection, World * w, int i, int j, const boost::system::error_code& error)
		{
			if (!error)
			{
				cout << "Client [" << new_connection->who() << "] connected" << endl;
				new_connection->start();
				start_accept(w, i, j);
				connec_tab[nb_connec] = new_connection;
				nb_connec++;
			}
		}
		
	public:
	
		//Constructeur du serveur
		ServerTCP(boost::asio::io_service& ios, string ip, int port, World * w, int alg, int agents) : m_acceptor(ios, tcp::endpoint(boost::asio::ip::address::from_string(ip), port))
		{
			cout << "> TCP server initialization | IP : " << (m_acceptor.local_endpoint()).address() << " | PORT : " << (m_acceptor.local_endpoint()).port() << endl << endl;
			world = w;
			start_accept(world, alg, agents);
			nb_connec = 0;
			pthread_t server_thread;
			pthread_create(&server_thread, 0, &StartThread, this);
			if(alg == 4)
			{
				pthread_t disable_thread;
				pthread_create(&disable_thread, 0, &StartThread2, this);
			}
		}
		
		//Consommation des messages de sortie
		void CheckForUpdate()
		{
			string message, id, answer;
			int k;
			while(true)
			{
				for(int i = 0; i < world->GetHeight(); i++)
				{
					for(int j = 0; j < world->GetWidth(); j++)
					{
						if(!world->GetTile(j,i)->queue_out.empty())
						{
							pthread_mutex_lock(&world->GetTile(j,i)->mutex_out);
							
								k = 0;
								message.clear();
								id.clear();
								message = world->GetTile(j,i)->queue_out.front();
								world->GetTile(j,i)->queue_out.pop();
								id = message.substr(0, message.find_first_of('#'));
								answer = message.substr(message.find_first_of('#') + 1);
								//cout << "Tile : " << *world->GetTile(j,i) << " OUT => [ID = " << id << " | Answer = " << answer << " ]" << endl;
								for(int l = 0; l <= nb_connec; l++)
								{
									string who = (*connec_tab[l]).who().address().to_string();
									who += ':';
									ostringstream oss;
									oss << (*connec_tab[l]).who().port();
									who += oss.str();
									if(id == who)
									{
										connec_tab[l]->send_answer(answer);
										break;
									}
								}				
	
							pthread_mutex_unlock(&world->GetTile(j,i)->mutex_out);
							//cout << *world << endl;
						}
					}
				}
				usleep(10);
			}
		}
		
		//Desactiver une dalle
		void DisableTile()
		{
			while(true)
			{
				int x,y,a;
				cout << "Enable => 1 | Disable => 2 : ";
				cin >> a;
				cout << " x = ";
				cin >> x;
				cout << " y = ";
				cin >> y;
				world->GetTile(x,y)->SetEnabled((a == 1 ? true : false));
				cout << "Tile " << *world->GetTile(x,y) << " " << (a == 1 ? "enabled" : "disabled") << endl;
			}
		}
		
		static void * StartThread(void * o)
		{
			reinterpret_cast<ServerTCP *>(o)->CheckForUpdate();
		}
		
		static void * StartThread2(void * o)
		{
			reinterpret_cast<ServerTCP *>(o)->DisableTile();
		}
};
