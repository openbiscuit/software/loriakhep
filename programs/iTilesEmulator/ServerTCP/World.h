#include <iostream>
using namespace std;
#include <vector>
#include <pthread.h>
#include <limits.h>
#include "Tile.h"

class World
{
	private :
		
		int nb_agents;
		vector<Tile> map;
		int map_height;
		int map_width;
		float coef_phero;
		
	public:
	
		World(double _coef_phero = 0.999999);
		World(int h, int w,double _coef_phero = 0.999999);
		~World();
		Tile * GetTile(int x, int y);
		int GetHeight() const;
		int GetWidth() const;
		int GetNTiles() const;
		int GetNAgents() const;
		void IncNAgents();
		void DecNAgents();
		friend ostream & operator <<(ostream &, const World &);
		void CheckForUpdate();
		static void * StartThread(void * o)
		{
			reinterpret_cast<World *>(o)->CheckForUpdate();
		}
};

World::World(double _coef_phero)
{

}

World::World(int h, int w,double _coef_phero)
{
	coef_phero=_coef_phero;
	worldStateCounter = 0;
	nb_agents = 0;
	map_height = h;
	map_width = w;
	map.resize(map_height * map_width);
	//Création des dalles
	for(int i = 0; i < map_height; i++)
	{
		for(int j = 0; j < map_width; j++)
		{
			map[j + i * map_width] = Tile(j,i,coef_phero);
		}
	}
	//Création des jonctions entre dalles
	for(int k = 0; k < map_height; k++)
	{
		for(int l = 0; l < map_width; l++)
		{
			if(k != map_height - 1)
			{
				map[l + k * map_width].S = map[l + k * map_width].TileTab[2] = &map[l + k * map_width + map_width];
			}
			if(k != 0)
			{
				map[l + k * map_width].N = map[l + k * map_width].TileTab[0] = &map[l + k * map_width - map_width];
			}
			if(l != 0)
			{
				map[l + k * map_width].W = map[l + k * map_width].TileTab[3] = &map[l + k * map_width - 1];
			}
			if(l != map_width - 1)
			{
				map[l + k * map_width].E = map[l + k * map_width].TileTab[1] = &map[l + k * map_width + 1];
			}
		}
	}
	//Lancement du thread de sortie
	pthread_t world_thread;
	pthread_create(&world_thread, 0, &StartThread, this);
}

World::~World()
{

}


void World::CheckForUpdate()
{
	while(true)
	{
		for(int i = 0; i < map_height; i++)
		{
			for(int j = 0; j < map_width; j++)
			{
				if(!GetTile(j,i)->queue_in.empty())
				{
					GetTile(j,i)->Main();
				}
				GetTile(j,i)->Environment();
			}
		}
		worldStateCounter += (worldStateCounter + 1 == ULONG_MAX ? -worldStateCounter : 1);
		usleep(10);
	}
}

Tile * World::GetTile(int x, int y)
{
	int pos = x + y * map_width;
	if(pos < 0 || pos > map_height * map_width)
	{
		return NULL;
	}
	Tile * t = &map[pos];
	return t;
}

int World::GetHeight() const
{
	return map_height;
}

int World::GetWidth() const
{
	return map_width;
}

int World::GetNTiles() const
{
	return map_height * map_width;
}

int World::GetNAgents() const
{
	return nb_agents;
}

void World::IncNAgents()
{
	nb_agents++;
}

void World::DecNAgents()
{
	nb_agents--;
}

ostream & operator <<(ostream & s, World & w)
{
	//Voisins
/*	for(int i = 0; i < w.GetHeight(); i++)
	{
		for(int j = 0; j < w.GetWidth(); j++)
		{
			s << *w.GetTile(j,i) << " | ";
			s << "N : ";
			(*w.GetTile(j,i)).N == NULL ? s << "NULL" : s << *(*w.GetTile(j,i)).N;
			s << " | S : ";
			(*w.GetTile(j,i)).S == NULL ? s << "NULL" : s << *(*w.GetTile(j,i)).S;
			s << " | W : ";
			(*w.GetTile(j,i)).W == NULL ? s << "NULL" : s << *(*w.GetTile(j,i)).W;
			s << " | E : ";
			(*w.GetTile(j,i)).E == NULL ? s << "NULL" : s << *(*w.GetTile(j,i)).E;
			s << endl;
		}
	}
	*/
	//Accessibilité
/*	for(int i = 0; i < w.GetHeight(); i++)
	{
		for(int j = 0; j < w.GetWidth(); j++)
		{
			s << *w.GetTile(j,i) << "[";
			w.GetTile(j,i)->GetEnabled() == true ? s << "X]" : s << " ]";
			s << " ";
		}
		s << endl;
	}
	*/
	//EVAW
/*	for(int i = 0; i < w.GetHeight(); i++)
	{
		for(int j = 0; j < w.GetWidth(); j++)
		{
			s << *w.GetTile(j,i) << "[" << w.GetTile(j,i)->memory.find("Date")->second << "] ";
		}
		s << endl;
	}
	*/
	//EVAP
	for(int i = 0; i < w.GetHeight(); i++)
	{
		for(int j = 0; j < w.GetWidth(); j++)
		{
			s << *w.GetTile(j,i) << "[" << w.GetTile(j,i)->memory.find("Phero")->second << "] ";
		}
		s << endl;
	}
	return s;
}
