#include <iostream>
using namespace std;
#include <math.h>
#include <cairomm/cairomm.h>
#include <gtkmm/drawingarea.h>
using namespace Gtk;
class InterfaceCairo : public DrawingArea
{
	private:
		
		World * w;
		int algo_id;
		int g_size;
	
	public:
		
		InterfaceCairo();
		InterfaceCairo(World * world, int algo, int size);
		bool on_expose_event();
};

InterfaceCairo::InterfaceCairo()
{
	
}

InterfaceCairo::InterfaceCairo(World * world, int algo, int size)
{
	w = world;
	algo_id = algo;
	g_size = size;
	Glib::signal_timeout().connect(sigc::mem_fun(*this, &InterfaceCairo::on_expose_event), 500);
}

bool InterfaceCairo::on_expose_event()
{
	Glib::RefPtr<Gdk::Window> window = get_window();
	if(window)
	{
		Cairo::RefPtr<Cairo::Context> cr = window->create_cairo_context();
		cairo_t * cr2 = cr->cobj();
		cairo_select_font_face(cr2, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
		cairo_set_font_size(cr2, floor(g_size * 1.1 / 10));
		int i,j;
		float phero_value;

		for(i = 0; i < w->GetHeight(); i++)
		{
			for(j = 0; j < w->GetWidth(); j++)
			{
				if(!w->GetTile(j,i)->GetEnabled())
				{
					cr->set_source_rgb(0.0, 0.0, 1.0);
					cr->rectangle(j * g_size, i * g_size, g_size, g_size);
					cr->fill();	
				}
				else
				{
					string info_text;
					switch(algo_id)
					{
						case 1:
							cr->set_source_rgb(1.0, 1.0, 1.0);
							break;
						case 2:
							cr->set_source_rgb(1.0, 1.0, 1.0);
							break;
						case 3:					
							phero_value = atof((w->GetTile(j,i)->memory.find("Phero")->second).c_str()) / 100;
							cr->set_source_rgb(phero_value, phero_value, phero_value);
							break;
						case 5:					
							cr->set_source_rgb(1 - w->GetTile(j,i)->currentA, 1 - w->GetTile(j,i)->currentA, 1 - w->GetTile(j,i)->currentA);
							break;
						default:
							cr->set_source_rgb(1.0, 1.0, 1.0);
							break;
					}
					cr->rectangle(j * g_size, i * g_size, g_size, g_size);
					cr->fill();
					ostringstream oss;
					ostringstream oss2;
					ostringstream oss3;
					std::map<string,string>::iterator it;
					std::map<string,string>::iterator it2;
					int max = 0;
					switch(algo_id)
					{
						case 1:
							break;
						case 2:
							cr->set_source_rgb(1.0, 0.0, 0.0);
							cr->move_to(j * g_size + 7, i * g_size + floor(g_size * 0.7));
							info_text = "Date :";
							cairo_show_text(cr2, info_text.c_str());
							info_text = w->GetTile(j,i)->memory.find("Date")->second;
							cr->move_to(j * g_size + 7, i * g_size + 80);
							cairo_show_text(cr2, info_text.c_str());
							break;
						case 3:					
							cr->set_source_rgb(1.0, 0.0, 0.0);
							cr->move_to(j * g_size + 7, i * g_size + floor(g_size * 0.7));
							oss << floor(atof((w->GetTile(j,i)->memory.find("Phero")->second).c_str()));
							info_text = "Phero = " + oss.str();
							cairo_show_text(cr2, info_text.c_str());
							break;
						case 4:
							cr->set_source_rgb(1.0, 0.0, 0.0);
							cr->move_to(j * g_size + 7, i * g_size + floor(g_size * 0.71));
							info_text = "Message :";
							cairo_show_text(cr2, info_text.c_str());
							cr->set_source_rgb(0.0, 0.0, 1.0);
							info_text = w->GetTile(j,i)->last_message;
							info_text += ":";
							oss3 <<  w->GetTile(j,i)->last_hops;
							info_text += oss3.str();
							cr->move_to(j * g_size + 7, i * g_size + floor(g_size * 0.82));
							cairo_show_text(cr2, info_text.c_str());
							break;
						case 5:
							cr->set_source_rgb(1.0, 0.0, 0.0);
							cr->move_to(j * g_size + 7, i * g_size + floor(g_size * 0.7));
							if(w->GetTile(j,i)->GetCurrentID() != "")
							{
								oss << "P : " << w->GetTile(j,i)->currentP;
								oss2 << "I : " << w->GetTile(j,i)->currentI;
							}
							else
							{
								oss << "";
								oss2 << "";
							}
							cairo_show_text(cr2, oss.str().c_str());
							cr->move_to(j * g_size + 11, i * g_size + floor(g_size * 0.82));
							cairo_show_text(cr2, oss2.str().c_str());
							break;
						default:
							break;
					}
				}
				if(w->GetTile(j,i)->GetCurrentID() != "")
				{
					Glib::RefPtr<Gdk::Pixbuf> image = Gdk::Pixbuf::create_from_file("khepera3_mini.png");
					image->render_to_drawable(get_window(), get_style()->get_black_gc(), 
					0, 0, j * g_size + ((g_size - image->get_width()) / 2), i * g_size + 4, image->get_width(), image->get_height(), 
					Gdk::RGB_DITHER_NONE, 0, 0);
					
					cairo_select_font_face(cr2, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
					cairo_set_font_size(cr2, floor(g_size * 1.1 / 10));
					cr->set_source_rgb(1.0, 0.0, 0.0);
					cr->move_to(j * g_size + 11, i * g_size + floor(g_size * 0.6));
					cairo_show_text(cr2, (w->GetTile(j,i)->GetCurrentID().substr(0, w->GetTile(j,i)->GetCurrentID().find_first_of(':'))).c_str());
				}
				cairo_select_font_face(cr2, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
				cairo_set_font_size(cr2, floor(g_size * 1.1 / 10));
				cr->set_source_rgb(1.0, 0.0, 0.0);
				cr->move_to(j * g_size + 3, i * g_size + floor(g_size * 0.96));
				ostringstream oss;
				oss << "[" << w->GetTile(j,i)->GetX() << "," << w->GetTile(j,i)->GetY() << "]";
				cairo_show_text(cr2, (oss.str()).c_str());
				cr->stroke();
			}
		}
		
		int small_wall_size = 4;
    	cr->set_source_rgb(0.0, 0.0, 1.0);
		for(i = 0; i < w->GetHeight(); i++)
		{
			for(j = 0; j < w->GetWidth(); j++)
			{
				for(int k = 0; k < 4; k++)
				{
					if(w->GetTile(j,i)->TileTab[k] == 0)
					{
						switch(k)
						{
							case 0:
								cr->rectangle(j * g_size, i * g_size, g_size, small_wall_size);
								cr->fill();	
								break;
							case 1:
								cr->rectangle(j * g_size + g_size - small_wall_size, i * g_size, small_wall_size, g_size);
								cr->fill();
								break;
							case 2:
								cr->rectangle(j * g_size, i * g_size + g_size - small_wall_size, g_size, small_wall_size);
								cr->fill();
								break;
							case 3:
								cr->rectangle(j * g_size, i * g_size, small_wall_size, g_size);
								cr->fill();
								break;
							default:
								break;
						}
					}
				}
			}
		}
		
		cr->set_line_width(3.0);
    	cr->set_source_rgb(1.0, 0.0, 0.0);
		for(i = 0; i <= w->GetWidth(); i++)
		{
			cr->move_to(i * g_size, 0);
			cr->line_to(i * g_size, w->GetHeight() * g_size);
		}
		for(j = 0; j <= w->GetHeight(); j++)
		{
			cr->move_to(0, j * g_size);
			cr->line_to(w->GetWidth() * g_size, j * g_size);
		}
		cr->stroke();
	}
	return true;
}
