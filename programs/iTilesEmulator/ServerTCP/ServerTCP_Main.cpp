#include <iostream>
using namespace std;
#include <limits.h>
#include "ServerTCP.h"
#include "InterfaceCairo.h"
#include <gtkmm/main.h>
#include <gtkmm/window.h>
using namespace Gtk;

// Compilation : g++ $(pkg-config --libs --cflags cairomm-1.0 gtkmm-2.4) ServerTCP_Main.cpp -o ServerTCP -lboost_system -lboost_thread

int algo;
//Modifier cette variable pour faire varier la taille des dalles dans l'interface
int g_size = 89;
int wx,wy;
static void * StartThread(void * o)
{
	World * w = reinterpret_cast<World *>(o);
	InterfaceCairo I(w, algo, g_size);
	Window win;
	win.set_default_size(w->GetWidth() * g_size, w->GetHeight() * g_size);
	win.set_icon_from_file("khepera3.png");
	win.set_position(WIN_POS_CENTER);
   	win.set_title("I-Tiles Emulator");
  	win.add(I);
   	I.show();
   	Gtk::Main::run(win);
}

int main(int argc, char ** argv)
{
	Gtk::Main kit(argc, argv);
	double coef_phero = 0.999;
	if(argc>=3)
	{
		wx = atoi(argv[1]);
		wy=atoi(argv[2]);
		
		
	}
	else
	{
		wx=2;wy=2;
	}
	if(argc>=4)
	{
		coef_phero=atof(argv[3]);
		printf("evap coef:%f\n",coef_phero);
	}
	//Paramètres du serveur :
	int port = 5757;
	string ip = "192.168.0.20";
	//string ip = "127.0.0.1";
	
	
	algo = 0;
	int agents_required = 1;
	if(argc>= 6)
	{
		algo = atoi(argv[4]);;
		agents_required = atoi(argv[5]);
	}
		else
		{
	cout << endl << "> [ I-Tiles Emulator ]" << endl << endl;
	cout << "  1 : Random Track" << endl;
	cout << "  2 : EVAW" << endl; 
	cout << "  3 : EVAP" << endl;
	cout << "  4 : Spreading" << endl;
	cout << "  5 : Sat-Alt" << endl;
	cout << endl;
	cout << "  Algo ? : ";
	cin >> algo;
	switch(algo)
	{
		case 1:
			break;
		case 2:
			cout << "  Agents ? : ";
			cin >> agents_required;
			break;
		case 3:
			cout << "  Agents ? : ";
			cin >> agents_required;
			break;
		case 4:
			break;
		case 5:
			break;
		default:
			break;
	}
	cout << endl; 
}
	try
	{
		//World(Hauteur, Largeur)
		World * w = new World(wx,wy,coef_phero);		
		
		/*w->GetTile(0,0)->BreakLink("E");
		w->GetTile(1,0)->BreakLink("W");
		w->GetTile(2,1)->BreakLink("E");
		w->GetTile(3,1)->BreakLink("W");
		w->GetTile(0,2)->BreakLink("E");
		w->GetTile(1,2)->BreakLink("W");
		w->GetTile(1,3)->BreakLink("E");
		w->GetTile(2,3)->BreakLink("W");
		
		w->GetTile(2,0)->BreakLink("S");
		w->GetTile(2,1)->BreakLink("N");
		w->GetTile(3,1)->BreakLink("S");
		w->GetTile(3,2)->BreakLink("N");
		w->GetTile(1,1)->BreakLink("S");
		w->GetTile(1,2)->BreakLink("N");
		w->GetTile(2,2)->BreakLink("S");
		w->GetTile(2,3)->BreakLink("N");
		w->GetTile(0,2)->BreakLink("S");
		w->GetTile(0,3)->BreakLink("N");*/
		/*w->GetTile(1,0)->BreakLink("E");
		w->GetTile(2,0)->BreakLink("W");
		
		w->GetTile(1,2)->BreakLink("E");
		w->GetTile(2,2)->BreakLink("W");
		
		w->GetTile(2,1)->BreakLink("E");
		w->GetTile(3,1)->BreakLink("W");
		
		w->GetTile(2,2)->BreakLink("E");
		w->GetTile(3,2)->BreakLink("W");
		
		w->GetTile(4,1)->BreakLink("E");
		w->GetTile(5,1)->BreakLink("W");
		
		w->GetTile(4,2)->BreakLink("E");
		w->GetTile(5,2)->BreakLink("W");
		
		w->GetTile(5,0)->BreakLink("E");
		w->GetTile(6,0)->BreakLink("W");
		
		w->GetTile(5,1)->BreakLink("E");
		w->GetTile(6,1)->BreakLink("W");
		
		w->GetTile(5,2)->BreakLink("E");
		w->GetTile(6,2)->BreakLink("W");
		
		w->GetTile(0,5)->BreakLink("E");
		w->GetTile(1,5)->BreakLink("W");
		
		w->GetTile(0,6)->BreakLink("E");
		w->GetTile(1,6)->BreakLink("W");
		
		w->GetTile(3,5)->BreakLink("E");
		w->GetTile(4,5)->BreakLink("W");
		
		w->GetTile(3,6)->BreakLink("E");
		w->GetTile(4,6)->BreakLink("W");
		
		w->GetTile(4,5)->BreakLink("E");
		w->GetTile(5,5)->BreakLink("W");
		
		w->GetTile(4,6)->BreakLink("E");
		w->GetTile(5,6)->BreakLink("W");
		
		w->GetTile(6,5)->BreakLink("E");
		w->GetTile(7,5)->BreakLink("W");
		
		w->GetTile(6,6)->BreakLink("E");
		w->GetTile(7,6)->BreakLink("W");
		
		w->GetTile(0,2)->BreakLink("S");
		w->GetTile(0,3)->BreakLink("N");
		
		w->GetTile(1,2)->BreakLink("S");
		w->GetTile(1,3)->BreakLink("N");
		
		w->GetTile(3,0)->BreakLink("S");
		w->GetTile(3,1)->BreakLink("N");
		
		w->GetTile(4,0)->BreakLink("S");
		w->GetTile(4,1)->BreakLink("N");
		
		w->GetTile(4,2)->BreakLink("S");
		w->GetTile(4,3)->BreakLink("N");
		
		w->GetTile(6,1)->BreakLink("S");
		w->GetTile(6,2)->BreakLink("N");
		
		w->GetTile(6,2)->BreakLink("S");
		w->GetTile(6,3)->BreakLink("N");
		
		w->GetTile(1,3)->BreakLink("S");
		w->GetTile(1,4)->BreakLink("N");
		
		w->GetTile(2,3)->BreakLink("S");
		w->GetTile(2,4)->BreakLink("N");
		
		w->GetTile(3,3)->BreakLink("S");
		w->GetTile(3,4)->BreakLink("N");
		
		w->GetTile(4,3)->BreakLink("S");
		w->GetTile(4,4)->BreakLink("N");
		
		w->GetTile(5,3)->BreakLink("S");
		w->GetTile(5,4)->BreakLink("N");
		
		w->GetTile(6,3)->BreakLink("S");
		w->GetTile(6,4)->BreakLink("N");
		
		w->GetTile(0,4)->BreakLink("S");
		w->GetTile(0,5)->BreakLink("N");
		
		w->GetTile(1,4)->BreakLink("S");
		w->GetTile(1,5)->BreakLink("N");
		
		w->GetTile(2,4)->BreakLink("S");
		w->GetTile(2,5)->BreakLink("N");
		
		w->GetTile(3,4)->BreakLink("S");
		w->GetTile(3,5)->BreakLink("N");
		
		w->GetTile(5,4)->BreakLink("S");
		w->GetTile(5,5)->BreakLink("N");
		
		w->GetTile(6,4)->BreakLink("S");
		w->GetTile(6,5)->BreakLink("N");
		
		w->GetTile(2,6)->BreakLink("S");
		w->GetTile(2,7)->BreakLink("N");
		
		w->GetTile(3,6)->BreakLink("S");
		w->GetTile(3,7)->BreakLink("N");
		
		w->GetTile(6,6)->BreakLink("S");
		w->GetTile(6,7)->BreakLink("N");*/
		
		//cout << *w << endl;

		pthread_t interface_thread;
		pthread_create(&interface_thread, 0, &StartThread, w);
		
		boost::asio::io_service io_service;
		ServerTCP server(io_service, ip, port, w, algo, agents_required);
		io_service.run();
	}
	catch (exception& e)
	{
		cerr << e.what() << endl;
	}
    return 0;
}
