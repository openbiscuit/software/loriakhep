#
# (c) 2006-2008 EPFL, Lausanne, Switzerland
# Thomas Lochmatter
# 
# 22/01/2009 Modified by Alain Dutech 
#            so that both 'i686' and 'arm' version are build
#

# Static target, used when compiling with "make static"
#STATICTARGET_ARM	:= $(TARGET_ARM)-static

################## Toolchain Selection ##################
# Activate one of the toolchains below by commenting out the corresponding lines.
# Note that if you change the toolchain, you need to recompile all modules before recompiling all programs. The k3-make-all script will help you doing this.

# Toolchain (compiler, linker, ...) if you are using kernel 2.4 (korebot-tools-0.1.2)
#CC			:= arm-linux-gcc

# Toolchain (compiler, linker, ...) if you are using kernel 2.6 (korebot-oetools-1.0)
CC_ARM			:= arm-angstrom-linux-gnueabi-gcc
#########################################################
CC_I686		:= gcc
#########################################################

# Preparing files and flags
SRCS		:= $(wildcard ClientTCP.c)
OBJS_ARM	:= $(SRCS:.c=_arm.o)
OBJS_I686	:= $(SRCS:.c=_i686.o)
MODS_ARM	:= $(MODS_ARM_ADD)
MODS_ARM	:= $(MODS_ARM) $(foreach MOD,$(MY_MODULES),$(MOD)_arm/*.a)
MODS_ARM	:= $(MODS_ARM) $(foreach MOD,$(MODULES),$(K3_ROOT)/Modules/$(MOD)/$(MOD)_arm.a)
MODS_I686	:= $(MODS_I686_ADD)
MODS_I686	:= $(MODS_I686) $(foreach MOD,$(MY_MODULES),$(MOD)/*_i686.a)
MODS_I686	:= $(MODS_I686) $(foreach MOD,$(MODULES),$(K3_ROOT)/Modules/$(MOD)/$(MOD)_i686.a)
INCS		:= $(INCS_ADD)
INCS		:= $(INCS) $(foreach MOD,$(MY_MODULES),-I $(MOD))
INCS		:= $(INCS) $(foreach MOD,$(MODULES),-I $(K3_ROOT)/Modules/$(MOD))
LIBS		:= -lm $(LIBS_ADD) -lpthread
CFLAGS_ARM 	:= 
CFLAGS_I686	:=


.PHONY: all
all: arm i686

$(TARGET_ARM): $(OBJS_ARM) $(MODS_ARM)
	@echo "=== Building for arm $@"
	$(CC_ARM) -o $@ $(OBJS_ARM) $(MODS_ARM) $(LIBS) $(INCS) $(CFLAGS_ARM)

$(STATICTARGET_ARM): $(OBJS_ARM)
	@echo "=== Building for arm $@ (statically)"
	$(CC_ARM) -o $@ $(OBJS_ARM) $(MODS_ARM) $(LIBS) -static $(INCS) $(CFLAGS_ARM)

$(TARGET_I686): $(OBJS_I686) $(MODS_I686)
	@echo "=== Building for i686 $@"
	$(CC_I686) -o $@ $(OBJS_I686) $(MODS_I686) $(LIBS) $(INCS) $(CFLAGS_I686)

.PHONY: arm
arm: 	$(TARGET_ARM)
.PHONY: i686
i686: 	$(TARGET_I686)

.PHONY: static
static: $(STATICTARGET_ARM)

.PHONY: clean
clean:
	@echo "=== Cleaning"
	rm -f *.o .depend $(TARGET_ARM) $(STATICTARGET_ARM) $(TARGET_I686) *~

.PHONY: depend
depend:
	@echo "=== Building dependencies"
	rm -f .depend
	touch .depend
	makedepend $(SYS_INCLUDES) $(INCS) -Y -f .depend $(SRCS)

%_arm.o: %.c
	@echo "=== Compiling $@"
	$(CC_ARM) -Wall $(INCS) -c $(CFLAGS_ARM) $< -o $@

%_i686.o: %.c
	@echo "=== Compiling $@"
	$(CC_I686) -Wall $(INCS) -c $(CFLAGS_I686) $< -o $@

ifeq (.depend,$(wildcard .depend))
include .depend 
endif
