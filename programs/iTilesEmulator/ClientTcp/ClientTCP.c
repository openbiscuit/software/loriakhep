#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <math.h>
#include <sys/time.h>
#include <time.h>
#include "khepera3.h"
#include "odometry_track.h"
#include "odometry_goto.h"
#include "local_remote_lib.h"
#include "nmea.h"
#include "FIFO.h"
#define PI 3.14159265358979

//Compilation I686 : gcc -o ClientTCP ClientTCP.c -lpthread
//Compilation ARM + I686 : make

//Buffers d'entrée/sortie   
char buffer_in[64];
char buffer_out[64];
//Identifiant de la socket client
int socket_client;
//File
FIFO * F;
//Indicateur de file libre
int FIFOFREE;
//Positions sur les dalles
int x;
int y;

float dist_angle = 0;
int side = 0;

int bot_hour;
int bot_min;
int bot_sec;

int connec_hour;
int connec_min;
int connec_sec;

int tile_stop = 1;

char tileID[] = "";

int algo = 1;

int gradVal[4];

int irLimit = 1300;

void gotoM(float x, float y)
{
	khepera3_drive_set_current_position(0,0);
	khepera3_drive_goto_position_using_profile((int) (x * 214.6), (int) (y * 214.6));
}

void gotoP(float x, float y)
{
	odometry_track_init();
	odometry_goto_init();
	
	struct sOdometryTrack ot;
	struct sOdometryGoto og;
	
	odometry_track_start(&ot);
	odometry_goto_start(&og, &ot);
	
	odometry_goto_set_goal(&og, x * 0.0098, y * 0.0098);
	
	while (og.result.atgoal == 0) 
	{	
		odometry_track_step(og.track);
		odometry_goto_step(&og);
		khepera3_drive_set_speed(og.result.speed_left, og.result.speed_right);
		if(og.track->result.x >= x && og.track->result.y >= y)
		{
			break;
		}
	}
	khepera3_drive_set_speed(0, 0);
}

void gotoR(float x)
{
	odometry_track_init();
	odometry_goto_init();
	
	struct sOdometryTrack ot;
	struct sOdometryGoto og;
	
	odometry_track_start(&ot);
	odometry_goto_start(&og, &ot);
	
	float diff_theta;
	
	while (1) 
	{
		odometry_track_step(og.track);
		diff_theta = (PI * (x * 0.985) / 180) - og.track->result.theta;
		//diff_theta = x - og.track->result.theta;
		while (diff_theta > PI)
		{
			diff_theta -= 2 * PI;
		}
		while (diff_theta < -PI)
		{
			diff_theta += 2 * PI;
		}
		if (fabs(diff_theta) < 0.01) 
		{
			break;
		}
		khepera3_drive_set_speed_differential_bounded(og.configuration.speed_max / 1.5, 0, 0, diff_theta * 8., 1);
	}
	khepera3_drive_set_speed(0, 0);
}

float gotoPIN(float x, float y)
{
	khepera3_drive_set_current_position(0,0);

	odometry_track_init();
	odometry_goto_init();
	
	struct sOdometryTrack ot;
	struct sOdometryGoto og;
	
	odometry_track_start(&ot);
	odometry_goto_start(&og, &ot);
	
	odometry_goto_set_goal(&og, x * 0.0098, y * 0.0098);
	
	int l_right, l_left, ir_l, ir_r;
	int start_cross_left = 0;
	int start_cross_right = 0;
	float pos_start = 0.0, pos_end = 0.0;
	int start_record = 0;
	float speed_coef = 2;
	int black_in = 0;
	int pos_grad_in;
	int ir_limit = irLimit;

	khepera3_drive_get_current_position();
	int pos_l_start = khepera3.motor_left.current_position;
	int pos_r_start = khepera3.motor_right.current_position;
	printf("START : L = %d | R = %d \n", pos_l_start, pos_r_start);

	while(og.result.atgoal == 0 || black_in == 0) 
	{
		khepera3_infrared_proximity();
		l_right = khepera3.infrared_proximity.sensor[cKhepera3SensorsInfrared_FloorRight];
		l_left = khepera3.infrared_proximity.sensor[cKhepera3SensorsInfrared_FloorLeft];
		//printf("IR left : %d | IR right : %d\n", l_left, l_right);
		
		if(start_cross_left == 0 && l_left < ir_limit)
		{
			ir_l = l_left;
			//printf("IR Left = %d\n", l_left);
			start_cross_left++;
			if(start_record == 0)
			{
				khepera3_drive_get_current_position();
				pos_start = (-khepera3.motor_left.current_position + khepera3.motor_right.current_position) / 2;
				start_record = 1;
			}
		}
		
		if(start_cross_right == 0 && l_right < ir_limit)
		{
			ir_r = l_right;
			//printf("IR Right = %d\n", l_right);
			start_cross_right++;
			if(start_record == 0)
			{
				khepera3_drive_get_current_position();
				pos_start = (-khepera3.motor_left.current_position + khepera3.motor_right.current_position) / 2;
				start_record = 2;
			}
		}
		
		if(start_cross_left + start_cross_right == 2)
		{
			start_cross_left++;	
			khepera3_drive_get_current_position();
			pos_end = (-khepera3.motor_left.current_position + khepera3.motor_right.current_position) / 2;
			float distance = (pos_end - pos_start) * 0.0465856;
			dist_angle = distance;
			//printf("Distance Theta = %0.1f mm\n", distance);
			double cosA = (double) (17.5 / (sqrt(pow(distance, 2) + 306.25)));
			float theta = (acos(cosA) / PI) * 180;
			//printf("Angle = %.1f°\n", start_record == 1 ? -theta : theta);
			khepera3_drive_set_current_position(0,0);
			gotoR(start_record == 1 ? -theta : theta);
			pos_grad_in = pos_end;
			speed_coef = 2;
			black_in = 1;
			return (theta / 180) * PI;
		}
		
		odometry_track_step(og.track);
		odometry_goto_step(&og);
		khepera3_drive_set_speed(og.result.speed_left / speed_coef, og.result.speed_right / speed_coef);

		khepera3_drive_get_current_position();
		int pos_l_current = khepera3.motor_left.current_position;
		int pos_r_current = khepera3.motor_right.current_position;

		//printf("CURRENT : L = %d | R = %d \n", pos_l_current, pos_r_current);
		if((-pos_l_current + pos_r_current) / 2 > 4450)
		{
			return -1;
		}
	}
	khepera3_drive_set_speed(0, 0);
	printf("\n");
	return 0;
}

void lookIR()
{
	int l_right, l_left;
	//while(1)
	//{
		khepera3_infrared_proximity();
		l_right = khepera3.infrared_proximity.sensor[cKhepera3SensorsInfrared_FloorRight];
		l_left = khepera3.infrared_proximity.sensor[cKhepera3SensorsInfrared_FloorLeft];
		printf("IR Left : %d | IR Right : %d\n", l_left, l_right);
	//}
}

float gotoGrad(float theta)
{
	odometry_track_init();
	odometry_goto_init();
	
	struct sOdometryTrack ot;
	struct sOdometryGoto og;
	
	odometry_track_start(&ot);
	odometry_goto_start(&og, &ot);
	
	odometry_goto_set_goal(&og, 10 * 0.0098, 0 * 0.0098);
	
	khepera3_drive_get_current_position();
	int pos_start = (-khepera3.motor_left.current_position + khepera3.motor_right.current_position) / 2;
	int pos_end = pos_start;
	
	int grad_width = 0.75;
	float d_to_center = 17 - (58 - (cos(theta - asin(8.5/58)) * (58 - dist_angle))) + grad_width;
	//printf("D to Center = %f\n", d_to_center);
	
	while(og.result.atgoal == 0 && (pos_end - pos_start) * 0.0465856 < d_to_center) 
	{
		khepera3_drive_get_current_position();
		pos_end = (-khepera3.motor_left.current_position + khepera3.motor_right.current_position) / 2;
		odometry_track_step(og.track);
		odometry_goto_step(&og);
		khepera3_drive_set_speed(og.result.speed_left / 3, og.result.speed_right / 3);
	}
	khepera3_drive_set_speed(0, 0);
	khepera3_infrared_proximity();
	int l_right = khepera3.infrared_proximity.sensor[cKhepera3SensorsInfrared_FloorRight];
	int l_left = khepera3.infrared_proximity.sensor[cKhepera3SensorsInfrared_FloorLeft];
	int pos_r = 0, pos_l = 0;
	
	pos_r = (l_right < gradVal[0] ? 0 : pos_r);
	pos_r = (l_right > gradVal[0] && l_right < gradVal[1] ? 1 : pos_r);
	pos_r = (l_right > gradVal[1] && l_right < gradVal[2] ? 2 : pos_r);
	pos_r = (l_right > gradVal[2] && l_right < gradVal[3] ? 3 : pos_r);
	pos_r = (l_right > gradVal[3] ? 4 : pos_r);
	pos_l = (l_left < gradVal[0] ? 0 : pos_l);
	pos_l = (l_left > gradVal[0] && l_left < gradVal[1] ? 1 : pos_l);
	pos_l = (l_left > gradVal[1] && l_left < gradVal[2] ? 2 : pos_l);
	pos_l = (l_left > gradVal[2] && l_left < gradVal[3] ? 3 : pos_l);
	pos_l = (l_left > gradVal[3] ? 4 : pos_l);
	
	//printf("Left IR = %d | Left X = %d\nRight IR = %d | Right X = %d\n", l_left, pos_l, l_right, pos_r);
	side = (l_left > l_right ? 1 : -1);
	return ((pos_l + pos_r) / 2) * 15;
}

//Parser de commandes
//Entrée : message reçu par le serveur
//Type de message : description(arg1,arg2)
//Attention : un message valide est un message de la forme => xxxxx(x,x)
void Parse(char * com)
{
	FIFOFREE = 1;
	int i;
	int par1 = -1;
	int par2 = -1;
	int virg = -1;
	char * instr = NULL;
	char * args1 = NULL;
	char * args2 = NULL;
	float arg1 = 0.0;
	float arg2 = 0.0;
	for(i = 0; i < strlen(com); i = i + 1)
	{
		switch(com[i])
		{
			case '(':
				par1 = i;
				break;
			case ',':
				virg = i;
				break;
			case ')':
				par2 = i;
				break;
			default:
				break;
		}
	}
	if(par1 == -1 || virg == -1 || par2 == -1)
	{
		return;
	}
	instr = malloc((par1+1)*sizeof(char));
	strncpy(instr, com, par1);
	instr[par1] = '\0';
	args1 = malloc((virg - par1 + 1)*sizeof(char));
	for(i = par1 + 1; i < virg; i = i + 1)
	{
		args1[i - par1 - 1] = com[i];
	}
	args1[virg] = '\0';
	sscanf(args1,"%f",&arg1);
	args2 = malloc((par2 - virg + 1)*sizeof(char));
	for(i = virg + 1; i < par2; i = i + 1)
	{
		args2[i - virg - 1] = com[i];
	}
	args2[par2] = '\0';
	sscanf(args2,"%f",&arg2);
	
	//gotoM(x,x)
	if(strcmp(instr, "gotoM") == 0)
	{
		gotoM(arg1,arg2);
	}
	//gotoP(x,x)
	else if(strcmp(instr, "gotoP") == 0)
	{
		gotoP(arg1,arg2);
	}
	//gotoR(x,)
	else if(strcmp(instr, "gotoR") == 0)
	{
		gotoR(arg1);
	}
	//gotoPIN(x,x)
	else if(strcmp(instr, "gotoPIN") == 0)
	{
		gotoPIN(arg1,arg2);
	}
	//crossLine(x,x)
	else if(strcmp(instr, "crossLine") == 0)
	{
		char dir_string[4];
		strcpy(dir_string, "NESW");
		char dir_string_op[4];
		strcpy(dir_string_op, "SWNE");
		
		float theta = gotoPIN(arg1,0);
		if(theta == -1 || theta > 45.0)
		{
			int pre = (int)(arg2 > 9 ? arg2 / 10 : arg2) - 1;
			bzero(&buffer_out, 64);
			sprintf(buffer_out, "leaving_A(%c)", (pre == 0 ? 'N' : dir_string[pre]));
			printf("> [Client] : %s\n", buffer_out);
			send(socket_client, buffer_out, 64, 0);
		
			if(algo == 4)
			{
				bzero(&buffer_out, 64);
				sprintf(buffer_out, "check_ID()");
				printf("> [Client] : %s\n", buffer_out);
				send(socket_client, buffer_out, 64, 0);
			}
		
			bzero(&buffer_out, 64);
			sprintf(buffer_out, "arriving_A()");
			printf("> [Client] : %s\n", buffer_out);
			send(socket_client, buffer_out, 64, 0);
		}
		else
		{
			float pos_grad = gotoGrad(theta);
			float new_angle = asin(pos_grad / 160) * 180 / PI;
			//printf("Position X = %f | New angle = %f\n", pos_grad, new_angle);
		
			gotoR(side * new_angle);
		
			int pre = (int)(arg2 > 9 ? arg2 / 10 : arg2) - 1;
			bzero(&buffer_out, 64);
			sprintf(buffer_out, "leaving_A(%c)", (pre == 0 ? 'N' : dir_string[pre]));
			printf("> [Client] : %s\n", buffer_out);
			send(socket_client, buffer_out, 64, 0);
		
			if(algo == 4)
			{
				bzero(&buffer_out, 64);
				sprintf(buffer_out, "check_ID()");
				printf("> [Client] : %s\n", buffer_out);
				send(socket_client, buffer_out, 64, 0);
			}
		
			bzero(&buffer_out, 64);
			sprintf(buffer_out, "arriving_A()");
			printf("> [Client] : %s\n", buffer_out);
			send(socket_client, buffer_out, 64, 0);
			
			gotoP((160 / cos(asin(pos_grad / 160))) / 10, 0);
			gotoR(-side * new_angle);
		}
	}
	//lookIR(,)
	else if(strcmp(instr, "lookIR") == 0)
	{
		lookIR();
	}
	//stop(,)
	else if(strcmp(instr, "stop") == 0)
	{
		tile_stop = 1;
	}
	FIFOFREE = 0;
}

//Fonction de passage de commandes en manuel     
void * Manual()
{
	while(1)
	{
		printf("> ");
		bzero(buffer_in, 64);
		scanf("%s",&buffer_in[0]);
		add(F,buffer_in);
		if(FIFOFREE == 0)
		{
			Parse(get(F));
		}
	}
}

//Fonction de réception des messages provenant du serveur     
void * Read()
{
	while(1)
	{
	//Effacement du buffer
		bzero(buffer_in, 64);
	//Lecture
		if(read(socket_client, &buffer_in, 64) < 0)
		{
			printf("  [Read server message error]\n");
			exit(1);
		}
	//Traitement du message
		add(F,buffer_in);
		//print(F);
		if(FIFOFREE == 0)
		{
			Parse(get(F));
		}
	}
}

//Fonction d'envoi de messages vers le serveur
void * Send()
{
	while(1)
	{	
	//Effacement du buffer
		bzero(&buffer_out, 64);
	//Edition du message à envoyer
		scanf("%s", &buffer_out[0]);
		//strcpy(buffer_out, "I'm a robot");
	//Affichage	
		printf("[Client] : %s\n", buffer_out);
	//Envoi du message au serveur
		send(socket_client, buffer_out, 64, 0);
		//usleep(2000000);
	}
}

void EVAP()
{
	char command[16];
	float ang = 180;
	srand(time(0));
	int dir;
	int pre = 0;
	char dir_string[4];
	strcpy(dir_string, "NESW");
	float phero;
	float min_phero;
	float phero_tab[4];
	int i;
	
	while(1)
	{	
		min_phero = 100.0;
		for(i = 0; i < 4; i++)
		{
			bzero(&buffer_out, 64);
			sprintf(buffer_out, "check_Phero(%c)", dir_string[i]);
			send(socket_client, buffer_out, 64, 0);
			bzero(buffer_in, 64);
			if(read(socket_client, &buffer_in, 64) < 0)
			{
				printf("> [Read server message error]\n");
			}
			printf("> [Server] : %s\n", buffer_in);
			if(buffer_in[8] != 'n')
			{
				char * p;
				int j = 1;
				p = strtok(buffer_in, "(:)");
				while(p != NULL)
				{
					j++;
					if(j == 4)
					{
						phero = atof(p);
					}
					p = strtok(NULL, "(:)");
				}
				phero_tab[i] = phero;
				min_phero = (phero_tab[i] < min_phero ? phero_tab[i] : min_phero);
			}
			else
			{
				phero_tab[i] = -1;
			}
		}
		int neigh_free[4];
		int j = 0;
		int k = 0;
		for(i = 0; i < 4; i++)
		{
			if(phero_tab[i] != -(1.0) && phero_tab[i] == min_phero)
			{
				neigh_free[j+k] = i + 1;
				j++;
			}
			else
			{
				neigh_free[j+k] = -1;
				k++;
			}
		}

		int choice_tab[j];
		k = 0;
		for(i = 0; i < 4; i++)
		{
			if(neigh_free[i] != -1)
			{
				choice_tab[k] = neigh_free[i];
				k++;
			}
		}
		dir = choice_tab[rand() % j];
			
		bzero(&buffer_out, 64);
		sprintf(buffer_out, "reserv_A(%c)", dir_string[dir - 1]);	

		printf("> [Client] : %s\n", buffer_out);
		send(socket_client, buffer_out, 64, 0);
	
		bzero(buffer_in, 64);
		if(read(socket_client, &buffer_in, 64) < 0)
		{
			printf("> [Read server message error]\n");
		}
		printf("> [Server] : %s\n", buffer_in);
		if(buffer_in[9] == 'g')
		{
			float phero_max = 100.0;
			sprintf(buffer_out, "update_Phero(%c:%f)", dir_string[dir - 1], phero_max);
			printf("> [Client] : %s\n", buffer_out);
			send(socket_client, buffer_out, 64, 0);
			
			gotoR(((dir - 1) * 90) - ang);
			switch(dir)
			{
				case 1 : ang = 0; pre = 3; break;
				case 2 : ang = 90; pre = 4; break;
				case 3 : ang = 180; pre = 1; break;
				case 4 : ang = 270; pre = 2; break;
			}
	
			sprintf(command, "crossLine(150,%d)", pre);
			add(F,command);
			while(F->size != 0)
			{
				if(FIFOFREE == 0)
				{
					Parse(get(F));
				}
			}
		}
	}
}

void EVAW()
{
	char command[16];
	float ang = 180;
	srand(time(0));
	int dir;
	int pre = 0;
	char dir_string[4];
	strcpy(dir_string, "NESW");
	int date_tab[4];
	int i;
	int h,m,s;
	
	time_t t;
	struct tm * ts;
	int current_time;
	int min_time;
	
	int current_hour;
	int current_min;
	int current_sec;
	
	while(1)
	{	
		t = time(NULL);
		ts = localtime(&t);
		current_hour = connec_hour - bot_hour + ts->tm_hour;
		current_min = connec_min - bot_min + ts->tm_min;
		current_sec = connec_sec - bot_sec + ts->tm_sec;
		if(current_min < 0)
		{
			current_min = 60 + current_min;
			current_hour--;
		}
		else if(current_min > 60)
		{
			current_min = current_min - 60;
			current_hour++;
		}
		if(current_sec < 0)
		{
			current_sec = 60 + current_sec;
			current_min--;
		}
		else if(current_sec > 60)
		{
			current_sec = current_sec - 60;
			current_min++;
		}
		current_time =  current_hour * 3600 + current_min * 60 + current_sec;
		min_time = current_time;
		//printf("Time = %d\n", current_time);
		for(i = 0; i < 4; i++)
		{
			bzero(&buffer_out, 64);
			sprintf(buffer_out, "check_Date(%c)", dir_string[i]);
			send(socket_client, buffer_out, 64, 0);
			bzero(buffer_in, 64);
			if(read(socket_client, &buffer_in, 64) < 0)
			{
				printf("> [Read server message error]\n");
			}
			printf("> [Server] : %s\n", buffer_in);
			if(buffer_in[7] != 'n')
			{
				char * p;
				int j = 1;
				h = m = s = 0;
				p = strtok(buffer_in, "(:)");
				while(p != NULL)
				{
					j++;
					if(j == 4)
					{
						h = atoi(p);
					}
					if(j == 5)
					{
						m = atoi(p);
					}
					if(j == 6)
					{
						s = atoi(p);
					}
					p = strtok(NULL, "(:)");
				}
				date_tab[i] = h * 3600 + m * 60 + s;
				min_time = (date_tab[i] < min_time ? date_tab[i] : min_time);
			}
			else
			{
				date_tab[i] = -1;
			}
		}
		int neigh_free[4];
		int j = 0;
		int k = 0;
		for(i = 0; i < 4; i++)
		{
			if(date_tab[i] != -1 && date_tab[i] == min_time)
			{
				neigh_free[j+k] = i + 1;
				j++;
			}
			else
			{
				neigh_free[j+k] = -1;
				k++;
			}
		}
		if(j != 0)
		{
			int choice_tab[j];
			k = 0;
			for(i = 0; i < 4; i++)
			{
				if(neigh_free[i] != -1)
				{
					choice_tab[k] = neigh_free[i];
					k++;
				}
			}
			dir = choice_tab[rand() % j];

			bzero(&buffer_out, 64);
			sprintf(buffer_out, "reserv_A(%c)", dir_string[dir - 1]);	

			printf("> [Client] : %s\n", buffer_out);
			send(socket_client, buffer_out, 64, 0);
		
			bzero(buffer_in, 64);
			if(read(socket_client, &buffer_in, 64) < 0)
			{
				printf("> [Read server message error]\n");
			}
			printf("> [Server] : %s\n", buffer_in);
			if(buffer_in[9] == 'g')
			{
				t = time(NULL);
				ts = localtime(&t);
				bzero(&buffer_out, 64);
				current_hour = connec_hour - bot_hour + ts->tm_hour;
				current_min = connec_min - bot_min + ts->tm_min;
				current_sec = connec_sec - bot_sec + ts->tm_sec;
				if(current_min < 0)
				{
					current_min = 60 + current_min;
					current_hour--;
				}
				else if(current_min > 60)
				{
					current_min = current_min - 60;
					current_hour++;
				}
				if(current_sec < 0)
				{
					current_sec = 60 + current_sec;
					current_min--;
				}
				else if(current_sec > 60)
				{
					current_sec = current_sec - 60;
					current_min++;
				}
				sprintf(buffer_out, "update_Date(%c:%d:%d:%d)", dir_string[dir - 1], current_hour, current_min, current_sec);
				printf("> [Client] : %s\n", buffer_out);
				send(socket_client, buffer_out, 64, 0);
				
				gotoR(((dir - 1) * 90) - ang);
				switch(dir)
				{
					case 1 : ang = 0; pre = 3; break;
					case 2 : ang = 90; pre = 4; break;
					case 3 : ang = 180; pre = 1; break;
					case 4 : ang = 270; pre = 2; break;
				}
		
				sprintf(command, "crossLine(150,%d)", pre);
				add(F,command);
				while(F->size != 0)
				{
					if(FIFOFREE == 0)
					{
						Parse(get(F));
					}
				}
			}
		}
	}
}

void RandomTrack()
{
	char command[16];
	float ang = 180;
	srand(time(0));
	int dir;
	int pre = 0;
	char dir_string[4];
	strcpy(dir_string, "NESW");
	int dir_access_tab[4];
	int i = 0;
	for(i = 0; i < 4; i++)
	{
		dir_access_tab[i] = 1;
	}
	int nb_free = 0;
	
	while(1)
	{	
		nb_free = 0;
		for(i = 0; i < 4; i++)
		{
			nb_free += dir_access_tab[i];
		}
		if(nb_free <= 1)
		{
			dir = pre;
		}
		else
		{
			dir = (rand() % 4) + 1;
			while(dir == pre || dir_access_tab[dir - 1] == 0)
			{
				dir = (rand() % 4) + 1;
			}
		}

		bzero(&buffer_out, 64);
		sprintf(buffer_out, "reserv_A(%c)", dir_string[dir - 1]);	

		printf("> [Client] : %s\n", buffer_out);
		send(socket_client, buffer_out, 64, 0);
		
		bzero(buffer_in, 64);
		if(read(socket_client, &buffer_in, 64) < 0)
		{
			printf("> [Read server message error]\n");
		}
		printf("> [Server] : %s\n", buffer_in);
		if(buffer_in[9] == 'g')
		{
			for(i = 0; i < 4; i++)
			{
				dir_access_tab[i] = 1;
			}
			nb_free = 4;
			gotoR(((dir - 1) * 90) - ang);
			switch(dir)
			{
				case 1 : ang = 0; pre = 3; break;
				case 2 : ang = 90; pre = 4; break;
				case 3 : ang = 180; pre = 1; break;
				case 4 : ang = 270; pre = 2; break;
			}
			sprintf(command, "crossLine(150,%d)", pre);
			add(F,command);
			while(F->size != 0)
			{
				if(FIFOFREE == 0)
				{
					Parse(get(F));
				}
			}
		}
		else
		{
			dir_access_tab[dir - 1] = 0;
		}
	}
}

void Spreading()
{
	char command[16];
	char message[32];
	char hops[32];
	char dir_string[4];
	strcpy(dir_string, "NESW");
	int dir;
	int pre = 0;
	float ang = 180;
	int state = 0;
	printf("\n> Source : 1 | Goal : 2 >> Mode = ");
	scanf("%d", &state);
	
	if(state == 1)
	{
		bzero(&buffer_out, 64);
		printf("> Message : ");
		scanf("%s", message);
		printf("> Hops : ");
		scanf("%s", hops);
		sprintf(buffer_out, "spread_This(B:%s!%s#0>ALL$)", message, hops);
		while(1)
		{
			printf("> [Client] : %s\n", buffer_out);
			send(socket_client, buffer_out, 64, 0);
			usleep(100000);
		}
	}
	else if(state == 2)
	{
		int reserv = 0;
		char currentID[6] = "";
		char lastID[6] = "";

		while(1)
		{
			bzero(buffer_in, 64);
			if(read(socket_client, &buffer_in, 64) < 0)
			{
				printf("> [Read server message error]\n");
			}
			printf("> [Server] : %s\n", buffer_in);
			
			if(buffer_in[0] == 'r' && reserv == 0)
			{
				char * par;
				char * dot;
				par = strchr(buffer_in, '(');
				dot = strchr(buffer_in, ':');
				int pos_par = (int)(par - buffer_in);
				int pos_dot = (int)(dot - buffer_in);
				strncpy(currentID, &buffer_in[pos_par + 1], pos_dot - pos_par - 1);
				
				if(strcmp(tileID, lastID) != 0 && strcmp(tileID, currentID) == 0)
				{
					//printf("TileID = %s | LastID = %s | CurrentID = %s\n", tileID, lastID, currentID);
					char * pos;
					pos = strchr(buffer_in, ')');
					int dir_pos = (int)(pos - buffer_in);
					char * dir_char;
					dir_char = strchr(dir_string, buffer_in[dir_pos - 1]);
					dir = (int)(dir_char - dir_string) + 1;
			
					bzero(&buffer_out, 64);
					sprintf(buffer_out, "reserv_A(%c)", dir_string[dir - 1]);	

					printf("> [Client] : %s\n", buffer_out);
					send(socket_client, buffer_out, 64, 0);
					reserv = 1;
				}
			}
			else if(buffer_in[9] == 'g' && reserv == 1)
			{
				strcpy(lastID,tileID);
				gotoR(((dir - 1) * 90) - ang);
				switch(dir)
				{
					case 1 : ang = 0; pre = 3; break;
					case 2 : ang = 90; pre = 4; break;
					case 3 : ang = 180; pre = 1; break;
					case 4 : ang = 270; pre = 2; break;
				}
				sprintf(command, "crossLine(150,%d)", pre);
				add(F,command);
				while(F->size != 0)
				{
					if(FIFOFREE == 0)
					{
						Parse(get(F));
					}
				}
				reserv = 0;
			}
			else if(buffer_in[9] == 'd')
			{
				reserv = 0;
			}
			else if(buffer_in[0] == 't')
			{
				char * pos1;
				char * pos2;
				pos1 = strchr(buffer_in, '(');
				pos2 = strchr(buffer_in, ')');
				int id_pos1 = (int)(pos1 - buffer_in);
				int id_pos2 = (int)(pos2 - buffer_in);
				strncpy(tileID, &buffer_in[8], id_pos2 - id_pos1 - 1);
			}
		}
	}
}

void SatAlt()
{
	int P = 127;
	int I = P;
	int cste = 10;
	int blocked = 0;
	int altruist = 0;
	int Vp = 0;
	int nb_walls = 0;
	int nb_agents = 0;
	int coef_wall = 3;
	int coef_agent = 1;	
	
	char command[16];
	char dir_string[4];
	strcpy(dir_string, "NESW");
	int dir_access_tab[4];
	int dir;
	int pre = 1;
	float ang = 180;
	int k = 0;
	for(k = 0; k < 4; k++)
	{
		dir_access_tab[k] = 1;
	}
	int nb_free = 0;
	
	while(1)
	{
		nb_walls = 0;
		nb_agents = 0;
		
		bzero(&buffer_out, 64);
		sprintf(buffer_out, "state_I(%d:%d!%d)", P, I, altruist);
		printf("> [Client] : %s\n", buffer_out);
		send(socket_client, buffer_out, 64, 0);
		
		bzero(&buffer_out, 64);
		sprintf(buffer_out, "perception_A()");
		printf("> [Client] : %s\n", buffer_out);
		send(socket_client, buffer_out, 64, 0);
		
		int i;
		for(i = 0; i < 4; i++)
		{
			bzero(buffer_in, 64);
			if(read(socket_client, &buffer_in, 64) < 0)
			{
				printf("> [Read server message error]\n");
			}
			printf("> [Server] : %s\n", buffer_in);
			
			if(buffer_in[16] == 'n')
			{
				nb_walls++;
			}
			else if(buffer_in[16] == 'a')
			{
				nb_agents++;
			}
		}
		
		blocked = (nb_walls + nb_agents == 4 ? 1 : 0);
		Vp = (blocked == 1 ? -(coef_wall * nb_walls + coef_agent * nb_agents) : cste);
		P = P + Vp;
		//P = (P > 127 ? 127 : P);
		//P = (P < -127 ? -127 : P);
		if(P > 127) P = 127;
		if(P < -127) P = -127;
		
		bzero(buffer_in, 64);
		if(read(socket_client, &buffer_in, 64) < 0)
		{
			printf("> [Read server message error]\n");
		}
		printf("> [Server] : %s\n", buffer_in);
		
		char * p;
		int j = 1;
		p = strtok(buffer_in, "(:)");
		int I_ext = 0;
		char dir_max[1];
		while(p != NULL)
		{
			j++;
			if(j == 3)
			{
				dir_max[0] = p[0];
			}
			if(j == 4)
			{
				I_ext = atoi(p);
			}
			p = strtok(NULL, "(:)");
		}
		printf("Dir max = %s | I_ext = %d\n", dir_max, I_ext);
		if(I_ext < 0 && I_ext <= I && I_ext < P)
		{
			altruist = 1;
		}
		else
		{
			altruist = 0;
		}
		
		if(altruist == 0 && P < 0)
		{
			I = P;
		}
		else if(altruist == 1 && I_ext <= I)
		{
			I = I_ext;
		}
		else if(blocked == 0)
		{
			I = 0;
		}
		
		if(blocked == 0)
		{
			if(altruist == 0)
			{
				nb_free = 0;
				for(i = 0; i < 4; i++)
				{
					nb_free += dir_access_tab[i];
				}
				if(nb_free <= 1)
				{
					dir = pre;
				}
				else
				{
					dir = (rand() % 4) + 1;
					while(dir == pre || dir_access_tab[dir - 1] == 0)
					{
						dir = (rand() % 4) + 1;
					}
				}
			}
			else if(altruist == 1)
			{
				dir = (rand() % 4) + 1;
			}
			
			bzero(&buffer_out, 64);
			sprintf(buffer_out, "reserv_A(%c)", dir_string[dir - 1]);	

			printf("> [Client] : %s\n", buffer_out);
			send(socket_client, buffer_out, 64, 0);
		
			bzero(buffer_in, 64);
			if(read(socket_client, &buffer_in, 64) < 0)
			{
				printf("> [Read server message error]\n");
			}
			printf("> [Server] : %s\n", buffer_in);
			if(buffer_in[9] == 'g')
			{
				for(i = 0; i < 4; i++)
				{
					dir_access_tab[i] = 1;
				}
				nb_free = 4;
				gotoR(((dir - 1) * 90) - ang);
				switch(dir)
				{
					case 1 : ang = 0; pre = 3; break;
					case 2 : ang = 90; pre = 4; break;
					case 3 : ang = 180; pre = 1; break;
					case 4 : ang = 270; pre = 2; break;
				}
				sprintf(command, "crossLine(150,%d)", pre);
				add(F,command);
				while(F->size != 0)
				{
					if(FIFOFREE == 0)
					{
						Parse(get(F));
					}
				}
			}
			else
			{
				dir_access_tab[dir - 1] = 0;
			}
		}
		//usleep(10000);
	}
}			

//Fonction de création et de connexion du client   
void ClientTCP(const char * ip, int port)
{
	socklen_t client_len;
	struct sockaddr_in sockaddr_client; 

	//Création et initialisation de la connexion
	printf("\n> TCP client initialization @ IP : %s | PORT : %d\n\n", ip, port);
    if((socket_client = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
		printf("  [Socket client creation error]\n");
		exit(1);
    }
    printf("  [Socket client creation OK]\n");
  	
    sockaddr_client.sin_family = AF_INET;
    sockaddr_client.sin_port = htons(port);
    sockaddr_client.sin_addr.s_addr = inet_addr(ip);
    
    client_len = sizeof(sockaddr_client);

    if(connect(socket_client, (struct sockaddr*)&sockaddr_client, client_len) < 0)
    {
       printf("  [Socket client connect error]\n");
       exit(1);
    }
  	printf("  [Socket client connected OK]\n");
    printf("  [TCP client ready]\n\n");
	
	//Lecture du message de bienvenue du serveur
	bzero(buffer_in, 64);
	if(read(socket_client, &buffer_in, 64) < 0)
	{
		printf("  [Read server message error]\n");
		exit(1);
	}
	printf("[Server] : %s\n", buffer_in);
	
	char * p;
	int j = 1;

	while(buffer_in[j] != '#')
	{
		j++;
	}
	algo = atoi(&buffer_in[j+1]);
	j = 1;
	p = strtok(buffer_in, "@:#");
	while(p != NULL)
	{
		j++;
		if(j == 3)
		{
			connec_hour = atoi(p);
		}
		if(j == 4)
		{
			connec_min = atoi(p);
		}
		if(j == 5)
		{
			connec_sec = atoi(p);
		}
		p = strtok(NULL, "@:#");
	}
	
	time_t t = time(NULL);
	struct tm * ts = localtime(&t);
	bot_hour = ts->tm_hour;
	bot_min = ts->tm_min;
	bot_sec = ts->tm_sec;
	
	//int x,y;
	bzero(&buffer_out, 64);
	if((x == -1) || (y == -1))
	{
		printf("\n X = ");
		scanf("%d",&x);
		printf(" Y = ");
		scanf("%d",&y);
		printf("\n");
	}
	sprintf(buffer_out,"%d,%d",x,y);
	printf("> [Client] : %s\n", buffer_out);
	send(socket_client, buffer_out, 64, 0);

	sprintf(tileID,"%d%d",x,y);
	
	while(buffer_in[0] == 'g' && buffer_in[1] == 'o')
	{
		bzero(buffer_in, 64);
		if(read(socket_client, &buffer_in, 64) < 0)
		{
			printf("  [Read server message error]\n");
			exit(1);
		}
		printf("[Server] : %s\n", buffer_in);
	}
	
	//Création des threads de gestion d'entrées/sorties
	//pthread_t thread_in;
   	//pthread_t thread_out;
   	//pthread_t thread_manual;

	//Création et lancement des threads de lecture et écriture
   	//pthread_create(&thread_in, NULL, Read, NULL);
	//pthread_create(&thread_out, NULL, Write, NULL);
	//pthread_create(&thread_manual, NULL, Manual, NULL);

   	//pthread_join(thread_manual, NULL);
   	
   	switch(algo)
   	{
   		case 1:
   			RandomTrack();
   			break;
   		case 2:
   			EVAW();
   			break;
   		case 3:
   			EVAP();
   			break;
   		case 4:
   			Spreading();
   			break;
   		case 5:
   			SatAlt();
   		default:
   			RandomTrack();
   			break;
   	}
}

int main(int argc,char *argv[])
{
	//Initialisation du robot
	khepera3_drive_stop();
	khepera3_init();
	khepera3_drive_start();
	khepera3_drive_set_speed(0, 0);
	local_remote_usage_init();

	x = -1;
	y = -1;
	if(argc >= 3)
	{
		x = atoi(argv[1]);
		y = atoi(argv[2]);
	}

	if(argc >= 4)
	{
		irLimit = atoi(argv[3]);
	}
	
	//Config Stage 2009
	/*gradVal[0] = 920;
	gradVal[1] = 1100;
	gradVal[2] = 1570;
	gradVal[3] = 2465;*/

	//Config 2010
	gradVal[0] = 900;
	gradVal[1] = 1300;
	gradVal[2] = 1700;
	gradVal[3] = 2300;

	if(argc >= 8)
	{
		gradVal[0] = atoi(argv[4]);
		gradVal[1] = atoi(argv[5]);
		gradVal[2] = atoi(argv[6]);
		gradVal[3] = atoi(argv[7]);
	}

	//Paramètres de connexion
	const char * ip = "192.168.0.20";
	int port = 5757;
	
	//Initialisation de la file
	F = (FIFO *) malloc(sizeof(FIFO));
	init(F);
	FIFOFREE = 0;
	
	//Création du client
	ClientTCP(ip, port);
	
	//Arrêt du robot
	khepera3_drive_stop();
    return 0;
}
