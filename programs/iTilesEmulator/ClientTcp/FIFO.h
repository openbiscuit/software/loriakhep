#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Elément de la file
typedef struct ElementFIFO
{
	char * message;
	struct ElementFIFO * next;
	struct ElementFIFO * back;
}Element;

//File
typedef struct _FIFO
{
	Element * start;
	Element * end;
	int size;
}FIFO;

//Initialisation de la file
void init(FIFO * F)
{
	F->start = NULL;
	F->end = NULL;
	F->size = 0;
}

//Ajout d'un élément en fin de file
int add(FIFO * F, char * m)
{
	Element * e;
	if ((e = (Element *) malloc(sizeof(Element))) == NULL)
	{
		return -1;
	}
	if ((e->message = (char *) malloc(256 * sizeof (char))) == NULL)
	{
		return -1;
	}
	strcpy(e->message, m);
	e->next = NULL;
	if(F->size == 0)
	{
		F->start = F->end = e;
	}
	else
	{
		F->end->next = e;
		F->end = e;
	}
	F->size++;
	return 0;
}

//Retrait du premier élément
char * get(FIFO * F)
{
	if(F == NULL || F->size < 1)
	{
		return "";
	}
	Element * current;
	current = F->start;
	if(F->size == 1)
	{
		F->start = F->end = NULL;
	}
	else
	{
		F->start = F->start->next;
	}	
	char * m = current->message;
	free(current);
	F->size--;
	return m;
}

//Affichage de la file
void print(FIFO * F)
{
	if(F->size == 0)
	{
		return;
	}
	Element * current;
	current = F->start;
	while(current != NULL)
	{
		printf ("%p - %s\n", current, current->message);
		current = current->next;
	}
}

