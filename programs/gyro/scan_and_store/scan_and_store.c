#include "dsp3000.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define device "/dev/ttyS0"
int main(int argc, char *argv[])
{
  int i;
  char default_file[]="gyro.out";
  char *out_file;
  
  if (argc>=2)
  {
    out_file=argv[1];
  }
  else
  {
    out_file=default_file;
  }
  
  dsp3000_init(device,38400);
set_integrated_mode(); ///switch to integrated mode
  FILE *f=fopen(out_file,"w"); /* on écrit les données dans un fichier texte */
  for (i=0;i<100;++i)
  {
    double a=dsp3000_read();
    fprintf(f,"%f\n",a);
  }
  fclose(f);
  return 0;
}
