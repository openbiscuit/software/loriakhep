#include <iomanip>
#include <sstream>
#include <iostream>
#include <pthread.h>
#include <locale.h>
#include <sigc++/retype_return.h>
#include <gtkmm.h>
#include "../tcp_connection/remote_serveur.h"
#include <math.h>
//class AgentCheckButton;
#define PI 3.14159265

class sensor_widget {
public:
sensor_widget(Glib::RefPtr<Gtk::Builder> interface,std::string bot,int port,int type);
~sensor_widget(){};
Gtk::DrawingArea *drawingarea;
Glib::RefPtr<Gtk::Builder> xml_interface;
Glib::RefPtr<Gtk::Builder> xml_widget;
Glib::RefPtr<Gdk::Pixmap> pxp;
void refresh();
void draw_rectangle ( int x,int y,int width,int height,unsigned int r,unsigned int g,unsigned int b );
void clear();
bool on_drawingarea_configure_event ( GdkEventConfigure *ev );
bool on_drawingarea_expose_event ( GdkEventExpose *event );
void draw_line ( int x1,int y1,int x2,int y2,unsigned int r,unsigned int g,unsigned int b );
int Timer(int _n);
void draw_sensor(int value,float rad);
float teta;
remoteserver *rs;
float offsetteta;
static void *runThread(void *param);
void Thread();
pthread_mutex_t mutex;
int n[9];
};

void *sensor_widget::runThread(void *param)
{
sensor_widget *target=(sensor_widget*)param;
target->Thread();
}

void sensor_widget::Thread()
{
open_connection(rs);
int *z;
while(1)
{
int n_cap;
int c=remote_server_get_int_array(rs,"IR_PROXI",&n_cap,&z);
pthread_mutex_lock(&mutex);
for(int i=0;i<9;i++)if(c>0)n[i]=z[i];else n[i]=4095;
pthread_mutex_unlock(&mutex);
usleep(100000);
}

}

int sensor_widget::Timer(int _n){

clear();
pthread_mutex_lock(&mutex);


draw_sensor((40-n[0]/(4095/40)),-3*PI/4);
draw_sensor((40-n[1]/(4095/40)),-PI/2);
draw_sensor((40-n[2]/(4095/40)),-PI/4);
draw_sensor((40-n[3]/(4095/40)),-PI/6);
draw_sensor((40-n[4]/(4095/40)),PI/6);
draw_sensor((40-n[5]/(4095/40)),PI/4);
draw_sensor((40-n[6]/(4095/40)),PI/2);
draw_sensor((40-n[7]/(4095/40)),3*PI/4);
draw_sensor((40-n[8]/(4095/40)),PI);
pthread_mutex_unlock(&mutex);
return true;
}

void sensor_widget::draw_sensor(int value,float rad)
{
float radian=rad+offsetteta+teta;
while(radian<0)radian+=2*PI;
while(radian>2*PI)radian-=2*PI;
int x1,y1,x2,y2,x,y;

x=value*cos(radian);
y=value*sin(radian);
//printf("x %d y %d \n",x,y);
x1=drawingarea->get_allocation().get_width()/2;
y1=drawingarea->get_allocation().get_height()/2;

x2=x1+x;
y2=y1+y;
draw_line (x1,y1,x2,y2,65535,0,0 );
}

void sensor_widget::draw_line ( int x1,int y1,int x2,int y2,unsigned int r,unsigned int g,unsigned int b )
{
	if ( pxp )
	{
		Glib::RefPtr<Gdk::GC> gc = Gdk::GC::create ( drawingarea->get_window() );
		Gdk::Color color;
		color.set_rgb ( r,g,b );
		gc->set_rgb_fg_color ( color );
		pxp->draw_line ( gc,x1,y1,x2,y2 );
		gc.clear();
		refresh();
	}

}
void sensor_widget::draw_rectangle ( int x,int y,int width,int height,unsigned int r,unsigned int g,unsigned int b )
{
	if ( pxp )
	{
		Glib::RefPtr<Gdk::GC> gc = Gdk::GC::create ( drawingarea->get_window() );
		Gdk::Color color;
		color.set_rgb ( r,g,b );
		gc->set_rgb_fg_color ( color );
		pxp->draw_rectangle ( gc,true,x,y,width,height );
		gc.clear();
		refresh();
	}

}
void sensor_widget::clear()
{
int size=20;
	draw_rectangle ( 0,0,drawingarea->get_allocation().get_width(),drawingarea->get_allocation().get_height(),65535,65535,65535 );
	draw_rectangle((drawingarea->get_allocation().get_width()-size)/2,(drawingarea->get_allocation().get_height()-size)/2,size,size,0,0,65535);


}
sensor_widget::sensor_widget(Glib::RefPtr<Gtk::Builder> interface,std::string bot,int port,int type):teta(0),offsetteta(-PI/2)
{
Gtk::VBox *vb;
Gtk::Frame *frame;
Gtk::Label *label;
pthread_mutex_init(&mutex,NULL);
xml_interface = interface;
xml_widget = Gtk::Builder::create_from_file("drawing_widget.xml"); 
xml_widget->get_widget("drawingarea1",drawingarea);
xml_widget->get_widget("frame1",frame);
xml_widget->get_widget("label1",label);
xml_interface->get_widget("vbox1",vb);
label->set_text(bot);
vb->pack_start(*frame);

//frame->show_all_children(true);
frame->show();
drawingarea->signal_expose_event().connect ( /*SigC::slot*/sigc::mem_fun ( *this, &sensor_widget::on_drawingarea_expose_event ) );
 drawingarea->signal_configure_event().connect ( /*SigC::slot*/sigc::mem_fun ( *this, &sensor_widget::on_drawingarea_configure_event ), false );
drawingarea->set_size_request(90,90);
rs=new_remoteserver((char*)bot.c_str(),port,type);


	sigc::slot<bool> my_slot =sigc::bind (  sigc::mem_fun ( *this,&sensor_widget::Timer), 1 );

	Glib::signal_timeout().connect ( my_slot,200);
pthread_t t;
pthread_create(&t,NULL,sensor_widget::runThread,this);

}


bool sensor_widget::on_drawingarea_configure_event ( GdkEventConfigure *ev )
{
	if ( pxp )
		pxp.clear();

	pxp = Gdk::Pixmap::create ( drawingarea->get_window(),drawingarea->get_allocation().get_width(),drawingarea->get_allocation().get_height(),-1 );
	clear();
	return TRUE;

	return 0;
}

bool sensor_widget::on_drawingarea_expose_event ( GdkEventExpose *event )
{


	refresh();

	return true;


}
void sensor_widget::refresh()
{
	Glib::RefPtr<Gdk::Window> window = drawingarea->get_window();

	if ( window )
	{


		window->draw_drawable ( drawingarea->get_style()->get_fg_gc ( (Gtk::StateType)(window->get_state()) ),pxp,0,0,0,0 );
	}
}
int main (int argc, char *argv[])
{




	Gtk::Main kit(argc, argv);
	Glib::RefPtr<Gtk::Builder> xml_interface = Gtk::Builder::create_from_file("sensor_view.xml"); 
	
	Gtk::Window *window;
	xml_interface->get_widget("window1", window); 
	sensor_widget w(xml_interface,"192.168.0.1",3000,1);
sensor_widget w1(xml_interface,"192.168.0.2",3000,0);
sensor_widget w2(xml_interface,"192.168.0.4",3000,0);
	//xml_interface->get_widget("drawingarea1",drawing);
//printf("runaway\n");

	kit.run(*window);
	return 0;
} 