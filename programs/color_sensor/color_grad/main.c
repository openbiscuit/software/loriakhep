#include <stdio.h>    /* Standard input/output definitions */
#include <stdlib.h>
#include <stdint.h>   /* Standard types */
#include <string.h>   /* String function definitions */
#include <unistd.h>   /* UNIX standard function definitions */
#include <fcntl.h>    /* File control definitions */
#include <errno.h>    /* Error number definitions */
#include <termios.h>  /* POSIX terminal control definitions */
#include <sys/ioctl.h>
#include <getopt.h>
#include <time.h>
#include <signal.h>
#include "khepera3.h"
#include "color_sensor.h"
/*int serialport_init(const char* serialport, int baud);
int serialport_writebyte(int fd, uint8_t b);
int serialport_write(int fd, const char* str);
int serialport_read_until(int fd, char* buf, char until);*/
#define _GAIN 5
#define _FWD_SPEED 4000
int FWD_SPEED= _FWD_SPEED;
int GAIN=_GAIN;


void sigint_cb(int sig)
{
    // printf("\nInterrupted..\n");
    khepera3_drive_set_speed(0,0);
    exit(sig);
}

static void
sig_tstp(int signo) /* signal handler for SIGTSTP */
{
    sigset_t    mask;

    /* ... move cursor to lower left corner, reset tty mode ... */

    /*
     * Unblock SIGTSTP, since it's blocked while we're handling it.
     */
    khepera3_drive_stop();
    sigemptyset(&mask);
    sigaddset(&mask, SIGTSTP);
    sigprocmask(SIG_UNBLOCK, &mask, NULL);

    signal(SIGTSTP, SIG_DFL);   /* reset disposition to default */

    kill(getpid(), SIGTSTP);    /* and send the signal to ourself */

    /* we won't return from the kill until we're continued */

    signal(SIGTSTP, sig_tstp);  /* reestablish signal handler */
    khepera3_drive_start();
    /* ... reset tty mode, redraw screen ... */
}
void init(void)
{
    signal(SIGINT,sigint_cb);
    signal(SIGKILL,sigint_cb);
    signal(SIGTSTP, sig_tstp);
    khepera3_init();
    khepera3_drive_start();
}
/*
int clear_port(int fd)
{
    char b[1];
    int i=0;
int n;
char c;
    do {
	n = read(fd, &c, 1);  // read a char at a time
    } while( n<1 );

    return 0;
}
*/
int main(int argc, char *argv[])
{
    char serialport[]="/dev/ttyS2";
    //  int fd = 0;
FILE *out_redr,*out_redl,*out_clearr,*out_clearl;
out_redr=fopen("out_redr","w");
out_redl=fopen("out_redl","w");
out_clearr=fopen("out_clearr","w");
out_clearl=fopen("out_clearl","w");
    color_sensor_value right;
    color_sensor_value left;
    color_sensor_value val[7];
//struct timeval t1,t2;
    if (argc>1)
    {
        color_sensor_init(argv[1]);
        if (argc>2)
        {

            FWD_SPEED=atoi(argv[2]);
            if (argc>3)
            {
                GAIN=atoi(argv[3]);

            }
        }
    }
    else
        color_sensor_init(serialport);

    init();
    /*printf("mettre du blanc\n");

    getchar();
    color_sensor_calibrateclear();
    printf("mettre du rouge\n");

    getchar();
    color_sensor_calibratered();
    printf("mettre du bley\n");

    getchar();
    color_sensor_calibrateblue();
    printf("mettre du vert\n");

    getchar();
    color_sensor_calibrategreen();*/
    color_sensor_led(0);
usleep(50000);
color_sensor_ledir1(1);
usleep(50000);
color_sensor_ledir2(1);
/*usleep(500000);
printf("calibrate red...\n");
color_sensor_calibratered();
usleep(500000);
printf("calibrate clear...\n");
    color_sensor_calibrateclear();






usleep(500000);
printf("calbirate green\n");
    color_sensor_calibrategreen();
usleep(500000);
printf("calibrate blue\n");
    color_sensor_calibrateblue();
usleep(500000);
printf("calibrate done\n");
return 0;*/
set_filter_mask(0xFFFFFFFF); //filter 5 low bit
    static const int Connections_A[9] = { 2,  2,  2, -2, -22, -10, -8, 2, 4}; 
    static const int Connections_B[9] = { 2, -8, -10, -22,  -2,  2,  2, 2, 4};
  
  static const int GAIN=4;
  static const int fwSpeed=4000;
  int braiten;
int i;
  int ir[9];
  int lspeed,rspeed;
    while (1) {
        /*serialport_write(fd,"g\n");
         if(!serialport_read_until(fd, buf, '\n'))
        {
        sscanf(buf,"%d %d %d %d %d %d %d %d\n",&c, &r,&g,&b,&c2, &r2,&g2,&b2);

        int rdiff=r-r2;
        int gdiff = g-g2;
        int speedleft=FWD_SPEED + (rdiff-gdiff)*GAIN;
        int speedright=       FWD_SPEED - (rdiff-gdiff)*GAIN;
        gettimeofday(&t2,NULL);
                khepera3_drive_set_speed(speedleft,speedright);
                usleep(20000);
        float ti1,ti2;
        ti1 = (t1.tv_usec/1000.0);
        ti2 =  (t2.tv_usec/1000.0);
        printf("r : %d g : %d b: %d c : %d\nr2 : %d g2 : %d b2: %d c2 : %d\n",r,g,b,c,r2,g2,b2,c2);
        printf("time:\n%f\n%f\n%f\n",ti1,ti2,ti2-ti1);
        }*/

braiten=0;
  khepera3_infrared_proximity();
        /* copy and truncate IR */
        /* TODO: remove high and low bits instead */
        for (i=0;i<9;i++)
        {
            ir[i]=khepera3.infrared_proximity.sensor[i];
//	    printf("%d ", ir[i]);
if(ir[i]>1000)
{
braiten=1;

}
        }
if(braiten!=braiten)
{
        
        lspeed=0;rspeed=0;
        for (i = 0; i < 9; i++)
        {
            lspeed+=Connections_A[i]*ir[i];
            rspeed+=Connections_B[i]*ir[i];
        }
    
        lspeed=lspeed*GAIN+fwSpeed;
        rspeed=rspeed*GAIN+fwSpeed;
        lspeed=lspeed*GAIN+fwSpeed;
        rspeed=rspeed*GAIN+fwSpeed;
//	printf("%d %d\n",lspeed,rspeed);
        khepera3_drive_set_speed(lspeed,rspeed);

}
else
{


        int n;

	color_sensor_value right2,left2;
        get_color_sensor_value2(val,&n);
	
        right = val[6]; right2=val[5];
        left=val[0];left2=val[1];
int imax = 0;
int max=val[2].red;
int rsp,lsp;
rsp=0;lsp=0;
int j;
for( j=2;j<n-2;j++)
{
if(val[j].red>max)
imax=j;


}
switch(imax)
{
case 3:
rsp=0;
lsp=0;
break;

case 0:
case 1:
case 2:
rsp=5000;

break;
case 4:
case 5:
case 6:
lsp=5000;
break;



}

	float trC,tlC,trR,tlR,trL,tlL,tgrC,tglC,tgrR,tglR;
/*
	trC= (float)(right.red+20)/1024.0;
	trL = (float)(right.clear)/1024.0;
	tlL= (float)(left.clear)/1024.0;
	tlC=(float)(left.red)/1024.0;
	trR = trC/trL;
	tlR = tlC/tlL;
	right.red= (int)((trR)*1024.0);
	left.red= (int)((tlR)*1024.0);

	tgrC= (float)(right.green)/1024.0;
	tglC=(float)(left.green)/1024.0;
	tgrR = tgrC/trL;
	tglR = tglC/tlL;
	right.green= (int)((tgrR)*1024.0);
	left.green= (int)((tglR)*1024.0);*/
	/*left.green-=left.clear;
	right.green-=right.clear;*/
        /*int rdiff=right.red-left.red+right2.red-left2.red;
        int gdiff = right.green-left.green+right2.green-left2.green;
	//gdiff=0;
	//rdiff=0;
        int speedleft=FWD_SPEED -(rdiff-gdiff)*GAIN;
        int speedright=      FWD_SPEED + (rdiff-gdiff)*GAIN;*/
	
//khepera3_drive_set_speed(_FWD_SPEED+right.red*GAIN,_FWD_SPEED+left.red*GAIN);
////tile code
/*
if(right.blue > 2*right.red)
	{
khepera3_drive_set_speed(-_FWD_SPEED,_FWD_SPEED);
	}
	else if (right.red > 2*right.blue)
{
khepera3_drive_set_speed(_FWD_SPEED,-_FWD_SPEED);	

}
else
{
printf("vec3 clear%d\n",val[3].clear);
if(val[3].clear > 900)
	khepera3_drive_set_speed(_FWD_SPEED,_FWD_SPEED);
else
{
int diffz = val[4].clear-val[2].clear;
diffz*=GAIN;
int s1,s2;
s1=_FWD_SPEED-diffz;
s2=_FWD_SPEED+diffz;
	khepera3_drive_set_speed(s1,s2);


}	

}
*/
	//khepera3_drive_set_speed(0,0);

       // khepera3_drive_set_speed(speedleft,speedright);
fprintf(out_redr,"%d\n",right.red);
fprintf(out_redl,"%d\n",left.red);
fprintf(out_clearr,"%d\n",right.clear);
fprintf(out_clearl,"%d\n",left.clear);
        int r,g,b,r2,g2,b2,c,c2;
        r=right.red;
        g=right.green;
        b=right.blue;
        c=right.clear;
        r2=left.red;
        g2=left.green;
        b2=left.blue;
        c2=left.clear;

        printf("r : %d g : %d b: %d c : %d\nr2 : %d g2 : %d b2: %d c2 : %d\n",r,g,b,c,r2,g2,b2,c2);
/*usleep(500000);
color_sensor_ledir1(0);
usleep(10000);
color_sensor_ledir2(1);
usleep(500000);
color_sensor_ledir1(1);
usleep(10000);
color_sensor_ledir2(0);
usleep(500000);

color_sensor_ledir2(1);*/
}
              usleep(20000);
//usleep(1*1000);
    }

    exit(EXIT_SUCCESS);
} // end main

