#include <stdio.h>    /* Standard input/output definitions */
#include <stdlib.h>
#include <stdint.h>   /* Standard types */
#include <string.h>   /* String function definitions */
#include <unistd.h>   /* UNIX standard function definitions */
#include <fcntl.h>    /* File control definitions */
#include <errno.h>    /* Error number definitions */
#include <termios.h>  /* POSIX terminal control definitions */
#include <sys/ioctl.h>
#include <getopt.h>
#include <time.h>
#include <signal.h>
#include "khepera3.h"
#include "color_sensor.h"
#include <math.h>
#define discretisor 1
#define minamp 0.0
#define PI 3.1416
#define ANGAIN 200.0
#define AGAIN2 15.0

/*int serialport_init(const char* serialport, int baud);
int serialport_writebyte(int fd, uint8_t b);
int serialport_write(int fd, const char* str);
int serialport_read_until(int fd, char* buf, char until);*/
#define _GAIN 10
#define _FWD_SPEED 10000
int FWD_SPEED= _FWD_SPEED;
int GAIN=_GAIN;

float sigmlanbda =2.5;
///give the needed angle with the force
float normalised_sigmoid(float  x,float binf,float bsup,float lambda)
{
    float nx;

    nx=((2.0*(x-binf))/(bsup-binf))-1.0;

    float res=1.0/(1.0+exp(-lambda*nx));

    return res;


}


void follow_gradien3(color_sensor_value *val,float *ang,int n,int *speedleft,int *speedright)
{
    int *value = (int*)malloc(sizeof(int)*n);
    int i;
    for (i=0; i<n; i++)
    {
        printf("----%d %d \n",i ,val[i].red);
        float tmp=(float)(val[i].red-val[i].green);
        value[i]=tmp;
        int v1,v2,d1,d2;
        v1=value[i]/discretisor;
        v1*=discretisor;
        v2=v1+discretisor;
        printf("value i %d v1 %d v2 %d\n",value[i],v1,v2);
        d1=(v1-value[i])*(v1-value[i]);
        d2=(v2-value[i])*(v2-value[i]);
        if (d1<d2)
            value[i]=v1;
        else
            value[i]=v2;
    }

    int maxi,maxx;
    maxi=3;
    maxx=value[3];
    for (i=0; i<n; i++)
    {
        printf("%d %d\n",i,value[i]);
        if (value[i]<maxx)
        {
            maxx=value[i];
            maxi=i;
        }

    }
    int mamp = value[maxi]-value[0];
    if (mamp<0)
        mamp=-mamp;
    for (i=0; i<n; i++)
    {
        int tmpz=value[maxi]-value[i];
        if (tmpz<0)
        {
            tmpz=-tmpz;

        }
        if (tmpz>mamp)
            mamp=tmpz;

    }

    if (mamp<minamp)
        maxi=3;

    printf("maxi %d\n",maxi);
    int force=mamp;
    float am1=normalised_sigmoid((float) mamp,0.0,2048.0,sigmlanbda)*2048.0;
    force=(int)am1;

    float diff1=PI/2-ang[maxi];
    float diff2=PI/2+ang[maxi];
    diff1*=AGAIN2;
    diff2*=AGAIN2;

    *speedleft=_FWD_SPEED+diff1*force;
    *speedright=_FWD_SPEED-diff1*force;
}

void follow_gradien1(color_sensor_value *val,int n,int *speedleft,int *speedright)
{
    int *value = (int*)malloc(sizeof(int)*n);
    int i;
    for (i=0; i<n; i++)
    {
        printf("----%d %d \n",i ,val[i].red);
        float tmp=(float)(val[i].red-val[i].green);

        /*tmp*= (float)val[i].clear/1024.0;
        value[i]=(int)tmp/discretisor;*/
        value[i]=tmp;
        int v1,v2,d1,d2;
        v1=value[i]/discretisor;
        v1*=discretisor;
        v2=v1+discretisor;
        printf("value i %d v1 %d v2 %d\n",value[i],v1,v2);
        d1=(v1-value[i])*(v1-value[i]);
        d2=(v2-value[i])*(v2-value[i]);
        if (d1<d2)
            value[i]=v1;
        else
            value[i]=v2;
    }

    int maxi,maxx;
    maxi=3;
    maxx=value[3];
    for (i=0; i<n; i++)
    {
        printf(" %d %d\n",i,value[i]);
        if (value[i]<maxx)
        {
            maxx=value[i];
            maxi=i;
        }

    }
    int mamp = value[maxi]-value[0];
    if (mamp<0)
        mamp=-mamp;

    for (i=0; i<n; i++)
    {
        int tmpz=value[maxi]-value[i];
        if (tmpz<0)
        {
            tmpz=-tmpz;

        }
        if (tmpz>mamp)
            mamp=tmpz;

    }

    if (mamp<minamp)
        maxi=3;
    int rsp,lsp;

    switch (maxi)
    {
    case 3:
        rsp=5000;
        lsp=5000;
        break;

    case 0:

        rsp=2000;
        lsp=-2000;
        break;

    case 1:
    case 2:
        rsp=5000;
        lsp=3000;
        break;

    case 6:
        rsp=-2000;
        lsp=2000;
        break;

    case 4:
    case 5:
        rsp=3000;
        lsp=5000;

        break;

    }
    *speedleft=lsp*2;
    *speedright=rsp*2;
}




void follow_gradien2(color_sensor_value *val,int n,int *speedleft,int *speedright)
{
    int *value =(int*)malloc(sizeof(int)*n);
    int *value2 =(int*)malloc(sizeof(int)*n);
    int i;
    for (i=0; i<n; i++)
    {

        value[i]=(val[i].red - val[i].green);

    }

    for (i=0; i<n-1; i++)
    {

        value2[i]=(value[i]+value[i+1])/discretisor;

    }
    int maxi,maxx;
    maxi=2;
    maxx=value2[2];
    for (i=0; i<n-1; i++)
    {
        printf("v%d %d\n",i,value2[i]);
        if (value2[i]>maxx)
        {
            maxx=value2[i];
            maxi=i;
        }

    }

    int rsp,lsp,diff;
    printf("max %d\n",maxi);
    switch (maxi)
    {
    case 2:
    case 3:
        diff=value2[2]-value2[3];
        diff*=GAIN;

        rsp=5000+diff;
        lsp=5000-diff;
        break;
    case 0:
        rsp=5000;
        lsp=-5000;
        break;
    case 1:
        rsp=5000;
        lsp=-3000;
        break;


    case 4:
        rsp=-3000;
        lsp=5000;
        break;
    case 5:
        rsp=-5000;
        lsp=5000;
        break;


    }
    *speedleft=lsp*5;
    *speedright=rsp*5;
}


int repulsion(int *c,float *a,int n,float *resa)
{
    int i;

    int force;
    float xt,yt;
    xt=0.0;
    yt=0.0;
    for (i=0; i<n; i++)
    {
        xt+=(cos(a[i])*(float)c[i]);

        yt+=(sin(a[i])*(float)c[i]);
    }
    *resa=atan2(yt,xt);
    if (*resa<0)
        *resa+=2.0*PI;
    force=(int)(sqrt(xt*xt+yt*yt));
    return 5*force;


}
void sigint_cb(int sig)
{
    // printf("\nInterrupted..\n");
    khepera3_drive_set_speed(0,0);
    exit(sig);
}

static void
sig_tstp(int signo) /* signal handler for SIGTSTP */
{
    sigset_t    mask;

    /* ... move cursor to lower left corner, reset tty mode ... */

    /*
     * Unblock SIGTSTP, since it's blocked while we're handling it.
     */
    khepera3_drive_stop();
    sigemptyset(&mask);
    sigaddset(&mask, SIGTSTP);
    sigprocmask(SIG_UNBLOCK, &mask, NULL);

    signal(SIGTSTP, SIG_DFL);   /* reset disposition to default */

    kill(getpid(), SIGTSTP);    /* and send the signal to ourself */

    /* we won't return from the kill until we're continued */

    signal(SIGTSTP, sig_tstp);  /* reestablish signal handler */
    khepera3_drive_start();
    /* ... reset tty mode, redraw screen ... */
}
void init(void)
{
    signal(SIGINT,sigint_cb);
    signal(SIGKILL,sigint_cb);
    signal(SIGTSTP, sig_tstp);
    khepera3_init();
    khepera3_drive_start();
}
/*
int clear_port(int fd)
{
    char b[1];
    int i=0;
int n;
char c;
    do {
	n = read(fd, &c, 1);  // read a char at a time
    } while( n<1 );

    return 0;
}
*/
int main(int argc, char *argv[])
{
    char serialport[]="/dev/ttyS2";

    color_sensor_value right;
    color_sensor_value left;
    color_sensor_value val[7];
//struct timeval t1,t2;

    if (argc>1)
        sigmlanbda =atof(argv[1]);

    /* if (argc>1)
     {
         color_sensor_init(argv[1]);
         if (argc>2)
         {

             FWD_SPEED=atoi(argv[2]);
             if (argc>3)
             {
                 GAIN=atoi(argv[3]);

             }
         }
     }
     else*/
    color_sensor_init(serialport);

    init();

    color_sensor_led(0);
    usleep(50000);
    color_sensor_ledir1(1);
    usleep(50000);
    color_sensor_ledir2(1);
    set_filter_mask(0xFFFFFFFF); //no filter
    int braiten;
    int i;
    int ir[9];
    int lspeed,rspeed;
    while (1) {

        braiten=0;
        khepera3_infrared_proximity();

        for (i=1; i<7; i++)
        {
            ir[i]=khepera3.infrared_proximity.sensor[i];
//	    printf("%d ", ir[i]);
            if (ir[i]>30)
            {
                braiten=1;

            }
        }
        printf("braiten : %d\n",braiten);
        if (braiten)
        {
            printf("oho\n");
            lspeed=0;
            rspeed=0;
            float angle[6];
            angle[0]=PI;
            angle[1]=PI-PI/4.0;
            angle[2]=PI-PI/3.0;
            angle[3]=PI/3.0;
            angle[4]=PI/4.0;
            angle[5]=0.0;
            int cap[6];
            for (i=0; i<6; i++)
            {
                cap[i]=ir[i+1]/8;
            }
            float an;
            int f;
            f=repulsion(cap,angle,6,&an);
            float difa1=PI/2.0-an;
            difa1*=ANGAIN;
            float difa2=PI/2.0+an;
            difa2*=ANGAIN;
            int sp1,sp2;
            sp1=(int)difa1*f;

            printf("%d %d\n",sp1,sp2);
            khepera3_drive_set_speed(_FWD_SPEED-sp1,_FWD_SPEED+sp1);
        }
        else
        {


            int n;
            int spl,spr;
            float an[7];
            get_color_sensor_value2(val,&n);


            an[0]=PI;
            an[1]=5*PI/6.0;
            an[2]=4*PI/6.0;
            an[3]=PI/2.0;
            an[4]=PI/3.0;
            an[5]=PI/6.0;
            an[6]=0;

            follow_gradien3(val,an,7,&spl,&spr);
            printf("%d %d\n",spr,spl);
            khepera3_drive_set_speed(spl,spr);

        }
        usleep(20000);
//usleep(1*1000);
    }

    exit(EXIT_SUCCESS);
} // end main

