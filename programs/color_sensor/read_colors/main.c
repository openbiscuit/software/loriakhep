#include <stdlib.h>
#include <unistd.h>

#include "khepera3.h"
#include "color_sensor.h"

int main(int argc, char *argv[])
{
    char serialport[]="/dev/ttyS2";
    int n;
    int i;
    
    color_sensor_value val[7];

    if (argc>1)
    {
        color_sensor_init(argv[1]);
    }
    else
    {
        color_sensor_init(serialport);
    }
    
    color_sensor_led(0); // pour éteindre les leds blanches (inutiles sur la table)
    usleep(50000);
    
    get_color_sensor_value2(val,&n);
    printf("%d sensors found (should be 7)\n",n);
    
    for (i=0;i<n;i++)
    {
      printf("Sensor %d r=%d g=%d b=%d clear=%d\n",i,
	     val[i].red,
	     val[i].green,
	     val[i].blue,
	     val[i].clear);
    }
    return EXIT_SUCCESS;
}
