/*
 * (c) 2009 INRIA, Nancy, France
 * Havet L., Guenard A.
 */

#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_cblas.h>
#include <gsl/gsl_poly.h>
#include <gsl/gsl_cblas.h>

#include <math.h>

int main(int argc, char *argv[])
{
	/* matrix test */
	int i,j;
	gsl_matrix * A=gsl_matrix_alloc(3,3);
	gsl_matrix * B=gsl_matrix_alloc(3,3);
	gsl_matrix_set(A,0,0,1);
	gsl_matrix_set(A,0,1,0);
	gsl_matrix_set(A,0,2,0);
	gsl_matrix_set(A,1,0,0);
	gsl_matrix_set(A,1,1,1);
	gsl_matrix_set(A,1,2,0);
	gsl_matrix_set(A,2,0,0);
	gsl_matrix_set(A,2,1,0);
	gsl_matrix_set(A,2,2,1);
	gsl_matrix_set(B,0,0,1);
	gsl_matrix_set(B,0,1,2);
	gsl_matrix_set(B,0,2,3);
	gsl_matrix_set(B,1,0,4);
	gsl_matrix_set(B,1,1,5);
	gsl_matrix_set(B,1,2,6);
	gsl_matrix_set(B,2,0,7);
	gsl_matrix_set(B,2,1,8);
	gsl_matrix_set(B,2,2,9);
	printf("\nA \n");
		
	for (i=0;i<3;i++)
	{	
		for (j=0;j<3;j++)	
			printf("%f ",gsl_matrix_get(A,i,j));
		printf("\n");	
	}


	printf("\nB \n");
		
	for (i=0;i<3;i++)
	{	
		for (j=0;j<3;j++)	
			printf("%f ",gsl_matrix_get(B,i,j));
		printf("\n");	
	}
	printf("\nB transpose\n");
	gsl_matrix_transpose(B);

	for ( i=0;i<3;i++)
	{	
		for (j=0;j<3;j++)	
			printf("%f ",gsl_matrix_get(B,i,j));
		printf("\n");	
	}
	
	printf("\nA + B transpose\n");

	gsl_matrix_add(A,B);

	for ( i=0;i<3;i++)
	{	
		for (j=0;j<3;j++)	
			printf("%f ",gsl_matrix_get(A,i,j));
		printf("\n");	
	}

	gsl_matrix_free (A);
	gsl_matrix_free (B);


	/* polynomials test */
	printf("\n Roots of P(x) = x^5 - 1 \n");
	/* coefficients of P(x) =  -1 + x^5  */
       double a[6] = { -1, 0, 0, 0, 0, 1 };  
       double z[10];
     
       gsl_poly_complex_workspace * w 
           = gsl_poly_complex_workspace_alloc (6);
       
       gsl_poly_complex_solve (a, 6, w, z);
     
       gsl_poly_complex_workspace_free (w);
     
       for (i = 0; i < 5; i++)
         {
           printf ("z%d = %+.18f %+.18f\n", 
                   i, z[2*i], z[2*i+1]);
         }

       printf("\n CBLAS SGEMM test \n");
	int lda = 3;
     
       float A1[] = { 0.11, 0.12, 0.13,
                     0.21, 0.22, 0.23 };
     
       int ldb = 2;
       
       float B1[] = { 1011, 1012,
                     1021, 1022,
                     1031, 1032 };
     
       int ldc = 2;
     
       float C1[] = { 0.00, 0.00,
                     0.00, 0.00 };
     
       /* Compute C = A B */
       printf("\n A= \n");
       printf ("[ %g, %g, %g \n", A1[0], A1[1], A1[2]);
       printf ("  %g, %g, %g ]\n", A1[3], A1[4], A1[5]);
       printf("\n B= \n");
       printf ("[ %g, %g\n", B1[0], B1[1]);
       printf ("  %g, %g ]\n",B1[2], B1[3]);
       printf ("  %g, %g ]\n",B1[4], B1[5]);

       cblas_sgemm (CblasRowMajor, 
                    CblasNoTrans, CblasNoTrans, 2, 2, 3,
                    1.0, A1, lda, B1, ldb, 0.0, C1, ldc);
       printf("\n A.B =\n");
       printf ("[ %g, %g\n", C1[0], C1[1]);
       printf ("  %g, %g ]\n", C1[2], C1[3]);


	return 0;

}

