#define _GNU_SOURCE
#include <stdlib.h>       
#include <stdio.h>
#include <string.h>

#include <libxml/nanohttp.h>
#include "jpeglib.h"

int jpg_decompress(char* buf, int len,char ** image, int *w, int *h)
/* Decompress a jpeg image from in memory data
   buf: jpeg data
   len: buf lenght (in bytes)
   image: output image (w*h RGB, total size will be w*h*3)
   TODO: should mention row_stride instead
   w,h: image width and height
   returns: 1 on success
*/

{
    struct jpeg_decompress_struct cinfo;
    JSAMPARRAY buffer;		/* Output row buffer */
    int row_stride;		/* physical row width in output buffer */
    struct jpeg_error_mgr jerr;
    FILE *stream;
    char * dest;
    
    stream=fmemopen(buf,len,"r");
    
    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_decompress(&cinfo);
    jpeg_stdio_src(&cinfo,stream);

    jpeg_read_header(&cinfo,TRUE);
    jpeg_start_decompress(&cinfo);

    /* JSAMPLEs per row in output buffer */
    row_stride = cinfo.output_width * cinfo.output_components;
    /* Make a one-row-high sample array that will go away when done with image */
    buffer = (*cinfo.mem->alloc_sarray)
            ((j_common_ptr) &cinfo, JPOOL_IMAGE, row_stride, 1);
    
    *image=malloc(cinfo.output_width*cinfo.output_height*3); /* TODO: should use row_stride instead */
    *w=cinfo.output_width;
    *h=cinfo.output_height;
    dest=*image;
    while (cinfo.output_scanline < cinfo.output_height) {
    /* jpeg_read_scanlines expects an array of pointers to scanlines.
        * Here the array is only one element long, but you could ask for
        * more than one scanline at a time if that's more convenient.
    */
        jpeg_read_scanlines(&cinfo,buffer,1);
        /* Assume put_scanline_someplace wants a pointer and sample count. */
        memcpy(dest,buffer[0],3*cinfo.output_width); /* TODO: should use row_stride instead */
        dest+=3*cinfo.output_width;
//        put_scanline_someplace(buffer[0], row_stride);
    }

    jpeg_finish_decompress(&cinfo);
    jpeg_destroy_decompress(&cinfo);
    fclose(stream);
    return 1;
}

void error(const char* msg)
{
    fprintf(stderr," *** %s\n",msg);
    exit(EXIT_FAILURE);
}

int main()
{
    char * content_type;
    void * ctx;
    int len,len_read;
    char * buf;
    char * rgb_image;
    int w,h;
    
    xmlNanoHTTPInit();
    
    ctx=xmlNanoHTTPOpen("http://korwlcam2.loria.fr/cgi-bin/video.jpg",&content_type);
    if (!ctx)
        error("cannot GET image (network problem ? wrong URL ?)");
    
    free(content_type);
    len=xmlNanoHTTPContentLength(ctx);
    if (len==-1)
        error("ContentLenght not provided, aborting");
    buf=malloc(len);
    len_read=xmlNanoHTTPRead(ctx,buf,len);
    xmlNanoHTTPClose(ctx);
    if (len!=len_read)
        error("could not get file");
    
    /* Decompress */
    jpg_decompress(buf,len_read,&rgb_image,&w,&h);
    printf("Success: got and decompressed a %dx%d image\n",w,h);
    
    free(rgb_image);
    free(buf);
    return EXIT_SUCCESS;
}
