#ifndef _WINDOWWATCH_HH
#define _WINDOWWATCH_HH
#include <locale.h>
#include <sigc++/retype_return.h>
#include <gtkmm.h>
class Agent;
class WindowWatchdog
{
public:
    WindowWatchdog(Glib::RefPtr<Gtk::Builder> interface);
    ~WindowWatchdog() {};
    static WindowWatchdog  *getInstance(Glib::RefPtr<Gtk::Builder> interface);
    static WindowWatchdog  *getInstance();
    //void load(std::string filename);
    void setAgent(Agent *agent);
    Agent *linkedAgent;
    bool enter(GdkEventKey* event);


protected:
    Gtk::Label *label;
    Gtk::Entry *entry;
    Gtk::Entry *entrypingtime;
    Gtk::Entry *entrywatchtime;
    Gtk::Entry *entrywatchsoft;
    Gtk::Entry *entrywatcharg;
    static WindowWatchdog *instance;
    Glib::RefPtr<Gtk::Builder> xml_interface;
    Gtk::Window *window;
};

#endif