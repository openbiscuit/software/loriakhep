#ifndef _ErrorBox_HH
#define _ErrorBox_HH
#include <iomanip>
#include <sstream>
#include <iostream>

#include <locale.h>
#include <sigc++/retype_return.h>
#include <gtkmm.h>

class ErrorBox
{

public:
    //ModelColumnsBehaviour m_Columns;
    ErrorBox(std::string Err);
    void click();
    Gtk::Window *w;
};

//void BehaviourTab::addAgent(Agent *a);


#endif