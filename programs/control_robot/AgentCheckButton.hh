#ifndef _AgentCB_H
#define _AgentCB_H

#include <iomanip>
#include <sstream>
#include <iostream>

#include <locale.h>
#include <sigc++/retype_return.h>
#include <gtkmm.h>
#define TYPE_BEHAVIOUR 0
#define TYPE_OTHER 1
class Agent;
class Behaviour;


class AgentCheckButton: public Gtk::CheckButton
{
public:
    AgentCheckButton(Agent *agent_);
    AgentCheckButton(Agent *agent_,Behaviour *behaviour_);
    ~AgentCheckButton() {};
    Agent *agent;
    void destroy();
    void onclick();
    Behaviour *behaviour;
    int TYPE;
};


#endif