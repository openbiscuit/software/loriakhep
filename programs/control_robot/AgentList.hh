#ifndef _AgentList_HH
#define _AgentList_HH
#include <iomanip>
#include <sstream>
#include <iostream>

#include <locale.h>
#include <sigc++/retype_return.h>
#include <gtkmm.h>
#include <pthread.h>
class Agent;

class ModelColumnsAgent : public Gtk::TreeModel::ColumnRecord
{
public:

    ModelColumnsAgent()
    {
        add(m_col_name);
        add(m_col_behaviour);
        add(m_agent_ptr);
        add(m_col_state);
        add(m_col_arg);
    }

    Gtk::TreeModelColumn<Glib::ustring> m_col_name;
    Gtk::TreeModelColumn<Glib::ustring> m_col_state;
    Gtk::TreeModelColumn<Glib::ustring> m_col_behaviour;
    Gtk::TreeModelColumn<Glib::ustring> m_col_arg;
    Gtk::TreeModelColumn<Agent*> m_agent_ptr;


};
class AgentList
{
public:
    ModelColumnsAgent m_Columns;
    ~AgentList() {};
    bool sshtab(GdkEventButton *event);
    static AgentList *getInstance(Glib::RefPtr<Gtk::Builder> interface);
    static AgentList *getInstance();
    void remove(Agent *agt);
    void add(Agent *a);
    void update(Agent *a);
    Agent* get_agent();
    void on_row_activated(const Gtk::TreeModel::Path& path, Gtk::TreeViewColumn* column);
    bool pressed(GdkEventButton *event);
    bool menuReleased(GdkEventButton *event);
    bool menuWatchdogReleased(GdkEventButton *event);
protected:
    AgentList(Glib::RefPtr<Gtk::Builder> interface);

    Gtk::TreeView *t;
    Glib::RefPtr<Gtk::ListStore> liste;
//Gtk::ListStore list;
    Glib::RefPtr<Gtk::Builder> xml_interface;
    static AgentList *instance;
    pthread_mutex_t _mutex;
};

#endif