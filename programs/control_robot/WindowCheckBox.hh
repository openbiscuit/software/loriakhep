#ifndef _WINDOWAGENTCHECKBOXHH
#define _WINDOWAGENTCHECKBOXHH
#include <locale.h>
#include <sigc++/retype_return.h>
#include <gtkmm.h>
#include <vector>
#include "AgentCheckButton.hh"
class Agent;
class WindowCheckBox
{
public:

    virtual ~WindowCheckBox() {};
    static WindowCheckBox  *getInstance(Glib::RefPtr<Gtk::Builder> interface);
    static WindowCheckBox  *getInstance();
    bool enter(GdkEventKey* event);
    void show();
    virtual void ok_click();
    virtual void annuler_click();

protected:

    Gtk::VBox *vbox;
    virtual void link();
    WindowCheckBox(Glib::RefPtr<Gtk::Builder> interface);
    static std::vector<AgentCheckButton *> vec;
    WindowCheckBox() {};
    static WindowCheckBox *instance;
    Glib::RefPtr<Gtk::Builder> xml_interface;
    Gtk::Window *window;
};

#endif