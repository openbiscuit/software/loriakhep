#ifndef _SshTab_HH
#define _SshTab_HH
#include <iomanip>
#include <sstream>
#include <iostream>

#include <locale.h>
#include <sigc++/retype_return.h>
#include <gtkmm.h>
#include "Agent.hh"
#include <pthread.h>
#include "ProcessusOpen.hh"
#include <vector>
class SshTab
{

public:
    //ModelColumnsBehaviour m_Columns;
    SshTab(Glib::RefPtr<Gtk::Builder> interface,Agent *a);
    ~SshTab();
    Gtk::Notebook *notebook;
    static void *launcher(void *param);
    void thread();
    pthread_mutex_t mutex;
    int Timer(int timernumber);
    sigc::connection conec;
    static void Clean();
    void del();
protected:
    Agent *agent;
    Glib::RefPtr<Gtk::Builder> xml_interface;
    static std::vector<SshTab*> BTV ;
    static int current_tab;
    Gtk::TextView *t;
    Glib::RefPtr<Gtk::TextBuffer> textbuffer;
    std::string buff;
    int change;
    Gtk::Adjustment *adj;
    Gtk::ScrolledWindow *wind;
    static std::vector<SshTab *> vec;
    ProcessusOpen *po;
};


#endif