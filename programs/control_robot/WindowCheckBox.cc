#include "WindowCheckBox.hh"
#include "AgentCheckButton.hh"
#include "Agent.hh"
#include <gtkmm.h>
WindowCheckBox *WindowCheckBox::instance=NULL;
std::vector<AgentCheckButton *> WindowCheckBox::vec;
WindowCheckBox *WindowCheckBox::getInstance(Glib::RefPtr<Gtk::Builder> interface) {
    if (instance==NULL)
    {

        if (interface==NULL)
            return NULL;
        instance=new WindowCheckBox(interface);

    }

    return instance;

}
WindowCheckBox *WindowCheckBox::getInstance() {
    return instance;
}
void WindowCheckBox::ok_click() {

    printf("sshkey_click\n");
}
void WindowCheckBox::annuler_click() {

    printf("sshkey_cancel_click\n");
}
void WindowCheckBox::show() {

    window->show();
}
bool WindowCheckBox::enter(GdkEventKey* event) {

    if ((event->keyval==65421) || (event->keyval==65293))
    {



    }

}
void WindowCheckBox::link()
{
    xml_interface->get_widget("vbox2_ssh_key",vbox);
    xml_interface->get_widget("window_sshkey",window);
    Gtk::Button *b;
    xml_interface->get_widget("button_ok_sshkey",b);
    b->signal_clicked().connect(sigc::mem_fun(*this,&WindowCheckBox::ok_click),false);
    xml_interface->get_widget("button_annuler_sshkey",b);
    b->signal_clicked().connect(sigc::mem_fun(*this,&WindowCheckBox::annuler_click),false);
}
WindowCheckBox::WindowCheckBox(Glib::RefPtr<Gtk::Builder> interface)
{


    xml_interface = interface;
    link();
    for (std::vector<Agent*>::iterator it=Agent::agentrefsvector.begin(); (it!=Agent::agentrefsvector.end()); it++)
    {
        Agent *a=(*it);
        AgentCheckButton *acb=new AgentCheckButton(a);
        vec.push_back(acb);
        vbox->add(*acb);
        acb->show();

    }
//window->show();
}