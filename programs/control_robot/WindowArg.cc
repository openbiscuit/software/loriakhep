#include "WindowArg.hh"
#include "Agent.hh"
#include <gtkmm.h>
WindowArg *WindowArg::instance=NULL;
WindowArg *WindowArg::getInstance(Glib::RefPtr<Gtk::Builder> interface) {
    if (instance==NULL)
    {

        if (interface==NULL)
            return NULL;
        instance=new WindowArg(interface);

    }

    return instance;

}
WindowArg *WindowArg::getInstance() {
    return instance;
}
bool WindowArg::enter(GdkEventKey* event) {
    printf("keyval : %d\n",event->keyval);
    if ((event->keyval==65421) || (event->keyval==65293))
    {

        linkedAgent->set_args(entry->get_text());
        entry->set_text("");
        window->hide();

    }

}
void WindowArg::setAgent(Agent *agent) {
    linkedAgent=agent;
    std::string tmp = "arguments " + linkedAgent->get_name() + " :";
    label->set_text(tmp);
    entry->set_text(linkedAgent->get_args());
    window->show();
}
WindowArg::WindowArg(Glib::RefPtr<Gtk::Builder> interface)
{

    xml_interface = interface;
    xml_interface->get_widget("window_arg",window);
    xml_interface->get_widget("entry_arg",entry);
    xml_interface->get_widget("label_arg",label);
    entry->signal_key_release_event().connect(sigc::mem_fun(*this,&WindowArg::enter),false);
}