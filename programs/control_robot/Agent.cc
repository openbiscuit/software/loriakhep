#include <iomanip>
#include <sstream>
#include <iostream>
#include <stdio.h>
#include <locale.h>
#include <sigc++/retype_return.h>
#include <gtkmm.h>
#include "Agent.hh"
#include "AgentCheckButton.hh"
#include "AgentList.hh"
#include "Behaviour.hh"
#include "BehaviourList.hh"
#include "BehaviourTab.hh"
#include "ErrorBox.hh"
#include <string.h>
#include "ProcessusOpen.hh"
#include "GlobalParameter.hh"
//#include "../../tcp_connection/remote_serveur.h"
sem_t Agent::semaphore;
bool Agent::make_before_upload;
bool Agent::upload_error;
pthread_mutex_t Agent::upload_error_mutex = PTHREAD_MUTEX_INITIALIZER;
std::vector<Agent*> Agent::agentrefsvector ;


int Agent::save_parameter(std::string filename) {
    FILE *fil = fopen(filename.c_str(),"w");
    for (std::vector<Agent*>::iterator it=agentrefsvector.begin(); (it!=agentrefsvector.end()); it++)
    {
        Agent *a=(*it);
        if (a->get_name().size()>0)
            fprintf(fil,"%s\n",a->get_name().c_str());
        else
            fprintf(fil,"\n");
        if (a->get_args().size()>0)
            fprintf(fil,"%s\n",a->get_args().c_str());
        else
            fprintf(fil,"\n");
        if ((a->get_behaviour()!=NULL) && (a->get_behaviour()->orignalpath.size()>0))
            fprintf(fil,"%s\n",a->get_behaviour()->orignalpath.c_str());
        else
            fprintf(fil,"\n");
        if (a->get_watchsoft().size()>0)
            fprintf(fil,"%s\n",a->get_watchsoft().c_str());
        else
            fprintf(fil,"\n");
        if (a->get_watchsoftarg().size()>0)
            fprintf(fil,"%s\n",a->get_watchsoftarg().c_str());
        else
            fprintf(fil,"\n");




        fprintf(fil,"%d\n%d\n",a->get_timeping(),a->get_timewatch());

    }
    fclose(fil);
}
Agent *Agent::find(std::string _name)
{
    for (std::vector<Agent*>::iterator it=agentrefsvector.begin(); (it!=agentrefsvector.end()); it++)
    {
        Agent *a=(*it);
        if (a->get_name()==_name)
            return a;
    }


}
void Agent::upload_sshkey()
{
    char tmp[256];

    FILE *F;
    char buffer[128];
    int i;
    int n;
    if ((pclose(popen("cd ~/.ssh/ && ls id_rsa.pub","r")))==512)
    {
        //on créer id_rsa.pub
        printf("generating id_rsa...%d\n",n);
        F=popen("cd ~ && ssh-keygen -t rsa","w");
        fprintf(F,"\n\n\n");
        pclose(F);
        if (pclose(popen("cd ~/.ssh/ && ls id_rsa.pub","r")))
        {
            printf("impossible de génerer id_rsa.pub");
        }


    }


    std::string str;
    switch (get_type())
    {
    case 0:
    case 1:
        str = "./upload_ssh_key root "  + this->get_hostname() + " kheperoot";
        break;

    default:

        break;


    }


    ProcessusOpen *p = new ProcessusOpen(str);
    p->run("w");
    p->close_();

}
int Agent::load_parameter(std::string filename) {
    std::string _name, _args, _orgpath,_watchsoft,_watcharg;
    int _timeping,_timewatch;
    Agent *a;
    char buffer[256];
    char c;
    int i,j;
    FILE *f;
    f=fopen(filename.c_str(),"r");
    while (!feof(f))
    {
        for (j=0; j<7; j++)
        {
            i=0;
            do {
                c=fgetc(f);
                if (c==-1) return 0;
                buffer[i++]=c;
            } while (c!='\n' && c!=-1);
            buffer[i-1]='\0';
            printf("buffer %s\n",buffer);

            switch (j)
            {
            case 0:
                _name = std::string(buffer);
                break;

            case 1:
                _args = std::string(buffer);
                break;
            case 2:
                _orgpath = std::string(buffer);
                break;
            case 3:
                _watchsoft = std::string(buffer);
                break;
            case 4:
                _watcharg = std::string(buffer);
                break;
            case 5:
                _timeping = atoi(buffer);
                break;
            case 6:
                _timewatch = atoi(buffer);
                break;
                std::cout << _name << std::endl;
            }
        }
        a=find(_name);
        Behaviour *b;
        BehaviourList *bl= BehaviourList::getInstance();
        b= bl->find(_orgpath);
        if (b!=NULL)
        {
            a->set_behaviour(b);
            a->ActiveButton(b);
        }
        a->set_args(_args);
        a->set_watchsoft(_watchsoft);
        a->set_watchsoftarg(_watcharg);
        a->set_timeping(_timeping);
        a->set_timewatch(_timewatch);
        //exit(-1);
    }
    //exit(-1);
    fclose(f);


}
void Agent::upload_thread() {
    char buffer[512];

    if (Agent::make_before_upload)
    {
        if (behaviour->make(get_type()))
        {

            pthread_mutex_lock(&Agent::upload_error_mutex);

            Agent::upload_error = true;
            pthread_mutex_unlock(&Agent::upload_error_mutex);
            set_on_upload(false);
            sem_wait(&Agent::semaphore);
            pthread_exit (NULL);
        }
//printf("buffer %s\n",buffer);

    }
    GlobalParameter *gp = GlobalParameter::getInstance();
    printf("upload:: %d\n",get_type());
    switch (get_type())
    {
    case 0:
        printf("type 0\n");
        sprintf(buffer,"scp %s root@%s:%s/",behaviour->patharm.c_str(),hostname.c_str(),gp->get_remote_binary_path().c_str());
        break;
    case 1:
        printf("type 1\n");
        sprintf(buffer,"scp %s root@%s:%s/",behaviour->patharmgs.c_str(),hostname.c_str(),gp->get_remote_binary_path().c_str());
        break;
    default:
        printf("default\n");
        sprintf(buffer,"scp %s %s:%s/",behaviour->orignalpath.c_str(),hostname.c_str(),gp->get_remote_binary_path().c_str());
        break;
    }

    if (pclose(popen(buffer,"r")))
    {
        std::string error;
        error = "Erreur lors de l'upload de  " + behaviour->patharm + " vérifiez qu'il n'est pas en cours d'éxecution sur le robot";
        new ErrorBox(error);
        pthread_mutex_lock(&Agent::upload_error_mutex);
        Agent::upload_error = true;
        pthread_mutex_unlock(&Agent::upload_error_mutex);
        set_on_upload(false);
        sem_wait(&Agent::semaphore);
        pthread_exit (NULL);

    }
    set_on_upload(false);
    sem_wait(&Agent::semaphore);
    pthread_exit (NULL);
}
void *Agent::upload_launcher(void *param) {

    Agent *a = (Agent*)param;

    a->upload_thread();
}

void Agent::readloop_thread() {
    char buffer[256];
    //printf("read loop yeah\n");
    while (1) {
        receiv_command_NONBLOCK(rs,buffer,256);
        usleep(100000);
    }
    pthread_exit(NULL);
}
void *Agent::readloop_launcher(void *param) {

    Agent *a = (Agent*)param;
    a->readloop_thread();
}
int Agent::Timer(int timernumber)
{
    if (get_changed())
    {
        //printf("update : %s %d %d",this->get_name().c_str(),this->state);
        AgentList *l=AgentList::getInstance();
        if (l)
            l->update(this);

        set_changed(0);
    }
    return true;
}
int Agent::Timer2(int timernumber)
{
    int n;
    sem_getvalue(&Agent::semaphore,&n);

    if (n!=0)
        return true;
    else
    {
        pthread_mutex_lock(&Agent::upload_error_mutex);
        if (!Agent::upload_error)
        {
            pthread_mutex_unlock(&Agent::upload_error_mutex);
            for (std::vector<Agent*>::iterator it=agentrefsvector.begin(); (it!=agentrefsvector.end()); it++)
            {

                if ((*it)->need_to_run)
                    (*it)->run();

            }
        }
        else
        {
            pthread_mutex_unlock(&Agent::upload_error_mutex);
        }
        return false;

    }
}
void Agent::setBehaviour(Behaviour *b) {
//lock();
    changed=1;
    this->behaviour=b;
    AgentList *l=AgentList::getInstance();
    if (l)
        l->update(this);

//unlock();
}

void Agent::connexion_accepted() {

    //lock();
    changed=1;
    this->state = 1;

    register_remoteserver(rs);
    //unlock();
    pthread_t t;
    pthread_create(&t,NULL,Agent::readloop_launcher,this);

    /*AgentList *l=AgentList::getInstance();
    			if(l)
    			{

    			l->update(this);

    		}*/

//printf("agent connected\n");
}

void Agent::connexion_accepted_w(Agent *a) {
    a->connexion_accepted();

}

void Agent::deconnexion() {

    //print_rs(rs);
    unregister_remoteserver(rs->id);
    //	print_rs(rs);
    open_connection(rs);

    changed=1;
    this->state = 0;
    AgentList *l=AgentList::getInstance();
    if (l)
        l->update(this);




}

void Agent::deconnexion_w(Agent *a) {
    a->deconnexion();

}
void Agent::referencebutton(AgentCheckButton *but) {

    buttonvector.push_back(but);
}
Agent::~Agent() {
    /* printf("delete %x\n",this);
     getchar();*/
    //conec.disconnect();
    //usleep(200000);
    bool finish = false;

    for (std::vector<Agent*>::iterator it=agentrefsvector.begin(); (it!=agentrefsvector.end())&&(!finish); it++)
    {

        if ((*it)==this)
        {
            agentrefsvector.erase(it);
            finish=true;
        }

    }

    for (std::vector<AgentCheckButton*>::iterator it=buttonvector.begin(); it!=buttonvector.end(); it++)
    {
        AgentCheckButton *b;

        b=(*it);
        b->destroy();


    }

    AgentList *l=AgentList::getInstance();
    if (l)
        l->remove(this);
}
Agent::Agent(Glib::ustring _name)
{
    need_to_run=false;
    set_on_upload(false);
    state_pause=0;
    changed=0;
    timeping = 500;
    watchtime = 2;
    watchsoft = "";
    watchsoftarg= "";
    pthread_mutex_init(&mutex,NULL);
    state=0;
    set_name(_name);
    behaviour=NULL;
    agentrefsvector.push_back(this);
    AgentList *l=AgentList::getInstance();
    if (l!=NULL)
    {
        l->add(this);
    }
    sigc::slot<bool> my_slot =sigc::bind ( sigc::mem_fun ( *this,
                                           &Agent::Timer ), 1 );

    conec = Glib::signal_timeout().connect ( my_slot,100);

    remoteserver *s = new_remoteserver((char*)"localhost",3000,1);
    rs=s;
    s->ptr_connexion_func = (void (*)(void*))Agent::connexion_accepted_w;
    s->ptr_deconnexion_func = (void (*)(void*))Agent::deconnexion_w;
    s->ptr_object=(long int)this;
    open_connection(s);

}
void Agent::stop()
{
    char buffer[32];

    sprintf(buffer,"$KILL_ALL\n");
    send_command(rs,buffer);


}
void Agent::deleteAll()
{
///TO FIX : segfault ???
    for (std::vector<Agent*>::iterator it=agentrefsvector.begin(); (it!=agentrefsvector.end()); it++)
    {
        delete (*it);
    }
    agentrefsvector.clear();
}
void Agent::stopAll()
{
    for (std::vector<Agent*>::iterator it=agentrefsvector.begin(); (it!=agentrefsvector.end()); it++)
    {
        (*it)->stop();
    }
}
void Agent::pause()
{
    char buffer[32];
    state_pause=1;
    sprintf(buffer,"$PAUSE_ALL\n");
    send_command(rs,buffer);


}
void Agent::pauseAll()
{
    for (std::vector<Agent*>::iterator it=agentrefsvector.begin(); (it!=agentrefsvector.end()); it++)
    {
        (*it)->pause();
    }
}

void Agent::armWatchDog()
{
    char buffer[256];
    state_pause=1;
    sprintf(buffer,"$ACTIVE_WATCHDOG,%d,%d",timeping,watchtime);
    if (watchsoft.size()>2)
    {
        sprintf(buffer,"%s,%s",buffer,watchsoft.c_str());
    }
    if (watchsoftarg.size()>2)
    {
        sprintf(buffer,"%s,%s",buffer,watchsoftarg.c_str());
    }
    sprintf(buffer,"%s\n",buffer);
    //printf("On lauch le watchdog: %s\n",buffer);
    send_command(rs,buffer);


}
void Agent::armWatchDogAll()
{
    for (std::vector<Agent*>::iterator it=agentrefsvector.begin(); (it!=agentrefsvector.end()); it++)
    {
        (*it)->armWatchDog();
    }
}

void Agent::togglepause()
{
    if (state_pause)
        unpause();
    else
        pause();

}
void Agent::togglepauseAll()
{
    for (std::vector<Agent*>::iterator it=agentrefsvector.begin(); (it!=agentrefsvector.end()); it++)
    {
        (*it)->togglepause();
    }
}
void Agent::unpause()
{
    state_pause=0;
    char buffer[32];

    sprintf(buffer,"$UNPAUSE_ALL\n");
    send_command(rs,buffer);


}
void Agent::unpauseAll()
{
    for (std::vector<Agent*>::iterator it=agentrefsvector.begin(); (it!=agentrefsvector.end()); it++)
    {
        (*it)->unpause();
    }
}

void Agent::uploadAll()
{

    sem_init(&Agent::semaphore,NULL,agentrefsvector.size());
    for (std::vector<Agent*>::iterator it=agentrefsvector.begin(); (it!=agentrefsvector.end()); it++)
    {
        (*it)->need_to_run=true;

    }
    sigc::slot<bool> my_slot =sigc::bind (
                                  &Agent::Timer2, 1 );

    Glib::signal_timeout().connect ( my_slot,100);
    Agent::make_before_upload=false;
    Agent::upload_error=false;
    for (std::vector<Agent*>::iterator it=agentrefsvector.begin(); (it!=agentrefsvector.end()); it++)
    {
        (*it)->upload();
    }

}
void Agent::uploadAndRun()
{
    sem_init(&Agent::semaphore,NULL,1);
    need_to_run=true;
    sigc::slot<bool> my_slot =sigc::bind (
                                  &Agent::Timer2, 1 );

    Glib::signal_timeout().connect ( my_slot,100);
    Agent::make_before_upload=false;
    Agent::upload_error=false;

    upload();



}
int Agent::upload()
{

    if (is_open(rs) && behaviour!=NULL ) {
        printf("upload...\n");
        set_on_upload(true);
        pthread_t t;
        pthread_create(&t,NULL,Agent::upload_launcher,this);

    }
    else
    {
        sem_wait(&Agent::semaphore);
    }

}


void Agent::makeAll()
{
    sem_init(&Agent::semaphore,NULL,agentrefsvector.size());
    for (std::vector<Agent*>::iterator it=agentrefsvector.begin(); (it!=agentrefsvector.end()); it++)
    {
        (*it)->need_to_run=true;

    }
    sigc::slot<bool> my_slot =sigc::bind (
                                  &Agent::Timer2, 1 );

    Glib::signal_timeout().connect ( my_slot,100);
    Agent::upload_error=false;
    Agent::make_before_upload=true;
    for (std::vector<Agent*>::iterator it=agentrefsvector.begin(); (it!=agentrefsvector.end()); it++)
    {

        (*it)->make();
    }

}
void Agent::makeAndRun()
{

    sem_init(&Agent::semaphore,NULL,1);
    need_to_run=true;
    sigc::slot<bool> my_slot =sigc::bind (
                                  &Agent::Timer2, 1 );

    Glib::signal_timeout().connect ( my_slot,100);
    Agent::upload_error=false;
    Agent::make_before_upload=true;

    make();

}
int Agent::make()
{
    //printf("make\n");

    if (is_open(rs) && behaviour!=NULL) {
        set_on_upload(true);
        pthread_t t;
        pthread_create(&t,NULL,Agent::upload_launcher,this);

    }
    else
    {
        sem_wait(&Agent::semaphore);
    }

}

void Agent::run()
{
    std::string tmp;
    state_pause=0;
    char *buffer;
    tmp = this->args;
    int n=tmp.rfind("\t");

    ///replace tab by space
    while (n!=-1)
    {
        tmp.replace(n,1," ");
        n=tmp.rfind("\t");
    }
    ///replace double space by space
    n=tmp.rfind("  ");

    while (n!=-1)
    {
        tmp.replace(n,2," ");
        n=tmp.rfind("  ");
    }
    ///replace space by ,
    n=tmp.rfind(" ");

    while (n!=-1)
    {
        tmp.replace(n,1,",");
        n=tmp.rfind(" ");
    }


    if (behaviour!=NULL) {
        buffer=(char*)malloc(sizeof(char)*(/*strlen(behaviour->name.c_str())*/behaviour->name.size() + tmp.size() +32));

        GlobalParameter *gp = GlobalParameter::getInstance();

        switch (this->get_type())
        {
        case 0:
            sprintf(buffer,"$EXECUTE,%s/%s",gp->get_remote_binary_path().c_str(),behaviour->namearm.c_str());
            break;
        case 1:
            sprintf(buffer,"$EXECUTE,%s/%s",gp->get_remote_binary_path().c_str(), behaviour->namearmgs.c_str());
            break;
        default:
            sprintf(buffer,"$EXECUTE,%s/%s",gp->get_remote_binary_path().c_str(),behaviour->namei686.c_str());
            break;

        }

        if (tmp.size()!=0)
            sprintf(buffer,"%s,%s\n",buffer,tmp.c_str());
        else
            sprintf(buffer,"%s\n",buffer);

        send_command(rs,buffer);

        free(buffer);
    }
    need_to_run=false;

}
void Agent::runAll()
{
    for (std::vector<Agent*>::iterator it=agentrefsvector.begin(); (it!=agentrefsvector.end()); it++)
    {
        (*it)->run();
    }
}
Agent::Agent(Glib::ustring _name,std::string host,int port,int type)
{


    pthread_mutex_init(&mutex,NULL);
    set_on_upload(false);
    state=0;
    timeping = 500;
    watchtime = 2;
    watchsoft = "";
    watchsoftarg= "";
    set_name( _name);
    behaviour=NULL;
    set_hostname(host);
    this->type = type;
    changed=0;
    agentrefsvector.push_back(this);

    sigc::slot<bool> my_slot =sigc::bind ( sigc::mem_fun ( *this,
                                           &Agent::Timer ), 1 );

    Glib::signal_timeout().connect ( my_slot,100);
    remoteserver *s = new_remoteserver((char*)host.c_str(),port,type);
    s->type=type;
    rs=s;
    s->ptr_connexion_func = (void (*)(void*))Agent::connexion_accepted_w;
    s->ptr_deconnexion_func = (void (*)(void*))Agent::deconnexion_w;
    s->ptr_object=(long int)this;
    open_connection(s);
//	std::cout << "name:" << this->get_name() << "\n" << std::endl;

}
Agent *Agent::create(Glib::ustring _name,std::string host,int port,int type)
{
    Agent *a = new Agent(_name,host,port,type);
    //std::cout << "name:" << a->get_name() << "\n" << std::endl;
    AgentList *l=AgentList::getInstance();
    //a->lock();
    if (l!=NULL)
    {
        l->add(a);
    }
    BehaviourTab::addAgentAll(a);
    //a->unlock();
    return a;
}

int Agent::load(std::string filename)
{
    FILE *f;
    char buff[256];
    char buff2[256];
    int n;
    int t;
    remoteserver *s;
    std::string name;
    std::string host;
    f=fopen(filename.c_str(),"r");
    if (!f)
    {
        std::cout <<"no file " << filename << "\n" << std::endl;
        return -1;
    }
    while (!feof(f))
    {
        fscanf(f,"%s %s %d %d\n",buff,buff2,&n,&t);
        if (strlen(buff)>3)
        {
            name = buff;
            host= buff2;


            BehaviourTab::addAgentAll(Agent::create(name,host,n,t));
            /*s = new_remoteserver(buff,n,1);
            		s->type = t;

            		open_connection(s);
            		register_remoteserver(s);
            		*///printf("%s %d\n",buff,n);
        }
    }
    fclose(f);

    return 1;


}
void Agent::lock()
{

    pthread_mutex_lock(&mutex);

}
void Agent::unlock()
{

    pthread_mutex_unlock(&mutex);
}
void Agent::ActiveButton(AgentCheckButton *checkbutton)
{
    for (std::vector<AgentCheckButton*>::iterator it=buttonvector.begin(); it!=buttonvector.end(); it++)
    {
        AgentCheckButton *b;

        b=(*it);
        if (b!=checkbutton)
        {
            b->set_active(false);
        }
    }
    setBehaviour(checkbutton->behaviour);
}

void Agent::ActiveButton(Behaviour *_behaviour)
{
    for (std::vector<AgentCheckButton*>::iterator it=buttonvector.begin(); it!=buttonvector.end(); it++)
    {
        AgentCheckButton *b;

        b=(*it);
        if (b->behaviour!=_behaviour)
        {
            b->set_active(false);
        }
        else
        {
            b->set_active(true);
        }
    }
    setBehaviour(_behaviour);
}
void Agent::link(BehaviourTab *l)
{
    AgentBehaviour = l;
}


int Agent::get_changed() {
    int ret;
    lock();
    ret=changed;
    unlock();
    return ret;
}
int Agent::get_type() {
    int ret;
    lock();
    ret=type;
    unlock();
    return ret;
}
int Agent::get_timeping() {
    int ret;
    lock();
    ret=timeping;
    unlock();
    return ret;
}
int Agent::get_timewatch() {
    int ret;
    lock();
    ret=watchtime;
    unlock();
    return ret;
}
int Agent::get_state_pause() {
    int ret;
    lock();
    ret=state_pause;
    unlock();
    return ret;
}
Glib::ustring Agent::get_name() {
    Glib::ustring ret;
    lock();
    ret=name;
    unlock();
    return ret;
}
Glib::ustring Agent::get_watchsoft() {
    Glib::ustring ret;
    lock();
    ret=watchsoft;
    unlock();
    return ret;
}
Glib::ustring Agent::get_watchsoftarg() {
    Glib::ustring ret;
    lock();
    ret=watchsoftarg;
    unlock();
    return ret;
}
Glib::ustring Agent::get_args() {
    Glib::ustring ret;
    lock();
    ret=args;
    unlock();
    return ret;
}
BehaviourTab *Agent::get_AgentBehaviour() {
    BehaviourTab * ret;
    lock();
    ret=AgentBehaviour;
    unlock();
    return ret;
}

int Agent::get_state() {
    int ret;
    lock();
    ret=state;
    unlock();
    return ret;
}
remoteserver *Agent::get_rs() {
    remoteserver * ret;
    lock();
    ret=rs;
    unlock();
    return ret;
}
Behaviour *Agent::get_behaviour() {
    Behaviour * ret;
    lock();
    ret=behaviour;
    unlock();
    return ret;
}
Glib::ustring Agent::get_hostname() {
    Glib::ustring ret;
    lock();
    ret=hostname;
    unlock();
    return ret;
}

bool Agent::get_on_upload()
{
    bool ret;
    lock();
    ret=on_upload;
    unlock();
    return ret;
}
void Agent::set_on_upload(bool value)
{
    lock();
    changed=1;
    on_upload=value;
    unlock();
}
void Agent::set_changed(int value) {
    lock();
    changed=value;
    unlock();
}
void Agent::set_type(int value) {
    lock();
    changed=1;
    type=value;
    unlock();
}
void Agent::set_state_pause(int value) {
    lock();
    changed=1;
    state_pause=value;
    unlock();
}
void Agent::set_name(Glib::ustring value) {
    lock();
    changed=1;
    name=value;
    unlock();
}
void Agent::set_args(Glib::ustring value) {
    lock();
    changed=1;
    args=value;
    unlock();
}
void Agent::set_watchsoftarg(Glib::ustring value) {
    lock();
    changed=1;
    watchsoftarg=value;
    unlock();
}
void Agent::set_watchsoft(Glib::ustring value) {
    lock();
    changed=1;
    watchsoft=value;
    unlock();
}
/*void Agent::set_args(Glib::ustring value){
lock();
changed=1;
args=value;
unlock();
}*/
void Agent::set_AgentBehaviour(BehaviourTab *value) {
    lock();
    changed=1;
    AgentBehaviour=value;
    unlock();
}
void Agent::set_state(int value) {
    lock();
    changed=1;
    state=value;
    unlock();
}
void Agent::set_timeping(int value) {
    lock();
    changed=1;
    timeping=value;
    unlock();
}
void Agent::set_timewatch(int value) {
    lock();
    changed=1;
    watchtime=value;
    unlock();
}
void Agent::set_rs(remoteserver *value) {
    lock();
    changed=1;
    rs=value;
    unlock();
}
void Agent::set_behaviour(Behaviour *value) {
    lock();
    changed=1;
    behaviour=value;
    unlock();
}
void Agent::set_hostname(Glib::ustring value) {
    lock();
    changed=1;
    hostname=value;
    unlock();

}