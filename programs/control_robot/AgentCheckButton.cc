#include <iomanip>
#include <sstream>
#include <iostream>

#include <locale.h>
#include <sigc++/retype_return.h>
#include <gtkmm.h>
#include "Agent.hh"
#include "AgentCheckButton.hh"
#include "Behaviour.hh"
AgentCheckButton::AgentCheckButton(Agent *agent_,Behaviour *behaviour_)
{
    TYPE=TYPE_BEHAVIOUR;
    agent=agent_;
    behaviour=behaviour_;
    agent->referencebutton(this);
//std::cout << " namez " << agent->name << "\n" << std::endl;
//	agent->lock();
//11std::cout << " namez " << agent->name << "\n" << std::endl;
    this->set_label(agent->get_name());
//	agent->unlock();
    this->signal_clicked().connect(sigc::mem_fun(*this,&AgentCheckButton::onclick),false);
    if (agent->get_behaviour()==behaviour)
    {
        agent->ActiveButton(behaviour_);
        set_active(true);

    }


}
AgentCheckButton::AgentCheckButton(Agent *agent_)
{
    //TYPE=TYPE_BEHAVIOUR;
    agent=agent_;
    //behaviour=behaviour_;
    //agent->referencebutton(this);
//std::cout << " namez " << agent->name << "\n" << std::endl;
//	agent->lock();
//11std::cout << " namez " << agent->name << "\n" << std::endl;
    this->set_label(agent->get_name());
//	agent->unlock();
    this->signal_clicked().connect(sigc::mem_fun(*this,&AgentCheckButton::onclick),false);
    /*	if(agent->get_behaviour()==behaviour)
    	{
    		agent->ActiveButton(behaviour_);
    		set_active(true);

    	}
    */
}

void AgentCheckButton::onclick()
{

    switch (TYPE)
    {
    case TYPE_BEHAVIOUR:
        if (this->get_active())
        {
            agent->ActiveButton(behaviour);

        }
        else
        {
            printf("unactived\n");
            agent->setBehaviour(NULL);
        }
        break;
    case TYPE_OTHER:

        break;
    }
}
void AgentCheckButton::destroy()
{

    Gtk::VBox *b;
    b=(Gtk::VBox *)this->get_parent();
    b->remove(*this);
    delete this;
}