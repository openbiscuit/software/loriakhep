#include <iomanip>
#include <sstream>
#include <iostream>

#include <locale.h>
#include <sigc++/retype_return.h>
#include <gtkmm.h>
#include "AgentCheckButton.hh"
#include "Agent.hh"
#include "BehaviourTab.hh"
#include "BehaviourList.hh"
#include "AgentList.hh"
#include "remote_serveur.h"
#include "WindowArg.hh"
#include "WindowWatchdog.hh"
//class AgentCheckButton;
#include "SshTab.hh"
#include "ProcessusOpen.hh"
#include "WindowSshKey.hh"
#include <string.h>
#include "WindowTcpServer.hh"
#include "GlobalParameter.hh"

int ___state;
class buttonloadexperiment
{
public:
    buttonloadexperiment (Glib::RefPtr < Gtk::Builder > interface);
    ~buttonloadexperiment ()
    {
    };
    Gtk::FileChooserButton * f;
    Glib::RefPtr < Gtk::Builder > xml_interface;
    void on_loadfilechooser_selection_changed ();
};



void
buttonloadexperiment::on_loadfilechooser_selection_changed ()
{
    Agent::load_parameter (f->get_filename ());
}

buttonloadexperiment::buttonloadexperiment (Glib::RefPtr < Gtk::Builder >
        interface)
{
    xml_interface = interface;

    xml_interface->get_widget ("filechooseexperiment", f);
    f->signal_selection_changed ().
    connect (sigc::
             mem_fun (*this,
                      &buttonloadexperiment::
                      on_loadfilechooser_selection_changed), false);
}

class buttonagent
{
public:
    buttonagent (Glib::RefPtr < Gtk::Builder > interface);
    ~buttonagent ()
    {
    };
    Gtk::FileChooserButton * f;
    Glib::RefPtr < Gtk::Builder > xml_interface;
    void on_loadfilechooser_selection_changed ();
};

void
buttonagent::on_loadfilechooser_selection_changed ()
{
    Agent::load (f->get_filename ());
}

buttonagent::buttonagent (Glib::RefPtr < Gtk::Builder > interface)
{
    xml_interface = interface;

    xml_interface->get_widget ("filechooseragent", f);
    f->signal_selection_changed ().
    connect (sigc::
             mem_fun (*this,
                      &buttonagent::on_loadfilechooser_selection_changed),
             false);
}


class buttclosebehaviourtab
{
public:
    buttclosebehaviourtab (Glib::RefPtr < Gtk::Builder > interface);
    ~buttclosebehaviourtab ()
    {
    };
    Gtk::Button * b;
    Glib::RefPtr < Gtk::Builder > xml_interface;
    void click ();
};

class buttonWatchDog
{
public:
    buttonWatchDog (Glib::RefPtr < Gtk::Builder > interface);
    buttonWatchDog ()
    {
    };
    Gtk::Button * b;
    Glib::RefPtr < Gtk::Builder > xml_interface;
    void click ();
};

class buttonSshKey
{
public:
    buttonSshKey (Glib::RefPtr < Gtk::Builder > interface);
    buttonSshKey ()
    {
    };
    Gtk::Button * b;
    Glib::RefPtr < Gtk::Builder > xml_interface;
    void click ();
};

class buttonTcpServer
{
public:
    buttonTcpServer (Glib::RefPtr < Gtk::Builder > interface);
    buttonTcpServer ()
    {
    };
    Gtk::Button * b;
    Glib::RefPtr < Gtk::Builder > xml_interface;
    void click ();
};

class buttonSaveExperiment
{
public:
    buttonSaveExperiment (Glib::RefPtr < Gtk::Builder > interface);
    buttonSaveExperiment ()
    {
    };
    Gtk::Button * b;
    Glib::RefPtr < Gtk::Builder > xml_interface;
    void click ();
    void ok ();
    void cancel ();
    Gtk::FileChooserDialog * w;
    bool on_key_release_event (GdkEventKey * ev);
};

buttonSaveExperiment::buttonSaveExperiment (Glib::RefPtr < Gtk::Builder >
        interface)
{
    xml_interface = interface;
    Gtk::Button * b2, *b3;
    xml_interface->get_widget ("save_experiment_button", b);
    xml_interface->get_widget ("window_save_button_ok", b2);
    xml_interface->get_widget ("window_save_button_annuler", b3);
    b->signal_clicked ().
    connect (sigc::mem_fun (*this, &buttonSaveExperiment::click), false);
    b2->signal_clicked ().
    connect (sigc::mem_fun (*this, &buttonSaveExperiment::ok), false);
    b3->signal_clicked ().
    connect (sigc::mem_fun (*this, &buttonSaveExperiment::cancel), false);
    //w->signal_key_release_event().connect(sigc::mem_fun(*this,&buttonSaveExperiment::on_key_release_event),false);
    //w->signal_k

}

bool
buttonSaveExperiment::on_key_release_event (GdkEventKey * ev)
{
    if (ev->keyval == 65293)
    {
        ok ();
    }

}

void
buttonSaveExperiment::ok ()
{
    Agent::save_parameter (w->get_filename ());
    w->hide ();
}

void
buttonSaveExperiment::cancel ()
{
    w->hide ();
}

void
buttonSaveExperiment::click ()
{

    xml_interface->get_widget ("save_window", w);
    w->show ();


}

void
buttonSshKey::click ()
{
    WindowSshKey *w = WindowSshKey::getInstance ();
    w->show ();
}

buttonSshKey::buttonSshKey (Glib::RefPtr < Gtk::Builder > interface)
{
    xml_interface = interface;

    xml_interface->get_widget ("buttonuploadkey", b);
    b->signal_clicked ().connect (sigc::mem_fun (*this, &buttonSshKey::click),
                                  false);
}

void
buttonTcpServer::click ()
{
    WindowTcpServer *w = WindowTcpServer::getInstance ();
    w->show ();
}

buttonTcpServer::buttonTcpServer (Glib::RefPtr < Gtk::Builder > interface)
{
    xml_interface = interface;

    xml_interface->get_widget ("buttontcpserver", b);
    b->signal_clicked ().
    connect (sigc::mem_fun (*this, &buttonTcpServer::click), false);
}


void
buttonWatchDog::click ()
{
    //printf("WatchDog launch\n");
    Agent::armWatchDogAll ();
}

buttonWatchDog::buttonWatchDog (Glib::RefPtr < Gtk::Builder > interface)
{
    xml_interface = interface;

    xml_interface->get_widget ("watchdog_button", b);
    b->signal_clicked ().connect (sigc::mem_fun (*this, &buttonWatchDog::click),
                                  false);
}

class buttonWatchDogOne
{
public:
    buttonWatchDogOne (Glib::RefPtr < Gtk::Builder > interface);
    buttonWatchDogOne ()
    {
    };
    Gtk::Button * b;
    Glib::RefPtr < Gtk::Builder > xml_interface;
    void click ();
};

void
buttonWatchDogOne::click ()
{
    AgentList *l = AgentList::getInstance ();
    Agent *a = l->get_agent ();
    if (a)
        a->armWatchDog ();
}

buttonWatchDogOne::buttonWatchDogOne (Glib::RefPtr < Gtk::Builder > interface)
{
    xml_interface = interface;

    xml_interface->get_widget ("buttonwatchdogone", b);
    b->signal_clicked ().
    connect (sigc::mem_fun (*this, &buttonWatchDogOne::click), false);
}


class buttonParameter
{
public:
    buttonParameter (Glib::RefPtr < Gtk::Builder > interface);
    buttonParameter ()
    {
    };
    Gtk::Button * b;
    Glib::RefPtr < Gtk::Builder > xml_interface;
    void click ();
};

void
buttonParameter::click ()
{

    GlobalParameter *gp;
    gp = GlobalParameter::getInstance ();
    gp->show ();
}

buttonParameter::buttonParameter (Glib::RefPtr < Gtk::Builder > interface)
{
    xml_interface = interface;

    xml_interface->get_widget ("buttonparameter", b);
    b->signal_clicked ().
    connect (sigc::mem_fun (*this, &buttonParameter::click), false);
}


void
buttclosebehaviourtab::click ()
{
    BehaviourList *l = BehaviourList::getInstance ();
    l->removeSelected ();
    //Agent::load(f->get_filename());
}

buttclosebehaviourtab::buttclosebehaviourtab (Glib::RefPtr < Gtk::Builder >
        interface)
{
    xml_interface = interface;

    xml_interface->get_widget ("buttoncloseBehaviourTab", b);
    b->signal_clicked ().
    connect (sigc::mem_fun (*this, &buttclosebehaviourtab::click), false);
}

class buttonbehaviour
{
public:
    buttonbehaviour (Glib::RefPtr < Gtk::Builder > interface);
    ~buttonbehaviour ()
    {
    };
    Gtk::FileChooserButton * f;
    Glib::RefPtr < Gtk::Builder > xml_interface;
    void on_loadfilechooser_selection_changed ();
};

void
buttonbehaviour::on_loadfilechooser_selection_changed ()
{
    printf ("load behaviour...\n");
    BehaviourList *l = BehaviourList::getInstance ();
    l->add (f->get_filename ());

}

buttonbehaviour::buttonbehaviour (Glib::RefPtr < Gtk::Builder > interface)
{
    xml_interface = interface;

    xml_interface->get_widget ("filechooser_behaviour", f);
    f->signal_selection_changed ().
    connect (sigc::
             mem_fun (*this,
                      &buttonbehaviour::on_loadfilechooser_selection_changed),
             false);
}

class buttonStartAll
{
public:
    buttonStartAll (Glib::RefPtr < Gtk::Builder > interface);
    ~buttonStartAll ()
    {
    };
    void click ();


    Glib::RefPtr < Gtk::Builder > xml_interface;
};

class buttonStartOne
{
public:
    buttonStartOne (Glib::RefPtr < Gtk::Builder > interface);
    ~buttonStartOne ()
    {
    };
    void click ();


    Glib::RefPtr < Gtk::Builder > xml_interface;
};

class buttonStopOne
{
public:
    buttonStopOne (Glib::RefPtr < Gtk::Builder > interface);
    ~buttonStopOne ()
    {
    };
    void click ();


    Glib::RefPtr < Gtk::Builder > xml_interface;
};

class buttonPauseOne
{
public:
    buttonPauseOne (Glib::RefPtr < Gtk::Builder > interface);
    ~buttonPauseOne ()
    {
    };
    void click ();


    Glib::RefPtr < Gtk::Builder > xml_interface;
};

class buttonDeleteOne
{
public:
    buttonDeleteOne (Glib::RefPtr < Gtk::Builder > interface);
    ~buttonDeleteOne ()
    {
    };
    void click ();


    Glib::RefPtr < Gtk::Builder > xml_interface;
};

class buttonPauseAll
{
public:
    buttonPauseAll (Glib::RefPtr < Gtk::Builder > interface);
    ~buttonPauseAll ()
    {
    };
    void click ();


    Glib::RefPtr < Gtk::Builder > xml_interface;
};

class buttonStopAll
{
public:
    buttonStopAll (Glib::RefPtr < Gtk::Builder > interface);
    ~buttonStopAll ()
    {
    };
    void click ();


    Glib::RefPtr < Gtk::Builder > xml_interface;
};

buttonStartAll::buttonStartAll (Glib::RefPtr < Gtk::Builder > interface)
{
    xml_interface = interface;
    Gtk::Button * b;
    ___state = 0;
    xml_interface->get_widget ("start_all", b);
    b->signal_clicked ().connect (sigc::mem_fun (*this, &buttonStartAll::click),
                                  false);


}


buttonStopAll::buttonStopAll (Glib::RefPtr < Gtk::Builder > interface)
{
    xml_interface = interface;
    Gtk::Button * b;
    ___state = 0;

    xml_interface->get_widget ("stop_all", b);
    b->signal_clicked ().connect (sigc::mem_fun (*this, &buttonStopAll::click),
                                  false);


}

buttonPauseAll::buttonPauseAll (Glib::RefPtr < Gtk::Builder > interface)
{
    xml_interface = interface;
    Gtk::Button * b;
    ___state = 0;

    xml_interface->get_widget ("pause_all", b);
    b->signal_clicked ().connect (sigc::mem_fun (*this, &buttonPauseAll::click),
                                  false);


}

buttonStartOne::buttonStartOne (Glib::RefPtr < Gtk::Builder > interface)
{
    xml_interface = interface;
    Gtk::Button * b;
    xml_interface->get_widget ("start_one", b);
    b->signal_clicked ().connect (sigc::mem_fun (*this, &buttonStartOne::click),
                                  false);


}

buttonDeleteOne::buttonDeleteOne (Glib::RefPtr < Gtk::Builder > interface)
{
    xml_interface = interface;
    Gtk::Button * b;
    xml_interface->get_widget ("delete_one", b);
    b->signal_clicked ().
    connect (sigc::mem_fun (*this, &buttonDeleteOne::click), false);


}

buttonStopOne::buttonStopOne (Glib::RefPtr < Gtk::Builder > interface)
{
    xml_interface = interface;
    Gtk::Button * b;
    xml_interface->get_widget ("stop_one", b);
    b->signal_clicked ().connect (sigc::mem_fun (*this, &buttonStopOne::click),
                                  false);


}

buttonPauseOne::buttonPauseOne (Glib::RefPtr < Gtk::Builder > interface)
{
    xml_interface = interface;
    Gtk::Button * b;
    xml_interface->get_widget ("pause_one", b);
    b->signal_clicked ().connect (sigc::mem_fun (*this, &buttonPauseOne::click),
                                  false);


}

void
buttonPauseOne::click ()
{
    AgentList *l = AgentList::getInstance ();
    Agent *a = l->get_agent ();
    if (a)
        a->togglepause ();
}

void
buttonDeleteOne::click ()
{
    AgentList *l = AgentList::getInstance ();
    Agent *a = l->get_agent ();
    if (a)
        delete (a);
}

void
buttonStopOne::click ()
{
    AgentList *l = AgentList::getInstance ();
    Agent *a = l->get_agent ();
    if (a)
        a->stop ();
}

void
buttonStartOne::click ()
{
    AgentList *l = AgentList::getInstance ();
    Agent *a = l->get_agent ();
    Gtk::CheckButton * b, *b2;

    xml_interface->get_widget ("checkbutton_upload", b);
    xml_interface->get_widget ("checkbutton_make", b2);
    if (a)
    {

        if (b2->get_active ())
            a->makeAndRun ();
        else if (b->get_active ())
            a->uploadAndRun ();
        else
            a->run ();

    }
}

void
buttonStartAll::click ()
{
    Gtk::CheckButton * b, *b2;

    xml_interface->get_widget ("checkbutton_upload", b);
    xml_interface->get_widget ("checkbutton_make", b2);
    if (b2->get_active ())
        Agent::makeAll ();
    else if (b->get_active ())
        Agent::uploadAll ();
    else
        Agent::runAll ();
    ___state = 0;
}

void
buttonStopAll::click ()
{


    Agent::stopAll ();


}

void
buttonPauseAll::click ()
{

    if (!___state)
    {
        Agent::pauseAll ();
        ___state = 1;
    }
    else
    {
        Agent::unpauseAll ();
        ___state = 0;
    }

}

static void
sig_tstp (int signo)
{
    exit(0);
}

int
main (int argc, char *argv[])
{

    char pwd[1024];
    char exc[256];
    char ___homedir[256];
    char tmp[512];
    std::string spwd;
    std::string sexc;
    int n;
    int i;
    FILE *popfile;
    popfile=popen("echo ~","r");
    i=0;
    while (!feof(popfile))
    {
        fscanf(popfile,"%c",&___homedir[i]);
        i++;
    }
    ___homedir[i-2]='\0';
    pclose(popfile);
    pclose(popen("mkdir ~/.control_robot","w"));
    for (i = strlen (argv[0]) - 1; i >= 0; i--)
    {
        if (argv[0][i] == '/')
        {
            n = i;
            i = -1;
        }

    }

    for (i = 0; i < n; i++)
    {

        pwd[i] = argv[0][i];
    }
    pwd[i] = '\0';

    for (i = n + 1; i < strlen (argv[0]); i++)
    {
        exc[i - (n + 1)] = argv[0][i];

    }
    exc[i - (n + 1)] = '\0';
    spwd = std::string (pwd);
    sexc = std::string (exc);
    if (spwd != ".")
    {
//exit(pclose(popen(pwd,"w")));
        std::string exec = "cd " + spwd + " && ./" + sexc;
        ProcessusOpen *pop = new ProcessusOpen (exec);
        pop->run ("n");
        exit (pop->close_ ());
    }

    Gtk::Main kit (argc, argv);
    Glib::RefPtr < Gtk::Builder > xml_interface=
        Gtk::Builder::create_from_file ("interface1.xml");
    Glib::RefPtr < Gtk::Builder > xml_interface2 =
        Gtk::Builder::create_from_file ("tab.xml");
    Gtk::Window * window;
    BehaviourTab *tab;
    buttonStartAll *s;
    buttonStopAll *stop;
    buttonPauseAll *pause;
    buttonStartOne *startone;
    buttonPauseOne *pauseone;
    buttonStopOne *stopone;
    WindowArg *warg = WindowArg::getInstance (xml_interface);
    WindowWatchdog *waf = WindowWatchdog::getInstance (xml_interface);
    buttonSaveExperiment wsavexp (xml_interface);
    buttonloadexperiment bexpload (xml_interface);
    buttonagent __bezrze (xml_interface);
    buttonbehaviour __bezrz (xml_interface);
    buttonDeleteOne nzer (xml_interface);
    buttonWatchDog bwatch (xml_interface);
    buttonWatchDogOne bwatchone (xml_interface);
    BehaviourList *l = BehaviourList::getInstance (xml_interface);
    s = new buttonStartAll (xml_interface);
    stop = new buttonStopAll (xml_interface);
    pause = new buttonPauseAll (xml_interface);
    startone = new buttonStartOne (xml_interface);
    stopone = new buttonStopOne (xml_interface);
    pauseone = new buttonPauseOne (xml_interface);
    new buttclosebehaviourtab (xml_interface);
    AgentList::getInstance (xml_interface);
//notebook->set_tab_label_text(*vb2,"toto");
    /*l->add("followline.armgs");
    	l->add("gradient.arm");
    	l->add("uvctest.arm");
    	l->add("/home/beaufoni/beauf/loriakhep/trunk/tutorial/braiten_multi_robot/braiten_multi.arm");
    	l->add("/home/beaufoni/beauf/loriakhep/tutorialgoto/goto.arm");
    	l->add("/home/beaufoni/beauf/loriakhep/tutorialgoto/toto.arm");
    	l->add("boum");
    	l->add("boum2");
    */
    sprintf(tmp,"%s/.control_robot/.behaviourlist",___homedir);
    l->load (tmp);

    //Agent *a = Agent::create("robot 1","192.168.0.2",3000,0);

    /*BehaviourTab::addAgentAll(a);
       a =Agent::create("robot 2","192.168.0.4",3000,0);
       BehaviourTab::addAgentAll(a);
       a =  Agent::create("robot 3","192.168.0.12",3000,1);
       BehaviourTab::addAgentAll(a);
     */
    //SshTab st(xml_interface,a);
    sprintf(tmp,"%s/.control_robot/liste_robot",___homedir);
    Agent::load (tmp);
//tab = new BehaviourTab(xml_interface,"toto");
    xml_interface->get_widget ("window1", window);
    buttonTcpServer btcpserver (xml_interface);
    buttonSshKey bsshkey (xml_interface);
    WindowSshKey::getInstance (xml_interface);
    WindowTcpServer::getInstance (xml_interface);
    buttonParameter bpp (xml_interface);
    GlobalParameter::getInstance (xml_interface);
    signal (SIGINT, sig_tstp);
    signal (SIGKILL, sig_tstp);
    signal (SIGTSTP, sig_tstp);
    kit.run (*window);
    SshTab::Clean ();
//ProcessusOpen::Clean();
    return 0;
}
