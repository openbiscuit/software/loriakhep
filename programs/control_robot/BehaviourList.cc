#include <string>
#include <fstream>
#include <iostream>
#include "BehaviourList.hh"

#include "BehaviourTab.hh"
#include "Behaviour.hh"

BehaviourList *BehaviourList::instance=NULL;
BehaviourList *BehaviourList::getInstance(Glib::RefPtr<Gtk::Builder> interface) {
    if (instance==NULL)
    {

        if (interface==NULL)
            return NULL;
        instance=new BehaviourList(interface);

    }

    return instance;

}
BehaviourList *BehaviourList::getInstance() {
    return instance;
}

void BehaviourList::removeSelected()
{
    Glib::RefPtr<Gtk::TreeView::Selection> refSelection = t->get_selection();
    if (refSelection)
    {
        Gtk::TreeModel::iterator iter = refSelection->get_selected();
        if (iter)
        {
            Glib::ustring s=(*iter)[m_Columns.m_col_name];
            Glib::ustring ss=(*iter)[m_Columns.m_col_path];

            Behaviour *b=(*iter)[m_Columns.m_behaviour_ptr];
            //delete b;
            liste->erase(iter);

            b->closeBehaviourTab();
            delete b;
            //	new BehaviourTab(xml_interface,b);
            save(".behaviourlist");
        }
    }

}

void BehaviourList::save(std::string filename)
{
    printf("save ...\n");
    if (filename_is_attached) {
        printf("....\n");
        std::ofstream fi(attached_filename.c_str() );


        Glib::RefPtr<Gtk::TreeView::Selection> refSelection = t->get_selection();

        Gtk::TreeModel::iterator begin = liste->erase(liste->prepend());  ///no other metod to get begin and end iterator
        Gtk::TreeModel::iterator end=liste->erase(liste->append());
        Gtk::TreeModel::iterator iter;
        for (iter=begin; iter!=end; iter++)
        {
//= refSelection->get_selected();
            if (iter)
            {
                Behaviour *b=(*iter)[m_Columns.m_behaviour_ptr];
                fi << b->orignalpath << "\n";
                std::cout << b->orignalpath << "\n" << attached_filename << std::endl;



            }
        }
    }


}
void BehaviourList::on_row_activated(const Gtk::TreeModel::Path& path, Gtk::TreeViewColumn* column)
{

    Glib::RefPtr<Gtk::TreeView::Selection> refSelection = t->get_selection();
    if (refSelection)
    {
//		Gtk::TreeModel t;
        Gtk::TreeModel::iterator iter = refSelection->get_selected();
        if (iter)
        {

            Glib::ustring s=(*iter)[m_Columns.m_col_name];
            Glib::ustring ss=(*iter)[m_Columns.m_col_path];
            Behaviour *b=(*iter)[m_Columns.m_behaviour_ptr];

            new BehaviourTab(xml_interface,b);
        }
    }


}


BehaviourList::BehaviourList(Glib::RefPtr<Gtk::Builder> interface)
{
/// to move

    filename_is_attached=false;
    xml_interface = interface;
    liste = Gtk::ListStore::create(m_Columns);
    interface->get_widget("treeview_behaviour", t);
    t->set_model(liste);
    t->append_column("", m_Columns.m_col_name);

    t->signal_row_activated().connect(sigc::mem_fun(*this,&BehaviourList::on_row_activated),false);
}

Behaviour *BehaviourList::find(std::string origpath) {

    Gtk::TreeModel::iterator begin = liste->erase(liste->prepend());  ///no other metod to get begin and end iterator
    Gtk::TreeModel::iterator end=liste->erase(liste->append());
    Gtk::TreeModel::iterator iter;
    for (iter=begin; iter!=end; iter++)
    {
//= refSelection->get_selected();
        if (iter)
        {
            Behaviour *b=(*iter)[m_Columns.m_behaviour_ptr];
            if ( b->orignalpath ==origpath)
                return b;




        }
    }
    return NULL;

}
void BehaviourList::load(std::string filename)
{
    attached_filename = filename;
    std::fstream fi(filename.c_str());
    if (fi)
    {
        std::string ligne;
        while (std::getline(fi,ligne))
        {
            if (ligne.size()>2)
                this->add(ligne);
        }
    }

    filename_is_attached = true;

}
void BehaviourList::add(std::string s) {
    Gtk::TreeModel::Row row = *(liste->append());
    Behaviour *b;
    b= new Behaviour(s);
    row[m_Columns.m_behaviour_ptr]=b;
    row[m_Columns.m_col_path]=b->patharm;

    row[m_Columns.m_col_name] = b->name;
    if (filename_is_attached) {
        std::ofstream fi(attached_filename.c_str(), std::ios_base::app );
        fi << s << "\n";
        std::cout << s << "\n" << attached_filename << std::endl;
    }
}