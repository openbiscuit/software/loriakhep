#ifndef _Behaviour_HH
#define _Behaviour_HH
#include <iostream>
#include <string>
#include <pthread.h>
class BehaviourTab;
class Behaviour {
public:
    Behaviour(std::string behaviourpath);
    ~Behaviour() {};
    void closeBehaviourTab();
    int make(int type);
    bool inmake;
    pthread_mutex_t inmakemutex;
    int type;
    std::string name;
    std::string namearm;
    std::string namearmgs;
    std::string namei686;
    std::string patharm;
    std::string patharmgs;
    std::string pathi686;
    std::string orignalpath;
    BehaviourTab *bt;

};

#endif