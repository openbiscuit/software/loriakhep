#include "WindowTcpServer.hh"
#include "Agent.hh"
#include "SshTab.hh"
WindowTcpServer *WindowTcpServer::instance_=NULL;
std::vector<AgentCheckButton *> WindowTcpServer::vec;
void WindowTcpServer::ok_click() {



    for (std::vector<AgentCheckButton*>::iterator it=vec.begin(); (it!=vec.end()); it++)
    {
        AgentCheckButton*acb=(*it);
        if (acb->get_active())
        {
            std::cout << acb->agent->get_name() << std::endl;
            SshTab *sst = new SshTab(xml_interface,acb->agent);
        }

    }

    window->hide();
}
void WindowTcpServer::annuler_click() {
    window->hide();

}
void WindowTcpServer::link()
{

    xml_interface->get_widget("vbox2_ssh_server",vbox);
    xml_interface->get_widget("window_sshserver",window);
    Gtk::Button *b;
    xml_interface->get_widget("button_ok_sshserver",b);
    b->signal_clicked().connect(sigc::mem_fun(*this,&WindowTcpServer::ok_click),false);
    xml_interface->get_widget("button_annuler_sshserver",b);
    b->signal_clicked().connect(sigc::mem_fun(*this,&WindowTcpServer::annuler_click),false);
}

WindowTcpServer *WindowTcpServer::getInstance(Glib::RefPtr<Gtk::Builder> interface) {
    if (instance_==NULL)
    {

        if (interface==NULL)
            return NULL;
        instance_=new WindowTcpServer(interface);

    }

    return instance_;

}

WindowTcpServer::WindowTcpServer(Glib::RefPtr<Gtk::Builder> interface) {
    xml_interface = interface;
    link();
    for (std::vector<Agent*>::iterator it=Agent::agentrefsvector.begin(); (it!=Agent::agentrefsvector.end()); it++)
    {
        Agent *a=(*it);
        AgentCheckButton *acb=new AgentCheckButton(a);
        vec.push_back(acb);
        vbox->add(*acb);
        acb->show();

    }
    window->set_title("run tcp server");
};

void WindowTcpServer::show()
{

    window->show();

}
WindowTcpServer *WindowTcpServer::getInstance() {
    return instance_;
}