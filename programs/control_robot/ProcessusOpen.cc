#include "ProcessusOpen.hh"
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
std::vector<ProcessusOpen *> ProcessusOpen::vec;
ProcessusOpen::ProcessusOpen(std::string _cmd) {

    cmd = _cmd;
    ProcessusOpen::vec.push_back(this);
}

void ProcessusOpen::Clean() {

    for (std::vector<ProcessusOpen*>::iterator it=ProcessusOpen::vec.begin(); it!=ProcessusOpen::vec.end(); it++)
    {
        delete *it;
    }
}
ProcessusOpen::~ProcessusOpen() {
    kill_();
    close_();
}
void ProcessusOpen::kill_(int signal) {
    kill(pid,signal);
    close_();

}
int ProcessusOpen::close_() {

    int res;
    waitpid(pid,&res,0);
    return res;
}
int ProcessusOpen::run(std::string str) {

    int tube[2];
    int tube2[2];
    int tube3[2];
    pipe(tube2);
    pipe(tube3);
    FILE *flux;
    pid_t fils;
    int r,w;
    if (str=="r")
    {
        r=1;
        w=0;

    }
    else if (str=="w")
    {
        r=0;
        w=1;

    }
    else if (str=="n")
    {
        r=0;
        w=0;
    }
    else
    {
        r=1;
        w=1;
    }

    if (pipe(tube)==-1)
        return -1;
    if ((fils=fork())==-1) {
        close(tube[0]);
        close(tube[1]);
        return -1;
    }

    switch (fils) {
    case 0:

        ///child
        /*  close(tube[0]);

        			close(1);
        			dup(tube[1]);
        			close(tube[1]);
         /* close(0);
        			dup(tube[0]);
        			close(tube[0]);
         */
        if (r)
        {
            close(tube[0]);

            close(1);

            dup2(tube[1],1);
        }
        if (w)
        {
            close(tube3[0]);

            close(2);

            dup2(tube3[1],2);

            close(tube[1]);
            close(0);
            dup2(tube2[0],0);
//close(0);

            close(tube2[0]);
        }
        /*while(1)
        				{
        				write(tube[1],"i",1);
        				usleep(100000);

        		}*/

        execlp("/bin/sh","sh","-c",cmd.c_str(),NULL);
        exit(EXIT_FAILURE);
    default:
        pid=fils;
        ///father
        close(tube[1]);
        close(tube2[0]);
        close(tube3[1]);

        error=tube3[0];
        read=tube[0];
        write=tube2[1];
        /*	int mode;
        	mode = fcntl(error, F_GETFL, 0);
        	mode |= O_NONBLOCK;
        	fcntl(error, F_SETFL, O_NONBLOCK);
        	mode = fcntl(read, F_GETFL, 0);
        	mode |= O_NONBLOCK;
        	fcntl(read, F_SETFL, O_NONBLOCK);
        	mode = fcntl(write, F_GETFL, 0);
        	mode |= O_NONBLOCK;
        	fcntl(write, F_SETFL, O_NONBLOCK);*/
        //	fread = fdopen(read,"r");
//	fwrite = fdopen(write,"w");
        break;
    }

    /*
      	switch(fils) {
      	case 0:
      	close(tube[1]);
      	close(0);
      	dup(tube[0]);
      	close(tube[0]);
      	execlp("/bin/sh","sh","-c",commande,NULL);
      	exit(EXIT_FAILURE);
      	break;
      	default:
      	close(tube[0]);
      	fwrite = fdopen(tube[1],"w");
      	break;
      }
    */

    /* if (flux==NULL) {
       	kill(fils,SIGUSR1);
       }*/
    return 0;
}

int ProcessusOpen::get_pid()
{
    return pid;
}
FILE *ProcessusOpen::get_fread() {
    return fread;
}
FILE *ProcessusOpen::get_fwrite() {
    return fwrite;


}

/*
	int main()
	{
	ProcessusOpen *po;
	po= new ProcessusOpen("ssh localhost");

	po->run();
//printf(" c %c\n",fgetc(popen("ssh localhost","r")));
//sleep(3);
//write(po->write,"./tcp_server.arm\n",strlen("./tcp_server.arm\n"));
	write(po->write,"\n",strlen("\n"));
//fprintf(po->get_fwrite(),"./tcp_server.arm \n");
	while(1)
	{
	char c;
	read(po->read,&c,1);
	printf("%cx",c);

//sleep(1);
}
	delete po;
	return 0;
}*/