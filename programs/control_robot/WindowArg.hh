#ifndef _WINDOWARG_HH
#define _WINDOWARG_HH
#include <locale.h>
#include <sigc++/retype_return.h>
#include <gtkmm.h>
class Agent;
class WindowArg
{
public:
    WindowArg(Glib::RefPtr<Gtk::Builder> interface);
    ~WindowArg() {};
    static WindowArg  *getInstance(Glib::RefPtr<Gtk::Builder> interface);
    static WindowArg  *getInstance();
    void load(std::string filename);
    void setAgent(Agent *agent);
    Agent *linkedAgent;
    bool enter(GdkEventKey* event);

protected:
    Gtk::Label *label;
    Gtk::Entry *entry;
    static WindowArg *instance;
    Glib::RefPtr<Gtk::Builder> xml_interface;
    Gtk::Window *window;
};

#endif