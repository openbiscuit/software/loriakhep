#ifndef __ProcessusOpenHH
#define __ProcessusOpenHH
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <signal.h>
class ProcessusOpen
{
public:
    ProcessusOpen(std::string _cmd);
    ~ProcessusOpen();
    void kill_(int signal=SIGINT);
    int close_();
    int run(std::string str = "rw");
    FILE *get_fread();
    FILE *get_fwrite();
    int read;
    int get_pid();
    int write;
    int error;
    static void Clean();
    static std::vector<ProcessusOpen *> vec;
protected:
    std::string cmd;
    FILE *fread;
    FILE *fwrite;

    int pid;



};
#endif