#ifndef _Agent_HH
#define _Agent_HH
#include <iomanip>
#include <sstream>
#include <iostream>

#include <locale.h>
#include <sigc++/retype_return.h>
#include <gtkmm.h>
#include "remote_serveur.h"
#include <semaphore.h>
#include <pthread.h>
class AgentCheckButton;
class ModelColumnsBehaviour ;
class BehaviourTab;
class Behaviour;
class Agent
{
public:
    static bool make_before_upload;
    static bool upload_error;
    static pthread_mutex_t upload_error_mutex;
    sigc::connection conec;
    void uploadAndRun();
    void makeAndRun();
    static sem_t semaphore;
    Agent(Glib::ustring _name);
    void upload_sshkey();
    Agent(Glib::ustring _name,std::string host,int port,int type);
    static Agent *create(Glib::ustring _name,std::string host,int port,int type);
    pthread_mutex_t mutex;
    static int load(std::string filename);
    static int save_parameter(std::string filename);
    static int load_parameter(std::string filename);
    static Agent* find(std::string name);
    int Timer(int timernumber);
    static int Timer2(int timernumber);
    void run();
    static void runAll();
    void armWatchDog();
    static void armWatchDogAll();
    void stop();
    static void stopAll();
    void pause();
    static void pauseAll();
    void togglepause();
    static void togglepauseAll();
    void unpause();
    static void unpauseAll();
    static void uploadAll();
    int upload();
    static void makeAll();
    int make();
    void upload_thread();
    static void *upload_launcher(void *param);
    static void deleteAll();
    void ActiveButton(AgentCheckButton *checkbutton);
    ~Agent();
    void link(BehaviourTab *l);

    void setBehaviour(Behaviour *b);
    std::vector<AgentCheckButton*> buttonvector;
    void referencebutton(AgentCheckButton *but);
    static std::vector<Agent*> agentrefsvector ;

    void connexion_accepted();
    static void connexion_accepted_w(Agent *a);
    void deconnexion();
    static void deconnexion_w(Agent *a);
    int get_timewatch();
    int get_timeping();
    int get_changed();
    int get_type();
    int get_state_pause();
    bool get_on_upload();
    Glib::ustring get_name();
    Glib::ustring get_args();
    Glib::ustring get_watchsoft();
    Glib::ustring get_watchsoftarg();
    BehaviourTab *get_AgentBehaviour();
    int get_state();
    remoteserver* get_rs();
    Behaviour *get_behaviour();
    Glib::ustring get_hostname();

    void set_type(int value);
    void set_state_pause(int value);
    void set_name(Glib::ustring value);
    void set_args(Glib::ustring value);
    void set_watchsoft(Glib::ustring value);
    void set_watchsoftarg(Glib::ustring value);
    void set_AgentBehaviour(BehaviourTab *value);
    void set_state(int value);
    void set_rs(remoteserver *value);
    void set_behaviour(Behaviour *value);
    void set_hostname(Glib::ustring value);
    void set_on_upload(bool value);
    void set_timeping(int value);
    void set_timewatch(int value);
    void ActiveButton(Behaviour *_behaviour);
    bool need_to_run;
protected:
    void set_changed(int value);

    void readloop_thread();
    static void *readloop_launcher(void *param);
    bool on_upload;
    void lock();
    void unlock();
    int changed;
    //
    int type;
    int state_pause;

    Glib::ustring name;
    Glib::ustring args;
    Glib::ustring watchsoft;
    Glib::ustring watchsoftarg;
    int timeping;
    int watchtime;
    BehaviourTab *AgentBehaviour;
    int state;
    remoteserver *rs;

    Behaviour *behaviour;
    Glib::ustring hostname;
};


#endif