#ifndef _WINDOWSSHKEYHH
#define _WINDOWSSHKEYHH
#include <locale.h>
#include <sigc++/retype_return.h>
#include <gtkmm.h>
#include "AgentCheckButton.hh"
class Agent;
class WindowSshKey
{
public:

    ~WindowSshKey() {};
    static WindowSshKey  *getInstance(Glib::RefPtr<Gtk::Builder> interface);
    static WindowSshKey  *getInstance();
    void ok_click();
    void annuler_click();
    void link();
    void show();


protected:


    WindowSshKey() {};
    Gtk::VBox *vbox;
    static std::vector<AgentCheckButton *> vec;
    Glib::RefPtr<Gtk::Builder> xml_interface;
    Gtk::Window *window;
    WindowSshKey(Glib::RefPtr<Gtk::Builder> interface);
    /*	WindowSshKey(){};
    */
    static WindowSshKey *instance_;
    /*	Glib::RefPtr<Gtk::Builder> xml_interface;
    	Gtk::Window *window;*/
};

#endif