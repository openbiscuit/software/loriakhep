#include "AgentList.hh"
#include "Agent.hh"
#include "Behaviour.hh"
#include "BehaviourTab.hh"
#include "WindowArg.hh"
#include "WindowWatchdog.hh"
#include "SshTab.hh"

AgentList *AgentList::instance=NULL;
AgentList *AgentList::getInstance(Glib::RefPtr<Gtk::Builder> interface) {
    if (instance==NULL)
    {

        if (interface==NULL)
            return NULL;
        instance=new AgentList(interface);

    }

    return instance;

}
AgentList *AgentList::getInstance() {
    return instance;
}
Agent* AgentList::get_agent() {
    Glib::RefPtr<Gtk::TreeView::Selection> refSelection = t->get_selection();
    if (refSelection)
    {
        Gtk::TreeModel::iterator iter = refSelection->get_selected();
        if (iter)
        {
            Agent *a=(*iter)[m_Columns.m_agent_ptr];
            Glib::ustring s=a->get_name();//    Glib::ustring ss=(*iter)[m_Columns.m_col_path];


            return a;
            //Behaviour *b=(*iter)[m_Columns.m_behaviour_ptr];
//	printf("yooo\n");
            //new BehaviourTab(xml_interface,b);
        }
    }
    return NULL;
}
void AgentList::on_row_activated(const Gtk::TreeModel::Path& path, Gtk::TreeViewColumn* column)
{
    pthread_mutex_lock(&_mutex);
    Glib::RefPtr<Gtk::TreeView::Selection> refSelection = t->get_selection();
    if (refSelection)
    {
        Gtk::TreeModel::iterator iter = refSelection->get_selected();
        if (iter)
        {
            Agent *a=(*iter)[m_Columns.m_agent_ptr];
            Glib::ustring s=a->get_name();//    Glib::ustring ss=(*iter)[m_Columns.m_col_path];

            std::cout << "  Selected =" << s << " \n" << std::endl;
            /*char *cs=(char*)malloc(sizeof(char)*10);
            cs[0]='1';
            cs[1]='0';
            cs[2]='\0';
            printf("%s\n",cs);
            std::string us((const char*)cs);
            free(cs);
            std::cout << "str::" << us << "\n";*/
            //Behaviour *b=(*iter)[m_Columns.m_behaviour_ptr];
//	printf("yooo\n");
            //new BehaviourTab(xml_interface,b);
        }
    }
    pthread_mutex_unlock(&_mutex);

}
bool AgentList::menuWatchdogReleased(GdkEventButton *event) {
    WindowWatchdog *waf = WindowWatchdog::getInstance();
    if (event->button==1)
    {
        pthread_mutex_lock(&_mutex);
        Glib::RefPtr<Gtk::TreeView::Selection> refSelection = t->get_selection();
        if (refSelection)
        {
            Gtk::TreeModel::iterator iter = refSelection->get_selected();
            if (iter)
            {
                Agent *a=(*iter)[m_Columns.m_agent_ptr];
                /*pthread_mutex_unlock(&_mutex);
                			remove(a);
                			BehaviourTab::removeAgentAll(a);
                //delete a;
                pthread_mutex_lock(&_mutex);*/
                waf->setAgent(a);
//delete a;
            }
        }


        pthread_mutex_unlock(&_mutex);
    }
}
bool AgentList::menuReleased(GdkEventButton *event) {
    WindowArg *warg = WindowArg::getInstance();

    if (event->button==1)
    {
        pthread_mutex_lock(&_mutex);
        Glib::RefPtr<Gtk::TreeView::Selection> refSelection = t->get_selection();
        if (refSelection)
        {
            Gtk::TreeModel::iterator iter = refSelection->get_selected();
            if (iter)
            {
                Agent *a=(*iter)[m_Columns.m_agent_ptr];
                warg->setAgent(a);
            }
        }
        pthread_mutex_unlock(&_mutex);

    }
}
bool AgentList::pressed(GdkEventButton *event) {

    Gtk::Menu *m;
    Gtk::Window *w;

    if (event->button==3)
    {
        xml_interface->get_widget("menuarg",m);
        m->popup(event->button, event->time);
//  xml_interface->get_widget("window_arg",w);


//w->show();
//return true;

    }
    return false;



}
bool AgentList::sshtab(GdkEventButton *event)
{

    Gtk::Menu *m;
    Gtk::Window *w;

    if (event->button==1)
    {
        pthread_mutex_lock(&_mutex);
        Glib::RefPtr<Gtk::TreeView::Selection> refSelection = t->get_selection();
        if (refSelection)
        {
            Gtk::TreeModel::iterator iter = refSelection->get_selected();
            if (iter)
            {
                Agent *a=(*iter)[m_Columns.m_agent_ptr];
                /*pthread_mutex_unlock(&_mutex);
                			remove(a);
                			BehaviourTab::removeAgentAll(a);
                //delete a;
                pthread_mutex_lock(&_mutex);*/
                SshTab *t = new SshTab(xml_interface,a);
//delete a;
            }
        }


        pthread_mutex_unlock(&_mutex);
    }
    return false;



}
AgentList::AgentList(Glib::RefPtr<Gtk::Builder> interface)
{

//t->signal_row_activated().connect(sigc::mem_fun(*this,&AgentList::on_row_activated),false);
    pthread_mutex_init(&_mutex,NULL);
    Gtk::MenuItem *mi;
    Gtk::MenuItem *watchmi;
    Gtk::MenuItem *sshitem;
    xml_interface = interface;
    liste = Gtk::ListStore::create(m_Columns);
    interface->get_widget("treeview_agent", t);
    interface->get_widget("menuitemarg",mi);
    interface->get_widget("watchdogitem",watchmi);
    interface->get_widget("itemssh",sshitem);
    t->set_model(liste);

//t->append_column("ID", m_Columns.m_col_id);

    t->append_column("Agent", m_Columns.m_col_name);
    t->append_column("Comportement",m_Columns.m_col_behaviour);
    t->append_column("Etat",m_Columns.m_col_state);
    t->append_column("Arguments",m_Columns.m_col_arg);
    t->signal_row_activated().connect(sigc::mem_fun(*this,&AgentList::on_row_activated),true);
    t->signal_button_press_event().connect(sigc::mem_fun(*this,&AgentList::pressed),false);
    mi->signal_button_release_event().connect(sigc::mem_fun(*this,&AgentList::menuReleased),false);
    watchmi->signal_button_release_event().connect(sigc::mem_fun(*this,&AgentList::menuWatchdogReleased),false);
    sshitem->signal_button_release_event().connect(sigc::mem_fun(*this,&AgentList::sshtab),false);
}
void AgentList::remove(Agent *agt)
{
    pthread_mutex_lock(&_mutex);
    Gtk::TreeModel::iterator begin = liste->erase(liste->prepend());
    Gtk::TreeModel::iterator end=liste->erase(liste->append());
    Gtk::TreeModel::iterator iter;
    for (iter=begin; iter!=end; iter++)
    {
        Agent *a=(*iter)[m_Columns.m_agent_ptr];
        std::cout << a->get_name() << "\n";
        if (a==agt)
        {

            liste->erase(iter);
//return;
        }
    }
    t->queue_draw();
    pthread_mutex_unlock(&_mutex);

}
void AgentList::add(Agent *agt) {
//pthread_mutex_lock(&_mutex);

    Gtk::TreeModel::Row row = *(liste->append());


    row[m_Columns.m_agent_ptr]=agt;
    //row[m_Columns.m_col_path]=b->patharm;
//std::cout << "name :" << agt->get_name() << "\n" << std::endl;
    row[m_Columns.m_col_name] = agt->get_name();
    switch (agt->get_state())
    {
    case 0:
        row[m_Columns.m_col_state]="En attente de connexion";
        break;
    case 1:
        row[m_Columns.m_col_state]="Connecté//En attente d'ordre";
        break;
    }
    t->queue_draw();
//pthread_mutex_unlock(&_mutex);
}

void AgentList::update(Agent *agt) {
    Gtk::TreeModel::Row row;
    pthread_mutex_lock(&_mutex);
//printf("update\n");
    Gtk::TreeModel::iterator begin = liste->prepend();
    begin = liste->erase(begin);
    Gtk::TreeModel::iterator end = liste->append();
    end = liste->erase(end);
//printf("update..\n");
//Gtk::TreeModel::iterator end=liste->erase(liste->append());
    Gtk::TreeModel::iterator iter;
    if (!liste->iter_is_valid(begin))
    {
        printf("on empeche le segfault\n");
        exit(-1);

    }
    for (iter=begin; iter!=end; iter++)
    {

        Agent *a=(*iter)[m_Columns.m_agent_ptr];
        if (a==agt)
        {
            row=(*iter);
        }

    }

    row[m_Columns.m_agent_ptr]=agt;
    //row[m_Columns.m_col_path]=b->patharm;

    row[m_Columns.m_col_name] = agt->get_name();
    row[m_Columns.m_col_arg] = agt->get_args();
    if (agt->get_behaviour())
    {
        row[m_Columns.m_col_behaviour]=agt->get_behaviour()->name;
    }
    else
    {
        row[m_Columns.m_col_behaviour]="";
    }
    if (agt->get_on_upload())
    {
        row[m_Columns.m_col_state]="upload du comportement...";
    }
    else
    {
        switch (agt->get_state())
        {
        case 0:
            row[m_Columns.m_col_state]="En attente de connexion";
            break;
        case 1:
            row[m_Columns.m_col_state]="Connecté//En attente d'ordre";
            break;
        }
    }
    t->queue_draw();
//printf("end update\n");
    pthread_mutex_unlock(&_mutex);
}