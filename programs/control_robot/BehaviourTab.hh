#ifndef _BehaviourTab_HH
#define _BehaviourTab_HH
#include <iomanip>
#include <sstream>
#include <iostream>

#include <locale.h>
#include <sigc++/retype_return.h>
#include <gtkmm.h>
class Agent;
class Behaviour;
class BehaviourTab
{

public:
    //ModelColumnsBehaviour m_Columns;
    BehaviourTab(Glib::RefPtr<Gtk::Builder> interface,Behaviour *b);
    ~BehaviourTab();
    void remove();
    void hide();
    Glib::ustring _name;
    void add(std::string s,std::string s2);
    Gtk::Notebook *notebook;
    Gtk::VBox *vbox;
    Gtk::CheckButton *checkbutton[64];
    int current_checkbutton;
    int n_page;
    static void addAgentAll(Agent *a);
    void addAgent(Agent *agent);
    void removeAgent(Agent *agent);
    static void removeAgentAll(Agent *agent);
    bool on_label_release_event(GdkEventButton* event);
    bool on_my_enter_notify_event(GdkEventCrossing* event);
    bool	on_my_enter_motion_event(GdkEventMotion* event);
protected:
    Behaviour *behaviour;
    Glib::RefPtr<Gtk::Builder> xml_interface;
    static std::vector<BehaviourTab*> BTV ;
    std::vector<Agent*> agentvector;
    static int current_tab;
};

//void BehaviourTab::addAgent(Agent *a);


#endif