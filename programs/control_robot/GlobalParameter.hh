#ifndef _GLOBALPARAMETER_HH
#define _GLOBALPARAMETER_HH
#include <locale.h>
#include <sigc++/retype_return.h>
#include <gtkmm.h>
#include <string>

class GlobalParameter
{
public:
    GlobalParameter(Glib::RefPtr<Gtk::Builder> interface);
    ~GlobalParameter() {};
    static GlobalParameter  *getInstance(Glib::RefPtr<Gtk::Builder> interface);
    static GlobalParameter  *getInstance();
    std::string get_binary_tcp_server_path() {
        return binary_tcp_server_path;
    };
    std::string get_remote_binary_path() {
        return remote_binary_path;
    };
    std::string get_remote_tcp_server_path() {
        return remote_tcp_server_path;
    };

    void show();
    char ___homedir[256];
protected:
    void load();
    void save();
    void ok_click();
    void cancel_click();
    static GlobalParameter *instance;
    Glib::RefPtr<Gtk::Builder> xml_interface;
    Gtk::Window *window;
    Gtk::Entry *entry_tcp_server_path;
    Gtk::Entry *entry_remote_binary_path;

    Gtk::Entry *entry_remote_tcp_server_path;
    std::string binary_tcp_server_path;
    std::string remote_binary_path;
    std::string remote_tcp_server_path;
};

#endif