
#include "SshTab.hh"
#include <string.h>
std::vector<SshTab *> SshTab::vec;
int SshTab::Timer(int timernumber)
{
    pthread_mutex_lock(&mutex);
    if (change)
    {
        std::string tmp = buff;

        textbuffer->set_text(tmp);
        //textbuffer+=buff;
        t->set_buffer(textbuffer);
        Gtk::TextBuffer::iterator it = textbuffer->end();

        //	it.forward_to_line_end()

        t->scroll_to(textbuffer->get_insert(),0.0);
        change=0;
        //adj->set_value(adj->get_lower());
    }
    pthread_mutex_unlock(&mutex);
    return true;
}
void *SshTab::launcher(void *param) {
    SshTab *ob = (SshTab*)param;
    ob->thread();
}
void SshTab::thread()
{
    char buffer[128];
    char buffer2[128];
    char tmp[128];
    char c;
    FILE *fifo;
    int n;
    int n_open;

    switch (agent->get_type())
    {
    case 0:

        sprintf(tmp,"ssh root@%s",agent->get_hostname().c_str());
        sprintf(tmp,"%s \"./tcp_server.arm\"",tmp);


        break;
    case 1:
        sprintf(tmp,"ssh root@%s",agent->get_hostname().c_str());
        sprintf(tmp,"%s \"./tcp_server.armgs\"",tmp);
        break;

    default:
        sprintf(tmp,"ssh %s",agent->get_hostname().c_str());
        sprintf(tmp,"%s \"./tcp_server.i686\"",tmp);
        break;


    }
    std::string s(tmp);
    po=new ProcessusOpen(s);
    po->run();
// 	//write(po->write,"echo pouet\n",strlen("echo pouet\n"));
    fd_set open;
    FD_ZERO(&open);
    FD_SET(po->read,&open);
    FD_SET(po->error,&open);
    n_open=2;
    int max;
    if (po->read>po->error)
        max=po->read;
    else
        max=po->error;

    while (n_open>0)
    {
        fd_set ready;
        ready=open;
        if (select(max+1,&ready,NULL,NULL,NULL)<0)
            printf("select error\n");


        if (FD_ISSET(po->read,&ready))
        {
            n=read(po->read,buffer,127);
            if (n>0)
            {
                buffer[n]='\0';
                std::string s(buffer);
                pthread_mutex_lock(&mutex);
                buff+=s;
                change=1;
                pthread_mutex_unlock(&mutex);
            }
            else if (n==0)
            {
                close(po->read);
                n_open--;
                FD_CLR(po->read,&open);

            }


        }


        if (FD_ISSET(po->error,&ready))
        {
            n=read(po->error,buffer2,127);
            if (n>0)
            {
                buffer2[n]='\0';
                std::string s(buffer2);
                pthread_mutex_lock(&mutex);
                buff+=s;
                change=1;
                pthread_mutex_unlock(&mutex);
            }
            else if (n==0)
            {
                close(po->error);
                n_open--;
                FD_CLR(po->error,&open);

            }


        }
        usleep(10000);
    }

}


SshTab::SshTab(Glib::RefPtr<Gtk::Builder> interface,Agent *a) {
    change=0;
    xml_interface=interface;
    agent=a;
    interface->get_widget("notebookssh", notebook);
    pthread_mutex_init(&mutex,NULL);
    wind = new Gtk::ScrolledWindow();
    t= new Gtk::TextView();
    wind->add(*t);
    textbuffer = Gtk::TextBuffer::create();
    textbuffer->set_text(a->get_name());//write(po->write,"./tcp_server.arm//write(po->write,"./tcp_server.arm\n",strlen("./tcp_server.arm\n")+1);\n",strlen("./tcp_server.arm\n")+1);
    t->set_buffer(textbuffer);
//notebook->add(*t);
    Gtk::Label *la = new Gtk::Label(a->get_name());
    notebook->append_page(*wind,*la);
    t->show();
    wind->show();
//adj = new Gtk::Adjustment();
//wind->set_vadjustment(adj);



    pthread_t t;
    pthread_create(&t,NULL,SshTab::launcher,this);

    sigc::slot<bool> my_slot =sigc::bind ( sigc::mem_fun ( *this,
                                           &SshTab::Timer ), 1 );

    conec = Glib::signal_timeout().connect ( my_slot,100);
    vec.push_back(this);
}
void SshTab::Clean()
{


    for (std::vector<SshTab*>::iterator it=SshTab::vec.begin(); it!=SshTab::vec.end(); it++)
    {
        (*it)->del();
    }
}
void SshTab::del()
{



    printf("kill ssh\n");
    ProcessusOpen *p=new ProcessusOpen("ssh root@192.168.0.1 \"killall -SIGINT tcp_server.armgs\"");
    p->run();
    p->close_();
    delete p;
    po->kill_();
    delete po;
}
SshTab::~SshTab()
{

}