#include "WindowWatchdog.hh"
#include "Agent.hh"
#include <gtkmm.h>
WindowWatchdog *WindowWatchdog::instance=NULL;
WindowWatchdog *WindowWatchdog::getInstance(Glib::RefPtr<Gtk::Builder> interface) {
    if (instance==NULL)
    {

        if (interface==NULL)
            return NULL;
        instance=new WindowWatchdog(interface);

    }

    return instance;

}
WindowWatchdog *WindowWatchdog::getInstance() {
    return instance;
}
bool WindowWatchdog::enter(GdkEventKey* event) {
    printf("keyval : %d\n",event->keyval);
    if ((event->keyval==65421) || (event->keyval==65293))
    {

//linkedAgent->set_args(entry->get_text());
        Glib::ustring s = entrypingtime->get_text();
        linkedAgent->set_timeping(atoi(s.c_str()));
        s=entrywatchtime->get_text();
        linkedAgent->set_timewatch(atoi(s.c_str()));
        linkedAgent->set_watchsoft(entrywatchsoft->get_text());
        linkedAgent->set_watchsoftarg(entrywatcharg->get_text());
//entry->set_text("");
        window->hide();

    }

}
void WindowWatchdog::setAgent(Agent *agent) {
    linkedAgent=agent;
    /*std::string tmp = "arguments " + linkedAgent->get_name() + " :";
    	label->set_text(tmp);
    	entry->set_text(linkedAgent->get_args());
    */
    int toto=100;
    std::ostringstream oss,oss2;
    oss << linkedAgent->get_timeping();
    oss.clear();

    entrypingtime->set_text(oss.str());
    oss2 << linkedAgent->get_timewatch();
    entrywatchtime->set_text(oss2.str());
    entrywatchsoft->set_text(linkedAgent->get_watchsoft());
    entrywatcharg->set_text(linkedAgent->get_watchsoftarg());
    window->show();
}
WindowWatchdog::WindowWatchdog(Glib::RefPtr<Gtk::Builder> interface)
{

    xml_interface = interface;
    xml_interface->get_widget("window_watchdog",window);
    xml_interface->get_widget("watchdogentryping",entrypingtime);
    xml_interface->get_widget("watchdogentrywatchtime",entrywatchtime);
    xml_interface->get_widget("watchdogentrywatchsoft",entrywatchsoft);
    xml_interface->get_widget("watchdogentrywatcharg",entrywatcharg);

//xml_interface->get_widget("label_arg",label);
    entrypingtime->signal_key_release_event().connect(sigc::mem_fun(*this,&WindowWatchdog::enter),false);
    entrywatchtime->signal_key_release_event().connect(sigc::mem_fun(*this,&WindowWatchdog::enter),false);
    entrywatchsoft->signal_key_release_event().connect(sigc::mem_fun(*this,&WindowWatchdog::enter),false);
    entrywatcharg->signal_key_release_event().connect(sigc::mem_fun(*this,&WindowWatchdog::enter),false);
}