#include "WindowSshKey.hh"
#include "Agent.hh"
WindowSshKey *WindowSshKey::instance_=NULL;
std::vector<AgentCheckButton *> WindowSshKey::vec;
void WindowSshKey::ok_click() {


    for (std::vector<AgentCheckButton*>::iterator it=vec.begin(); (it!=vec.end()); it++)
    {
        AgentCheckButton*acb=(*it);
        if (acb->get_active())
        {
            //std::cout << acb->agent->get_name() << std::endl;
            acb->agent->upload_sshkey();
        }

    }
    window->hide();
}
void WindowSshKey::annuler_click() {
    window->hide();

}
WindowSshKey::WindowSshKey(Glib::RefPtr<Gtk::Builder> interface) {
    xml_interface = interface;
    link();
    for (std::vector<Agent*>::iterator it=Agent::agentrefsvector.begin(); (it!=Agent::agentrefsvector.end()); it++)
    {
        Agent *a=(*it);
        AgentCheckButton *acb=new AgentCheckButton(a);
        vec.push_back(acb);
        vbox->add(*acb);
        acb->show();

    }
    window->set_title("Upload de la clé ssh");
};

void WindowSshKey::show()
{

    window->show();

}
void WindowSshKey::link()
{
    xml_interface->get_widget("vbox2_ssh_key",vbox);
    xml_interface->get_widget("window_sshkey",window);
    Gtk::Button *b;
    xml_interface->get_widget("button_ok_sshkey",b);
    b->signal_clicked().connect(sigc::mem_fun(*this,&WindowSshKey::ok_click),false);
    xml_interface->get_widget("button_annuler_sshkey",b);
    b->signal_clicked().connect(sigc::mem_fun(*this,&WindowSshKey::annuler_click),false);
}

WindowSshKey *WindowSshKey::getInstance(Glib::RefPtr<Gtk::Builder> interface) {
    if (instance_==NULL)
    {

        if (interface==NULL)
            return NULL;
        instance_=new WindowSshKey(interface);

    }

    return instance_;

}
WindowSshKey *WindowSshKey::getInstance() {
    return instance_;
}