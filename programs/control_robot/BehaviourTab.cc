#include "AgentCheckButton.hh"
#include "Agent.hh"
#include "BehaviourTab.hh"
#include "Behaviour.hh"
std::vector<BehaviourTab*> BehaviourTab::BTV ;
int BehaviourTab::current_tab=0;
void BehaviourTab::addAgentAll(Agent *a)
{
    for (std::vector<BehaviourTab*>::iterator it = BTV.begin(); it!=BTV.end(); it++)
    {

        (*it)->addAgent(a);
    }

}
BehaviourTab::~BehaviourTab()
{

}
void BehaviourTab::remove() {

    for (std::vector<BehaviourTab*>::iterator it = BTV.begin(); it!=BTV.end(); it++)
    {

        if ((*it)->behaviour == behaviour)
        {
            notebook->remove(*vbox);
            //vbox->hide();
            BTV.erase(it);
            delete this;

            return;
        }
    }
}
void BehaviourTab::hide() {

    for (std::vector<BehaviourTab*>::iterator it = BTV.begin(); it!=BTV.end(); it++)
    {

        if ((*it)->behaviour == behaviour)
        {
            //notebook->remove(*vbox);
            vbox->hide();
            //BTV.erase(it);
            //	delete this;

            return;
        }
    }
}
bool	BehaviourTab::on_label_release_event(GdkEventButton* event) {
    printf("%d\n",event->button);

}

bool	BehaviourTab::on_my_enter_notify_event(GdkEventCrossing* event) {
    printf("enter\n");
    return true;
}

bool	BehaviourTab::on_my_enter_motion_event(GdkEventMotion* event) {
    printf("enter\n");
    return true;
}



BehaviourTab::BehaviourTab(Glib::RefPtr<Gtk::Builder> interface,Behaviour *b) {
    xml_interface=interface;
    interface->get_widget("notebook1", notebook);
    for (std::vector<BehaviourTab*>::iterator it = BTV.begin(); it!=BTV.end(); it++)
    {
        //std::cout << (*it)->behaviour->name << "\n" << std::endl;
        if ((*it)->behaviour->name == b->name)
        {
            //	printf("%d\n",(*it)->n_page);
            (*it)->vbox->show();
            notebook->set_current_page((*it)->n_page);
            delete this;

            return;
        }
    }
    behaviour=b;
    b->bt=this;
    //_name = b->name;

    current_checkbutton=0;
    vbox = new Gtk::VBox::VBox(false,0);
    vbox->show();
    Gtk::Label lab;
    Gtk::Label *la;
    Gtk::Button *button = new Gtk::Button::Button();
    notebook->append_page(*vbox);
    notebook->set_tab_label_text(*vbox,b->name);
    la=(Gtk::Label*)notebook->get_tab_label(*vbox);


//	la=notebook->get_tab_label(lab);

    notebook->signal_button_release_event().connect(sigc::mem_fun(*this,&BehaviourTab::on_label_release_event),true);
    button->signal_clicked().connect(sigc::mem_fun(*this,&BehaviourTab::hide),true);
    n_page = notebook->get_n_pages()-1;
    notebook->set_current_page(n_page);
    button->set_label("X");
    vbox->pack_start(*button,false,false);
    button->show();
    //add("tous les robots","null");
    for (std::vector<Agent*>::iterator it=Agent::agentrefsvector.begin(); it!=Agent::agentrefsvector.end(); it++)
    {
        this->addAgent((*it));
    }
    BTV.push_back(this);

}
void BehaviourTab::add(std::string s,std::string s2)
{
    checkbutton[current_checkbutton]= new Gtk::CheckButton::CheckButton(s,false);

    vbox->pack_start(*(checkbutton[current_checkbutton]));
    checkbutton[current_checkbutton]->show();

}

void BehaviourTab::addAgent(Agent *agent) {
    bool ok=true;
//agent->lock();
    for (std::vector<Agent*>::iterator it = agentvector.begin(); it!=agentvector.end(); it++)
    {
        if ((*it)==agent)
            ok=false;

    }
    if (ok)
    {
        agentvector.push_back(agent);
        AgentCheckButton *c = new AgentCheckButton(agent,this->behaviour);
        vbox->pack_start(*c);
        c->show();
    }
//agent->unlock();
}
void BehaviourTab::removeAgent(Agent *agent) {
    bool finish =false;
    for (std::vector<Agent*>::iterator it = agentvector.begin(); ((it!=agentvector.end()) && !finish); it++)
    {
        if ((*it)==agent) {
            agentvector.erase(it);
            finish = true;
        }
    }

}

void BehaviourTab::removeAgentAll(Agent *agent)
{
    for (std::vector<BehaviourTab*>::iterator it = BTV.begin(); it!=BTV.end(); it++)
    {

        (*it)->removeAgent(agent);
    }


}