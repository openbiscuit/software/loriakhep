#include "Behaviour.hh"
#include <iostream>
#include "remote_serveur.h"
#include "ErrorBox.hh"
#include "BehaviourTab.hh"
int Behaviour::make(int type) {

    int res;

    pthread_mutex_lock(&inmakemutex);

    std::string execute;
    std::string path;
    char buffer[256];
    int pos;
    if (!inmake)
    {
        inmake=true;
        pthread_mutex_unlock(&inmakemutex);
///make

        switch (type)
        {
        case 0:
            pos = patharm.rfind("/");
            execute = patharm.substr(pos+1,patharm.size()-pos-1);
            path = patharm.substr(0,pos);
            break;
        case 1:
            pos = patharmgs.rfind("/");
            execute = patharmgs.substr(pos+1,patharmgs.size()-pos-1);
            path = patharmgs.substr(0,pos);

            break;
        default:

            break;
        }

        sprintf(buffer,"cd %s && make %s",path.c_str(),execute.c_str());
        if (pclose(popen(buffer,"w")))
        {
            pthread_mutex_lock(&inmakemutex);
            inmake=false;
            pthread_mutex_unlock(&inmakemutex);
            std::string error;
            error = "YOUR PROGRAM " + execute + " DOES NOT COMPILE TRY AGAIN";
            new ErrorBox(error);
            return -1;

        }

        pthread_mutex_lock(&inmakemutex);
        inmake=false;
        pthread_mutex_unlock(&inmakemutex);
    }
    else
    {
        pthread_mutex_unlock(&inmakemutex);
    }
    return 0;
}


void Behaviour::closeBehaviourTab()
{
    if (bt)
    {
        bt->remove();

    }
}
Behaviour::Behaviour(std::string behaviourpath)
{
    bt=NULL;
    std::string s;
    bool i686app=false;
    std::string tmp=behaviourpath;
    int n=tmp.rfind(".armgs");
    int n2=tmp.rfind(".arm");
    if ((n<0) && (n2<0))
    {
//no extension -> x86 app
        i686app=true;
        pathi686=tmp;
    }
//	inmake = false;
    if (n!=-1)
    {
        tmp.replace(n,6,".arm");

    }
    int i=tmp.length()-1;
    while (i>0 && tmp[i]!='/')
    {
        i--;
    };
    s = tmp;
    if (i>0)
        s.erase(0,i+1);

    this->patharm=tmp;
    n=tmp.rfind(".arm");
    if (n!=-1)
    {
        tmp.replace(n,4,".armgs");

    }
    patharmgs = tmp;


    this->name = s;
    if (i686app)
    {
        this->namei686=name;
        tmp=namei686;
        int z = tmp.rfind(".i686");
        if (z>0)
        {
            tmp.replace(z,5,"");
        }
        namearm = tmp + ".arm";
        namearmgs = tmp + ".armgs";
    }
    else
    {
        namearm = name;
        namearmgs=name + "gs";
        tmp=namearm;
        int z = tmp.rfind(".arm");
        if (z>0)
        {
            tmp.replace(z,4,".i686");
        }
        namei686 = tmp;
    }
    orignalpath = behaviourpath;
    pthread_mutex_init(&inmakemutex,NULL);
}