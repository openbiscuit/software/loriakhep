#include "ErrorBox.hh"
void ErrorBox::click()
{
    delete w;
    delete this;

}


ErrorBox::ErrorBox(std::string Err)
{

    Glib::RefPtr<Gtk::Builder> xml_interface = Gtk::Builder::create_from_file("window_error.xml");

    Gtk::Label *l;
    Gtk::Button *b;
    xml_interface->get_widget("label1",l);
    xml_interface->get_widget("window_error", w);
    xml_interface->get_widget("okbutton", b);
    l->set_text(Err);
    b->signal_clicked().connect(sigc::mem_fun(*this,&ErrorBox::click),false);
    w->show();
}