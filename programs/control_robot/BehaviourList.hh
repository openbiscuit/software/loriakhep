#ifndef _BehaviourList_HH
#define _BehaviourList_HH
/*#include <iomanip>
#include <sstream>
#include <iostream>
#include <string>*/
#include <locale.h>
#include <sigc++/retype_return.h>
#include <gtkmm.h>

class Behaviour;
class ModelColumnsBehaviour : public Gtk::TreeModel::ColumnRecord
{
public:

    ModelColumnsBehaviour()
    {
        add(m_col_name);
        add(m_col_path);
        add(m_behaviour_ptr);
    }

    Gtk::TreeModelColumn<Glib::ustring> m_col_name;
    Gtk::TreeModelColumn<Glib::ustring> m_col_path;
    Gtk::TreeModelColumn<Behaviour*> m_behaviour_ptr;

};
class BehaviourList
{
public:
    ModelColumnsBehaviour m_Columns;
    BehaviourList(Glib::RefPtr<Gtk::Builder> interface);
    ~BehaviourList() {};
    void removeSelected();
    void add(std::string s);
    void on_row_activated(const Gtk::TreeModel::Path& path, Gtk::TreeViewColumn* column);
    static BehaviourList  *getInstance(Glib::RefPtr<Gtk::Builder> interface);
    static BehaviourList  *getInstance();
    void load(std::string filename);
    std::string attached_filename;
    bool filename_is_attached;
    Behaviour *find(std::string origpath);
    void save(std::string);
protected:
    static BehaviourList *instance;
    Gtk::TreeView *t;
    Glib::RefPtr<Gtk::ListStore> liste;
//Gtk::ListStore list;
    Glib::RefPtr<Gtk::Builder> xml_interface;
};



#endif