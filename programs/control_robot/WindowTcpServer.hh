#ifndef _WINDOWTCPSERVERHH
#define _WINDOWTCPSERVERHH
#include <locale.h>
#include <sigc++/retype_return.h>
#include "AgentCheckButton.hh"
#include <gtkmm.h>
class Agent;
class WindowTcpServer
{
public:

    ~WindowTcpServer() {};
    static WindowTcpServer  *getInstance(Glib::RefPtr<Gtk::Builder> interface);
    static WindowTcpServer  *getInstance();
    void ok_click();
    void annuler_click();
    void link();
    void show();

protected:
    WindowTcpServer(Glib::RefPtr<Gtk::Builder> interface);
    WindowTcpServer() {};
    static WindowTcpServer *instance_;
    Gtk::VBox *vbox;
    static std::vector<AgentCheckButton *> vec;
    Glib::RefPtr<Gtk::Builder> xml_interface;
    Gtk::Window *window;


    /*	Glib::RefPtr<Gtk::Builder> xml_interface;
    	Gtk::Window *window;*/
};

#endif