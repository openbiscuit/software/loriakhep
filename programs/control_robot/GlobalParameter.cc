#include "GlobalParameter.hh"
#include "Agent.hh"
#include <gtkmm.h>
#include <stdio.h>
#include <sys/file.h>
#include <fstream>
#include <iostream>

GlobalParameter *GlobalParameter::instance=NULL;
GlobalParameter *GlobalParameter::getInstance(Glib::RefPtr<Gtk::Builder> interface) {
    if (instance==NULL)
    {

        if (interface==NULL)
            return NULL;
        instance=new GlobalParameter(interface);

    }

    return instance;

}
GlobalParameter *GlobalParameter::getInstance() {
    return instance;
}
void GlobalParameter::ok_click()
{
    remote_binary_path = entry_remote_binary_path->get_text();
    remote_tcp_server_path = entry_remote_tcp_server_path->get_text();
    binary_tcp_server_path= entry_tcp_server_path->get_text();
    save();
    window->hide();
}
void GlobalParameter::cancel_click()
{
    window->hide();
}
void GlobalParameter::show()
{
    window->show();

}

void GlobalParameter::save()
{

    FILE *f;
    char tmp[512];
    sprintf(tmp,"%s/.control_robot/.globalparameter",___homedir);
    f=fopen(tmp,"w");
    fprintf(f,"%s\n%s\n%s\n",remote_binary_path.c_str(),remote_tcp_server_path.c_str(),binary_tcp_server_path.c_str());
    fclose(f);

}
void GlobalParameter::load()
{
    char buffer[256];
    char tmp[512];
    sprintf(tmp,"%s/.control_robot/.globalparameter",___homedir);

    std::fstream fi(tmp);
    if (fi)
    {
        std::string ligne;
        std::getline(fi,ligne);
        remote_binary_path=ligne;
        std::getline(fi,ligne);
        remote_tcp_server_path=ligne;
        std::getline(fi,ligne);
        binary_tcp_server_path=ligne;

    }
    else
    {
        remote_binary_path ="/tmp";
        remote_tcp_server_path="~/";
        binary_tcp_server_path="../../tcp_connection";
        window->show();
    }
    entry_remote_binary_path->set_text(remote_binary_path);
    entry_remote_tcp_server_path->set_text(remote_tcp_server_path);
    entry_tcp_server_path->set_text(binary_tcp_server_path);
}

GlobalParameter::GlobalParameter(Glib::RefPtr<Gtk::Builder> interface)
{
    int i;
    FILE *popfile;
    popfile=popen("echo ~","r");
    i=0;
    while (!feof(popfile))
    {
        fscanf(popfile,"%c",&___homedir[i]);
        i++;
    }
    ___homedir[i-2]='\0';
    pclose(popfile);
    Gtk::Button *b,*b2;
    xml_interface = interface;
    xml_interface->get_widget("window_global_parameter",window);
    xml_interface->get_widget("entry_distant_binary_parameter",entry_remote_binary_path);
    xml_interface->get_widget("entry_tcp_server_distant_parameter",entry_remote_tcp_server_path);
    xml_interface->get_widget("entry_binary_tcp_server_parameter",entry_tcp_server_path);
    xml_interface->get_widget("button_parameter_ok",b);
    xml_interface->get_widget("button_parameter_cancel",b2);
    b->signal_clicked().connect(sigc::mem_fun(*this,&GlobalParameter::ok_click),false);
    b2->signal_clicked().connect(sigc::mem_fun(*this,&GlobalParameter::cancel_click),false);
    load();
}