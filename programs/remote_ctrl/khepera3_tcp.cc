//#ifndef _GNU_SOURCE
//#define _GNU_SOURCE
//#endif

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <fcntl.h>
#include <iostream>
#include <ctime>
#include <time.h>
#include "nmea.h"
#include <locale.h>
#include "khepera3_tcp.h"

#define BUFFER_SIZE 256

static void* read_loop_wraper(void *pdata);


char*
replace(char *buf,char in,char out)
{
    unsigned int i;
    char *result = (char*)malloc(sizeof(char)*(strlen(buf)+1));
    for (i=0;i<strlen(buf);i++)if (buf[i]==in)result[i]=out;
        else result[i]=buf[i];
    buf[i]='\0';
    return result;
}

double
super_atof(char * str)
{
    char*tmp;
    double result;
    if (*(localeconv()->decimal_point)==',') // on regarde si  le séparateur décimal est le point où la virgule (mode anglais ou francais) et dans le cas où c'est la virgule, on remplace dans notre chaine les point par des ,
    {
        tmp =replace(str,'.',',');
    }
    else
    {
        tmp =replace(str,',','.');
    }
    result = atof(tmp);
    free(tmp);
    return result;
}

#define atof super_atof

// ==========================================================================
static void *
read_loop_wraper(void *pdata)
{

    Khepera3_TCP *k = (Khepera3_TCP*) pdata;
    k->read_loop(NULL);
    return NULL;
}
// ==========================================================================


//struct sNMEAParser _client_snd_nmea_parser;
//struct sNMEAParser _client_rcv_nmea_parser;
//int _connect_filehandle;
//char _msg_buffer[256];

// ==========================================================================
// == private methods =======================================================
// ==========================================================================

int
Khepera3_TCP::tcp_connect( const char *address, int port, int *connect_filehandle)
{
    struct sockaddr_in serv_address;
    struct hostent *serv_host;
    int connect_res;

    // Don't touch anything if the port is zero
    if (port == 0) {
        return 0;
    }

    // Look for host
    serv_host = NULL;
    serv_host = gethostbyname(address);
    if ( serv_host == NULL ) {
        fprintf(stderr, "Unable to get server host.");
        return 0;
    }

    // Create Socket handle
    *connect_filehandle = socket( AF_INET, SOCK_STREAM, 0);
    if (*connect_filehandle < 0) {
        fprintf(stderr, "Unable to create socket: Error %d.\n", *connect_filehandle);
        *connect_filehandle = -1;
        return 0;
    }

    // Connect
    memset( &serv_address, 0x00, sizeof(serv_address));
    serv_address.sin_family = serv_host->h_addrtype;
    serv_address.sin_port = htons(port);
    memcpy( &serv_address.sin_addr, serv_host->h_addr, serv_host->h_length);
    connect_res = connect( *connect_filehandle,
                           (struct sockaddr *) &serv_address, sizeof(serv_address));

    if ( connect_res < 0 ) {
        fprintf(stderr, "Connect failed: Error %d.\n", connect_res);
        *connect_filehandle = -1;
        return 0;
    }

    // IMPORTANT : set fd as non blocking.
    //fcntl(*connect_filehandle, F_SETFL, O_NONBLOCK);
    // seems to slow down connection and not necessary as only ONE connection

    // Success!
    fprintf(stderr, "Connected on TCP port %d.\n", port);
    return 1;
}
// ==========================================================================
void
Khepera3_TCP::print_nmea(struct sNMEAMessage *msg)
{
    unsigned int i;

    printf( "%s with %d arguments\n", msg->command, msg->argument_count);
    for ( i=0; i< msg->argument_count; ++i) {
        printf( "  arg[%2.0d] = %s\n", i, msg->argument[i]);
    }
}

// ==========================================================================
void client_nmea_send_hook(int connect_filehandle, const char *buffer, int len)
{
    int nb_send = 0;
    char b[BUFFER_SIZE]; /*(char *) buffer*/;
    char *p = (char*)buffer;
    int tosend = len;
    memset(b,0,BUFFER_SIZE);
    strncpy(b,buffer,strlen(buffer)+1);
    p=b;
//printf("sended %s\n", p);fflush(stdout);
    while (tosend > 0)
    {
        nb_send = send(connect_filehandle, p, (int)tosend, MSG_NOSIGNAL);
        //nb_send=send(connect_filehandle, p, len, 0);
        //printf("send %d\n", nb_send);fflush(stdout);
        if ( nb_send <= 0 )
            return;
        p += nb_send;
        tosend -= nb_send;
    }

    //getchar();
//	printf("end send\n");
}
// ==========================================================================
void client_nmea_unknown_character(char c)
{
    printf("'%c'(%d) was not recognized\n", c, c);
}
// ==========================================================================
void
Khepera3_TCP::parse_ir_proxi(struct sNMEAMessage *msg)
{
    unsigned int i;
    for ( i = cKhepera3SensorsInfrared_Begin; i < cKhepera3SensorsInfrared_Count; ++i ) {
        // printf( "parse_ir_proxi [%d] = %ld\n", i, strtol(msg->argument[i], 0, 0));

        if (msg->argument_count>i)
            khepera3.infrared_proximity.sensor[i] = strtol(msg->argument[i], 0, 0);

        //	printf(" %d ", khepera3.infrared_proximity.sensor[i]);
    }
    if (msg->argument_count>i)
        khepera3.infrared_proximity.timestamp = strtol(msg->argument[i], 0, 0);

    //printf(" %d \n", khepera3.infrared_proximity.sensor[i]);
}
void
Khepera3_TCP::parse_ir_amb(struct sNMEAMessage *msg) // ---------------------
{
    unsigned int i;
    for ( i = cKhepera3SensorsInfrared_Begin; i < cKhepera3SensorsInfrared_Count; ++i ) {
        //printf( "parse_ir_amb [%d] = %ld\n", i, strtol(msg->argument[i], 0, 0));
        if (msg->argument_count>i)
            khepera3.infrared_ambient.sensor[i] = strtol(msg->argument[i], 0, 0);
    }
    if (msg->argument_count>i)
        khepera3.infrared_ambient.timestamp = strtol(msg->argument[i], 0, 0);
}
void
Khepera3_TCP::parse_us(struct sNMEAMessage *msg) // -------------------------
{
    unsigned int index = 0;
    int us_index;
    int echo;
    if (msg->argument_count>index)
        us_index = strtol(msg->argument[index++], 0, 0);
    if (msg->argument_count>index)
        khepera3.ultrasound.sensor[us_index].echos_count = strtol(msg->argument[index++], 0, 0);

    for ( echo = 0; echo < khepera3.ultrasound.sensor[us_index].echos_count; ++echo) {
        ;
        if (msg->argument_count>index)
            khepera3.ultrasound.sensor[us_index].distance[echo] = strtol(msg->argument[index++], 0, 0);

        if (msg->argument_count>index)
            khepera3.ultrasound.sensor[us_index].amplitude[echo] = strtol(msg->argument[index++], 0, 0);

        if (msg->argument_count>index)
            khepera3.ultrasound.sensor[us_index].timestamp[echo] = strtol(msg->argument[index++], 0, 0);

    }
}
void Khepera3_TCP::read_loop(void *pdata)
{
    clock_t start,end;
    float millisec;
    while (1)
    {
        start = clock();
        send_lock();
        if (battery_load)
        {
            //battery_lock();
            this->battery_capacity_remaining_absolute();
//usleep(100);
            this->battery_capacity_remaining_relative();
//usleep(100);
            this->battery_current_average();
//usleep(100);
            //battery_unlock();
        }


//usleep(100);

        //get_all();

        if (ir_proxi_load)
        {
            //infrared_proximity_lock();
            this->infrared_proximity();
            //	this->restart_odometry();
        }
        if (ir_ambient_load)
        {
            this->infrared_ambient();
        }
        if (odometry_load)
        {
            this->get_odometry();
        }
        //infrared_proximity_unlock();
        //		usleep(500);	//
//usleep(100);
//drive_get_current_position();
        //drive_lock();
        if (drive_load)
        {
            this->drive_get_current_speed();
            if (_drive_start_flag)
            {
                _drive_start();
                _drive_start_flag=0;
            }
            if (_drive_stop_flag)
            {
                _drive_stop();
                _drive_stop_flag=0;
            }
            if (_drive_stop_flag)
            {
                _drive_idle();
                _drive_idle_flag=0;
            }

            if (this->_drive_set_speed_flag)
            {
                this->_drive_set_speed(_speed_left,_speed_right);
                this->_drive_set_speed_flag=0;
            }
            else if (this->_drive_set_speed_using_profile_flag)
            {
                this->_drive_set_speed_using_profile(_speed_left,_speed_right);
                this->_drive_set_speed_using_profile_flag=0;
            }
//usleep(100);
//				usleep(500);
            if (this->_drive_goto_position_flag)
            {
                this->_drive_goto_position(_position_left,_position_right);
                this->_drive_goto_position_flag=0;
            }
            else if (this->_drive_goto_position_using_profile_flag)
            {
                this->_drive_goto_position_using_profile(_position_left,_position_right);
                this->_drive_goto_position_using_profile_flag=0;
            } else if (this->_drive_set_current_position_flag)
            {
                this->_drive_set_current_position(_position_left,_position_right);
                this->_drive_set_current_position_flag=0;
            }
            //drive_unlock();
        }


//usleep(100);
        //	usleep(500);
        if (ultrasound_load)
        {
            if (this->_ultrasound_set_max_echo_number_flag)
            {
                this->_ultrasound_set_max_echo_number(_nb_echo);
                this->_ultrasound_set_max_echo_number_flag=0;
            }
//usleep(100);
//				usleep(500);
            for (int i=0;i<5;i++)
            {
                if (this->_ultrasound_stop_flag[i])
                {
                    this->_ultrasound_flag[i]=0;
                    this->_ultrasound_stop(i);
                    this->_ultrasound_stop_flag[i]=0;
                }
                else if (this->_ultrasound_start_flag[i])
                {
                    this->_ultrasound_flag[i]=1;
                    this->_ultrasound_start(i);
                    this->_ultrasound_start_flag[i]=0;
                }
            }
            //ultrasound_unlock();

        }
        usleep(100);
        end = clock();
        millisec = (float)(end-start)/CLOCKS_PER_SEC;
//  printf("test %f\n",millisec);
        //usleep(2000);
        send_unlock();
    }
}

void Khepera3_TCP::parse_odometry(struct sNMEAMessage* msg) {



    if (msg->argument_count>2)
    {
        khepera3.odometry.x=atof(msg->argument[0]);
        khepera3.odometry.y=atof(msg->argument[1]);
        khepera3.odometry.theta=atof(msg->argument[2]);
    }
}

/*void Khepera3_TCP::parse_all(struct sNMEAMessage* msg)
{


}*/
void Khepera3_TCP::parse_battery(struct sNMEAMessage* msg) {

    if (msg->argument_count>0)
    {
        if (strcmp(msg->command, "BAT_VOLT")==0)
        {
            khepera3.battery.voltage=strtol(msg->argument[0], 0, 0);

        }
        else if (strcmp(msg->command, "BAT_CUR")==0)
        {
            khepera3.battery.current=strtol(msg->argument[0], 0, 0);
        }
        else if (strcmp(msg->command, "BAT_AVG_CUR")==0)
        {
            khepera3.battery.current_average=strtol(msg->argument[0], 0, 0);
        }
        else if (strcmp(msg->command, "BAT_CAP")==0)
        {
            khepera3.battery.capacity_remaining_absolute=strtol(msg->argument[0], 0, 0);

        }
        else if (strcmp(msg->command, "BAT_TEMP")==0)
        {
            khepera3.battery.temperature=strtol(msg->argument[0], 0, 0);
        }
        else if (strcmp(msg->command, "BAT_CAP_REL")==0)
        {
            khepera3.battery.capacity_remaining_relative=strtol(msg->argument[0], 0, 0);
        }
    }

}

//==================================================================================
void Khepera3_TCP::parse_all(struct sNMEAMessage* msg) {
    unsigned int i;
    unsigned int tmp=0;

    if (msg->argument_count>0)
    {

        for ( i = cKhepera3SensorsInfrared_Begin; i < cKhepera3SensorsInfrared_Count; ++i ) {
            // printf( "parse_ir_proxi [%d] = %ld\n", i, strtol(msg->argument[i], 0, 0));



            if (msg->argument_count>i)
                khepera3.infrared_proximity.sensor[i] = strtol(msg->argument[i], 0, 0);

            //	printf(" %d ", khepera3.infrared_proximity.sensor[i]);
        }
        if (msg->argument_count>i)
            khepera3.infrared_proximity.timestamp = strtol(msg->argument[i++], 0, 0);

        // i++;//jump seperator

        tmp=i;
        for ( i = cKhepera3SensorsInfrared_Begin+tmp; i < cKhepera3SensorsInfrared_Count+tmp; ++i ) {
            //printf( "parse_ir_amb [%d] = %ld\n", i, strtol(msg->argument[i], 0, 0));
            if (msg->argument_count>i)
                khepera3.infrared_ambient.sensor[i-tmp] = strtol(msg->argument[i], 0, 0);
        }
        if (msg->argument_count>i)
            khepera3.infrared_ambient.timestamp = strtol(msg->argument[i++], 0, 0);

//     i++;//jump seperator

        if (msg->argument_count>i+1)
        {
            khepera3.motor_left.current_speed = strtol(msg->argument[i++], 0, 0);
            khepera3.motor_right.current_speed = strtol(msg->argument[i++], 0, 0);
        }


        //   i++;//jump seperator
        if (msg->argument_count>i+2)
        {
            khepera3.odometry.x=atof(msg->argument[i++]);
            khepera3.odometry.y=atof(msg->argument[i++]);
            khepera3.odometry.theta=atof(msg->argument[i]);
        }

    }





}
void
Khepera3_TCP::parse_motor_speed(struct sNMEAMessage *msg) // ----------------
{
    if (msg->argument_count>1)
    {
        khepera3.motor_left.current_speed = strtol(msg->argument[0], 0, 0);
        khepera3.motor_right.current_speed = strtol(msg->argument[1], 0, 0);
    }
}
void
Khepera3_TCP::parse_motor_position(struct sNMEAMessage *msg) // ----------------
{
    if (msg->argument_count>1)
    {
        khepera3.motor_left.current_position = strtol(msg->argument[0], 0, 0);
        khepera3.motor_right.current_position = strtol(msg->argument[1], 0, 0);
    }
}

// ==========================================================================
int
Khepera3_TCP::process_send_wait_process(char * msg_buffer)
{
    char buffer[BUFFER_SIZE];
    ssize_t nb_read;

    // process
    if ( nmea_parser_process_data( &_client_snd_nmea_parser, msg_buffer, strlen(msg_buffer) ) != -1 ) {
        printf( "Msg not recognized\n");
        return 0;
    }

    // send msg as it is valid
    //printf( "msg recognized\n");
    //nmea_parser_send( _connect_filehandle, &_client_snd_nmea_parser, &_client_snd_nmea_parser.message);;
    client_nmea_send_hook( _connect_filehandle,msg_buffer,strlen(msg_buffer)+1);
    // check for answer
    //printf("**** wait for answer *****\n");
    nb_read = 0;
    while ( nb_read <= 0 ) {
        //printf("start reading\n");
        nb_read = read( _connect_filehandle, buffer, BUFFER_SIZE);
        //nb_read=1;

        if ( nb_read > 0 ) {
            //	printf( "read %d from connection\n", nb_read );
            //printf("Received : %s\n", buffer);
            nmea_parser_process_data( &_client_rcv_nmea_parser, buffer, nb_read );

//      print_nmea( &(_client_rcv_nmea_parser.message) );
            //printf("%s\n",_client_rcv_nmea_parser.message.command);

            if (strcmp(_client_rcv_nmea_parser.message.command, "ALL") == 0) { // $GET_PROXI */
                //printf("ir proxy\n");
                parse_all( &_client_rcv_nmea_parser.message );

            }
            else if (strcmp(_client_rcv_nmea_parser.message.command, "IR_PROXI") == 0) { // $GET_PROXI */
                //printf("ir proxy\n");
                parse_ir_proxi( &_client_rcv_nmea_parser.message );

            }
            else if (strcmp(_client_rcv_nmea_parser.message.command, "IR_AMB") == 0) { // $GET_AMB */
                parse_ir_amb( &_client_rcv_nmea_parser.message );
            }
            else if (strcmp(_client_rcv_nmea_parser.message.command, "ODOMETRY") == 0) { // $GET_ODOMETRY */
                parse_odometry( &_client_rcv_nmea_parser.message );

            }
            else if (strcmp(_client_rcv_nmea_parser.message.command, "US") == 0) { // $GET_US */
                parse_us( &_client_rcv_nmea_parser.message );

            }
            else if (strcmp(_client_rcv_nmea_parser.message.command, "MOTOR_SPEED") == 0) { // GET_SPEED */
                parse_motor_speed( &_client_rcv_nmea_parser.message );
            }
            else if (strcmp(_client_rcv_nmea_parser.message.command, "AKN") == 0) { //$AKN */
                // nothing special
            }
            else if (strcmp(_client_rcv_nmea_parser.message.command, "MOTOR_POS") == 0) { // GET_POS */
                parse_motor_position( &_client_rcv_nmea_parser.message );

            }
            else if ((strcmp(_client_rcv_nmea_parser.message.command, "BAT_VOLT") == 0)||(strcmp(_client_rcv_nmea_parser.message.command, "BAT_CUR") == 0)||(strcmp(_client_rcv_nmea_parser.message.command, "BAT_AVG_CUR") == 0)||(strcmp(_client_rcv_nmea_parser.message.command, "BAT_CAP") == 0)||(strcmp(_client_rcv_nmea_parser.message.command, "BAT_TEMP") == 0)||(strcmp(_client_rcv_nmea_parser.message.command, "BAT_CAP_REL")==0))   { //GET_BATTERY
                parse_battery( &_client_rcv_nmea_parser.message );

            }
            else {
                printf("Received : %s\n", buffer);
                printf("parse error\n");
                usleep(100);
                return 0;
            }
        }


    }

    usleep(100);
// printf("**** no more answer *****\n");
    return -1;
}

// ==========================================================================
// == public methods ========================================================
// ==========================================================================
//struct sKhepera3_TCP khepera3;
// ==========================================================================
int
Khepera3_TCP::tcp_init(char *ip_address, int port)
{
    fprintf(stderr, "Trying to connect to %s:%d\n", ip_address, port);
    // connect to the robot
    if ( tcp_connect( ip_address, port, &_connect_filehandle) != 1 ) {
        fprintf(stderr, "Erreur dans la connection");
        return -1;
    }

    pthread_create(&thread_reader, NULL, read_loop_wraper, this);

    return 0;
}
// ==========================================================================
//! Initializes this module.
void
Khepera3_TCP::init()
{
    // nmea parser for recieving msg

    //pthread_mutex_init(&_infrared_proximity_mutex,NULL);
    //pthread_mutex_init(&_infrared_ambiant_mutex,NULL);
    //pthread_mutex_init(&_drive_mutex,NULL);
    //pthread_mutex_init(&_ultrasound_mutex,NULL);
    //pthread_mutex_init(&_odometry_mutex,NULL);
    //pthread_mutex_init(&_battery_mutex,NULL);
    pthread_mutex_init(&_send_mutex,NULL);

    ir_proxi_load=0;
    drive_load=0;
    ir_ambient_load=0;
    ultrasound_load=0;
    odometry_load=0;
    battery_load=0;
    _drive_start_flag=0;
    _drive_stop_flag=0;
    _drive_idle_flag=0;
    _nb_echo=0;
    _us_index=0;
    for (int i = 0;i<5;i++)
    {
        _ultrasound_start_flag[i]=0;
        _ultrasound_stop_flag[i]=0;
    }
    _ultrasound_set_max_echo_number_flag=0;
    _drive_goto_position_flag=0;
    _drive_goto_position_using_profile_flag=0;
    _position_left=0;
    _position_right=0;
    _drive_set_speed_flag=0;
    _drive_set_speed_using_profile_flag=0;
    _drive_set_current_position_flag=0;
    _speed_left=0;
    _speed_right=0;
    for (int i=0;i<5;i++)
        _ultrasound_flag[i]=0;
    nmea_parser_init( & _client_rcv_nmea_parser);

    // nmea parser for sending msg
    nmea_parser_init( &_client_snd_nmea_parser);
    _client_snd_nmea_parser.hook_send = client_nmea_send_hook;
    _client_snd_nmea_parser.hook_process_unrecognized_char = client_nmea_unknown_character;
}
// ==========================================================================
//! Reads the last infrared proximity values and fills them into the khepera3 structure. The return value indicates success (-1) or failure (0). Transfer on I2C bus: 31 bytes.
int
Khepera3_TCP::infrared_proximity()
{
    sprintf( _msg_buffer, "$GET_PROXI\n");
    //printf( "Will send %s\n", _msg_buffer );
    //nmea_parser_process_data( this, &_client_snd_nmea_parser, _msg_buffer, strlen(_msg_buffer) );
    //print_nmea( &(_client_snd_nmea_parser.message) );
    return process_send_wait_process( _msg_buffer );
}
// ==========================================================================
//! Reads the last ambient infrared values and fills them into the khepera3 structure. The return value indicates success (-1) or failure (0). Transfer on I2C bus: 31 bytes.
int
Khepera3_TCP::infrared_ambient()
{
    sprintf( _msg_buffer, "$GET_AMB\n");
    //printf( "Will send %s\n", _msg_buffer );
    //nmea_parser_process_data( this, &_client_snd_nmea_parser, _msg_buffer, strlen(_msg_buffer) );
    //print_nmea( &(_client_snd_nmea_parser.message) );
    return process_send_wait_process( _msg_buffer );
}
// ==========================================================================
//! Puts both motors in normal (control) mode.
int
Khepera3_TCP::_drive_start()
{
    sprintf( _msg_buffer, "$START\n");
    return process_send_wait_process( _msg_buffer );
}
int
Khepera3_TCP::drive_start()
{
    _drive_start_flag=1;
    return -1;
}
// ==========================================================================
//! Stops both motors immediately
int
Khepera3_TCP::_drive_stop()
{
    sprintf( _msg_buffer, "$STOP\n");
    return process_send_wait_process( _msg_buffer );
}
int
Khepera3_TCP::drive_stop()
{
    _drive_stop_flag=1;
    return -1;
}
// ==========================================================================
//! Puts both motors in idle mode.
int
Khepera3_TCP::_drive_idle()
{
    sprintf( _msg_buffer, "$IDLE\n");
    return process_send_wait_process( _msg_buffer );
}
int
Khepera3_TCP::drive_idle()
{
    _drive_idle_flag=1;
    return -1;
}
// ==========================================================================
//! Reads the status register and updates the corresponding field in the motor structure. Transfer on I2C bus: 4 bytes.
// ADAPTED so as to set the status of both motors.
int
Khepera3_TCP::drive_get_status()
{
    //sprintf( _msg_buffer, "$MOTOR_STATUS\n" );
    //process_send_wait_process( _msg_buffer );
    return -1;
}
// ==========================================================================
//! Reads the error register and updates the corresponding field in the motor structure. Transfer on I2C bus: 4 bytes.
int
Khepera3_TCP::drive_get_error()
{
    //sprintf( _msg_buffer, "$MOTOR_STATUS\n" );
    //process_send_wait_process( _msg_buffer );
    return -1;
}
// ==========================================================================
//! Reads the current speed and updates the corresponding value the motor structures. Transfer on I2C bus: 32 bytes.
void
Khepera3_TCP::drive_get_current_speed()
{
    sprintf( _msg_buffer, "$GET_SPEED\n");
    process_send_wait_process( _msg_buffer );
}
// ==========================================================================
//! Sets the motor speeds. Transfer on I2C bus: 24 - 30 bytes.
void
Khepera3_TCP::_drive_set_speed(int speed_left,int speed_right)
{
    sprintf( _msg_buffer, "$SET_SPEED,%d,%d\n", speed_left, speed_right);
    process_send_wait_process( _msg_buffer );
}
void
Khepera3_TCP::drive_set_speed(int speed_left,int speed_right)
{
    _speed_left = speed_left;
    _speed_right=speed_right;
    _drive_set_speed_flag=1;
}
// ==========================================================================
//! Sets the motor speeds and uses the acceleration profile to reach that speed. Transfer on I2C bus: 24 - 30 bytes.
void
Khepera3_TCP::_drive_set_speed_using_profile(int speed_left, int speed_right)
{
    sprintf( _msg_buffer, "$SET_SPEED_CONTROL,%d,%d\n", speed_left, speed_right);
    process_send_wait_process( _msg_buffer );
}
void
Khepera3_TCP::drive_set_speed_using_profile(int speed_left, int speed_right)
{
    _speed_left = speed_left;
    _speed_right=speed_right;
    _drive_set_speed_using_profile_flag=1;
}
// ==========================================================================
//! Reads the current position and updates the corresponding value the motor structures. Transfer on I2C bus: 32 bytes.
void
Khepera3_TCP::drive_get_current_position()
{
    sprintf( _msg_buffer, "$GET_POS\n");
    process_send_wait_process( _msg_buffer );
}


void
Khepera3_TCP::get_all()
{
    sprintf( _msg_buffer, "$GET_ALL\n");
    process_send_wait_process( _msg_buffer );
}
// ==========================================================================
//! Sets the current encoder position. Transfer on I2C bus: 24 bytes.
void
Khepera3_TCP::_drive_set_current_position(int position_left, int position_right)
{
    sprintf( _msg_buffer, "$SET_POS,%d,%d\n", position_left, position_right);
    process_send_wait_process( _msg_buffer );
}
void
Khepera3_TCP::drive_set_current_position(int position_left, int position_right)
{
    _position_left=position_left;
    _position_right=position_right;
    _drive_set_current_position_flag=1;
}
// ==========================================================================
//! Goes to a specific motor position. Transfer on I2C bus: 24 - 30 bytes.
void
Khepera3_TCP::_drive_goto_position(int position_left, int position_right)
{
    sprintf( _msg_buffer, "$GOTO_POS,%d,%d\n", position_left, position_right);
    process_send_wait_process( _msg_buffer );
}
void
Khepera3_TCP::drive_goto_position(int position_left, int position_right)
{
    _position_left=position_left;
    _position_right=position_right;
    _drive_goto_position_flag=1;
}
// ==========================================================================
//! Goes to a specific motor position using the acceleration profile. Transfer on I2C bus: 24 - 30 bytes.
void
Khepera3_TCP::_drive_goto_position_using_profile(int position_left, int position_right)
{
    sprintf( _msg_buffer, "$GOTO_POS_CONTROL,%d,%d\n", position_left, position_right);
    process_send_wait_process( _msg_buffer );
}

void
Khepera3_TCP::drive_goto_position_using_profile(int position_left, int position_right)
{
    _position_left=position_left;
    _position_right=position_right;
    _drive_goto_position_using_profile_flag=1;
}
// ==========================================================================
//! Sets the maximum number of echos received by the ultrasound sensors. The return value indicates success (-1) or failure (0). Transfer on I2C bus: 8 bytes.
int
Khepera3_TCP::_ultrasound_set_max_echo_number(int max_echo_number)
{
    sprintf( _msg_buffer, "$US_MAX_ECHO,%d\n", max_echo_number);
    return process_send_wait_process( _msg_buffer ); // $AKN ?
}
int
Khepera3_TCP::ultrasound_set_max_echo_number(int max_echo_number)
{
    _nb_echo=max_echo_number;
    _ultrasound_set_max_echo_number_flag=1;
    return -1;
}
// ==========================================================================
//! Reads the last values of an ultrasound sensor and fills them into the khepera3 structure. The return value indicates success (-1) or failure (0). Transfer on I2C bus: 52 bytes.
int
Khepera3_TCP::ultrasound(int us_index)
{
    sprintf( _msg_buffer, "$GET_US,%d\n", us_index);
    return process_send_wait_process( _msg_buffer ); // $US,.....

}

int Khepera3_TCP::allultrasound()
{


    if (ultrasound_load)
    {
        //ultrasound_lock();
//				usleep(500);
        for (int i=0;i<5;i++)
        {
            if (_ultrasound_flag[i])
            {
                ///  send_lock();
                this->ultrasound(i);
                ///send_unlock();
//usleep(100);
            }
        }
    }
    return -1; // SUCCESS
}
// ==========================================================================
// Special in TCP : start an US sensor.
int
Khepera3_TCP::_ultrasound_start( int us_index )
{
    sprintf( _msg_buffer, "$START_US,%d\n", us_index);
    return process_send_wait_process( _msg_buffer ); // $AKN ?
}
int
Khepera3_TCP::ultrasound_start( int us_index )
{
    if (us_index<5)
        _us_index = us_index;
    else
        _us_index=0;
    _ultrasound_start_flag[_us_index]=1;
    return -1;
}
// Special in TCP : stop an US sensor.
int
Khepera3_TCP::_ultrasound_stop( int us_index )
{
    sprintf( _msg_buffer, "$STOP_US,%d\n", us_index);
    return process_send_wait_process( _msg_buffer ); // $AKN ?
    return -1;
}
int
Khepera3_TCP::ultrasound_stop( int us_index )
{
    if (us_index<5)
        _us_index = us_index;
    else
        _us_index=0;

    _ultrasound_stop_flag[_us_index]=1;
    return -1;
}

int Khepera3_TCP::battery_voltage()
{
    sprintf( _msg_buffer, "$GET_BAT,0\n");
    return process_send_wait_process( _msg_buffer );
}
int Khepera3_TCP::battery_current()
{
    sprintf( _msg_buffer, "$GET_BAT,1\n");
    return process_send_wait_process( _msg_buffer );
}
int Khepera3_TCP::battery_current_average()
{
    sprintf( _msg_buffer, "$GET_BAT,2\n");
    return process_send_wait_process( _msg_buffer );
}
int Khepera3_TCP::battery_capacity_remaining_absolute()
{
    sprintf( _msg_buffer, "$GET_BAT,3\n");
    return process_send_wait_process( _msg_buffer );
}
int Khepera3_TCP::battery_capacity_remaining_relative()
{
    sprintf( _msg_buffer, "$GET_BAT,5\n");
    return process_send_wait_process( _msg_buffer );
}
int Khepera3_TCP::battery_temperature()
{
    sprintf( _msg_buffer, "$GET_BAT,4\n");
    return process_send_wait_process( _msg_buffer );
}
int  Khepera3_TCP::get_odometry() {

    sprintf( _msg_buffer, "$GET_ODOMETRY\n");
    return process_send_wait_process( _msg_buffer );
}
int  Khepera3_TCP::restart_odometry() {
    sprintf( _msg_buffer, "$RESET_ODOMETRY\n");
    return process_send_wait_process( _msg_buffer );
}
// ==========================================================================
