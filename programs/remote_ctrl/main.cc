#include <stdio.h>
#include <unistd.h>

#include "khepera3_tcp.h"
#define GAIN 5
#define FWD_SPEED 10000



/**** Sur le robot: lancer 
 * ./tcp_server.armgs -k -p 3000 
 * 
 * 
 * ****/
int main(int argc,char *argv[])
{
	    static const int Connections_A[9] = { 2,  2,  2, -2, -22, -10, -8, 2, 4}; 
    static const int Connections_B[9] = { 2, -8, -10, -22,  -2,  2,  2, 2, 4};
  
	 struct sKhepera3_TCP khepera3;
	Khepera3_TCP k3;
	k3.init();
	k3.tcp_init("khepera1",3001);

	    k3.ir_proxi_load=1;
   k3.drive_load=1;
   	k3.drive_start();
	for(int j=0;j<300;j++)
	{
		int right,left;
		
		
	k3.infrared_proximity();
	left=0;
	right=0;
	for(int i=0;i<9;i++)
	{
		left+=Connections_A[i]*k3.khepera3.infrared_proximity.sensor[i];
		right+=Connections_B[i]*k3.khepera3.infrared_proximity.sensor[i];

}
left=FWD_SPEED+GAIN*left;
right=FWD_SPEED+GAIN*right;


	k3.drive_set_speed(left,right);
	usleep(10000);
	
	
}
	k3.drive_set_speed(0,0);
	usleep(20000);
		k3.drive_idle();
			usleep(20000);
}
