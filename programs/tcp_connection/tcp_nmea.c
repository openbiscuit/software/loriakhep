/*!
 * (c) 2006-2007 EPFL, Lausanne, Switzerland
 * Thomas Lochmatter
 */

#include "nmea.h"
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#define BUFFER_SIZE 256
int tcp_nmea_server_filehandle;			//!< The filehandle of the listener socket.
int tcp_nmea_connection_filehandle;		//!< The filehandle of the connection. (Note that only one connection is accepted, i.e. any new connection closes the current one.)
//int tcp_nmea_socket[128];
//int n_socket=0;
fd_set readfs;
int max_socket;
//struct sNMEAParser tcp_nmea_parser;
void (*tcp_nmea_hook_process_message)(struct sNMEAMessage *m, int withchecksum);

void tcp_nmea_init(void (*hook_process_message)(struct sNMEAMessage *m, int withchecksum)) {

    /*int i;
    for(i=0;i<128;i++)
    tcp_nmea_socket[i]=-1;*/
    tcp_nmea_hook_process_message = hook_process_message;
}

int tcp_nmea_start(int port) {
    struct sockaddr_in sa;
    struct hostent *host;
    int bindres, listenres;

    // Don't touch anything if the port is zero
    if (port == 0) {
        return 0;
    }

    // Initialize
    tcp_nmea_server_filehandle = -1;
    tcp_nmea_connection_filehandle = -1;

    // Create socket handle
    tcp_nmea_server_filehandle = socket(AF_INET, SOCK_STREAM, 0);
    if (tcp_nmea_server_filehandle < 0) {
        fprintf(stderr, "Unable to create socket: Error %d.\n", tcp_nmea_server_filehandle);
        tcp_nmea_server_filehandle = -1;
        return 0;
    }
    int on=1;
    setsockopt(tcp_nmea_server_filehandle, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));
    // Bind (on all local addresses)
    host = gethostbyname("0.0.0.0");
    memcpy((char *)&sa.sin_addr, (char *)host->h_addr, host->h_length);
    sa.sin_family = host->h_addrtype;
    sa.sin_port = htons(port);

    bindres = bind(tcp_nmea_server_filehandle, (struct sockaddr *) & sa, sizeof(sa));
    if (bindres < 0) {
        if (bindres == EACCES) {
            fprintf(stderr, "Bind failed: You do not have enough access rights to listen on port %d.\n", port);
        } else {
            perror("bind");
            fprintf(stderr, "Bind failed: Error %d.\n", bindres);
        }
        tcp_nmea_server_filehandle = -1;
        return 0;
    }

    // Listen
    listenres = listen(tcp_nmea_server_filehandle, SOMAXCONN);
    if (listenres < 0) {
        if (listenres == EADDRINUSE) {
            fprintf(stderr, "Listen failed: Port %d is already in use.\n", port);
        } else {
            fprintf(stderr, "Listen failed: Error %d.\n", listenres);
        }
        tcp_nmea_server_filehandle = -1;
        return 0;
    }
    max_socket=tcp_nmea_server_filehandle;

    FD_ZERO(&readfs);
    FD_SET(tcp_nmea_server_filehandle, &readfs);


    // Set the non-blocking flag (important!), because we ask the kernel regularly whether a new connection is there (polling)
    //to_fix/fcntl(tcp_nmea_server_filehandle, F_SETFL, O_NONBLOCK);

    // Success!
    //fprintf(stderr, "Listening on TCP port %d.\n", port);
    return 1;
}
void *
thread_socket(void * num)
{
    char buffer[BUFFER_SIZE];
    int len,n;
    int *z;
    z= (int*)num;
    int i=*z;
    fd_set fs;
    struct sNMEAParser parser;
    nmea_parser_init(&parser);
    parser.hook_process_message = tcp_nmea_hook_process_message;
//printf("%d\n",i);
    FD_ZERO(&fs);
    FD_SET(i, &fs);
    while (1)
    {
        if (i < 0) {
//printf("%d\n",tcp_nmea_connection_filehandle);
            free(num);
//return 0;
            pthread_exit(NULL);
        }
        memset(buffer,0,BUFFER_SIZE);
        // Read and parse until there is nothing available any more
        while (1) {
            //	memset(buffer,0,BUFFER_SIZE);
            select(i + 1, &fs, NULL, NULL, NULL); //on s'endore jusqu'a ce que des données arrive sur la socket comme read est en mode non bloquant.
            len = read(i, buffer, BUFFER_SIZE);

            if (len < 1) {

                if (len==0)
                {
                    close(i);
                    //FD_CLR(tcp_nmea_socket[i],&readfs);
                    //tcp_nmea_socket[i]=-1;
                    free(num);
//return 0;
                    pthread_exit(NULL);
                }
//free(buffer);
//printf("on sort de ce truc\n");
//FD_SET(tcp_nmea_server_filehandle, &readfs);
                //return 0;
            }
            else {
//printf("receive : %s\n",buffer);
                //tcp_nmea_connection_filehandle=tcp_nmea_socket[i];
                nmea_parser_process_data_(&parser, buffer, BUFFER_SIZE,i);

            }

        }

    }

}
int tcp_nmea_accept() {
    struct sockaddr_in sc;
    socklen_t sclen;
    int fh;
    int *ptr;
    pthread_t t;
    // If there is no server, do nothing
    if (tcp_nmea_server_filehandle < 0) {
        return 0;
    }

    // Accept

    sclen = sizeof(sc);
    fh = accept(tcp_nmea_server_filehandle, (struct sockaddr *) & sc, &sclen);
    if (fh < 0) {
        if ((errno != EINTR) && (errno != EAGAIN)) {
            fprintf(stderr, "Accept failed (Error %d): connection request ignored.\n", errno);
        }
        return 0;
    }
    if (fh>=max_socket)
    {
        max_socket=fh;

    }
    //FD_SET(fh, &readfs);
    // Set non-blocking mode (important!), because our main loop shall run all the time and check if new data is available (polling)
    fcntl(fh, F_SETFL, O_NONBLOCK);

    // If we have a connection already, close it
    /*if (tcp_nmea_connection_filehandle > -1) {
    printf("on close une connection\n");
    close(tcp_nmea_connection_filehandle);
    tcp_nmea_connection_filehandle = -1;
    }*/

    //tcp_nmea_socket[n_socket++]=fh;

    // Set the new connection and initialize the parser
    printf("Connection accepted.\n");
    fflush(NULL);
    ptr=(int*)malloc(sizeof(int));
    *ptr=fh;
    pthread_create (&t, NULL,thread_socket , (void*)ptr);
    tcp_nmea_connection_filehandle = fh;

    return 1;
}

int tcp_nmea_receive() {
    char buffer[BUFFER_SIZE];
    int len,n,i;

    // Check for new connections

    FD_ZERO(&readfs);
    FD_SET(tcp_nmea_server_filehandle, &readfs);
    /*for(i=0;i<128;i++)
    {
    if(tcp_nmea_socket[i]!=-1)
    FD_SET(tcp_nmea_socket[i], &readfs);
    }*/
    if ((n=select(max_socket + 2, &readfs, NULL, NULL, NULL)) < 0)
    {
        perror("select()");
        exit(errno);
    }
//printf("on passe le select\n");


    if (FD_ISSET(tcp_nmea_server_filehandle, &readfs))
    {

        tcp_nmea_accept();


    }
    /*else
    {
    	for(i=0;i<128;i++)
    	{
    //tcp_nmea_accept();

    // Only go on if there is a connection

    	if(tcp_nmea_socket[i]!=-1)
    	{
    	if(FD_ISSET(tcp_nmea_socket[i], &readfs))
    	{
    	if (tcp_nmea_socket[i] < 0) {
    //printf("%d\n",tcp_nmea_connection_filehandle);

    	return 0;
    }
    //buffer = (char*)malloc(sizeof(char)*BUFFER_SIZE);
    		/*		memset(buffer,0,BUFFER_SIZE);
    // Read and parse until there is nothing available any more
    	while (1) {
    	len = read(tcp_nmea_socket[i], buffer, BUFFER_SIZE);

    	if (len < 1) {

    	if(len==0)
    	{
    	close(tcp_nmea_socket[i]);
    	FD_CLR(tcp_nmea_socket[i],&readfs);
    	tcp_nmea_socket[i]=-1;
    	return 0;
    }
    //free(buffer);
    //printf("on sort de ce truc\n");
    //FD_SET(tcp_nmea_server_filehandle, &readfs);
    						//return 0;
    }
    	else{
    //printf("receive : %s\n",buffer);
    	tcp_nmea_connection_filehandle=tcp_nmea_socket[i];
    	nmea_parser_process_data(&tcp_nmea_parser, buffer, BUFFER_SIZE);
    }

    }
    	pthread_create (&th1, NULL, my_thread_process, 0);

    }
    }
    }
    }*/
//free(buffer);
    return 1;
}
