#include "proc_mgr.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
int _pid[4096];

int count=0;

int new_proc(char *comm,char *argv[])
{
    int i,index;

    //printf("on create le truc :%s %s\n",comm,argv[1]);
    int pid = fork();
    if (pid==-1)
    {
        printf("erreur dans la création de processus\né");
        return -1;
    }
    else
    {
        if (pid)
        {
//father
            index=-1;
            for (i=0;i<count;i++)
            {
                if (_pid[i]==-1)
                {
                    index=i;
                    i=count;
                }
            }
            if (index!=-1)
            {
                _pid[index]=pid;
            }
            else
            {
                _pid[count]=pid;
                count++;
            }

        }
        else
        {
//child

            printf("%s\n%s\n%s %d\n---\n",comm,argv[0],argv[1],argv[2]);
            execvp(comm,argv);
            printf("echec de création\n");

        }
    }
    return 0;
}

void kill_all()
{
    sig_all(SIGINT);

}

void stop_all()
{
    sig_all(SIGTERM);

}
void pause_all()
{
    sig_all(SIGTSTP);


}
void unpause_all()
{
    sig_all(SIGCONT);
//printf("on a fini de unpausé\n");
//	sig_all(SIGUSR1);


}
void kill_one(int pid)
{
    int r;
    kill(pid,SIGKILL);
    waitpid(pid,&r,NULL);
}

void stop_one(int pid)
{
    int r;
    kill(pid,SIGTERM);
    waitpid(pid,&r,NULL);

}

void pause_on(int pid)
{
    kill(pid,SIGTSTP);

//wait(pid);
}


void sig_all(int signal)
{
    int i;
    for (i=0;i<count;i++)
    {
        if (_pid[i]!=-1)
        {
            printf("send %d to %d\n",signal,_pid[i]);

            kill(_pid[i],signal);

            if ((signal == SIGTERM) || (signal == SIGKILL))
            {
                wait(_pid[i]);
                _pid[i]=-1;
            }
        }
    }

}