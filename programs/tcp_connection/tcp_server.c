/**
 * (c) 2008 Maia, Loria, Nancy, France.
 * Alain Dutech
 */

#include "khepera3.h"
#include "commandline.h"
#include "tcp_nmea.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <signal.h>
#include "odometry_track.h"
//#include <iostream>
#include "khepera3_status.h"
#include "khepera3_behave.h"
#include <pthread.h>
#include <string.h>
#include "remote_serveur.h"
#include "remote_variable.h"
#include "proc_mgr.h"
#define BUFFER_SIZE 4096

int _debug = 1;
int _debug_all = 0;
int _debug_get = 0;
int _debug_ir_proxi = 0;
int _debug_ir_amb = 0;
int _debug_us = 0;
int _debug_drive = 0;
int _debug_odom = 0;
int _debug_battery = 0;
int _debug_proc = 0;
int _debug_behave = 0;

int buffering=1;
// Prints the help text.
pthread_mutex_t mutex_socket[4096];
int last_time_pong[4096];
remoteserver *_origs;
typedef struct thread_struct {
    int waitms;
    int *socks;
    int watchdogdelay;
    char *argument[128];
    char exec[128];
} thread_struct;
void
help() {
    if (_debug || _debug_all) printf("Opens a TCP port for listening and printing of command over a connected TCP client.\n");
    if (_debug || _debug_all) printf("\n");
    if (_debug || _debug_all) printf("Usage: my_tcp_server [OPTIONS]\n");
    if (_debug || _debug_all) printf("\n");
    if (_debug || _debug_all) printf("Options:\n");
    if (_debug || _debug_all) printf("  -p --port PORT     Listens on port PORT (default: 3000)\n");
    if (_debug || _debug_all) printf("\n");
}

void *ping_thread(void *param)
{
    thread_struct ts = *(thread_struct*)param;

    int waitms=ts.waitms;
    int *socks=ts.socks;
    int thissec,watchdogdelay;
    watchdogdelay=ts.watchdogdelay;
    struct timeval oldtime,now;
    gettimeofday(&now,NULL);
    last_time_pong[*socks]=now.tv_sec;
    printf("Watchdog armed\n");

    while (1)
    {

        pthread_mutex_lock(&(mutex_socket[*socks]));
///update thissec:

        oldtime=now;
        gettimeofday(&now,NULL);
        thissec=now.tv_sec;

        if (thissec-last_time_pong[*socks]>watchdogdelay)
        {
            /*printf("DELAYED\n");
            exit(-1);*/
///stop all programme
            kill_all();

///call watchdog software;
            printf("watchdog launched %x %s\n",ts.argument[0],ts.argument[0]);
            if (ts.argument[0]!=NULL)
                new_proc(ts.argument[0],ts.argument);
///kill this thread
            printf("Watchdog Disarmed\n");

            pthread_mutex_unlock(&(mutex_socket[*socks]));

            pthread_exit(NULL);
        }
///send ping;
        send(*socks, "$PING\n", 6, MSG_NOSIGNAL);

//update thissec
//last_time_pong[*socks] =thissec;

        pthread_mutex_unlock(&(mutex_socket[*socks]));



        usleep(waitms*1000);///wait waitms ms
    }
}

// This method is called each time a NMEA message is received.
// Normally, message received has been processed by nmea module.

void
tcp_server_nmea_receive_hook(struct sNMEAMessage *m, int withchecksum) {
    unsigned int i;
    int us_index;
    int nb_echo;
    int result;
    char buffer[BUFFER_SIZE];
    char buffer2[BUFFER_SIZE];
    int socks;
    int vi,type_var;
    double vd;
    char vc;
    char* vcs;
    int *viarray;
    double *vdarray;
    int sizearray;

    int res_behave;

    if (m->socket==-1)
        socks=tcp_nmea_connection_filehandle;
    else
        socks = m->socket;

    //buffer = (char*)malloc(sizeof(char)*BUFFER_SIZE);
    FILE *connection_file;
    memset(buffer,0,BUFFER_SIZE);
    memset(buffer2,0,BUFFER_SIZE);
    // print the command received
    if (_debug ) {
      // Print only when command is not "GET_something"
      if ((strstr( m->command, "GET" ) == NULL ) || _debug_get) {
	printf( "***** Received *****\n");
	printf( "%s, with %d arguments\n", m->command, m->argument_count );
	for ( i=0; i < m->argument_count; ++i ) {
	  printf( "arg[%2.0d] = %s\n", i, m->argument[i]);
	}
      }
    }
    //		printf("Command is : %s\n", m->command);

    if (strcmp(m->command, "SET_SPEED") == 0) {	// e.g. $SPEED,1000,1000
      if (_debug_drive || _debug_all) printf("Command is : %s\n", m->command);
        // Set the speed according to the arguments
        if (m->argument_count >= 2) {
            //pthread_mutex_lock (&_motor_mutex);
            pthread_mutex_lock(&_i2c_mutex);
            khepera3_drive_set_speed(strtol(m->argument[0], 0, 0), strtol(m->argument[1], 0, 0));
            pthread_mutex_unlock(&_i2c_mutex);
            //pthread_mutex_unlock (&_motor_mutex);
            if (!buffering) {

                send(socks, "$AKN\n", 5, MSG_NOSIGNAL);
            }
            else
            {
                sprintf(buffer,"$AKN\n");
            }
            if (_debug_drive || _debug_all) printf("Sent $AKN information\n");
        }
    }
    else if (strcmp(m->command, "SET_SPEED_CONTROL") == 0) {	// e.g. $SPEED_CONTROL,1000,1000
      if (_debug_drive || _debug_all) printf("Command is : %s\n", m->command);
        // Set the speed according to the arguments
        if (m->argument_count >= 2) {
            //pthread_mutex_lock (&_motor_mutex);
            pthread_mutex_lock(&_i2c_mutex);
            khepera3_drive_set_speed_using_profile(strtol(m->argument[0], 0, 0), strtol(m->argument[1], 0, 0));
            pthread_mutex_unlock(&_i2c_mutex);
            //pthread_mutex_unlock (&_motor_mutex);
            if (!buffering) {

                send(socks, "$AKN\n", 5, MSG_NOSIGNAL);
            }
            else
            {
                sprintf(buffer,"$AKN\n");
            }
            if (_debug_drive || _debug_all) printf("Sent $AKN information\n");
        }
    }
    // LANCER UN 'DRIVE' POUR X ms : $DRIVE_SPEED_TIME,vGauche,vDroit,duree,tolerance
    else if (strcmp(m->command, "DRIVE_SPEED_TIME") == 0) {
      if (_debug_behave || _debug_all) printf("Command is : %s\n", m->command);

      // Start beheavior with appropriate timer
      if (m->argument_count >= 4) {
	res_behave = behave_start_drive_timer( strtol(m->argument[0], 0, 0),
					       strtol(m->argument[1], 0 ,0),
					       strtol(m->argument[2], 0, 0),
					       strtol(m->argument[3], 0, 0));
	
      // Check that robot is not already moving
	if (res_behave != BEHAVE_NOTREADY) {

	  if (!buffering) {
	    send(socks, "$AKN\n", 5, MSG_NOSIGNAL);
	  }
	  else
	    {
	      sprintf(buffer,"$AKN\n");
	    }
	}
	// robot is already moving
	else {
	  if (!buffering) {
	    send(socks, "$NOT_READY\n", 5, MSG_NOSIGNAL);
	  }
	  else
	    {
	      sprintf(buffer,"$NOT_READY\n");
	    }
	}
      }
    }
    // ARRETER LE 'DRIVE' : $DRIVE_STOP
    else if (strcmp(m->command, "DRIVE_STOP") == 0) {
      if (_debug_behave || _debug_all) printf("Stop DriveBehavior\n");
      behave_cancel();

      if (!buffering) {
	send(socks, "$AKN\n", 5, MSG_NOSIGNAL);
      }
      else
	{
	  sprintf(buffer,"$AKN\n");
	}
      if (_debug_behave || _debug_all) printf("Sent $AKN\n");
    }
    // CONNAITRE LE STATUS DU 'DRIVE' : $GET_DRIVE_STATUS
    else if (strcmp(m->command, "GET_DRIVE_STATUS") == 0) {
      if (_debug_behave || _debug_get || _debug_all) printf("Report status of DriveBehavior\n");
      
      if (!buffering) {
	connection_file = fdopen(socks, "a");
	fprintf(connection_file, "$DRIVE_STATUS,%d\n", _behave_status );
	fflush(connection_file);
      }
      else {
	sprintf(buffer, "$DRIVE_STATUS,%d\n", _behave_status);
      }
      if (_debug_behave || _debug_get || _debug_all) printf("Sent $DRIVE_STATUS information\n");
    }
    // COMMENCER BRAITENBERG : $START_BRAIT
    else if (strcmp(m->command, "START_BRAIT") == 0) {
      if (_debug_behave || _debug_all) printf("Start Braitenberg\n");
      braitenberg_start();

      if (!buffering) {
	  send(socks, "$AKN\n", 5, MSG_NOSIGNAL);
      }
      else
	{
	  sprintf(buffer,"$AKN\n");
	}
      if (_debug_behave || _debug_all) printf("Sent $AKN\n");
    }
    // ARRETER BRAITENBERG : $STOP_BRAIT
    else if (strcmp(m->command, "STOP_BRAIT") == 0) {
      if (_debug_behave || _debug_all) printf("Stop Braitenberg\n");
      braitenberg_stop();

      if (!buffering) {
	send(socks, "$AKN\n", 5, MSG_NOSIGNAL);
      }
      else
	{
	  sprintf(buffer,"$AKN\n");
	}
      if (_debug_behave || _debug_all) printf("Sent $AKN\n");
    }
    else if (strcmp(m->command, "GET_ALL") == 0)
    {

        if (!buffering)
        {
            connection_file = fdopen(socks, "a");
            // fprintf(connection_file, "$ALL,IR_PROXI");
            fprintf(connection_file, "$ALL");
        }
        else
        {
            //  sprintf(buffer,"$ALL,IR_PROXI");
            sprintf(buffer,"$ALL");
        }
        for (i = 0; i < 11; i++) {
            if (!buffering) {

                fprintf(connection_file, ",%d", khepera3.infrared_proximity.sensor[i]);
            }
            else
            {
                sprintf(buffer, "%s,%d",buffer, khepera3.infrared_proximity.sensor[i]);
            }
            //fprintf(connection_file, ",%d", i*100);
        }
        if (!buffering)
        {
            fprintf(connection_file, ",%d", khepera3.infrared_proximity.timestamp);
        }
        else
        {
            sprintf(buffer,"%s,%d",buffer, khepera3.infrared_proximity.timestamp);
        }
        /*
        			for(i=0;i<5;i++)
        			{
        			if( _us_enabled[us_index] ) {
        			if( _update_us_flag == 0 ) { // Need to get value from robot
        					////pthread_mutex_lock (&_us_mutex);
        			pthread_mutex_lock(&_i2c_mutex);
        			khepera3_ultrasound((eKhepera3SensorsUltrasound)us_index);
        //std::cout << " " << khepera3.ultrasound.sensor[i].distance[0]<< "\n";
        			pthread_mutex_unlock(&_i2c_mutex);
        					//pthread_mutex_unlock (&_us_mutex);
        		}
        			if(!buffering)
        			{
        			fprintf(connection_file, ",US");
        			fprintf(connection_file, ",%d", us_index );
        			fprintf(connection_file, ",%d", khepera3.ultrasound.sensor[us_index].echos_count);
        		}
        			else
        			{
        			sprintf(buffer,"%s,US",buffer);
        			sprintf(buffer,"%s,%d", buffer,us_index );
        			sprintf(buffer,"%s,%d", buffer,khepera3.ultrasound.sensor[us_index].echos_count);
        		}
        			for( i=0; (int)i<khepera3.ultrasound.sensor[us_index].echos_count; ++i) {
        			if(!buffering)
        			{
        			fprintf(connection_file, ",%d", khepera3.ultrasound.sensor[us_index].distance[i]);
        			fprintf(connection_file, ",%d", khepera3.ultrasound.sensor[us_index].amplitude[i]);
        			fprintf(connection_file, ",%d", khepera3.ultrasound.sensor[us_index].timestamp[i]);
        		}
        			else
        			{
        			sprintf(buffer,"%s,%d",buffer, khepera3.ultrasound.sensor[us_index].distance[i]);
        			sprintf(buffer,"%s,%d",buffer, khepera3.ultrasound.sensor[us_index].amplitude[i]);
        			sprintf(buffer,"%s,%d",buffer, khepera3.ultrasound.sensor[us_index].timestamp[i]);

        		}
        		}
        		}
        		}
        */
        /*  if(!buffering)
          		{
          		fprintf(connection_file, ",IR_AMB");
          	}
          		else
          		{
          		sprintf(buffer,"%s,IR_AMB",buffer);
          	}*/
        for (i = 0; i < 11; i++) {
            if (!buffering)
            {
                fprintf(connection_file, ",%d", khepera3.infrared_ambient.sensor[i]);
            }
            else
            {
                sprintf(buffer,"%s,%d",buffer, khepera3.infrared_ambient.sensor[i]);
            }
            //fprintf(connection_file, ",%d", i*100);
        }
        if (!buffering)
        {
            fprintf(connection_file, ",%d", khepera3.infrared_ambient.timestamp);

            //fprintf(connection_file, ",MOTOR_SPEED");
            fprintf(connection_file, ",%d", khepera3.motor_left.current_speed);
            fprintf(connection_file, ",%d", khepera3.motor_right.current_speed);

            //fprintf(connection_file, ",ODOMETRY");
            fprintf(connection_file, ",%f", ot.result.x);
            fprintf(connection_file, ",%f", ot.result.y);
            fprintf(connection_file, ",%f", ot.result.theta);
            fprintf(connection_file, "\n");
            fflush(connection_file);
        }
        else
        {
            sprintf(buffer,"%s,%d",buffer, khepera3.infrared_ambient.timestamp);

            //sprintf(buffer,"%s,MOTOR_SPEED",buffer);
            sprintf(buffer,"%s,%d",buffer, khepera3.motor_left.current_speed);
            sprintf(buffer,"%s,%d",buffer, khepera3.motor_right.current_speed);


            //sprintf(buffer,"%s,ODOMETRY",buffer);
            sprintf(buffer,"%s,%f",buffer, ot.result.x);
            sprintf(buffer,"%s,%f",buffer, ot.result.y);
            sprintf(buffer,"%s,%f\n",buffer, ot.result.theta);
        }

    }
    else if (strcmp(m->command, "GOTO_POS") == 0) {	// e.g. $GOTO_POS,1000,1000
      if (_debug_drive || _debug_all) printf("Command is : %s\n", m->command);
        // Set the speed according to the arguments
        if (m->argument_count >= 2) {
            //pthread_mutex_lock (&_motor_mutex);
            pthread_mutex_lock(&_i2c_mutex);
            khepera3_drive_goto_position(strtol(m->argument[0], 0, 0), strtol(m->argument[1], 0, 0));
            pthread_mutex_unlock(&_i2c_mutex);
            //pthread_mutex_unlock (&_motor_mutex);
            if (!buffering)
            {
                send(socks, "$AKN\n", 5, MSG_NOSIGNAL);
            }
            else
            {
                sprintf(buffer,"$AKN\n");
            }
            if (_debug_drive || _debug_all) printf("Sent $AKN information\n");
        }
    }
    else if (strcmp(m->command, "GOTO_POS_CONTROL") == 0) {	// e.g. $GOTO_POS_CONTROL,1000,1000
      if (_debug_drive || _debug_all) printf("Command is : %s\n", m->command);
        // Set the speed according to the arguments
        if (m->argument_count >= 2) {
            //pthread_mutex_lock (&_motor_mutex);
            pthread_mutex_lock(&_i2c_mutex);
            khepera3_drive_goto_position_using_profile(strtol(m->argument[0], 0, 0), strtol(m->argument[1], 0, 0));
            pthread_mutex_unlock(&_i2c_mutex);
            //pthread_mutex_unlock (&_motor_mutex);
            if (!buffering)
            {
                send(socks, "$AKN\n", 5, MSG_NOSIGNAL);
            }
            else
            {
                sprintf(buffer,"$AKN\n");
            }
            if (_debug_drive || _debug_all) printf("Sent $AKN information\n");
        }
    }
    else if (strcmp(m->command, "SET_POS") == 0) {	// e.g. $SET_POS,1000,1000
      if (_debug_drive || _debug_all) printf("Command is : %s\n", m->command);
        // Set the speed according to the arguments
        if (m->argument_count >= 2) {
            //pthread_mutex_lock (&_motor_mutex);
            pthread_mutex_lock(&_i2c_mutex);
            khepera3_drive_set_current_position(strtol(m->argument[0], 0, 0), strtol(m->argument[1], 0, 0));
            pthread_mutex_unlock(&_i2c_mutex);
            //pthread_mutex_unlock (&_motor_mutex);
            if (!buffering)
            {
                send(socks, "$AKN\n", 5, MSG_NOSIGNAL);
            }
            else
            {
                sprintf(buffer,"$AKN\n");
            }
            if (_debug_drive || _debug_all) printf("Sent $AKN information\n");
        }
    }
    else if (strcmp(m->command, "STOP") == 0) {	// e.g. $SPEED,1000,1000
      if (_debug_drive || _debug_all) printf("Command is : %s\n", m->command);
        // Set the speed according to the arguments
        //pthread_mutex_lock (&_motor_mutex);
        pthread_mutex_lock(&_i2c_mutex);
        khepera3_drive_stop();
        pthread_mutex_unlock(&_i2c_mutex);
        //pthread_mutex_unlock (&_motor_mutex);
        if (!buffering)
        {
            send(socks, "$AKN\n", 5, MSG_NOSIGNAL);
        }
        else
        {
            sprintf(buffer,"$AKN\n");
        }
        if (_debug_drive || _debug_all) printf("Sent $AKN information\n");
    }
    else if (strcmp(m->command, "START") == 0) {	// e.g. $SPEED,1000,1000
      if (_debug_drive || _debug_all) printf("Command is : %s\n", m->command);
        // Set the speed according to the arguments
        //pthread_mutex_lock (&_motor_mutex);
        pthread_mutex_lock(&_i2c_mutex);
        khepera3_drive_start();
        pthread_mutex_unlock(&_i2c_mutex);
        //pthread_mutex_unlock (&_motor_mutex);
        if (!buffering)
        {
            send(socks, "$AKN\n", 5, MSG_NOSIGNAL);
        }
        else
        {
            sprintf(buffer,"$AKN\n");
        }
        if (_debug_drive || _debug_all) printf("Sent $AKN information\n");
    }
    else if (strcmp(m->command, "IDLE") == 0) {	// e.g. $SPEED,1000,1000
      if (_debug_drive || _debug_all) printf("Command is : %s\n", m->command);
        // Set the speed according to the arguments
        //pthread_mutex_lock (&_motor_mutex);
        pthread_mutex_lock(&_i2c_mutex);
        khepera3_drive_idle();
        pthread_mutex_unlock(&_i2c_mutex);
        //pthread_mutex_unlock (&_motor_mutex);
        if (!buffering)
        {
            send(socks, "$AKN\n", 5, MSG_NOSIGNAL);
        }
        else
        {
            sprintf(buffer,"$AKN\n");
        }
        if (_debug_drive || _debug_all) printf("Sent $AKN information\n");
    }
    else if (strcmp(m->command, "GET_PROXI") == 0) {	// $IR */
      if (_debug_ir_proxi || _debug_get || _debug_all) printf("Command is : %s\n", m->command);
        //khepera3_infrared_proximity();

        if (!buffering)
        {
            connection_file = fdopen(socks, "a");
            fprintf(connection_file, "$IR_PROXI");
        }
        else
        {
            sprintf(buffer,"$IR_PROXI");
        }
        for (i = 0; i < 11; i++) {
            if (!buffering)
            {
                fprintf(connection_file, ",%d", khepera3.infrared_proximity.sensor[i]);
            }
            else
            {
                sprintf(buffer, "%s,%d",buffer, khepera3.infrared_proximity.sensor[i]);
            }
            //fprintf(connection_file, ",%d", i*100);
        }
        if (!buffering)
        {
            fprintf(connection_file, ",%d", khepera3.infrared_proximity.timestamp);
            fprintf(connection_file, "\n");
            fflush(connection_file);
        }
        else
        {
            sprintf(buffer, "%s,%d\n",buffer, khepera3.infrared_proximity.timestamp);
        }

        if (_debug_ir_proxi || _debug_get || _debug_all) printf("Sent $IR_PROXI information\n");
    }
    else if (strcmp(m->command, "GET_AMB") == 0) {	// $IR */
      if (_debug_ir_amb || _debug_get || _debug_all) printf("Report the IR ambient values\n");
        //khepera3_infrared_proximity();

        if (!buffering)
        {
            connection_file = fdopen(socks, "a");
            fprintf(connection_file, "$IR_AMB");
        }
        else
        {
            sprintf(buffer, "$IR_AMB");
        }
        for (i = 0; i < 11; i++) {
            if (!buffering)
            {
                fprintf(connection_file, ",%d", khepera3.infrared_ambient.sensor[i]);
            }
            else
            {
                sprintf(buffer, "%s,%d",buffer, khepera3.infrared_ambient.sensor[i]);
            }
            //fprintf(connection_file, ",%d", i*100);
        }
        if (!buffering)
        {
            fprintf(connection_file, ",%d", khepera3.infrared_ambient.timestamp);
            fprintf(connection_file, "\n");
            fflush(connection_file);
        }
        else
        {
            sprintf(buffer, "%s,%d\n",buffer, khepera3.infrared_ambient.timestamp);
        }
        if (_debug_ir_amb || _debug_get || _debug_all) printf("Sent $IR_AMB information\n");
    }
    else if (strcmp(m->command, "GET_POS") == 0) {	// $IR */
      if (_debug_drive || _debug_get || _debug_all) printf("Command is : %s\n", m->command);
        //khepera3_infrared_proximity();

        if (!buffering)
        {
            connection_file = fdopen(socks, "a");
            fprintf(connection_file, "$MOTOR_POS");
            fprintf(connection_file, ",%d", khepera3.motor_left.current_position);
            fprintf(connection_file, ",%d", khepera3.motor_right.current_position);
            fprintf(connection_file, "\n");
            fflush(connection_file);
        }
        else
        {
            sprintf(buffer, "$MOTOR_POS");
            sprintf(buffer, "%s,%d",buffer, khepera3.motor_left.current_position);
            sprintf(buffer, "%s,%d\n",buffer, khepera3.motor_right.current_position);

        }
        if (_debug_drive || _debug_get || _debug_all) printf("Sent $MOTOR_POS information\n");
    }
    else if (strcmp(m->command, "GET_SPEED") == 0) {	// $GET_SPEED*/
      if (_debug_drive || _debug_get || _debug_all) printf("Command is : %s\n", m->command);
        //khepera3_infrared_proximity();

        if (!buffering)
        {
            connection_file = fdopen(socks, "a");
            fprintf(connection_file, "$MOTOR_SPEED");
            fprintf(connection_file, ",%d", khepera3.motor_left.current_speed);
            fprintf(connection_file, ",%d", khepera3.motor_right.current_speed);
            fprintf(connection_file, "\n");
            fflush(connection_file);
        }
        else
        {
            sprintf(buffer, "$MOTOR_SPEED");
            sprintf(buffer, "%s,%d",buffer, khepera3.motor_left.current_speed);
            sprintf(buffer, "%s,%d\n",buffer, khepera3.motor_right.current_speed);

        }
        if (_debug_drive || _debug_get || _debug_all) printf("Sent $MOTOR_SPEED information\n");
    }
    else if (strcmp(m->command, "RESET_ODOMETRY") == 0)
    {
      //printf("restart odometry\n");
        odometry_track_start(&ot);
        if (!buffering)
        {
            connection_file = fdopen(socks, "a");

            fprintf(connection_file, "$ODOMETRY");
            fprintf(connection_file, ",%f", ot.result.x);
            fprintf(connection_file, ",%f", ot.result.y);
            fprintf(connection_file, ",%f", ot.result.theta);
            fprintf(connection_file, "\n");
            fflush(connection_file);
        }
        else
        {
            sprintf(buffer, "$ODOMETRY");
            sprintf(buffer, "%s,%f",buffer, ot.result.x);
            sprintf(buffer, "%s,%f",buffer, ot.result.y);
            sprintf(buffer, "%s,%f\n",buffer, ot.result.theta);
        }
        if (_debug_odom || _debug_all) printf("Sent $ODOMETRY information\n");

    }
    else if (strcmp(m->command, "GET_ODOMETRY") == 0)
    {
        if (!buffering)
        {
            connection_file = fdopen(socks, "a");

            fprintf(connection_file, "$ODOMETRY");
            fprintf(connection_file, ",%f", ot.result.x);
            fprintf(connection_file, ",%f", ot.result.y);
            fprintf(connection_file, ",%f", ot.result.theta);
            fprintf(connection_file, "\n");
            fflush(connection_file);
        }
        else
        {
            sprintf(buffer, "$ODOMETRY");
            sprintf(buffer, "%s,%f",buffer, ot.result.x);
            sprintf(buffer, "%s,%f",buffer, ot.result.y);
            sprintf(buffer, "%s,%f\n",buffer, ot.result.theta);
        }
        if (_debug_odom || _debug_get|| _debug_all) printf("Sent $ODOMETRY information\n");

    }
    else if (strcmp(m->command, "GET_BAT") == 0) { //$GET_BAT */
      if (_debug_battery || _debug_get|| _debug_all) printf("Command is : %s\n", m->command);
        // Read battery according to param
        if (m->argument_count == 1) {
            if (!buffering)
                connection_file = fdopen(socks, "a");
            switch ( strtol(m->argument[0], 0, 0 )) {
            case 0:
                // Ask the robot if needed
                if ( _update_battery_flag == 0 )  {
                    //pthread_mutex_lock (&_bat_mutex);
                    pthread_mutex_lock(&_i2c_mutex);
                    khepera3_battery_voltage();
                    pthread_mutex_unlock(&_i2c_mutex);
                    ////pthread_mutex_unlock (&_bat_mutex);
                }
                if (!buffering)
                {
                    fprintf(connection_file, "$BAT_VOLT");
                    fprintf(connection_file, ",%d", khepera3.battery.voltage);
                    fprintf(connection_file, "\n");
                    fflush(connection_file);
                }
                else
                {
                    sprintf(buffer, "$BAT_VOLT");
                    sprintf(buffer, "%s,%d\n",buffer, khepera3.battery.voltage);
                }
                if (_debug_battery || _debug_get|| _debug_all) printf("Sent $BAT_VOLT information");
                break;
            case 1:
                // Ask the robot if needed
                if ( _update_battery_flag == 0 ) {
                    //pthread_mutex_lock (&_bat_mutex);
                    pthread_mutex_lock(&_i2c_mutex);
                    khepera3_battery_current();
                    pthread_mutex_unlock(&_i2c_mutex);
                    //pthread_mutex_unlock (&_bat_mutex);
                }
                if (!buffering)
                {
                    fprintf(connection_file, "$BAT_CUR");
                    fprintf(connection_file, ",%d", khepera3.battery.current);
                    fprintf(connection_file, "\n");
                    fflush(connection_file);
                }
                else
                {
                    sprintf(buffer, "$BAT_CUR");
                    sprintf(buffer, "%s,%d\n",buffer, khepera3.battery.current);
                }
                if (_debug_battery || _debug_get|| _debug_all) printf("Sent $BAT_CURRENT information");
                break;
            case 2:
                // Ask the robot if needed
                if ( _update_battery_flag == 0 ) {
                    //pthread_mutex_lock (&_bat_mutex);
                    pthread_mutex_lock(&_i2c_mutex);
                    khepera3_battery_current_average();
                    pthread_mutex_unlock(&_i2c_mutex);
                    //pthread_mutex_unlock (&_bat_mutex);
                }
                if (!buffering)
                {

                    fprintf(connection_file, "$BAT_AVG_CUR");
                    fprintf(connection_file, ",%d", khepera3.battery.current_average);
                    fprintf(connection_file, "\n");
                    fflush(connection_file);
                }
                else
                {
                    sprintf(buffer, "$BAT_AVG_CUR");
                    sprintf(buffer, "%s,%d\n",buffer, khepera3.battery.current_average);
                }
                if (_debug_battery || _debug_get|| _debug_all) printf("Sent $BAT_AVG_CURRENT information");
                break;
            case 3:
                // Ask the robot if needed
                if ( _update_battery_flag == 0 ) {
                    //pthread_mutex_lock (&_bat_mutex);
                    pthread_mutex_lock(&_i2c_mutex);
                    khepera3_battery_capacity_remaining_absolute();
                    pthread_mutex_unlock(&_i2c_mutex);
                    //pthread_mutex_unlock (&_bat_mutex);
                }
                if (!buffering)
                {
                    fprintf(connection_file, "$BAT_CAP");
                    fprintf(connection_file, ",%d", khepera3.battery.capacity_remaining_absolute);
                    fprintf(connection_file, "\n");
                    fflush(connection_file);
                }
                else
                {
                    sprintf(buffer, "$BAT_CAP");
                    sprintf(buffer, "%s,%d\n",buffer, khepera3.battery.capacity_remaining_absolute);
                }
                if (_debug_battery || _debug_get|| _debug_all) printf("Sent $BAT_CAP information");
                break;
            case 4:
                // Ask the robot if needed
                if ( _update_battery_flag == 0 ) {
                    //pthread_mutex_lock (&_bat_mutex);
                    pthread_mutex_lock(&_i2c_mutex);
                    khepera3_battery_temperature();
                    pthread_mutex_unlock(&_i2c_mutex);
                    //pthread_mutex_unlock (&_bat_mutex);
                }
                if (!buffering)
                {
                    fprintf(connection_file, "$BAT_TEMP");
                    fprintf(connection_file, ",%d", khepera3.battery.temperature);
                    fprintf(connection_file, "\n");
                    fflush(connection_file);
                }
                else
                {
                    sprintf(buffer, "$BAT_TEMP");
                    sprintf(buffer, "%s,%d\n",buffer, khepera3.battery.temperature);

                }
                if (_debug_battery || _debug_get|| _debug_all) printf("Sent $BAT_TEMP information");
                break;
            case 5:
                // Ask the robot if needed
                if ( _update_battery_flag == 0 ) {
                    //pthread_mutex_lock (&_bat_mutex);
                    pthread_mutex_lock(&_i2c_mutex);
                    khepera3_battery_capacity_remaining_relative();
                    pthread_mutex_unlock(&_i2c_mutex);
                    //pthread_mutex_unlock (&_bat_mutex);
                }
                if (!buffering)
                {

                    fprintf(connection_file, "$BAT_CAP_REL");
                    fprintf(connection_file, ",%d", khepera3.battery.capacity_remaining_relative);
                    fprintf(connection_file, "\n");
                    fflush(connection_file);
                }
                else
                {
                    sprintf(buffer, "$BAT_CAP_REL");
                    sprintf(buffer, "%s,%d\n",buffer, khepera3.battery.capacity_remaining_relative)	;
                }
                if (_debug_battery || _debug_get|| _debug_all) printf("Sent $BAT_CAP_REL information");
                break;
            default:
                if (!buffering)
                {
                    send(socks, "$PARAM\n", 7, MSG_NOSIGNAL);
                }
                else
                {
                    sprintf(buffer,"$PARAM\n");
                }
                if (_debug_battery || _debug_get|| _debug_all) printf("Sent $PARAM information\n");
            }
        }
    }
    else if (strcmp(m->command, "GET_US") == 0) { //$GET_US */
      if (_debug_us || _debug_get|| _debug_all) printf("Command is : %s\n", m->command);
        // Read us according to param
        if (m->argument_count == 1) {
            if (!buffering)
                connection_file = fdopen(socks, "a");

            us_index = strtol(m->argument[0], 0, 0 );
            if (_debug_us || _debug_get|| _debug_all) printf( "Reading utltrasound %d\n",us_index);
            if ( _us_enabled[us_index] ) {
                if ( _update_us_flag == 0 ) { // Need to get value from robot
                    //pthread_mutex_lock (&_us_mutex);
                    pthread_mutex_lock(&_i2c_mutex);
                    khepera3_ultrasound(/*(eKhepera3SensorsUltrasound)*/us_index);
//std::cout << " " << khepera3.ultrasound.sensor[i].distance[0]<< "\n";
                    pthread_mutex_unlock(&_i2c_mutex);
                    //pthread_mutex_unlock (&_us_mutex);
                }
                if (!buffering)
                {
                    fprintf(connection_file, "$US");
                    fprintf(connection_file, ",%d", us_index );
                    fprintf(connection_file, ",%d", khepera3.ultrasound.sensor[us_index].echos_count);
                }
                else
                {
                    sprintf(buffer, "$US");
                    sprintf(buffer, "%s,%d",buffer, us_index );
                    sprintf(buffer, "%s,%d",buffer, khepera3.ultrasound.sensor[us_index].echos_count);
                }
                for ( i=0; (int)i<khepera3.ultrasound.sensor[us_index].echos_count; ++i) {
                    if (!buffering)
                    {
                        fprintf(connection_file, ",%d", khepera3.ultrasound.sensor[us_index].distance[i]);
                        fprintf(connection_file, ",%d", khepera3.ultrasound.sensor[us_index].amplitude[i]);
                        fprintf(connection_file, ",%d", khepera3.ultrasound.sensor[us_index].timestamp[i]);
                    }
                    else
                    {
                        sprintf(buffer, "%s,%d",buffer, khepera3.ultrasound.sensor[us_index].distance[i]);
                        sprintf(buffer, "%s,%d",buffer, khepera3.ultrasound.sensor[us_index].amplitude[i]);
                        sprintf(buffer, "%s,%d",buffer, khepera3.ultrasound.sensor[us_index].timestamp[i]);
                    }
                }
                if (!buffering)
                {
                    fprintf(connection_file, "\n");
                    fflush(connection_file);
                }
                else
                {
                    sprintf(buffer,"%s\n",buffer);
                }


                /*if(_debug) printf( "$US");
                						if(_debug) printf(",%d", us_index );
                						if(_debug) printf(",%d", khepera3.ultrasound.sensor[us_index].echos_count);
                						for( i=0; i<khepera3.ultrasound.sensor[us_index].echos_count; ++i) {
                						if(_debug) printf(",%d", khepera3.ultrasound.sensor[us_index].distance[i]);
                						if(_debug) printf(",%d", khepera3.ultrasound.sensor[us_index].amplitude[i]);
                						if(_debug) printf(",%d", khepera3.ultrasound.sensor[us_index].timestamp[i]);
                					}
                						if(_debug) printf("\n");
                						*/if (_debug) printf("Sent $GET_US information\n");
            }
            else {
                if (!buffering)
                {
                    send(socks, "$DISABLED\n", 10, MSG_NOSIGNAL);
                }
                else
                {
                    sprintf(buffer,"$DISABLED\n");
                }
                //if (_debug) printf("Sent $DISABLED information\n");
            }
        }
        else {
            if (!buffering)
            {
                send(socks, "$PARAM\n", 7, MSG_NOSIGNAL);
            }
            else
            {
                sprintf(buffer,"$PARAM\n");
            }
            if (_debug_us || _debug_get|| _debug_all) printf("Sent $PARAM information\n");
        }
    }
    else if (strcmp(m->command, "EXECUTE") == 0) {
      if (_debug_proc || _debug_all) printf("Command is : %s\n", m->command);
        new_proc(m->argument[0],m->argument);

    }
    else if (strcmp(m->command, "SET_VAR") == 0) {
        if (_debug) printf("Command is : %s\n", m->command);

        //new_proc(m->argument[0],m->argument);
        if (m->argument_count>=3)
        {
            int inc;

            if (atoi(m->argument[0])> T_CHARSTAR)
            {
///c'est un tableau



                switch (atoi(m->argument[0]))
                {
                case T_INT_ARRAY:

                    sizearray=atoi(m->argument[2]);
                    if (m->argument_count==sizearray+3)
                    {

                        viarray = (int*)malloc(sizeof(int)*sizearray);

                        for (inc=0;inc<sizearray;inc++)
                        {

                            viarray[inc]=atoi(m->argument[3+inc]);

                        }

                        add_array(m->argument[1],atoi(m->argument[0]),sizearray,viarray,NULL);
                    }
                    /*else
                    								{
                    								sprintf(buffer,"$WRONG_FORMAT\n");
                    						}*/
                    break;
                case T_DOUBLE_ARRAY:

                    sizearray=atoi(m->argument[2]);
                    if (m->argument_count==sizearray+3)
                    {
                        vdarray = (int*)malloc(sizeof(double)*sizearray);

                        for (inc=0;inc<sizearray;inc++)
                        {
                            vdarray[inc]=atof(m->argument[3+inc]);

                        }
                        add_array(m->argument[1],atoi(m->argument[0]),sizearray,NULL,vdarray);
                    }
                    break;

                }
            }
            else
            {



                if (m->argument_count==3)
                {

                    add_var(m->argument[1],atoi(m->argument[0]),atoi(m->argument[2]),atof(m->argument[2]),*(m->argument[2]),m->argument[2]);


                }
            }
        }
    }
    else if (strcmp(m->command, "GET_VAR") == 0) {
      if (_debug_proc || _debug_all) printf("Command is : %s\n", m->command);

        //new_proc(m->argument[0],m->argument);
        if (m->argument_count==1)
        {

            get_var(m->argument[0],&type_var,&vi,&vd,&vc,&vcs,&sizearray,&viarray,&vdarray);

            switch (type_var)
            {
                int inc;
            case T_INT:
                sprintf(buffer,"$VAR,%s,%d\n",m->argument[0],vi);
                break;
            case T_DOUBLE:
                sprintf(buffer,"$VAR,%s,%f\n",m->argument[0],vd);
                break;
            case T_CHAR:
                sprintf(buffer,"$VAR,%s,%c\n",m->argument[0],vc);
                break;
            case T_CHARSTAR:
                sprintf(buffer,"$VAR,%s,%s\n",m->argument[0],vcs);
                break;
            case T_INT_ARRAY:
                sprintf(buffer,"$VAR,%s",m->argument[0]);
                for (inc=0;inc<sizearray;inc++)
                    sprintf(buffer,"%s,%d",buffer,viarray[inc]);
                sprintf(buffer,"%s\n",buffer);
                break;
            case T_DOUBLE_ARRAY:
                sprintf(buffer,"$VAR,%s",m->argument[0]);
                for (inc=0;inc<sizearray;inc++)
                    sprintf(buffer,"%s,%f",buffer,vdarray[inc]);
                sprintf(buffer,"%s\n",buffer);
                break;
            case -1:
                sprintf(buffer,"$UNKNOW_VAR\n");
                break;
            }

        }

    }
    else if (strcmp(m->command, "BROADCAST") == 0) {
      if (_debug_proc || _debug_all) printf("Command is : %s\n", m->command);

        //new_proc(m->argument[0],m->argument);
        if (m->argument_count==1)
        {
            int *arrayint;
            double *arraydouble;
            int sizearray;
            get_var(m->argument[0],&type_var,&vi,&vd,&vc,&vcs,&sizearray,&arrayint,&arraydouble);

            switch (type_var)
            {
            case T_INT:
                sprintf(buffer2,"$SET_VAR,%d,%s,%d\n",type_var,m->argument[0],vi);
                break;
            case T_DOUBLE:
                printf("on renvoi un double %f\n",vd);
                sprintf(buffer2,"$SET_VAR,%d,%s,%f\n",type_var,m->argument[0],vd);
                break;
            case T_CHAR:
                sprintf(buffer2,"$SET_VAR,%d,%s,%c\n",type_var,m->argument[0],vc);
                break;
            case T_CHARSTAR:
                sprintf(buffer2,"$SET_VAR,%d,%s,%s\n",type_var,m->argument[0],vcs);
                break;
            case T_INT_ARRAY:

                send_int_array_all(m->argument[0],sizearray,arrayint);
                break;
            case T_DOUBLE_ARRAY:

                send_double_array_all(m->argument[0],sizearray,arraydouble);
                break;

            }
            switch (type_var)
            {
            case T_INT:
            case T_DOUBLE:
            case T_CHAR:
            case T_CHARSTAR:
                if (type_var!=-1)
                {
                    send_command_all(buffer2);

                }
                break;
            }

        }

    }
    else if (strcmp(m->command, "KILL_ALL") == 0) {
      if (_debug_proc || _debug_all) printf("Command is : %s\n", m->command);
        kill_all();
    }
    else if (strcmp(m->command, "ACTIVE_WATCHDOG") == 0) {
        pthread_t pt;
        thread_struct threadarg;
        threadarg.waitms = 1000;
        threadarg.socks=&socks;
        threadarg.watchdogdelay=5;
        threadarg.argument[0]=NULL;

        if (m->argument_count>=1)
        {
            threadarg.waitms = atoi(m->argument[0]);
        }
        if (m->argument_count>=2)
        {
            threadarg.watchdogdelay = atoi(m->argument[1]);
        }
        if (m->argument_count>=3)
        {
            int inc;
            strncpy(threadarg.exec,m->argument[2],127);
            threadarg.exec[127]='\0';
            threadarg.argument[0]=threadarg.exec;
            for (inc=3;inc<m->argument_count;inc++)
                threadarg.argument[inc-3]=m->argument[inc];
            threadarg.argument[inc-2]=NULL;
        }
        pthread_create(&pt,NULL,ping_thread,(void*)&threadarg);
        if (_debug_proc || _debug_all) printf("Command is : %s\n", m->command);
        //kill_all();
    }
    else if (strcmp(m->command, "PAUSE_ALL") == 0) {
      if (_debug_proc || _debug_all) printf("Command is : %s\n", m->command);
        pause_all();
        printf("on ne s'est pas endormi\n");
    }
    else if (strcmp(m->command, "UNPAUSE_ALL") == 0) {
      if (_debug_proc || _debug_all) printf("Command is : %s\n", m->command);
        unpause_all();
    }
    else if (strcmp(m->command, "START_US") == 0) { // STARTS_US */
      if (_debug_us || _debug_all) printf("Command is : %s\n", m->command);
        // Read us according to param
        if (m->argument_count == 1) {
            if (!buffering)
                connection_file = fdopen(socks, "a");

            us_index = strtol(m->argument[0], 0, 0 );
            if (_debug_us || _debug_all) printf( "Start us %d\n", us_index);
            _us_mask |= khepera3_ultrasound_getsensorbitbysensor( (enum eKhepera3SensorsUltrasound) us_index );
            //pthread_mutex_lock (&_us_mutex);
            pthread_mutex_lock(&_i2c_mutex);
            result = khepera3_ultrasound_enable( (enum eKhepera3SensorsUltrasoundBit)_us_mask );
            pthread_mutex_unlock(&_i2c_mutex);
            //pthread_mutex_unlock (&_us_mutex);
            if ( result == -1) {
                _us_enabled[us_index] = 1;
                if (!buffering)
                {
                    send(socks, "$AKN\n", 5, MSG_NOSIGNAL);
                }
                else
                {
                    sprintf(buffer,"$AKN\n");
                }
                if (_debug_us || _debug_all) printf("Sent $AKN information\n");
            }
            else {
                if (!buffering)
                {
                    send(socks, "$FAILED\n", 8, MSG_NOSIGNAL);
                }
                {
                    sprintf(buffer,"$FAILED\n");
                }
                if (_debug_us || _debug_all) printf("Sent $FAILED information\n");
            }
        }
        else {
            if (!buffering)
            {
                send(socks, "$PARAM\n", 7, MSG_NOSIGNAL);
            }
            else
            {
                sprintf(buffer,"$PARAM\n");
            }

            if (_debug_us || _debug_all) printf("Sent $PARAM information\n");
        }
    }
    else if (strcmp(m->command, "STOP_US") == 0) { // STOP_US */
      if (_debug_us || _debug_all) printf("Command is : %s\n", m->command);
        // Read us according to param
        if (m->argument_count == 1) {
            if (!buffering)
                connection_file = fdopen(socks, "a");

            us_index = strtol(m->argument[0], 0, 0 );
            if (_debug_us || _debug_all) printf( "Start us %d\n", us_index);
            // MASK = MASK AND (NOT SENSOR_MASK)
            _us_mask &= (~khepera3_ultrasound_getsensorbitbysensor( (enum eKhepera3SensorsUltrasound) us_index ));
            //pthread_mutex_lock (&_us_mutex);
            pthread_mutex_lock(&_i2c_mutex);
            result = khepera3_ultrasound_enable( (enum eKhepera3SensorsUltrasoundBit)_us_mask );
            pthread_mutex_unlock(&_i2c_mutex);
            //pthread_mutex_unlock (&_us_mutex);
            if ( result == -1) {
                _us_enabled[us_index] = 0;
                if (!buffering)
                {
                    send(socks, "$AKN\n", 5, MSG_NOSIGNAL);
                }
                else
                {
                    sprintf(buffer,"$AKN\n");
                }
                if (_debug_us || _debug_all) printf("Sent $AKN information\n");
            }
            else {
                if (!buffering)
                {
                    send(socks, "$FAILED\n", 8, MSG_NOSIGNAL);
                }
                {
                    sprintf(buffer,"$FAILED\n");
                }
                if (_debug_us || _debug_all) printf("Sent $FAILED information\n");
            }
        }
        else {
            if (!buffering)
            {
                send(socks, "$PARAM\n", 7, MSG_NOSIGNAL);
            }
            else
            {
                sprintf(buffer,"$PARAM\n");
            }

            if (_debug_us || _debug_all) printf("Sent $PARAM information\n");
        }
    }
    else if (strcmp(m->command, "US_MAX_ECHO") == 0) { // US_MAX_ECHO */
      if (_debug_us || _debug_all) printf("Command is : %s\n", m->command);
        // Read param
        if (m->argument_count == 1) {
            if (!buffering)
                connection_file = fdopen(socks, "a");
            nb_echo = strtol(m->argument[0], 0, 0 );
            if (_debug_us || _debug_all) printf( "Set nb echo %d\n", nb_echo);
            //pthread_mutex_lock (&_us_mutex);
            pthread_mutex_lock(&_i2c_mutex);
            result = khepera3_ultrasound_set_max_echo_number( nb_echo);
            pthread_mutex_unlock(&_i2c_mutex);
            //pthread_mutex_unlock (&_us_mutex);
            if ( result == -1) {
                if (!buffering)
                {
                    send(socks, "$AKN\n", 5, MSG_NOSIGNAL);
                }
                else
                {
                    sprintf(buffer,"$AKN\n");
                }
                if (_debug_us || _debug_all) printf("Sent $AKN information\n");
            }
            else {
                if (!buffering)
                {
                    send(socks, "$FAILED\n", 8, MSG_NOSIGNAL);
                }
                {
                    sprintf(buffer,"$FAILED\n");
                }
                if (_debug_us || _debug_all) printf("Sent $FAILED information\n");
            }
        }
        else {
            if (!buffering)
            {
                send(socks, "$PARAM\n", 7, MSG_NOSIGNAL);
            }
            else
            {
                sprintf(buffer,"$PARAM\n");
            }

            if (_debug_us || _debug_all) printf("Sent $PARAM information\n");
        }
    }
    else if (strcmp(m->command, "PING") == 0)
    {
        if (!buffering)
        {
            connection_file = fdopen(socks, "a");

            fprintf(connection_file, "$PONG");
            fprintf(connection_file, "\n");
            fflush(connection_file);
        }
        else
        {
            sprintf(buffer, "$PONG");
            sprintf(buffer, "%s\n",buffer);
        }
        if (_debug_proc || _debug_all) printf("Pong\n");

    }
    else if (strcmp(m->command, "PONG") == 0)
    {
        struct timeval tv;
        gettimeofday(&tv,NULL);
        last_time_pong[socks]=tv.tv_sec;
        /*if(!buffering)
        {
        connection_file = fdopen(socks, "a");

        fprintf(connection_file, "$AKN);
        fprintf(connection_file, "\n");
        fflush(connection_file);
        }
        else
        {
        sprintf(buffer, "$PONG");
        sprintf(buffer, "%s\n",buffer);
        }*/
        if (_debug_proc || _debug_all) printf("Pong recv\n");

    }
    /* else if (strcmp(m->command, "HELP") == 0) {	// $IR *\/ */
    /*    if(_debug) printf("Command is : %s\n", m->command); */
    /*     connection_file = fdopen(socks, "a"); */
    /*     fprintf(connection_file, "$HELP"); */
    /*     fprintf(connection_file, ",SET_SPEED left right / GET_SPEED"); */
    /*     fprintf(connection_file, ",SET_POS left right / "); */
    /*     fprintf(connection_file, ",GOTO_POS left right"); */
    /*     fprintf(connection_file, ",GET_POS"); */
    /*     fprintf(connection_file, ",STOP"); */
    /*     fprintf(connection_file, ",START"); */
    /*     fprintf(connection_file, ",GET_PROXI"); */
    /*     fprintf(connection_file, ",GET_AMB"); */
    /*     fprintf(connection_file, ",GET_US param"); */
    /*     fprintf(connection_file, ",GET_BAT param"); */
    /*     fprintf(connection_file, ",HELP"); */
    /*     fprintf(connection_file, "\n"); */
    /*     fflush(connection_file); */
    /*    if(_debug) printf("Sent $HELP information\n"); */
    /*  } */
    else {
        //connection_file = fdopen(socks, "a");
        //fprintf(connection_file, "$ACK");
        //fprintf(connection_file, "\n");
        if (!buffering)
        {
            send(socks, "$UKN\n", 5, MSG_NOSIGNAL);
        }
        else
        {
            sprintf(buffer,"$UKN\n");
        }
        if (_debug) printf("Sent $UKN information\n");
    }
    if (strlen(buffer)!=0 && buffering)
    {

        send(socks,buffer,strlen(buffer) ,MSG_NOSIGNAL);
        //printf("%s\n",buffer);
    }
    fflush(NULL);
    //free(buffer);
}

// Executes the main loop.
void tcp_server_run(int port) {
    // Set up the listener socket
    tcp_nmea_init(&tcp_server_nmea_receive_hook);
    tcp_nmea_start(port);

    // Put the wheels in normal (control) mode
    //khepera3_drive_start();

    while (1) {
        // Check if new data from is available on the TCP port and process it (this will call tcp_server_nmea_receive_hook)
        tcp_nmea_receive();

        // Sleep for a while
        //        printf("while...\n");
        //usleep(100);
    }
}
void traiter_signal(int numero_signal)
{
    switch (numero_signal)
    {

    case SIGTERM:
    case SIGKILL:
    default:
        kill_all();
        sleep(1);
        exit(0);

        break;


    }
}
// Main program.
int main(int argc, char *argv[]) {
    int port;
    pthread_t thread;
    int ret;
    struct sigaction a;

    a.sa_handler = traiter_signal;      /* fonction à lancer */
    sigemptyset(&a.sa_mask);        /* rien à masquer */

    //	sigaction(SIGTSTP, &a, NULL);   /* pause contrôle-Z */
    sigaction(SIGTERM, &a, NULL);
    //sigaction(SIGKILL, &a, NULL);
    sigaction(SIGINT,&a,NULL);
    // Command line parsing
    commandline_init();
    commandline_parse(argc, argv);

    // Help
    if (commandline_option_provided("-h", "--help")) {
        help();
        exit(1);
    }

    // Initialization
    //khepera3_status_init();

    // Read arguments
    port = commandline_option_value_int("-p", "--port", 3000);
    _origs = new_remoteserver("localhost",port,0);
    if (commandline_option_provided("-b", "--battery")) {
        _update_battery_flag = 1;
    }
    else {
        _update_battery_flag = 0;
    }
    if (commandline_option_provided("-u", "--ultrasound")) {
        _update_us_flag = 1;
    }
    else {
        _update_us_flag = 0;
    }
    if (commandline_option_provided("-k", "--khepera")) {
      if (_debug || _debug_all) printf("Initialize khepera3 library\n");
        // init
        khepera3_status_init();
	behave_init();
        // run update
        if ( (ret = pthread_create( &thread, NULL, update_run, (void *) 0)) != 0) {
            fprintf( stderr, "thread udpdate_run : %s", strerror(ret));
            exit(1);
        }
    }
    else {
        _update_battery_flag = 1;
        // run update
        khepera3_status_init_fake();
	behave_init();

        if ( (ret = pthread_create( &thread, NULL, update_run_fake, (void *) 0)) != 0) {
            fprintf( stderr, "thread udpdate_run : %s", strerror(ret));
            exit(1);
        }
    }

    // Run server


///TODO FIX: à coder pas en dure, charger un fichier
    /*remoteserver *s = new_remoteserver("192.168.0.1",3000,1);
    					remoteserver *s1 = new_remoteserver("192.168.0.3",3000,1);
    					remoteserver *s2 = new_remoteserver("192.168.0.6",3000,1);
    					open_connection(s);
    					open_connection(s1);
    					open_connection(s2);
    					register_remoteserver(s);
    					register_remoteserver(s1);
    					register_remoteserver(s2);*/
    load_file_server("config_server",_origs);
//getchar();
    tcp_server_run(port);
    return 0;
}


