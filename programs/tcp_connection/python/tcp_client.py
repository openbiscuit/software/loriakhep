#!/usr/bin/env python
# -*- coding: utf-8 -*-
import telnetlib
import socket
import sys

#TODO: tester avec un vrai robot !

class TcpClient:
  """ tcp_connection API (see tcp_server.c)
  """
  def __init__(self,host,port=3000):
    try:
      self.tn=telnetlib.Telnet(host,port)
    except socket.error, msg:
      # happens all the time, catch it
      # => error message will be easier to read
      print " *** TcpClient - Cannot connect: ",msg
      sys.exit()

  def send_and_receive(self,cmd):
    self.tn.write(cmd+"\n")
    r=self.tn.read_until("\n")
    return r.strip()

  def parse_nmea(self,s):
    """ return: key,[params]
	where key looks like $KEY
    """
    s=s.strip().split(",")
    if (len(s)==0):
      print " *** TcpClient - protocol or transmission error"
      sys.exit()
    k=s[0]
    param=s[1:]
    return k,param

  def check_akn(self,r):
    """ helper/sanity check"""
    if (r!="$AKN"):
      print " *** TcpClient - protocol or transmission error",r," != AKN"
      sys.exit()

  def set_speed(self,l,r):
    r=self.send_and_receive("$SET_SPEED,%d,%d"%(l,r))
    self.check_akn(r)

  def speed_control(self,l,r):
    r=self.send_and_receive("$SPEED_CONTROL,%d,%d"%(l,r))
    self.check_akn(r)

  def get_all(self):
    pass
    # TODO: fixme

  def goto_pos(self,l,r):
    r=self.send_and_receive("$GOTO_POS,%d,%d"%(l,r))
    self.check_akn(r)
  
  def goto_pos_control(self,l,r):
    r=self.send_and_receive("$GOTO_POS_CONTROL,%d,%d"%(l,r))
    self.check_akn(r)
  
  def set_pos(self,l,r):
    r=self.send_and_receive("$SET_POS,%d,%d"%(l,r))
    self.check_akn(r)
  
  def stop(self):
    r=self.send_and_receive("$STOP")
    self.check_akn(r)

  def start(self):
    r=self.send_and_receive("$START")
    self.check_akn(r)

  def idle(self):
    r=self.send_and_receive("$IDLE")
    self.check_akn(r)

  def get_proxi(self):
    r=self.send_and_receive("$GET_PROXI")
    k,p=self.parse_nmea(r)
    return map(int,p[:11]),int(p[11])

  def get_amb(self):
    r=self.send_and_receive("$GET_AMB")
    k,p=self.parse_nmea(r)
    return map(int,p[:11]),int(p[11])

  def get_pos(self):
    r=self.send_and_receive("$GET_POS")
    k,p=self.parse_nmea(r)
    return map(int,p)

  def get_speed(self):
    r=self.send_and_receive("$GET_SPEED")
    k,p=self.parse_nmea(r)
    return map(int,p)

  def reset_odometry(self):
    r=self.send_and_receive("$RESET_ODOMETRY")
    k,p=self.parse_nmea(r)
    return map(int,p)

  def get_odometry(self):
    r=self.send_and_receive("$GET_ODOMETRY")
    k,p=self.parse_nmea(r)
    return map(int,p)

  def get_bat(self,key):
    "key=enum (volt,cur,avg_c,cap,t,cap_re)"
    r=self.send_and_receive("$GET_BAT,%d"%key)
    k,p=self.parse_nmea(r)
    return int(p[0])

  def get_us(self,index):
    r=self.send_and_receive("$GET_US,%d"%index)
    k,p=self.parse_nmea(r)
    if (k=="$DISABLED"):
      return []
    else:
      return [ (p[i*3+1],p[i*3+2],p[i*3+3]) for i in range(nb_echo)]

  def start_us(self,index):
    r=self.send_and_receive("$START_US,%d"%index)
    if r=="$FAILED":
      print " *** could not start US",index

  def stop_us(self,index):
    r=self.send_and_receive("$STOP_US,%d"%index)
    if r=="$FAILED":
      print " *** could not stop US",index

  def us_max_echo(self,nbmax):
    r=self.send_and_receive("$US_MAX_ECHO,%d"%nbmax)
    if r=="$FAILED":
      print " *** could not set US_MAX_ECHO"

if __name__=="__main__":
  t=TcpClient("127.0.0.1")
  t.set_speed(1000,1000)
  print t.get_proxi()
  print t.get_amb()
  print t.get_pos()
  print t.get_bat(0)
  t.start_us(0)
  print t.get_us(0)
