#include "remote_variable.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

pthread_mutex_t whiteboard_mutex= PTHREAD_MUTEX_INITIALIZER;
int count_var=0;



remotevariable rv[MAX_VAR_NUM];

// Prints the help text.

void add_var(char *name,int type,int vi,double vd,char vc,char* vcs)
{
    int i;
    pthread_mutex_lock(&whiteboard_mutex);
    strncpy(rv[count_var].name, name,32);;

    for (i=0;i<count_var;i++)
    {
        if ((!strcmp(rv[i].name,name) ))
        {
            switch (rv[i].type)
            {
            case T_INT_ARRAY:
                free(rv[i].val_intarray);
                break;
            case T_DOUBLE_ARRAY:
                free(rv[i].val_doublearray);
                break;
            case T_CHARSTAR:
                free(rv[i].val_charstar);
                break;

            }
            rv[i].type = type;
            switch (type)
            {
            case T_INT:
                rv[i].val_int = vi;
                break;
            default:
            case T_DOUBLE:
                rv[i].val_double=vd;

                break;
            case T_CHAR:
                rv[i].val_char=vc;
                break;
            case T_CHARSTAR:
                rv[i].val_charstar = (char*)malloc(sizeof(char)*strlen(vcs)+1);
                strcpy(rv[i].val_charstar,vcs);
                break;

            }
            pthread_mutex_unlock(&whiteboard_mutex);
            return;
        }
    }
    rv[count_var].type = type;
    switch (type)
    {
    case T_INT:
        rv[count_var].val_int = vi;
        break;
    case T_DOUBLE:
        rv[count_var].val_double=vd;

        break;
    case T_CHAR:
        rv[count_var].val_char=vc;
        break;
    case T_CHARSTAR:
        rv[count_var].val_charstar = (char*)malloc(sizeof(char)*strlen(vcs)+1);
        strcpy(rv[count_var].val_charstar,vcs);
        printf("on a setté une str a %s\n",rv[count_var].val_charstar);
        break;

    }
    count_var++;
    pthread_mutex_unlock(&whiteboard_mutex);
}

void add_array(char *name,int type,int size,int *vi,double *vd)
{
    int i;
    pthread_mutex_lock(&whiteboard_mutex);
    strncpy(rv[count_var].name, name,32);;

    for (i=0;i<count_var;i++)
    {
        if ((!strcmp(rv[i].name,name) ))
        {

            switch (rv[i].type)
            {

            case T_INT_ARRAY:
                free(rv[i].val_intarray);
                break;
            case T_DOUBLE_ARRAY:
                free((rv[i].val_doublearray));
                break;
            case T_CHARSTAR:
                free(rv[i].val_charstar);
                break;

            }
            rv[i].type = type;
            switch (type)
            {
            case T_INT_ARRAY:
                rv[i].size_array=size;
                rv[i].val_intarray = vi;
                break;
            case T_DOUBLE_ARRAY:
                rv[i].size_array=size;
                rv[i].val_doublearray=vd;

                break;

            }
            pthread_mutex_unlock(&whiteboard_mutex);
            return;
        }
    }
    rv[count_var].type = type;
    switch (type)
    {
    case T_INT_ARRAY:
        rv[count_var].size_array=size;
        rv[count_var].val_intarray = vi;
        break;
    case T_DOUBLE_ARRAY:
        rv[count_var].size_array=size;
        rv[count_var].val_doublearray=vd;

        break;

    }
    count_var++;
    pthread_mutex_unlock(&whiteboard_mutex);
}


void get_var(char *name,int *type,int *vi,double *vd,char *vc,char** vcs,int *arraysize,int **viarray,double **vdarray)
{
    int i=0;
    pthread_mutex_lock(&whiteboard_mutex);
    *type=-1;
    for (i=0;i<count_var;i++)
    {
        if (!strcmp(rv[i].name,name))
        {
            *type = rv[i].type;
            switch (rv[i].type)
            {
            case T_INT:
                *vi = rv[i].val_int;
                break;
            case T_DOUBLE:
                *vd=rv[i].val_double;

                break;
            case T_CHAR:
                *vc=rv[i].val_char;
                break;
            case T_CHARSTAR:
                *vcs=rv[i].val_charstar;
                break;
            case T_INT_ARRAY:
                *viarray=rv[i].val_intarray;
                *arraysize=rv[i].size_array;
                break;
            case T_DOUBLE_ARRAY:
                *vdarray=rv[i].val_doublearray;
                *arraysize=rv[i].size_array;
                break;
            }


        }

    }
    pthread_mutex_unlock(&whiteboard_mutex);

}
int get_int_var(char *name)
{
    int i=0;
    for (i=0;i<count_var;i++)
    {
        if (!strcmp(rv[i].name,name))
            if (rv[i].type ==T_INT)
                return i;
    }
    return -1;
}