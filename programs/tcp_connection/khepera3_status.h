
#ifndef __KHEPERA3_STATUS_H
#define __KHEPERA3_STATUS_H

#include <pthread.h>
#include "khepera3.h"
#include "odometry_track.h"
extern struct sOdometryTrack ot;


extern pthread_mutex_t _i2c_mutex;

/**
 * Flag for battery update in 'slow' update or not.
 */
extern int _update_battery_flag;
/**
 * Flag for ultrasound update in 'fast' update or not.
 */
extern int _update_us_flag;
/**
 * Mask for US
*/
extern unsigned char _us_mask;
/**
 * Array for enabled US
 */
extern int _us_enabled[cKhepera3SensorsUltrasound_Count];

/**
 * Init khepera module.
 */
void khepera3_status_init();
void khepera3_status_init_fake();

/**
 * Run update.
 * - every UP_WAIT : update_status_frequent()
 * - every UP_COUNT * UP_WAIT : update_status_slow()
 */
void * update_run(void * num);
void * update_run_fake(void * num);

#endif // __KHEPERA3_STATUS_H
