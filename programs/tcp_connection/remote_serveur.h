/**
 * \file remote_server.h
 * \brief communication function interserver
 * \author Nicolas Beaufort
 * \version 1.0
 *
 *
 *
 */



#ifndef REMOTE_SERVEUR_H
#define REMOTE_SERVEUR_H

#ifdef __cplusplus
extern "C" {
#endif

#include <pthread.h>

typedef struct remoteserver {
    int socket;
    int open;
    char *location;
    int port;
    int id;
    int type;
    pthread_mutex_t mutex;
    pthread_mutex_t readymutex;
    void (*ptr_connexion_func)(void *) ;
    void (*ptr_deconnexion_func)(void *) ;
    long int ptr_object;
}remoteserver;
void print_rs(remoteserver *s);

void lock_rs(remoteserver *s);
void unlock_rs(remoteserver *s);

//!return when the connection of s is open
/*!
\param s the server
 */
void wait_open(remoteserver *s);
//! Open connection to the server s
/*!
\param s the server
 */
int open_connection(remoteserver *s);
int receiv_command(remoteserver *s,char *buff,int size);
//! return 1 if the connection of the server s is open, 0 if not
/*!
\param s the server
 */
int is_open(remoteserver *s);
//! get an integer variable from a server
/*!
 	\param s the server
	\param var the nam of the variable
 */
int remote_server_get_int_var(remoteserver *s,char *var);
//! get a double variable from a server
/*!
\param s the server
\param var the nam of the variable
 */
double remote_server_get_double_var(remoteserver *s,char *var);
//! get a char* variable from a server
/*!
\param s the server
\param var the nam of the variable
 */
char* remote_server_get_str_var(remoteserver *s,char *var);
//! send command to a server s
/*!
\param s the server
\param command the command to send
\return the number of bytes send
 */
int send_command(remoteserver *s,const char *command);
//! send command to all register server
/*!
\param command the command to send
 */
int send_command_all(char *command);

//! close connection of a server s
/*!
\param s the server to close
 */
int close_connection(remoteserver *s);

//! load and connect all the server contain in a file
/*!
\param file file to parse
\return -1 if file does not exist, 1 if it's ok
 */
int load_file_server(char *file,remoteserver *origs);

//! init a remoteserver struct with a location , a port and an id
/*!
\param s the struct to init
\param location the network location of the server
\param port the port of the server
\param the local id of the server
 */
void init_remoteserver(remoteserver *s,char *location,int port,int id);
//! create and init a remoteserver struct with a location , a port and an id
/*!
\param location the network location of the server
\param port the port of the server
\param type the type of the server
\return the new struct
 */
remoteserver *new_remoteserver(char *location,int port,int type);
void init_remoteservertab();

//! register a server s for the "send_all" function
/*!
\param s the server
 */
int register_remoteserver(remoteserver *s);
//! unregister a server s witch have the identification id for the "send_all" function
/*!
\param id the identification of the server to delete
 */
void unregister_remoteserver(int id);
void remote_set_int_var(remoteserver *s,char *name,int v);
void remote_set_double_var(remoteserver *s,char *name,double v);
void remote_set_char_var(remoteserver *s,char *name,char v);
void remote_set_str_var(remoteserver *s,char *name,char* v);

int remote_server_get_int_array(remoteserver *s,const char *var,int *size,int **res);
int remote_server_get_double_array(remoteserver *s,char *var,int *size,double **res);
void remote_set_int_array(remoteserver *s,char *name ,int size,int *array);
void remote_set_double_array(remoteserver *s,char *name,int size,double *array);
void send_int_array_all(char *name,int size,int *tab);
void send_double_array_all(char *name,int size,double *tab);
void remote_broadcast(remoteserver *s,char *name);

#ifdef __cplusplus
}
#endif

#endif
