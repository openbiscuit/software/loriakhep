#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <fcntl.h>

#include "nmea.h"
#include "commandline.h"


struct sNMEAParser _client_snd_nmea_parser;
struct sNMEAParser _client_rcv_nmea_parser;
int _connect_filehandle;

// Prints the help text.
void
help() {
    printf("Opens a TCP port for connection to server and sends NMEA messages.");
    printf("\n");
    printf("Usage: tcp_client [OPTIONS]\n");
    printf("\n");
    printf("Options:\n");
    printf("  -p --port PORT     Connect to port PORT (default: 3000)\n");
    printf("  -a --address ADR   Connect to host ADR (default: 127.0.0.1)\n");
    printf("\n");
}
void
print_nmea(struct sNMEAMessage *msg)
{
    unsigned int i;

    printf( "%s with %d arguments\n", msg->command, msg->argument_count);
    for ( i=0; i< msg->argument_count; ++i) {
        printf( "  arg[%2.0d] = %s\n", i, msg->argument[i]);
    }
}


void
client_nmea_processed_hook( struct sNMEAMessage * msg, int withchecksum)
{
    char buffer[256];
    ssize_t nb_read;

    printf( "msg recognized\n");
    nmea_parser_send( &_client_snd_nmea_parser, msg);

    // check for answer
    printf("**** wait for answer *****\n");
    nb_read = 0;
    while ( nb_read <= 0 ) {
        nb_read = read( _connect_filehandle, buffer, 256 );
        printf( "read %d from connection\n", nb_read );
        if ( nb_read > 0 ) {
            printf("Received : %s\n", buffer);
            nmea_parser_process_data( &_client_rcv_nmea_parser, buffer, nb_read );
            printf("**** Answer ****\n");
            print_nmea( &(_client_rcv_nmea_parser.message) );
        }
        usleep(200000);
    }
    printf("**** no more answer *****\n");
}

void
client_nmea_send_hook(const char *buffer, int len)
{
    int nb_send = 0;
    char *p = (char*) buffer;
    int tosend = len;

    // printf("sended %s\n", data);fflush(stdout);
    while (tosend > 0)
    {
        nb_send = send(_connect_filehandle, p, (int)tosend, MSG_NOSIGNAL);
        //   printf("send %d\n", nb);fflush(stdout);
        if ( nb_send <= 0 )
            return;
        p += nb_send;
        tosend -= nb_send;
    }
}


int
tcp_connect( const char *address, int port, int *connect_filehandle)
{
    struct sockaddr_in serv_address;
    struct hostent *serv_host;
    int connect_res;

    // Don't touch anything if the port is zero
    if (port == 0) {
        return 0;
    }

    // Look for host
    serv_host = NULL;
    serv_host = gethostbyname(address);
    if ( serv_host == NULL ) {
        fprintf(stderr, "Unable to get server host.");
        return 0;
    }

    // Create Socket handle
    *connect_filehandle = socket( AF_INET, SOCK_STREAM, 0);
    if (*connect_filehandle < 0) {
        fprintf(stderr, "Unable to create socket: Error %d.\n", *connect_filehandle);
        *connect_filehandle = -1;
        return 0;
    }

    // Connect
    memset( &serv_address, 0x00, sizeof(serv_address));
    serv_address.sin_family = serv_host->h_addrtype;
    serv_address.sin_port = htons(port);
    memcpy( &serv_address.sin_addr, serv_host->h_addr, serv_host->h_length);
    connect_res = connect( *connect_filehandle,
                           (struct sockaddr *) &serv_address, sizeof(serv_address));

    if ( connect_res < 0 ) {
        fprintf(stderr, "Connect failed: Error %d.\n", connect_res);
        *connect_filehandle = -1;
        return 0;
    }

    // IMPORTANT : set fd as non blocking.
    fcntl(*connect_filehandle, F_SETFL, O_NONBLOCK);
    // Success!
    fprintf(stderr, "Connected on TCP port %d.\n", port);
    return 1;
}


int
main(int argc, char *argv[])
{
    int port;
    ssize_t nb_read;
    char * line = NULL;
    size_t len = 0;
    char * address = NULL;
    //char buffer[256];

    // Command line parsing
    commandline_init();
    commandline_parse(argc, argv);

    // Help
    if (commandline_option_provided("-h", "--help")) {
        help();
        exit(1);
    }
    // Read arguments
    port = commandline_option_value_int("-p", "--port", 3000);

    // Address arguments
    address = (char *) commandline_option_value("-a", "--address", "127.0.0.1");

    printf("// init rcv parser\n");
    nmea_parser_init( & _client_rcv_nmea_parser);

    printf("// init snd parser\n");
    nmea_parser_init( &_client_snd_nmea_parser);
    _client_snd_nmea_parser.hook_send = client_nmea_send_hook;
    _client_snd_nmea_parser.hook_process_message = client_nmea_processed_hook;
    printf("// open connection to %s\n", address);
    if ( tcp_connect( address, port, &_connect_filehandle) != 1 ) {
        fprintf(stderr, "Erreur dans la connection");
        return -1;
    }

    //keep sending msg
    printf("\\>");
    while ((nb_read = getline(&line, &len, stdin)) != -1) {
        printf("Retrieved line of length %zu :\n", nb_read);
        printf("%s", line);
        nmea_parser_process_data( &_client_snd_nmea_parser, line, nb_read );
        printf( "parser status = %d\n", _client_snd_nmea_parser.state );
        print_nmea( &(_client_snd_nmea_parser.message) );

        //nmea_parser_send( &_client_snd_nmea_parser, &(_client_snd_nmea_parser.message));
        printf("\\>");
    }

    return 0;
}
