#ifndef PROC_MGR_H
#define PROC_MGR_H
int new_proc(char *comm,char *argv[]);

void kill_all();

void stop_all();
void pause_all();
void kill_one(int pid);
void unpause_all();
void stop_one(int pid);
void pause_on(int pid);

void sig_all(int signal);
#endif