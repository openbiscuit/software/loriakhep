#include <stdio.h>
#include <stdlib.h>
#include "remote_variable_server.h"
#include <sys/time.h>
#define SIZE 1000000/4

int main(int argc,char *argv[])
{
    int rint;
    int n=SIZE;
    int i;
    float *f;
    float *r;
    int mode=0;
    int secdiff;
    int usdiff;
    int msdiff;
    struct timeval tv1,tv2;
    client_remote_var *cli;
if(argc>2)
cli = new_client_remote_var(argv[2],3003);
else
    cli = new_client_remote_var("192.168.0.45",3003);

    client_launch_thread(cli);
    printf("wait connecting...\n");
    client_wait_ready(cli);
    printf("connecting\n");

    if (argc >1)
        mode=atoi(argv[1]);


    int size;
    if (mode==0 || mode==1)
    {
        f=(float*)malloc(sizeof(float)*n);
        for (i=0;i<n;i++)
        {
            f[i]=(float)i+0.5;

        }
        gettimeofday(&tv1,NULL);
        remote_server_variable_set_int_var(cli,"totoint",20);
	remote_variable_server_broadcast(cli,"totoint"); 
        gettimeofday(&tv2,NULL);
        secdiff=tv2.tv_sec - tv1.tv_sec;
        usdiff = tv2.tv_usec - tv1.tv_usec;
        printf("secdiff : %d usecdiff : %d\n",secdiff,usdiff);
        usdiff/=1000;
        secdiff*=1000;
        msdiff=secdiff+usdiff;
        printf("sendtime 1 int: %d\n",msdiff);

        gettimeofday(&tv1,NULL);
        remote_server_variable_set_float_array(cli,"toto",n,f);
        gettimeofday(&tv2,NULL);
        secdiff=tv2.tv_sec - tv1.tv_sec;
        usdiff = tv2.tv_usec - tv1.tv_usec;
        printf("secdiff : %d usecdiff : %d\n",secdiff,usdiff);
        usdiff/=1000;
        secdiff*=1000;
        msdiff=secdiff+usdiff;
        printf("sendtime 128 float: %d\n",msdiff);
//remote_server_variable_set_float_array(cli,"toto2",n,f);
    }

    if (mode==0 || mode ==2)
    {
        do
        {
            gettimeofday(&tv1,NULL);
            remote_server_variable_get_float_array( cli,"toto",&size,&r);
            gettimeofday(&tv2,NULL);
            secdiff=tv2.tv_sec - tv1.tv_sec;
            usdiff = tv2.tv_usec - tv1.tv_usec;
            printf("secdiff : %d usecdiff : %d\n",secdiff,usdiff);
            usdiff/=1000;
            secdiff*=1000;
            msdiff=secdiff+usdiff;
            printf("gettime 128 float: %d\n",msdiff);
            gettimeofday(&tv1,NULL);
            rint=remote_server_variable_get_int_var( cli,"totoint");
            gettimeofday(&tv2,NULL);
            secdiff=tv2.tv_sec - tv1.tv_sec;
            usdiff = tv2.tv_usec - tv1.tv_usec;
            printf("secdiff : %d usecdiff : %d\n",secdiff,usdiff);
            usdiff/=1000;
            secdiff*=1000;
            msdiff=secdiff+usdiff;
            printf("gettime 1 int: %d\n",msdiff);
            usleep(10000);
        }
        while (r==NULL);
        printf("size == %d r : %x\n",size,r);
        /* for (i=0;i<size;i++)
         {
             if (mode ==2)
                 printf("%f\n",r[i]);
             else
                 printf("%f\n",r[i],f[i]);

         }*/
        printf("%d\n",rint);

    }
if(mode==3)
{
printf("totoint : %d\n",remote_server_variable_get_int_var( cli,"totoint"));

}
//while(1);

}