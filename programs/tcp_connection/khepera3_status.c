#include <stdio.h>
#include <unistd.h>
#include "khepera3_status.h"
#include "khepera3.h"
#include "khepera3_behave.h"
#include <time.h>
#define UP_COUNT 20
#define UP_WAIT 100
#define UP_BRAIT 1


pthread_mutex_t _i2c_mutex = PTHREAD_MUTEX_INITIALIZER;
/** Flag for battery */
int _update_battery_flag = 0;
/** Flag for us */
int _update_us_flag = 0;
/** Mask for US */
unsigned char _us_mask = 0;
/** Array for enables US */
int _us_enabled[cKhepera3SensorsUltrasound_Count];

/** Only for fake. */
int _nb_times = 0;

struct sOdometryTrack ot;
/** Mutex for IR */
//extern pthread_mutex_t _ir_mutex;
/** Mutex for MOTOR. */
//extern pthread_mutex_t _motor_mutex;
/** Mutex for BATTERY. */
//extern pthread_mutex_t _bat_mutex;
/** Mutex for ULTRASOUND. */
//extern pthread_mutex_t _us_mutex;

/**
 * Update fonction that will be called 'often'.
 * - khepera3_infrared_proximity();
 */
void
update_status_frequent()
{
    int i;

    // IR PROXY, IR AMBIANT ??
    //pthread_mutex_lock (&_ir_mutex);
    pthread_mutex_lock(&_i2c_mutex);
    khepera3_infrared_proximity();
    khepera3_infrared_ambient();
    odometry_track_step(&ot);
    pthread_mutex_unlock(&_i2c_mutex);
    //pthread_mutex_unlock (&_ir_mutex);

    // MOTOR
    //pthread_mutex_lock (&_motor_mutex);
    pthread_mutex_lock(&_i2c_mutex);
    /* khepera3_motor_get_current_position( &khepera3.motor_left); */
    /*   khepera3_motor_get_current_position( &khepera3.motor_right); */
    /*   khepera3_motor_get_current_speed( &khepera3.motor_left); */
    /*   khepera3_motor_get_current_speed( &khepera3.motor_right); */
    khepera3_drive_get_current_position();
    khepera3_drive_get_current_speed();
    khepera3_drive_get_current_torque();
    pthread_mutex_unlock(&_i2c_mutex);
    //pthread_mutex_unlock (&_motor_mutex);

    // US
    if ( _update_us_flag == 1 ) {
        //pthread_mutex_lock (&_us_mutex);

        for ( i=cKhepera3SensorsUltrasound_Begin;
                i<cKhepera3SensorsUltrasound_End;
                ++i ) {
            if ( _us_enabled[i] )
            {
                pthread_mutex_lock(&_i2c_mutex);
                khepera3_ultrasound( (enum eKhepera3SensorsUltrasound) i );
//std::cout << " " << khepera3.ultrasound.sensor[i].distance[0]<< "\n";
                pthread_mutex_unlock(&_i2c_mutex);
            }
        }
        //pthread_mutex_unlock (&_us_mutex);
    }

}
void
update_status_frequent_fake()
{
    int i;
    // IR PROXY
// pthread_mutex_lock (&_ir_mutex);
    for ( i = cKhepera3SensorsInfrared_Begin; i < cKhepera3SensorsInfrared_Count; ++i ) {
        khepera3.infrared_proximity.sensor[i] = i;
        khepera3.infrared_proximity.timestamp = _nb_times;
    }
    //pthread_mutex_unlock (&_ir_mutex);

    // MOTOR
    //pthread_mutex_lock (&_motor_mutex);
    khepera3.motor_left.current_speed = _nb_times;
    khepera3.motor_left.current_position += _nb_times;
    khepera3.motor_right.current_speed = _nb_times;
    khepera3.motor_right.current_position += _nb_times;
    //pthread_mutex_unlock (&_motor_mutex);

    // US not implemented
}

/**
 * Update function that will be called less 'often'.
 * - khepera3_infrared_ambient()
 */
void
update_status_slow()
{

    // BATTERY
    if ( _update_battery_flag ) {
        //pthread_mutex_lock (&_bat_mutex);
        pthread_mutex_lock(&_i2c_mutex);
        khepera3_battery_voltage();
 khepera3_battery_current();
        khepera3_battery_current_average();
        khepera3_battery_capacity_remaining_absolute();
        khepera3_battery_capacity_remaining_relative();
        khepera3_battery_temperature();
        pthread_mutex_unlock(&_i2c_mutex);
        //  pthread_mutex_unlock (&_bat_mutex);
    }
}
void
update_status_slow_fake()
{
    int i;
    // AMBIANTE IR
// pthread_mutex_lock (&_ir_mutex);
    for ( i = cKhepera3SensorsInfrared_Begin; i < cKhepera3SensorsInfrared_Count; ++i ) {
        khepera3.infrared_ambient.sensor[i] = i;
        khepera3.infrared_ambient.timestamp = _nb_times;
    }
// pthread_mutex_unlock (&_ir_mutex);
    // BATTERY
    //pthread_mutex_lock (&_bat_mutex);
    khepera3.battery.voltage = 13;
    khepera3.battery.current = 13;
    khepera3.battery.current_average = 13;
    khepera3.battery.capacity_remaining_absolute = 13;
    khepera3.battery.capacity_remaining_relative = 99;
    khepera3.battery.temperature = 37;
    //pthread_mutex_unlock (&_bat_mutex);
}

/**
 * Run update.
 * - every UP_WAIT : update_status_frequent()
 * - every UP_COUNT * UP_WAIT : update_status_slow()
 */
void *
update_run(void * num)
{
    int count = UP_COUNT;
    int brait_count = UP_BRAIT;
    clock_t start,end;
    float millisec;

    update_status_slow();
    while (1) {
        start=clock();
        update_status_frequent();
        if ( --count == 0 ) {
            update_status_slow();
            count = UP_COUNT;
	    
	    // Braitenberg ??
	    if( --brait_count == 0) {
	      braitenberg_correction();
	      brait_count = UP_BRAIT;
	    }
        }
        end=clock();
        millisec = (float)(end-start) / (CLOCKS_PER_SEC / 1000);
//printf("%f\n",millisec);
        usleep(UP_WAIT);
    }
}
void *
update_run_fake(void * num)
{
    int count = UP_COUNT;
    update_status_slow_fake();
    while (1) {
        ++_nb_times;
        update_status_frequent_fake();
        if ( --count == 0 ) {
            update_status_slow_fake();
            count = UP_COUNT;
        }
        usleep(UP_WAIT);
    }
}

/**
 * Init khepera module.
 */
void
khepera3_status_init()
{
    int i;

    khepera3_init();
    khepera3_drive_init();
    khepera3_battery_init();
    khepera3_ultrasound_init();
    odometry_track_start(&ot);
    // disable all US except front
    for ( i=cKhepera3SensorsUltrasound_Begin;
            i<cKhepera3SensorsUltrasound_End;
            ++i ) {
        _us_enabled[i] = 0;
    }
    // set up _us_mask in order to start US Front and FrontRight
    printf( "Start US--\n");
    _us_mask = cKhepera3SensorsUltrasoundBit_Front;
    _us_mask |= cKhepera3SensorsUltrasoundBit_FrontRight;
    pthread_mutex_lock (&_i2c_mutex);
    if ( khepera3_ultrasound_enable( (enum eKhepera3SensorsUltrasoundBit) _us_mask ) == -1) {
        _us_enabled[cKhepera3SensorsUltrasound_Front] = 1;
        _us_enabled[cKhepera3SensorsUltrasound_FrontRight] = 1;
    }
    pthread_mutex_unlock (&_i2c_mutex);
}
void
khepera3_status_init_fake()
{
    int i;

    // disable all US except front
    for ( i=cKhepera3SensorsUltrasound_Begin;
            i<cKhepera3SensorsUltrasound_End;
            ++i ) {
        _us_enabled[i] = 0;
    }
    _us_mask |= cKhepera3SensorsUltrasoundBit_Front;
    _us_enabled[cKhepera3SensorsUltrasoundBit_Front] = 1;
}
