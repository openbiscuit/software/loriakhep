/*!
  \example scan_and_plot.c

*/
/// pour l'utiliser ./scan
#include "urg_ctrl.h"
#include <stdio.h>
#include <stdlib.h>
#include "math.h"


static void urg_exit(urg_t *urg, const char *message)
{
  printf("%s: %s\n", message, urg_error(urg));
  urg_disconnect(urg);

  exit(1);
}


int main(int argc, char *argv[])
{
const char default_file[]="out";
char *out_file;
if(argc>=2)
{
out_file=argv[1];

}
else
{
out_file=default_file;

}
const char device[] = "/dev/ttyACM0"; /* For Linux */

  char buffer[128];
  long *data;
  urg_parameter_t parameter;
  int first, last;
  int ret;
  int n;
  int i;
  double x,y; 
  double rad;


  urg_t urg;

  ret = urg_connect(&urg, device, 115200); /// on se connecte au telemetre
  if (ret < 0) {
    urg_exit(&urg, "urg_connect()");
  }
  urg_parameters(&urg, &parameter);       ///on recupère les paramètres
  first = parameter.area_min_;
  last = parameter.area_max_;
data=(long*)malloc(sizeof(long)*(last-first)); /// on alloue la mémoire pour les données

  ret = urg_requestData(&urg, URG_GD, first, last);  ///on demande les données du télemetres
  if (ret < 0) {
    urg_exit(&urg, "urg_requestData()");
  }

  n = urg_receiveData(&urg, data, last-first);/// on les récupères
  if (n < 0) {
    urg_exit(&urg, "urg_receivePartialData()");
  }

  /* Display */
FILE *f=fopen(out_file,"w"); /// on écrit les données dans un fichier texte
  for (i = 0; i < n; ++i) {
    rad = urg_index2rad(&urg,i);
    x=data[i]*cos(rad);
    y=data[i]*sin(rad);
   fprintf(f,"%f: %f\n",x, y);
  }
fclose(f);
  free(data);
  urg_disconnect(&urg);


  return 0;
}
