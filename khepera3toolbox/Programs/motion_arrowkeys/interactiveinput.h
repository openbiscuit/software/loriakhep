/*!
 * (c) 2006-2007 EPFL, Lausanne, Switzerland
 * Thomas Lochmatter
 */

void interactiveinput_init();
int interactiveinput_waitkey(int timeout_usec);

