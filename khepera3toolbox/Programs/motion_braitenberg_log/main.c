/*!
 * (c) 2006-2008 EPFL, Lausanne, Switzerland
 * Thomas Lochmatter
 */

#include "khepera3.h"
#include "commandline.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#define MAXBUFFERSIZE 100
#define PI 3.14159265358979

// Algorithm parameters and results
struct sBraitenberg {
	struct {
		int left;
		int right;
	} speed;
	int repeat;
	int wait_us;
	int weight_left[11];
	int weight_right[11];
	float bias[11];
};

// Level of verbosity (0=quiet, 1=normal, 2=verbose, 3=very verbose, ...)
int verbosity;

// Prints the help text.
void help() {
	printf("Runs a Braitenberg obstacle avoidance algorithm, and passes infrared proximity values through a log function to linearze them.\n");
	printf("\n");
	printf("Usage: motion_braitenberg_log [OPTIONS] [ALGORITHM]\n");
	printf("\n");
	printf("Options:\n");
	printf("  --speed SPEED         Sets the forward speed (default: 10000)\n");
	printf("  --speed-left SPEED    Sets the left wheel forward speed (default: --speed)\n");
	printf("  --speed-right SPEED   Sets the right wheel forward speed (default: --speed)\n");
	printf("  -r --repeat N         Sets the execution time in number of steps (default: infinity)\n");
	printf("  -w --wait-us US       Sets the waiting time in the perception-to-action loop (default: 5000)\n");
	printf("  -v --verbosity V      Sets the verbosity level (0=quiet, 1=normal, 2=verbose, 3=very verbose, ...)\n");
	printf("\n");
	printf("Algorithms:\n");
	printf("  obstacle_avoidance (default)\n");
	printf("  wall_following_left\n");
	printf("  wall_following_right\n");
	printf("  object_following\n");
	printf("  line_following_white\n");
	printf("  line_following_black\n");
	printf("\n");
}

void braitenberg_set_obstacle_avoidance(struct sBraitenberg *braitenberg) {
	int i;

	braitenberg->weight_left[cKhepera3SensorsInfrared_BackLeft] = -2000;
	braitenberg->weight_left[cKhepera3SensorsInfrared_Left] = 2000;
	braitenberg->weight_left[cKhepera3SensorsInfrared_FrontSideLeft] = 4000;
	braitenberg->weight_left[cKhepera3SensorsInfrared_FrontLeft] = 6000;
	braitenberg->weight_left[cKhepera3SensorsInfrared_FrontRight] = -6000;
	braitenberg->weight_left[cKhepera3SensorsInfrared_FrontSideRight] = -4000;
	braitenberg->weight_left[cKhepera3SensorsInfrared_Right] = -2000;
	braitenberg->weight_left[cKhepera3SensorsInfrared_BackRight] = 2000;
	braitenberg->weight_left[cKhepera3SensorsInfrared_Back] = 0;
	braitenberg->weight_left[cKhepera3SensorsInfrared_FloorRight] = 0;
	braitenberg->weight_left[cKhepera3SensorsInfrared_FloorLeft] = 0;

	for (i = 0; i < 11; i++) {
		braitenberg->weight_right[i] = -braitenberg->weight_left[i];
		braitenberg->bias[i] = -5.0;
	}
}

void braitenberg_set_wall_following_left(struct sBraitenberg *braitenberg) {
	braitenberg_set_obstacle_avoidance(braitenberg);
	braitenberg->bias[cKhepera3SensorsInfrared_BackLeft] = -7.0;
	braitenberg->bias[cKhepera3SensorsInfrared_Left] = -8.0;
	braitenberg->bias[cKhepera3SensorsInfrared_FrontSideLeft] = -7.0;
}

void braitenberg_set_wall_following_right(struct sBraitenberg *braitenberg) {
	braitenberg_set_obstacle_avoidance(braitenberg);
	braitenberg->bias[cKhepera3SensorsInfrared_BackRight] = -7.0;
	braitenberg->bias[cKhepera3SensorsInfrared_Right] = -8.0;
	braitenberg->bias[cKhepera3SensorsInfrared_FrontSideRight] = -7.0;
}

void braitenberg_set_object_following(struct sBraitenberg *braitenberg) {
	int i;

	braitenberg_set_obstacle_avoidance(braitenberg);
	for (i = 0; i < 8; i++) {
		braitenberg->weight_left[i] = -braitenberg->weight_left[i] - 1000;
		braitenberg->weight_right[i] = -braitenberg->weight_right[i] - 1000;
	}
}

void braitenberg_set_line_following_white(struct sBraitenberg *braitenberg) {
	braitenberg_set_obstacle_avoidance(braitenberg);
	braitenberg->weight_left[cKhepera3SensorsInfrared_FloorLeft] = -100;
	braitenberg->weight_left[cKhepera3SensorsInfrared_FloorRight] = 100;
	braitenberg->weight_right[cKhepera3SensorsInfrared_FloorLeft] = 100;
	braitenberg->weight_right[cKhepera3SensorsInfrared_FloorRight] = -100;
}

void braitenberg_set_line_following_black(struct sBraitenberg *braitenberg) {
	braitenberg_set_obstacle_avoidance(braitenberg);
	braitenberg->weight_left[cKhepera3SensorsInfrared_FloorLeft] = 100;
	braitenberg->weight_left[cKhepera3SensorsInfrared_FloorRight] = -100;
	braitenberg->weight_right[cKhepera3SensorsInfrared_FloorLeft] = -100;
	braitenberg->weight_right[cKhepera3SensorsInfrared_FloorRight] = 100;
}

void braitenberg_init(struct sBraitenberg *braitenberg) {
	braitenberg_set_obstacle_avoidance(braitenberg);
	int speed = commandline_option_value_int("-s", "--speed", 10000);
	braitenberg->speed.left = commandline_option_value_int("-sl", "--speed-left", speed);
	braitenberg->speed.right = commandline_option_value_int("-sr", "--speed-right", speed);
	braitenberg->repeat = commandline_option_value_int("-r", "--repeat", 0);
	braitenberg->wait_us = commandline_option_value_int("-w", "--wait-us", 5000);
}

void braitenberg_run(struct sBraitenberg *braitenberg) {
	int i;
	float speed_left, speed_right;

	// Put the wheels in normal (control) mode
	khepera3_drive_start();

	while (1) {
		// Take the next measurement
		khepera3_infrared_proximity();

		// Sum all sensor values with the Braitenberg bias and weights
		speed_left = 0;
		speed_right = 0;
		for (i = 0; i < 11; i++) {
			float sensor_value = (float)khepera3.infrared_proximity.sensor[i];
			float value = log(sensor_value) + braitenberg->bias[i];
			speed_left += value * braitenberg->weight_left[i];
			speed_right += value * braitenberg->weight_right[i];
		}

		// Shift to be in the right order of magnitude
		//speed_left = speed_left >> 8;
		//speed_right = speed_right >> 8;

		// Add the bias speed
		speed_left += braitenberg->speed.left;
		speed_right += braitenberg->speed.right;

		// Set that speed
		int speed_left_int = (int)floorf(speed_left + 0.5);
		int speed_right_int = (int)floorf(speed_right + 0.5);
		khepera3_drive_set_speed(speed_left_int, speed_right);

		// Print values if necessary
		if (verbosity > 1) {
			printf("$IRPROXIMITY");
			for (i = 0; i < 11; i++) {
				printf(",%d", khepera3.infrared_proximity.sensor[i]);
			}
			printf("%d\n", khepera3.infrared_proximity.timestamp);
		}
		if (verbosity > 1) {
			printf("$SPEED,%d,%d\n", speed_left_int, speed_right_int);
		}

		// Quit if desired
		if (braitenberg->repeat > 0) {
			braitenberg->repeat--;
			if (braitenberg->repeat == 0) {
				break;
			}
		}

		// Sleep
		usleep(braitenberg->wait_us);
	}

	khepera3_drive_set_speed(0, 0);
}

// Main program.
int main(int argc, char *argv[]) {
	struct sBraitenberg braitenberg;
	const char *algorithm = "obstacle_avoidance";

	// Command line parsing
	commandline_init();
	commandline_parse(argc, argv);

	// Help
	if (commandline_option_provided("-h", "--help")) {
		help();
		exit(1);
	}

	// Initialization
	khepera3_init();
	braitenberg_init(&braitenberg);

	// Read arguments
	verbosity = commandline_option_value_int("-v", "--verbosity", 1);

	// Select the algorithm
	algorithm = commandline_argument(0, algorithm);
	if (strcmp(algorithm, "obstacle_avoidance") == 0) {
		braitenberg_set_obstacle_avoidance(&braitenberg);
	} else if (strcmp(algorithm, "wall_following_left") == 0) {
		braitenberg_set_wall_following_left(&braitenberg);
	} else if (strcmp(algorithm, "wall_following_right") == 0) {
		braitenberg_set_wall_following_right(&braitenberg);
	} else if (strcmp(algorithm, "object_following") == 0) {
		braitenberg_set_object_following(&braitenberg);
	} else if (strcmp(algorithm, "line_following_white") == 0) {
		braitenberg_set_line_following_white(&braitenberg);
	} else if (strcmp(algorithm, "line_following_black") == 0) {
		braitenberg_set_line_following_black(&braitenberg);
	}

	// Run the algorithm
	braitenberg_run(&braitenberg);
	return 0;
}
