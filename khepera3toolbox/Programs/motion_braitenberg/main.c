/*!
 * (c) 2006-2008 EPFL, Lausanne, Switzerland
 * Thomas Lochmatter
 */

#include "khepera3.h"
#include "commandline.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#define MAXBUFFERSIZE 100
#define PI 3.14159265358979

// Algorithm parameters and results
struct sBraitenberg {
	int speed;
	int wait_us;
	int weight_left[11];
	int weight_right[11];
	int bias[11];
};

// Level of verbosity (0=quiet, 1=normal, 2=verbose, 3=very verbose, ...)
int verbosity;

// Prints the help text.
void help() {
	printf("Runs a Braitenberg obstacle avoidance algorithm.\n");
	printf("\n");
	printf("Usage: motion_braitenberg [OPTIONS] [ALGORITHM]\n");
	printf("\n");
	printf("Options:\n");
	printf("  --speed SPEED         Sets the forward speed (default: 10000)\n");
	printf("  -w --wait-us US       Sets the waiting time in the perception-to-action loop (default: 5000)\n");
	printf("  -v --verbosity V      Sets the verbosity level (0=quiet, 1=normal, 2=verbose, 3=very verbose, ...)\n");
	printf("\n");
	printf("Algorithms:\n");
	printf("  obstacle_avoidance (default)\n");
	printf("  wall_following_left\n");
	printf("  wall_following_right\n");
	printf("  object_following\n");
	printf("  line_following_white\n");
	printf("  line_following_black\n");
	printf("\n");
}

void braitenberg_set_obstacle_avoidance(struct sBraitenberg *braitenberg) {
	int i;

	braitenberg->weight_left[cKhepera3SensorsInfrared_BackLeft] = -20;
	braitenberg->weight_left[cKhepera3SensorsInfrared_Left] = 20;
	braitenberg->weight_left[cKhepera3SensorsInfrared_FrontSideLeft] = 40;
	braitenberg->weight_left[cKhepera3SensorsInfrared_FrontLeft] = 60;
	braitenberg->weight_left[cKhepera3SensorsInfrared_FrontRight] = -60;
	braitenberg->weight_left[cKhepera3SensorsInfrared_FrontSideRight] = -40;
	braitenberg->weight_left[cKhepera3SensorsInfrared_Right] = -20;
	braitenberg->weight_left[cKhepera3SensorsInfrared_BackRight] = 20;
	braitenberg->weight_left[cKhepera3SensorsInfrared_Back] = 0;
	braitenberg->weight_left[cKhepera3SensorsInfrared_FloorRight] = 0;
	braitenberg->weight_left[cKhepera3SensorsInfrared_FloorLeft] = 0;

	for (i = 0; i < 11; i++) {
		braitenberg->weight_right[i] = -braitenberg->weight_left[i];
		braitenberg->bias[i] = 0;
	}
}

void braitenberg_set_wall_following_left(struct sBraitenberg *braitenberg) {
	braitenberg_set_obstacle_avoidance(braitenberg);
	braitenberg->bias[cKhepera3SensorsInfrared_BackLeft] = -250;
	braitenberg->bias[cKhepera3SensorsInfrared_Left] = -500;
	braitenberg->bias[cKhepera3SensorsInfrared_FrontSideLeft] = -250;
}

void braitenberg_set_wall_following_right(struct sBraitenberg *braitenberg) {
	braitenberg_set_obstacle_avoidance(braitenberg);
	braitenberg->bias[cKhepera3SensorsInfrared_BackRight] = -250;
	braitenberg->bias[cKhepera3SensorsInfrared_Right] = -500;
	braitenberg->bias[cKhepera3SensorsInfrared_FrontSideRight] = -250;
}

void braitenberg_set_object_following(struct sBraitenberg *braitenberg) {
	int i;

	braitenberg_set_obstacle_avoidance(braitenberg);
	for (i = 0; i < 8; i++) {
		braitenberg->weight_left[i] = -braitenberg->weight_left[i] - 10;
		braitenberg->weight_right[i] = -braitenberg->weight_right[i] - 10;
	}
}

void braitenberg_set_line_following_white(struct sBraitenberg *braitenberg) {
	braitenberg_set_obstacle_avoidance(braitenberg);
	braitenberg->weight_left[cKhepera3SensorsInfrared_FloorLeft] = -10;
	braitenberg->weight_left[cKhepera3SensorsInfrared_FloorRight] = 10;
	braitenberg->weight_right[cKhepera3SensorsInfrared_FloorLeft] = 10;
	braitenberg->weight_right[cKhepera3SensorsInfrared_FloorRight] = -10;
}

void braitenberg_set_line_following_black(struct sBraitenberg *braitenberg) {
	braitenberg_set_obstacle_avoidance(braitenberg);
	braitenberg->weight_left[cKhepera3SensorsInfrared_FloorLeft] = 10;
	braitenberg->weight_left[cKhepera3SensorsInfrared_FloorRight] = -10;
	braitenberg->weight_right[cKhepera3SensorsInfrared_FloorLeft] = -10;
	braitenberg->weight_right[cKhepera3SensorsInfrared_FloorRight] = 10;
}

void braitenberg_init(struct sBraitenberg *braitenberg) {
	braitenberg_set_obstacle_avoidance(braitenberg);
	braitenberg->speed = commandline_option_value_int("-s", "--speed", 10000);
	braitenberg->wait_us = commandline_option_value_int("-w", "--wait-us", 5000);
}

void braitenberg_run(struct sBraitenberg *braitenberg) {
	int i;
	int speed_left, speed_right;

	// Put the wheels in normal (control) mode
	khepera3_drive_start();

	while (1) {
		// Take the next measurement
		khepera3_infrared_proximity();

		// Sum all sensor values with the Braitenberg bias and weights
		speed_left = 0;
		speed_right = 0;
		for (i = 0; i < 11; i++) {
			speed_left += (khepera3.infrared_proximity.sensor[i] + braitenberg->bias[i]) * braitenberg->weight_left[i];
			speed_right += (khepera3.infrared_proximity.sensor[i] + braitenberg->bias[i]) * braitenberg->weight_right[i];
		}

		// Shift to be in the right order of magnitude
		//speed_left = speed_left >> 8;
		//speed_right = speed_right >> 8;

		// Add the bias speed
		speed_left += braitenberg->speed;
		speed_right += braitenberg->speed;

		// Set that speed
		khepera3_drive_set_speed(speed_left, speed_right);

		// Print values if necessary
		if (verbosity > 1) {
			printf("$IRPROXIMITY");
			for (i = 0; i < 11; i++) {
				printf(",%d", khepera3.infrared_proximity.sensor[i]);
			}
			printf("%d\n", khepera3.infrared_proximity.timestamp);
		}
		if (verbosity > 1) {
			printf("$SPEED,%d,%d\n", speed_left, speed_right);
		}

		// Sleep
		usleep(braitenberg->wait_us);
	}
}

// Main program.
int main(int argc, char *argv[]) {
	struct sBraitenberg braitenberg;
	const char *algorithm = "obstacle_avoidance";

	// Command line parsing
	commandline_init();
	commandline_parse(argc, argv);

	// Help
	if (commandline_option_provided("-h", "--help")) {
		help();
		exit(1);
	}

	// Initialization
	khepera3_init();
	braitenberg_init(&braitenberg);

	// Read arguments
	verbosity = commandline_option_value_int("-v", "--verbosity", 1);

	// Select the algorithm
	algorithm=commandline_argument(0, algorithm);
	if (strcmp(algorithm, "obstacle_avoidance") == 0) {
		braitenberg_set_obstacle_avoidance(&braitenberg);
	} else if (strcmp(algorithm, "wall_following_left") == 0) {
		braitenberg_set_wall_following_left(&braitenberg);
	} else if (strcmp(algorithm, "wall_following_right") == 0) {
		braitenberg_set_wall_following_right(&braitenberg);
	} else if (strcmp(algorithm, "object_following") == 0) {
		braitenberg_set_object_following(&braitenberg);
	} else if (strcmp(algorithm, "line_following_white") == 0) {
		braitenberg_set_line_following_white(&braitenberg);
	} else if (strcmp(algorithm, "line_following_black") == 0) {
		braitenberg_set_line_following_black(&braitenberg);
	}

	// Run the algorithm
	braitenberg_run(&braitenberg);
	return 0;
}
