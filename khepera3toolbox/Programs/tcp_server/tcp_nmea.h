#ifndef TCP_NMEA
#define TCP_NMEA

#include "nmea.h"

// Connection filehandle
extern int tcp_nmea_connection_filehandle;

// Initializes the module.
void tcp_nmea_init(void (*hook_process_message)(struct sNMEAMessage *m, int withchecksum));
// Opens a TCP listener socket.
int tcp_nmea_start(int port);
// Checks for incoming connections or incoming data.
void tcp_nmea_receive();

#endif
