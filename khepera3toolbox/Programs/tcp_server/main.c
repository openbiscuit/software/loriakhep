/*!
 * (c) 2006-2008 EPFL, Lausanne, Switzerland
 * Thomas Lochmatter
 */

#include "khepera3.h"
#include "commandline.h"
#include "tcp_nmea.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// Prints the help text.
void help() {
	printf("Opens a TCP port for listening and executes motor commands of a connected TCP client.\n");
	printf("\n");
	printf("Usage: tcp_server [OPTIONS]\n");
	printf("\n");
	printf("Options:\n");
	printf("  -p --port PORT     Listens on port PORT (default: 3000)\n");
	printf("\n");
}

// This method is called each time a NMEA message is received.
void tcp_server_nmea_receive_hook(struct sNMEAMessage *m, int withchecksum) {
	int i;
	FILE *connection_file;

	if (strcmp(m->command, "SPEED") == 0) {	// e.g. $SPEED,1000,1000
		// Set the speed according to the arguments
		if (m->argument_count >= 2) {
			khepera3_drive_set_speed(strtol(m->argument[0], 0, 0), strtol(m->argument[1], 0, 0));
		}
	} else if (strcmp(m->command, "IR") == 0) {	// $IR
		// Report the IR proximity values
		khepera3_infrared_proximity();
		connection_file = fdopen(tcp_nmea_connection_filehandle, "a");
		fprintf(connection_file, "$IR");
		for (i = 0; i < 11; i++) {
			fprintf(connection_file, ",%d", khepera3.infrared_proximity.sensor[i]);
		}
		fprintf(connection_file, "\n");
	}
}

// Executes the main loop.
void tcp_server_run(int port) {
	// Set up the listener socket
	tcp_nmea_init(&tcp_server_nmea_receive_hook);
	tcp_nmea_start(port);

	// Put the wheels in normal (control) mode
	khepera3_drive_start();

	while (1) {
		// Check if new data from is available on the TCP port and process it (this will call tcp_server_nmea_receive_hook)
		tcp_nmea_receive();

		// Sleep for a while
		usleep(20000);
	}
}

// Main program.
int main(int argc, char *argv[]) {
	int port;

	// Command line parsing
	commandline_init();
	commandline_parse(argc, argv);

	// Help
	if (commandline_option_provided("-h", "--help")) {
		help();
		exit(1);
	}

	// Initialization
	khepera3_init();

	// Read arguments
	port = commandline_option_value_int("-p", "--port", 3000);

	// Run server
	tcp_server_run(port);
	return 0;
}

