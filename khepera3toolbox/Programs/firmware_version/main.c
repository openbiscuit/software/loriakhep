/*!
 * (c) 2006-2008 EPFL, Lausanne, Switzerland
 * Thomas Lochmatter
 */

#include "khepera3.h"
#include "commandline.h"
#include <stdio.h>
#include <stdlib.h>

// Prints the help text.
void help() {
	printf("Prints the firmware version of the dsPIC and the two motor controller PICs.\n");
	printf("\n");
	printf("Usage: firmware_version\n");
	printf("\n");
}

// Main program.
int main(int argc, char *argv[]) {
	// General initialization
	khepera3_init();

	// Command line parsing
	commandline_init();
	commandline_parse(argc, argv);

	// Help
	if (commandline_option_provided("-h", "--help")) {
		help();
		exit(1);
	}

	// Read and display firmware version
	khepera3_dspic_firmware_version();
	khepera3_motor_firmware_version(&(khepera3.motor_left));
	khepera3_motor_firmware_version(&(khepera3.motor_right));
	printf("dsPIC: version %d, revision %d\n", khepera3.dspic.firmware_version, khepera3.dspic.firmware_revision);
	printf("Left motor: version %d, revision %d\n", (khepera3.motor_left.firmware_version >> 4) & 0xf, khepera3.motor_left.firmware_version & 0xf);
	printf("Right motor: version %d, revision %d\n", (khepera3.motor_right.firmware_version >> 4) & 0xf, khepera3.motor_right.firmware_version & 0xf);
	return 0;
}

