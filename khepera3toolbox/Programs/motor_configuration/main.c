/*!
 * (c) 2006-2008 EPFL, Lausanne, Switzerland
 * Thomas Lochmatter
 */

#include "khepera3.h"
#include "commandline.h"
#include <stdio.h>
#include <stdlib.h>

// Prints the help text.
void help() {
	printf("Prints all motor configuration registers.\n");
	printf("This program only allows to view the configuration, but not to alter it. To alter the configuration, have a look at the source code of the motor_initialize program.\n");
	printf("\n");
	printf("Usage: motor_configuration\n");
	printf("\n");
}

void dump_motor_configuration_register8(struct sKhepera3Motor *motor, const char *label, enum eKhepera3MotorRegister8 reg) {
	unsigned int value;
	int res;

	res = khepera3_motor_read_register8_p(motor, reg, &value);
	if (res) {
		printf("    %s (0x%x) = %u\n", label + 24, (int)reg, value);
	} else {
		printf("    %s (0x%x) = ERROR\n", label + 24, (int)reg);
	}
}

void dump_motor_configuration_register16(struct sKhepera3Motor *motor, const char *label, enum eKhepera3MotorRegister16 reg) {
	unsigned int value;
	int res;

	res = khepera3_motor_read_register16_p(motor, reg, &value);
	if (res) {
		printf("    %s (0x%x) = %u\n", label + 25, (int)reg, value);
	} else {
		printf("    %s (0x%x) = ERROR\n", label + 25, (int)reg);
	}
}

void dump_motor_configuration_register32(struct sKhepera3Motor *motor, const char *label, enum eKhepera3MotorRegister32 reg) {
	unsigned int value;
	int res;

	res = khepera3_motor_read_register32_p(motor, reg, &value);
	if (res) {
		printf("    %s (0x%x) = %u\n", label + 25, (int)reg, value);
	} else {
		printf("    %s (0x%x) = ERROR\n", label + 25, (int)reg);
	}
}

#define DUMP8(reg) dump_motor_configuration_register8(motor, #reg, reg)
#define DUMP16(reg) dump_motor_configuration_register16(motor, #reg, reg)
#define DUMP32(reg) dump_motor_configuration_register32(motor, #reg, reg)

void dump_motor_configuration(struct sKhepera3Motor *motor) {
	printf("  8 Bit Registers:\n");
	DUMP8(cKhepera3MotorRegister8_FirmwareVersion);
	DUMP8(cKhepera3MotorRegister8_Mode);
	DUMP8(cKhepera3MotorRegister8_SetpointSource);
	DUMP8(cKhepera3MotorRegister8_HardwareOptions);
	DUMP8(cKhepera3MotorRegister8_SoftwareOptions);
	DUMP8(cKhepera3MotorRegister8_ControlType);
	DUMP8(cKhepera3MotorRegister8_ErrorFlags);
	DUMP8(cKhepera3MotorRegister8_StatusFlags);
	DUMP8(cKhepera3MotorRegister8_Filter);
	DUMP8(cKhepera3MotorRegister8_BlockedTime);
	DUMP8(cKhepera3MotorRegister8_IntGenPeriod);
	DUMP8(cKhepera3MotorRegister8_IntGenAmplitude);
	DUMP8(cKhepera3MotorRegister8_IntGenOffset);
	DUMP8(cKhepera3MotorRegister8_Acceleration);
	DUMP8(cKhepera3MotorRegister8_StaticFriction);
	DUMP8(cKhepera3MotorRegister8_HWCurrentLimit);
	DUMP8(cKhepera3MotorRegister8_NearTargetMargin);
	DUMP8(cKhepera3MotorRegister8_Prescale);
	DUMP8(cKhepera3MotorRegister8_VelocityPrescaler);

	printf("  16 Bit Registers:\n");
	DUMP16(cKhepera3MotorRegister16_CurrentTorque);
	DUMP16(cKhepera3MotorRegister16_TorqueBias);
	DUMP16(cKhepera3MotorRegister16_KpSpeed);
	DUMP16(cKhepera3MotorRegister16_KdSpeed);
	DUMP16(cKhepera3MotorRegister16_KiSpeed);
	DUMP16(cKhepera3MotorRegister16_SampleTime);
	DUMP16(cKhepera3MotorRegister16_SoftwareCurrentLimit);
	DUMP16(cKhepera3MotorRegister16_MinSampleTime);
	DUMP16(cKhepera3MotorRegister16_PIDOut);
	DUMP16(cKhepera3MotorRegister16_KpPosition);
	DUMP16(cKhepera3MotorRegister16_KdPosition);
	DUMP16(cKhepera3MotorRegister16_KiPosition);
	DUMP16(cKhepera3MotorRegister16_KpTorque);
	DUMP16(cKhepera3MotorRegister16_KdTorque);
	DUMP16(cKhepera3MotorRegister16_KiTorque);
	DUMP16(cKhepera3MotorRegister16_MaxSpeed);
	DUMP16(cKhepera3MotorRegister16_SpeedMultiplier);

	printf("  32 Bit Registers:\n");
	DUMP32(cKhepera3MotorRegister32_Setpoint);
	DUMP32(cKhepera3MotorRegister32_CurrentPosition);
	DUMP32(cKhepera3MotorRegister32_CurrentSpeed);
	DUMP32(cKhepera3MotorRegister32_SoftStopMin);
	DUMP32(cKhepera3MotorRegister32_SoftStopMax);
}

// Main program.
int main(int argc, char *argv[]) {
	// Command line parsing
	commandline_init();
	commandline_parse(argc, argv);

	// Help
	if (commandline_option_provided("-h", "--help")) {
		help();
		exit(1);
	}

	// Initialization
	khepera3_init();

	// Dump the configuration
	printf("Left motor:\n");
	dump_motor_configuration(&(khepera3.motor_left));
	printf("Right motor:\n");
	dump_motor_configuration(&(khepera3.motor_right));

	return 0;
}
