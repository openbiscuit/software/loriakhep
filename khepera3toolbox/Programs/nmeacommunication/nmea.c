#include "nmea.h"
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int nmea_initialize(int port) {
	struct sockaddr_in sa;
	struct hostent *host;
	int bindres, listenres;

	// Initialize
	nmea_server_filehandle=-1;
	nmea_connection_filehandle=-1;
	nmea_receive_buffer.bufferlen=0;
	nmea_receive_buffer.bufferreadpos=0;
	nmea_receive_message.state=NMEA_MESSAGE_STATE_START;

	// Don't touch anything if the port is not available
	if (port==0) {
		return 0;
	}

	// Create socket handle
	nmea_server_filehandle = socket(AF_INET, SOCK_STREAM, 0);
	if (nmea_server_filehandle<0) {
		nmea_server_filehandle=-1;
		return 0;
	}

	// Bind (on all local addresses)
	host=gethostbyname("0.0.0.0");
	memcpy((char *)&sa.sin_addr, (char *)host->h_addr, host->h_length);
	sa.sin_family=host->h_addrtype;
	sa.sin_port=htons(port);

	bindres=bind(nmea_server_filehandle, (struct sockaddr *)&sa, sizeof(sa));
	if (bindres<0) {
		if (bindres==EACCES) {
			fprintf(stderr, "Bind failed: You do not have enough access rights to listen on port %d.\n", port);
		} else {
			fprintf(stderr, "Bind failed: Error %d.\n", bindres);
		}
		nmea_server_filehandle=-1;
		return 0;
	}

	// Listen
	listenres=listen(nmea_server_filehandle, SOMAXCONN);
	if (listenres<0) {
		if (listenres==EADDRINUSE) {
			fprintf(stderr, "Listen failed: Port %d is already in use.\n", port);
		} else {
			fprintf(stderr, "Listen failed: Error %d.\n", listenres);
		}
		nmea_server_filehandle=-1;
		return 0;
	}

	// Set the important non-blocking flag
	fcntl(nmea_server_filehandle, F_SETFL, O_NONBLOCK);

	fprintf(stderr, "Listening on TCP port %d.\n", port);
	return 1;
}

int nmea_accept() {
	struct sockaddr_in sc;
	socklen_t sclen;
	int fh;

	// If there is no server, do nothing
	if (nmea_server_filehandle<0) {return 0;}

	// Accept
	sclen=sizeof(sc);
	fh=accept(nmea_server_filehandle, (struct sockaddr *)&sc, &sclen);
	if (fh<0) {
		if ((errno!=EINTR) && (errno!=EAGAIN)) {
			fprintf(stderr, "Accept failed (Error %d): connection request ignored.\n", errno);
		}
		return 0;
	}

	// Set non-blocking mode
	fcntl(fh, F_SETFL, O_NONBLOCK);

	// If we have a connection already, close it
	if (nmea_connection_filehandle>-1) {
		close(nmea_connection_filehandle);
		nmea_connection_filehandle=-1;
	}

	// Set the new connection
	fprintf(stderr, "Connection accepted.\n");
	nmea_connection_filehandle=fh;
	nmea_receive_message.state=NMEA_MESSAGE_STATE_START;
	return 1;
}

int nmea_receive_read() {
	int len;

	// Try reading a block
	nmea_receive_buffer.bufferlen=0;
	nmea_receive_buffer.bufferreadpos=0;
	len=read(nmea_connection_filehandle, nmea_receive_buffer.buffer, NMEA_RECEIVE_BUFFER_SIZE);

	// Upon error, close the connection
	if ((len<0) && (errno!=EINTR) && (errno!=EAGAIN)) {
		close(nmea_connection_filehandle);
		nmea_connection_filehandle=-1;
		return 0;
	}

	// Nothing available
	if (len<1) {return 0;}

	// Successfully read something
	nmea_receive_buffer.bufferlen=len;
	return 1;
}

int nmea_receive_parse() {
	while (nmea_receive_buffer.bufferreadpos<nmea_receive_buffer.bufferlen) {
		char inchar=nmea_receive_buffer.buffer[nmea_receive_buffer.bufferreadpos++];
		if (nmea_receive_message.state==NMEA_MESSAGE_STATE_START) {
			if (inchar=='$') {
				nmea_receive_message.state=NMEA_MESSAGE_STATE_BODY;
				nmea_receive_message.bufferwritepos=0;
				nmea_receive_message.checksum=0;
				nmea_receive_message.command=0;
				nmea_receive_message.nextarg=0;
			}
		} else if (nmea_receive_message.state==NMEA_MESSAGE_STATE_BODY) {
			if (inchar=='*') {
				nmea_receive_message.buffer[nmea_receive_message.bufferwritepos]=0;
				nmea_receive_message.state=NMEA_MESSAGE_STATE_CHECKSUM1;
			} else if ((inchar=='\n') || (inchar=='\r')) {
				nmea_receive_message.buffer[nmea_receive_message.bufferwritepos]=0;
				nmea_receive_message.state=NMEA_MESSAGE_STATE_COMPLETE;
				nmea_receive_message.nextarg=nmea_receive_message.buffer;
				nmea_receive_message.command=nmea_receive_message_getstring();
				return 1;
			} else {
				nmea_receive_message.buffer[nmea_receive_message.bufferwritepos++]=inchar;
				nmea_receive_message.checksum=nmea_receive_message.checksum ^ inchar;
			}
		} else if (nmea_receive_message.state==NMEA_MESSAGE_STATE_CHECKSUM1) {
			char ch="0123456789ABCDEF"[(nmea_receive_message.checksum >> 4) & 0xF];
			if ((inchar=='\n') || (inchar=='\r')) {
				nmea_receive_message.state=NMEA_MESSAGE_STATE_START;
			} else if (inchar==ch) {
				nmea_receive_message.state=NMEA_MESSAGE_STATE_CHECKSUM2;
			} else {
				nmea_receive_message.state=NMEA_MESSAGE_STATE_START;
			}
		} else if (nmea_receive_message.state==NMEA_MESSAGE_STATE_CHECKSUM2) {
			char ch="0123456789ABCDEF"[nmea_receive_message.checksum & 0xF];
			if (inchar==ch) {
				nmea_receive_message.state=NMEA_MESSAGE_STATE_COMPLETE;
				nmea_receive_message.nextarg=nmea_receive_message.buffer;
				nmea_receive_message.command=nmea_receive_message_getstring();
				return 1;
			}
			nmea_receive_message.state=NMEA_MESSAGE_STATE_START;
		}
	}
	
	// Message still incomplete
	return 0;
}

int nmea_receive() {
	// Check for new connections
	nmea_accept();

	// Clear the previous message
	if (nmea_receive_message.state==NMEA_MESSAGE_STATE_COMPLETE) {
		nmea_receive_message.state=NMEA_MESSAGE_STATE_START;
	}

	// Only go on if there is a connection
	if (nmea_connection_filehandle<0) {
		return 0;
	}

	// Read and parse until we have a message or until there is nothing available any more
	while (! nmea_receive_parse()) {
		if (! nmea_receive_read()) {return 0;}
	}

	return 1;
}

char *nmea_receive_message_command() {
	if (nmea_receive_message.state!=NMEA_MESSAGE_STATE_COMPLETE) {return 0;}
	return nmea_receive_message.command;
}

int nmea_receive_message_iscommand(char *cmd) {
	if (nmea_receive_message.state!=NMEA_MESSAGE_STATE_COMPLETE) {return 0;}
	return (strcmp(nmea_receive_message.command, cmd)==0);
}

char *nmea_receive_message_getstring() {
	char *str;

	// Return if the message is not complete yet
	if (nmea_receive_message.state!=NMEA_MESSAGE_STATE_COMPLETE) {return 0;}

	// Prepare next argument
	str=nmea_receive_message.nextarg;
	while (1) {
		if (nmea_receive_message.nextarg[0]==0) {break;}

		if (nmea_receive_message.nextarg[0]==',') {
			nmea_receive_message.nextarg[0]=0;
			nmea_receive_message.nextarg++;
			break;
		}

		nmea_receive_message.nextarg++;
	}

	// Return it
	return str;
}

int nmea_receive_message_getint() {
	char *arg;
	int val;

	arg=nmea_receive_message_getstring();
	if (! arg) {return 0;}
	val=(int)strtol(arg, NULL, 10);
	return val;
}

float nmea_receive_message_getfloat() {
	char *arg;
	float val;

	arg=nmea_receive_message_getstring();
	if (! arg) {return 0;}
	val=strtod(arg, NULL);
	return val;
}

int nmea_send(char *data, int len) {
	if (nmea_connection_filehandle<0) {return 0;}
	return write(nmea_connection_filehandle, data, len);
}

void nmea_send_message_new(tNMEASendMessage *sm, char *command) {
	sm->buffer[0]='$';
	strcpy(&(sm->buffer[1]), command);
	sm->bufferwritepos=strlen(command)+1;
}

void nmea_send_message_addstring(tNMEASendMessage *sm, char *value) {
	sm->buffer[sm->bufferwritepos++]=',';
	strcpy(&(sm->buffer[sm->bufferwritepos]), value);
	sm->bufferwritepos+=strlen(value);
}

void nmea_send_message_addint(tNMEASendMessage *sm, int value) {
	char str[32];
	sprintf(str, "%d", value);
	nmea_send_message_addstring(sm, str);
}

void nmea_send_message_addfloat(tNMEASendMessage *sm, float value) {
	char str[32];
	sprintf(str, "%f", value);
	nmea_send_message_addstring(sm, str);
}

int nmea_send_message(tNMEASendMessage *sm) {
	int n;
	int checksum;

	// Calculate checksum
	checksum=0;
	for (n=1; n<sm->bufferwritepos; n++) {
		checksum=checksum ^ sm->buffer[n];
	}
	sm->buffer[sm->bufferwritepos++]='*';
	sm->buffer[sm->bufferwritepos++]="0123456789ABCDEF"[(checksum >> 4) & 0xF];
	sm->buffer[sm->bufferwritepos++]="0123456789ABCDEF"[checksum & 0xF];
	sm->buffer[sm->bufferwritepos++]='\r';
	sm->buffer[sm->bufferwritepos++]='\n';

	// Send
	return nmea_send(sm->buffer, sm->bufferwritepos);
}
