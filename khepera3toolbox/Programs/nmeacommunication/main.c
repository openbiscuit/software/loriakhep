/*! 
 * \file
 *
 * \brief
 *
 * \author   Thomas Lochmatter
 *
 * \note     Copyright (C) 2006 EPFL
 */
 
#include <korebot/korebot.h>
#include <stdio.h>
#include <math.h>
#define PI 3.14159265358979
#define MAXBUFFERSIZE 100

#include "nmea.h"

static knet_dev_t * dsPic;

#define STATUS_IDLE 0
#define STATUS_OBSTACLE_FRONT 1
#define STATUS_OBSTACLE_BACK 2
#define STATUS_OBSTACLE_LEFT 3
#define STATUS_OBSTACLE_RIGHT 4
int status;
static char *status_text[5]={"IDLE", "OBSTACLE_FRONT", "OBSTACLE_BACK", "OBSTACLE_LEFT", "OBSTACLE_RIGHT"};

/*--------------------------------------------------------------------*/
/*! Prints the help text.
 */
void help() {
	printf("Tests some math functions\r\n");
}

/*--------------------------------------------------------------------*/
/*! Returns proximity light measures
 */
int getirproximity(unsigned int data[], unsigned int * counter) {
	int w=0;
	int r=1;
	char Buffer[MAXBUFFERSIZE];

	if (kh3_proximity_ir(Buffer, dsPic)==0) {
		return 0;
	}

	if (data) {
		while (w<9) {
			data[w]=(Buffer[r] | Buffer[r+1]<<8);
			w++;
			r+=2;
		}
	}

	if (counter) {
		*counter=((Buffer[19] | Buffer[20]<<8) | (Buffer[21] | Buffer[22]<<8)<<16);
	}

	return -1;
}

void set_status(int newstatus) {
	tNMEASendMessage sm;

	if (status==newstatus) {return;}

	status=newstatus;
	printf("new status %d\n", status);
	nmea_send_message_new(&sm, "STATUS");
	nmea_send_message_addstring(&sm, status_text[status]);
	nmea_send_message(&sm);
}

/*--------------------------------------------------------------------*/
/*! Main program.
 */
int run(int argc, char *argv[]) {
	unsigned int sensordata[9];
	unsigned int counter;
	unsigned int maxv;
	int maxi;
	int i;
	float x, y;

	nmea_initialize(3000);

	status=STATUS_IDLE;
	while (1) {
		getirproximity(sensordata, &counter);

		maxv=0;
		maxi=0;
		for (i=0; i<9; i++) {
			printf("%d ", sensordata[i]);
			if (sensordata[i]>maxv) {
				maxv=sensordata[i];
				maxi=i;
			}
		}
		printf(" => %d %d\n", maxv, maxi);

		if ((maxv>300) || ((status==STATUS_IDLE) && (maxv>200))) {
			if ((maxi<1) || (maxi>=7)) {
				set_status(STATUS_OBSTACLE_BACK);
			} else if (maxi<3) {
				set_status(STATUS_OBSTACLE_LEFT);
			} else if (maxi<5) {
				set_status(STATUS_OBSTACLE_FRONT);
			} else {
				set_status(STATUS_OBSTACLE_RIGHT);
			}
		} else {
			set_status(STATUS_IDLE);
		}

		while (nmea_receive()) {
			if (nmea_receive_message_iscommand("POSITION")) {
				x=nmea_receive_message_getfloat();
				y=nmea_receive_message_getfloat();
				printf("POS: %f %f\n", x, y);
			}
		}
		usleep(20000);
	}

	return 0;
}

/*--------------------------------------------------------------------*/
/*! Initialize and run the program.
 */
int main(int argc, char *argv[]) {
	// General initialization
	kh3_init();

	// Open dspic
	dsPic = knet_open("Khepera3:dsPic", KNET_BUS_I2C, 0, NULL);
	if (dsPic==0) {
		printf("dsPIC not found\r\n");
		return 255;
	}

	// Help
	if ((argc==2) && ((strcmp(argv[1], "help")==0) || (strcmp(argv[1], "-help")==0) || (strcmp(argv[1], "-h")==0) || (strcmp(argv[1], "--help")==0))) {
		help();
		return 0;
	}

	return run(argc, argv);
} 

