/*! 
 * \file
 *
 * \brief
 *
 * \author   Thomas Lochmatter
 *
 * \note     Copyright (C) 2007 EPFL
 */

#ifndef NMEA
#define NMEA

#define NMEA_MESSAGE_BUFFER_SIZE 1024
#define NMEA_RECEIVE_BUFFER_SIZE 256
#define NMEA_MESSAGE_STATE_START 0
#define NMEA_MESSAGE_STATE_BODY 1
#define NMEA_MESSAGE_STATE_CHECKSUM1 2
#define NMEA_MESSAGE_STATE_CHECKSUM2 3
#define NMEA_MESSAGE_STATE_COMPLETE 100

//! The structure holding an incoming message.
typedef struct sNMEAReceiveMessage {
	int state;								//!< The current parser state.
	char buffer[NMEA_MESSAGE_BUFFER_SIZE];	//!< The buffer holding unprocessed characters.
	int bufferwritepos;						//!< The current write position in the buffer.
	int checksum;							//!< The current checksum.
	char *command;							//!< Pointer to the command.
	char *nextarg;							//!< Pointer to the next argument.
} tNMEAReceiveMessage;

//! The receive buffer structure.
typedef struct sNMEAReceiveBuffer {
	char buffer[NMEA_RECEIVE_BUFFER_SIZE];	//!< The buffer holding unprocessed characters.
	int bufferlen;							//!< The number of available bytes.
	int bufferreadpos;						//!< The current read position in the buffer.
} tNMEAReceiveBuffer;

//! The structure holding a message to be sent.
typedef struct sNMEASendMessage {
	char buffer[NMEA_MESSAGE_BUFFER_SIZE];	//!< The buffer holding unprocessed characters.
	int bufferwritepos;						//!< The current write position in the buffer.
} tNMEASendMessage;

int nmea_server_filehandle;					//!< The filehandle of the listener socket.
int nmea_connection_filehandle;				//!< The filehandle of the connection. (Note that only one connection is accepted.)
tNMEAReceiveMessage nmea_receive_message;	//!< The currently receive message.
tNMEAReceiveBuffer nmea_receive_buffer;		//!< The receive buffer.

// Sockets
//! Initializes the module and binds a TCP listen port.
int nmea_initialize(int port);

// Receiving messages.
//! Processes incoming chars and returns true if a new message is complete.
int nmea_receive();
//! Returns the message command.
char *nmea_receive_message_command();
//! Returns the next argument as string.
char *nmea_receive_message_getstring();
//! Returns the next argument as integer.
int nmea_receive_message_getint();
//! Returns the next argument as double.
float nmea_receive_message_getfloat();

// Sending messages.
//! Starts a new send message.
void nmea_send_message_start(tNMEASendMessage *sm, char *command);
//! Adds a string parameter to the message.
void nmea_send_message_addstring(tNMEASendMessage *sm, char *value);
//! Adds an integer to the message.
void nmea_send_message_addint(tNMEASendMessage *sm, int value);
//! Adds an float to the message.
void nmea_send_message_addfloat(tNMEASendMessage *sm, float value);
//! Sends a message.
int nmea_send_message(tNMEASendMessage *sm);

#endif
