/*!
 * (c) 2006-2008 EPFL, Lausanne, Switzerland
 * Thomas Lochmatter
 */

#include "khepera3.h"
#include "commandline.h"
#include <stdio.h>
#include <math.h>
#define MAXBUFFERSIZE 100
#define PI 3.14159265358979

// Algorithm parameters and results
struct sRobotCentering {
	struct {
		int speed;
		int forward_coefficient;
	} parameters;
	struct {
		int precision;
		int turnangle;
		int forward_distance;
	} results;
};

// Level of verbosity (0=quiet, 1=normal, 2=verbose, 3=very verbose, ...)
int verbosity;

// Prints the help text.
void help() {
	printf("Centers the robot on a radial gradient drawn on the ground. The center of the gradient must be darker than the surrounding, and a dark circle sector must indicate the direction.\n");
	printf("\n");
	printf("Usage: robotcentering OPTIONS\n");
	printf("  -v --verbosity V   Sets the verbosity level (0=quiet, 1=normal, 2=verbose, 3=very verbose, ...)\n");
	printf("  --speed SPEED      Sets the speed (default: 10000)\n");
	printf("  -r --repeat R      Repeats the centering R times\n");
	printf("  --until-done       Repeats until the precision increases\n");
	printf("\n");
}

// Main program.
int main(int argc, char *argv[]) {
	struct sRobotCentering robotcentering;
	int repeat;
	int precision_prev;

	// Command line parsing
	commandline_init();
	commandline_parse(argc, argv);

	// Help
	if (commandline_option_provided("-h", "--help")) {
		help();
		exit(1);
	}

	// Initialization
	khepera3_init();

	// Read arguments
	robotcentering.parameters.speed=commandline_option_value_int("-s", "--speed", 1000);
	repeat=commandline_option_value_int("-r", "--repeat", 1);
	verbosity = commandline_option_value_int("-v", "--verbosity", 1);

	// Center the robot
	if (commandline_option_provided(0, "--until-done")) {
		// We run the algorithm as long as the result get better
		robotcentering_iteration(&robotcentering);
		robotcentering_reportresults(&robotcentering);
		precision_prev=robotcentering.results.precision;
		while (1) {
			robotcentering_iteration(&robotcentering);
			robotcentering_reportresults(&robotcentering);
			if (robotcentering.results.precision>precision_prev) {
				break;
			}

			precision_prev=robotcentering.results.precision;
		}
	} else {
		// We run the algorithm REPEAT times
		while (repeat>0) {
			robotcentering_iteration(&robotcentering);
			robotcentering_reportresults(&robotcentering);
			repeat--;
		}
	}

	return 0;
}

// TODO: adapt this function
int robotcentering_iteration(struct sRobotCentering robotcentering) {
	int i;
	int argi=0;
	int speed_fast=6000;
	int speed_slow=3000;
	int speed_veryslow=1000;
	int precision=4;
	int printvalues=1;
	int state=0;
	unsigned int sensordata[11];
	static const int floor_max=10000;
	float floor_left[floor_max];
	float floor_left_convolution;
	float floor_mean=0;
	int floor_integrate=16;
	float floor_threshold_down=2700*floor_integrate;
	float floor_threshold_up=3000*floor_integrate;
	int floor_count=0;
	int floor_w=0;
	int sync_down[2];
	int sync_up[2];
	int sync_motorpos[2];
	int sync_w;
	int data_start;
	int data_end;
	int data_zero;
	float data_wavelength;
	float x, y;
	float angle, amplitude2;
	int goal_motorpos;

	for (i=1; i<argc; i++) {
		if ((strcmp(argv[i], "-q")==0) || (strcmp(argv[i], "--quiet")==0)) {
			printvalues=0;
		} else if ((strcmp(argv[i], "-v")==0) || (strcmp(argv[i], "--verbose")==0)) {
			printvalues=2;
		} else if (argi==0) {
			speed_fast=atoi(argv[i]);
			argi++;
		} else if (argi==1) {
			precision=atoi(argv[i]);
			argi++;
		}
	}

	while (1) {
		// Turn
		kmot_SetPoint(mot1, kMotRegSpeed, speed_fast);
		kmot_SetPoint(mot2, kMotRegSpeed, speed_fast);

		// Fill in the first 4 values with zeros
		floor_w=0;
		floor_left_convolution=0;
		while (floor_w<floor_integrate) {
			floor_left[floor_w]=4000;
			floor_left_convolution+=floor_left[floor_w];
			floor_w++;
		}

		// Record all values until two syncs are found
		state=0;
		sync_w=0;
		while (1) {
			getirproximity(sensordata, 0);
			floor_left[floor_w]=(float)sensordata[9];

			floor_left_convolution+=floor_left[floor_w]-floor_left[floor_w-floor_integrate];
			if (state==0) {
				if (floor_left_convolution<floor_threshold_down) {
					state=1;
					sync_down[sync_w]=floor_w-1;
					sync_motorpos[sync_w]=kmot_GetMeasure(mot1, kMotMesPos)+kmot_GetMeasure(mot2, kMotMesPos);
					if (sync_w==1) {break;}
				}
			} else {
				if (floor_left_convolution>floor_threshold_up) {
					state=0;
					sync_up[sync_w]=floor_w-1;
					sync_w++;
				}
			}

			floor_w++;
			if (floor_w>=floor_max) {
				printf("Synchronization line not found.\n");
				return 1;
			}
		}

		// Switch to slow speed
		kmot_SetPoint(mot1, kMotRegSpeed, speed_slow);
		kmot_SetPoint(mot2, kMotRegSpeed, speed_slow);

		// Determine start and end of data
		data_start=sync_up[0]+30;
		data_end=sync_down[1]-70;
		data_wavelength=sync_down[1]-sync_down[0];
		data_zero=sync_down[0];

		// Calculate mean
		floor_mean=0;
		floor_count=0;
		for (i=data_start; i<data_end; i++) {
			floor_mean+=floor_left[i];
			floor_count++;
		}
		floor_mean/=floor_count;

		// Print sync values
		if (printvalues>0) {
			printf("Sync 0: %d %d (%d)\n", sync_down[0], sync_up[0], sync_motorpos[0]);
			printf("Sync 1: %d %d (%d)\n", sync_down[1], sync_up[1], sync_motorpos[1]);
			printf("Wavelength: %f samples\n", data_wavelength);
			printf("Mean: %f\n", floor_mean);
			if (printvalues>1) {
				printf("--- RECORDED SAMPLES ---\n");
				for (i=0; i<floor_w; i++) {
					printf("%d %f\n", i, floor_left[i]);
				}
				printf("---\n");
			}
		}

		// Extract phase
		x=0;
		y=0;
		for (i=data_start; i<data_end; i++) {
			angle=(float)(i-data_zero)/data_wavelength*2*PI;
			x+=cos(angle)*(floor_left[i]-floor_mean);
			y+=sin(angle)*(floor_left[i]-floor_mean);
		}
		if (printvalues) {printf("x=%f, y=%f\n", x, y);}

		// Calculate amplitude
		amplitude2=x*x+y*y;
		if (printvalues) {printf("amplitude2=%f\n", amplitude2);}
		if (amplitude2<10000) {
			printf("Finished!\n");
			return 0;
		}

		// Calculate goal angle
		angle=atan2(y, x)-10/180*PI;
		if (printvalues>0) {printf("angle=%f\n", angle/PI*180);}
		goal_motorpos=sync_motorpos[1]+(int)floorf((float)(sync_motorpos[1]-sync_motorpos[0])*angle/(2*PI)+0.5);
		if (printvalues>0) {printf("goal_motorpos=%d\n", goal_motorpos);}

		// Turn to this angle (TODO: check for overflow)
		while (kmot_GetMeasure(mot1, kMotMesPos)+kmot_GetMeasure(mot2, kMotMesPos)<goal_motorpos) {}
		kmot_SetPoint(mot1, kMotRegSpeed, -speed_slow);
		kmot_SetPoint(mot2, kMotRegSpeed, -speed_slow);
		while (kmot_GetMeasure(mot1, kMotMesPos)+kmot_GetMeasure(mot2, kMotMesPos)>goal_motorpos) {}

		// Move until the perceived brightness corresponds to the average brightness
		kmot_SetPoint(mot1, kMotRegSpeed, speed_veryslow);
		kmot_SetPoint(mot2, kMotRegSpeed, -speed_veryslow);
		/* while (1) {
			getirproximity(sensordata, 0);
			if (sensordata[9]<floor_mean) {
				break;
			}
		} */
		goal_motorpos=kmot_GetMeasure(mot1, kMotMesPos)+sqrt(amplitude2)*0.01;
		printf("goal_motorpos=%d\n", goal_motorpos);
		while (kmot_GetMeasure(mot1, kMotMesPos)<goal_motorpos) {}

		kmot_SetPoint(mot1, kMotRegSpeed, 0);
		kmot_SetPoint(mot2, kMotRegSpeed, 0);
		return 0;
	}

	return 0;
}
