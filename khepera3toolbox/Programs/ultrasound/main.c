/*!
 * (c) 2006-2008 EPFL, Lausanne, Switzerland
 * Thomas Lochmatter
 */

#include "khepera3.h"
#include "commandline.h"
#include "measurement.h"
#include <stdio.h>
#include <stdlib.h>

// Buffers
struct sMeasurement {
	int sample_number;
	struct sKhepera3SensorsUltrasoundSensor ultrasound_sensor;
};

#define LOG_SIZE 1024
struct sMeasurement log_buffer[LOG_SIZE];
unsigned int sample_number = 0;
unsigned int sensor_number;

// Prints the help text.
void help() {
	printf("Measures the ambient infrared light with the infrared sensors.\n");
	printf("\n");
	printf("Usage:\n");
	printf("  ultrasound S             Measures one set of values of sensor S (0..4)\n");
	printf("  ultrasound S -r [N]      Measures N sets of values (N=infinity if not provided)\n");
	printf("  ultrasound S -r N -f     Measures N sets of values and keeps them in memory before printing (for fast acquisition)\n");
	printf("  ultrasound S -r N -F     Measures N sets of values but does not print them\n");
	printf("  ultrasound S -w USEC     Waits USEC usec after each measurement\n");
	printf("\n");
	printf("Output format:\n");
	printf("  SAMPLE_NUMBER, TIMESTAMP, AMPLITUDE, DISTANCE [m]\n");
	printf("\n");
}

// Takes one measurement
void measurement_take(int i) {
	khepera3_ultrasound_p(&(log_buffer[i].ultrasound_sensor), sensor_number);
	log_buffer[i].sample_number = sample_number;
	sample_number++;
}

// Prints one measurement
void measurement_print(int i) {
	int s;
	for (s = 0; s < log_buffer[i].ultrasound_sensor.echos_count; s++) {
		printf("%d, %d, %d, %f\n",
		       log_buffer[i].sample_number,
		       log_buffer[i].ultrasound_sensor.timestamp[s],
		       log_buffer[i].ultrasound_sensor.amplitude[s],
		       (float)(log_buffer[i].ultrasound_sensor.distance[s])*khepera3.ultrasound.distance_per_increment
		      );
	}
}

// Main program.
int main(int argc, char *argv[]) {
	// Command line parsing
	commandline_init();
	measurement_init();
	measurement_commandline_prepare();
	commandline_parse(argc, argv);

	// Help
	if (commandline_option_provided("-h", "--help")) {
		help();
		exit(1);
	}

	// Initialization
	khepera3_init();

	// Read sensor number
	sensor_number = khepera3_ultrasound_getsensorbyname(commandline_argument(0, 0), -1);
	if ((sensor_number < 0) || (sensor_number > 4)) {
		help();
		exit(1);
	}

	// Take continuous measures
	measurement_configuration.log_size = LOG_SIZE;
	measurement_configuration.hook_measure = &measurement_take;
	measurement_configuration.hook_print = &measurement_print;
	measurement_start();

	return 0;
}
