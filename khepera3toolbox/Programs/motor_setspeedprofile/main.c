/*!
 * (c) 2006-2008 EPFL, Lausanne, Switzerland
 * Thomas Lochmatter
 */

#include "khepera3.h"
#include "commandline.h"
#include <stdio.h>
#include <stdlib.h>

// Prints the help text.
void help() {
	printf("Sets the speed profile for both motors.\r\n");
	printf("Usage: motor_setspeedprofile MAXSPEED ACCELERATION\r\n");
	printf("Note: The motor initialization program (./motor_initialize) sets the speed profile to 10000 128.\r\n");
}

// Sets the speed profile of one motor.
int set_speed_profile(struct sKhepera3Motor *motor, int maxspeed, int acceleration) {
	int res = -1;
	res &= khepera3_motor_write_register16(motor, cKhepera3MotorRegister16_MaxSpeed, maxspeed);
	res &= khepera3_motor_write_register8(motor, cKhepera3MotorRegister8_Acceleration, acceleration);
	return res;
}

// Main program.
int main(int argc, char *argv[]) {
	int maxspeed;
	int acceleration;
	int res;

	// Command line parsing
	commandline_init();
	commandline_parse(argc, argv);

	// Help
	if (commandline_option_provided("-h", "--help")) {
		help();
		exit(1);
	}

	// Read arguments
	if (commandline_argument_count() < 2) {
		help();
	}
	maxspeed = commandline_argument_int(0, 0);
	acceleration = commandline_argument_int(1, 0);

	// Initialization
	khepera3_init();

	// Set the speed profile
	res = set_speed_profile(&(khepera3.motor_left), maxspeed, acceleration);
	if (res) {
		printf("Left motor: set speed profile to max_speed = %d and acceleration = %d.\r\n", maxspeed, acceleration);
	} else {
		printf("Left motor: failed to set speed profile!\r\n");
	}

	res = set_speed_profile(&(khepera3.motor_right), maxspeed, acceleration);
	if (res) {
		printf("Right motor: set speed profile to max_speed = %d and acceleration = %d.\r\n", maxspeed, acceleration);
	} else {
		printf("Right motor: failed to set speed profile!\r\n");
	}

	return 0;
}
