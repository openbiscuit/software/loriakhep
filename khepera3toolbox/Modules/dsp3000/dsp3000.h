#ifndef DSP3000_H
#define DSP3000_H

int serialport_init(const char* serialport, int baud);
int dsp3000_init(char *port, int speed);
int set_incremental_mode();
int set_integrated_mode();
int set_rate_mode();
int zero_integrated_angle();
double dsp3000_read(void);
int clear_port();
#endif
