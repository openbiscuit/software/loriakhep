#include <stdio.h>    /* Standard input/output definitions */
#include <stdlib.h>
#include <stdint.h>   /* Standard types */
#include <string.h>   /* String function definitions */
#include <unistd.h>   /* UNIX standard function definitions */
#include <fcntl.h>    /* File control definitions */
#include <errno.h>    /* Error number definitions */
#include <termios.h>  /* POSIX terminal control definitions */
#include <sys/ioctl.h>
#include <getopt.h>
#include <time.h>
#include <sys/time.h>
#include <signal.h>
#include <pthread.h>
#include "dsp3000.h"

static int global_fd;
int dsp3000_value;
pthread_mutex_t dsp3000_mutex=PTHREAD_MUTEX_INITIALIZER;

int clear_port()
{
    int n;
	int tot=0;
    char c[4096];
	int i=0;
	
  pthread_mutex_lock(&dsp3000_mutex);
    do {
        n = read(global_fd, c, 4096);  // read a char at a time
	tot+=n;
	if(n==-1)
	{
	//usleep(50);
		i++;
	}
    } while ( n>0 || i<20);
  pthread_mutex_unlock(&dsp3000_mutex);
    return 0;
}



int serialport_write(int fd, const char* str)
{
    int len = strlen(str);
    int n;
    n=0;
    do
    {
        n+=write(fd, str+n, len-n);
    } while (n<len);
    if ( n!=len )
        return -1;
    return 0;
}


/*
int clear_port(int fd)
{
	int n;
	char c;
	do {
		n = read(fd, &c, 1);  // read a char at a time
	} while( n<1 );

	return 0;
}*/



int serialport_read_until(int fd, char* buf, char until, int max)
{
    char b;
    int i=0;
    int j;
    do {
        int n = read(fd, &b, 1);  // read a char at a time
        //if( n==-1){ printf("MOINS UN\n");return -1;}    // couldn't read
        if ( n<1 ) {
            if (n==0)
            {
                printf("SERIAL CLOSED\n");
                return 0;

            }
            usleep( 1000 ); // wait 10 msec try again
            continue;
        }
        if (!(i==0 && ((b==until))))
        {
            buf[i] = b;
            i++;
        }
    } while ( ((i<max) && (b!=until)) || (i==0) );

    buf[i] = 0;  // null terminate the

    return 0;
}

// takes the string name of the serial port (e.g. "/dev/tty.usbserial","COM1")
// and a baud rate (bps) and connects to that port at that speed and 8N1.
// opens the port in fully raw mode so you can send binary data.
// returns valid fd, or -1 on error
int set_incremental_mode()
{
int res;
        pthread_mutex_lock(&dsp3000_mutex);
    res=serialport_write(global_fd, "A");
	 res=serialport_write(global_fd, "A");
 res=serialport_write(global_fd, "A");
    pthread_mutex_unlock(&dsp3000_mutex);
	return res;
}

int set_integrated_mode()
{
int res;
        pthread_mutex_lock(&dsp3000_mutex);
    res=serialport_write(global_fd, "P");
 res=serialport_write(global_fd, "P");
 res=serialport_write(global_fd, "P");
    pthread_mutex_unlock(&dsp3000_mutex);
	return res;

}
int set_rate_mode()
{
int res;
        pthread_mutex_lock(&dsp3000_mutex);
    res= serialport_write(global_fd, "R");
    res= serialport_write(global_fd, "R");
    res= serialport_write(global_fd, "R");
    pthread_mutex_unlock(&dsp3000_mutex);
	return res;
}
int zero_integrated_angle()
{
int res;
        pthread_mutex_lock(&dsp3000_mutex);
    res=serialport_write(global_fd, "Z");
    res=serialport_write(global_fd, "Z");
    res=serialport_write(global_fd, "Z");
      pthread_mutex_unlock(&dsp3000_mutex);
 	return res;
}


int serialport_init(const char* serialport, int baud)
{
    struct termios toptions;
    int fd;

    //fprintf(stderr,"init_serialport: opening port %s @ %d bps\n",
    //        serialport,baud);

    fd = open(serialport, O_RDWR | O_NOCTTY | O_NDELAY);
    if (fd == -1)  {
        perror("init_serialport: Unable to open port ");
        return -1;
    }

    if (tcgetattr(fd, &toptions) < 0) {
        perror("init_serialport: Couldn't get term attributes");
        return -1;
    }
    speed_t brate = baud; // let you override switch below if needed
    switch (baud) {
    case 4800:
        brate=B4800;
        break;
    case 9600:
        brate=B9600;
        break;
#ifdef B14400
    case 14400:
        brate=B14400;
        break;
#endif
    case 19200:
        brate=B19200;
        break;
#ifdef B28800
    case 28800:
        brate=B28800;
        break;
#endif
    case 38400:
        brate=B38400;
        break;
    case 57600:
        brate=B57600;
        break;
    case 115200:
        brate=B115200;
        break;
    }
    cfsetispeed(&toptions, brate);
    cfsetospeed(&toptions, brate);

    // 8N1
    toptions.c_cflag &= ~PARENB;
    toptions.c_cflag &= ~CSTOPB;
    toptions.c_cflag &= ~CSIZE;
    toptions.c_cflag |= CS8;
    // no flow control
    toptions.c_cflag &= ~CRTSCTS;

    toptions.c_cflag |= CREAD | CLOCAL;  // turn on READ & ignore ctrl lines
    toptions.c_iflag &= ~(IXON | IXOFF | IXANY); // turn off s/w flow ctrl

    toptions.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); // make raw
    toptions.c_oflag &= ~OPOST; // make raw

    // see: http://unixwiz.net/techtips/termios-vmin-vtime.html
    toptions.c_cc[VMIN]  = 0;
    toptions.c_cc[VTIME] = 20;
    // tcsetattr(fd,tcsetattr(fd, TCSAFLUSH, &options);


    if ( tcsetattr(fd, TCSAFLUSH, &toptions) < 0) {
        perror("init_serialport: Couldn't set term attributes");
        return -1;
    }
int flags = fcntl(fd, F_GETFL, 0);
fcntl(fd, F_SETFL, flags ^ O_NONBLOCK);

    return fd;
}


double _dsp3000_read(void)
{
    char buf[128];
    char c;
    serialport_read_until(global_fd,buf,10,127);
    read(global_fd,&c,1);
    return strtod(buf,NULL);
}
void *dsp_thread_read(void *ptr)
{
 //struct timeval t1,t2;
    while (1)
    {
	//gettimeofday(&t1,NULL);
        pthread_mutex_lock(&dsp3000_mutex);
        dsp3000_value = _dsp3000_read();
        pthread_mutex_unlock(&dsp3000_mutex);
	//gettimeofday(&t2,NULL);
	//printf("t1 sec: %d t1usec: %d t2 sec: %d t2 usec: %d\n",t1.tv_sec,t1.tv_usec,t2.tv_sec,t2.tv_usec);
    //   usleep(delayus);
    }
}

double dsp3000_read(void)
{
    double res;
    pthread_mutex_lock(&dsp3000_mutex);
    res=dsp3000_value;
    pthread_mutex_unlock(&dsp3000_mutex);
    return res;
}
int clear_port_int(int n_)
{

    int n;
	int tot=0;
    char c[4096];
  pthread_mutex_lock(&dsp3000_mutex);
    do {
        n = read(global_fd, c, 4096);  // read a char at a time
	tot+=n;
	if(n==-1)
	{
	//usleep(50);
		n = read(global_fd, c, 4096);
	}
    } while ( tot<n_ );
  pthread_mutex_unlock(&dsp3000_mutex);
	printf("clear : %d\n",tot);
    return 0;


}
int dsp3000_init(char *port, int speed)
{
int i;
    global_fd = serialport_init(port,speed);
	close(global_fd);
    global_fd = serialport_init(port,speed);
//serial_port_synchronise(global_fd);
//fflush(NULL);
//exit(0);
   // clear_port_int(4096);
/*for(i=0;i<5000;i++)
_dsp3000_read();	/// on vide brutallement le buffer en lisant 4096 valeur
*/
	// clear_port();
    pthread_t t;
    pthread_create( &t,NULL,dsp_thread_read,NULL);
    return global_fd;
}
// int main()
// {
//   int i;
//   char b;
//   color_sensor_init("/dev/ttyS0",38400);
//   while(1)
//   {
//     int n = read(global_fd, &b, 1);  // read a char at a time
//     if (n==1)
//     {
//       printf("%d %c\n",b,b);
//     }
//   }
// }
