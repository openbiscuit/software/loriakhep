#include <stdio.h>
#include <stdlib.h>
#include "remote_serveur.h"
#include "remote_variable.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h> /* close */
#include <netdb.h> /* gethostbyname */
#include <string.h>
#include <fcntl.h>
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define _debug 1
#define closesocket(s) close(s)
typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;
char tmp_buffer[256];
int current_id=0;
#define MAX_SERVEUR 128



remoteserver *remoteservertab[MAX_SERVEUR];
void *ping_thread(void *param)
{
remoteserver *s=(remoteserver *)param;
char buffer[128];
int op;
while(1)
{
lock_rs(s);
op=s->open;
unlock_rs(s);
if(op)
{
send_command(s,"$PING\n");    
//printf("on fait le getvar\n");
usleep(10000);

receiv_command(s,buffer,128);


}

sleep(2);
}


}
void lock_rs(remoteserver *s) {

    pthread_mutex_lock(&(s->mutex));

}
void unlock_rs(remoteserver *s) {
    pthread_mutex_unlock(&(s->mutex));

}
void *open_connection_thread(void *arg)
{

    remoteserver* s = (remoteserver*)arg;
    SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == INVALID_SOCKET)
    {
        perror("socket()");
        exit(-1);
    }

    struct hostent *hostinfo = NULL;
    SOCKADDR_IN sin = { 0 }; // initialise la structure avec des 0
    char *hostname = s->location;

    while (1)
    {
        hostinfo = gethostbyname(hostname); // on récupère les informations de l'hôte auquel on veut se connecter
        if (hostinfo == NULL) // l'hôte n'existe pas
        {
            fprintf (stderr, "Unknown host %s %d\n", hostname,s->port);
            //exit(EXIT_FAILURE);
        }

        sin.sin_addr = *(IN_ADDR *) hostinfo->h_addr; // l'adresse se trouve dans le champ h_addr de la structure hostinfo
        sin.sin_port = htons(s->port); // on utilise htons pour le port
        sin.sin_family = AF_INET;
 /*  int             res; 
    fd_set          sready; 
    struct timeval  nowait; 

    FD_ZERO(&sready); 
    FD_SET((unsigned int)sock,&sready); 

    nowait.tv_usec=0;
   nowait.tv_sec=1;

    res = select(sock,&sready,NULL,NULL,&nowait); 
    if( FD_ISSET(sock,&sready) ) 
        res = 1; 

fcntl(sock, F_SETFL, O_NONBLOCK);
*/
fcntl(sock, F_SETFL, O_NONBLOCK);

        if (connect(sock,(SOCKADDR *) &sin, sizeof(SOCKADDR)) == SOCKET_ERROR)
        {

            perror("connect()");
            //return -1;
            //  exit(-1);
        }
        else
        {

            lock_rs(s);
            s->socket = sock;
            s->open = 1;
            unlock_rs(s);
            //	unlock_rs(s);
            if (s->ptr_object!=NULL)
            {
                s->ptr_connexion_func((void*)s->ptr_object);
            }
         //   fcntl(s->socket, F_SETFL, O_NONBLOCK);
            //pthread_exit(NULL);


            pthread_exit(NULL);

            return NULL;

        }
        sleep(1);
    }
}
int open_connection(remoteserver *s)
{

    /*SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock == INVALID_SOCKET)
    {
    perror("socket()");
    exit(-1);
    }

    struct hostent *hostinfo = NULL;
    SOCKADDR_IN sin = { 0 };
    char *hostname = s->location;

    hostinfo = gethostbyname(hostname);
    if (hostinfo == NULL)
    {
    fprintf (stderr, "Unknown host %s.\n", hostname);
    exit(EXIT_FAILURE);
    }

    sin.sin_addr = *(IN_ADDR *) hostinfo->h_addr;
    sin.sin_port = htons(s->port);
    sin.sin_family = AF_INET;

    if(connect(sock,(SOCKADDR *) &sin, sizeof(SOCKADDR)) == SOCKET_ERROR)
    {
    perror("connect()");
    	//return -1;
    //  exit(-1);
    }
    else
    {
    s->socket = sock;
    s->open = 1;
    }*/
    pthread_t t;
    pthread_mutex_unlock(&(s->readymutex));
    pthread_mutex_lock(&(s->readymutex));
    pthread_create( &t, NULL, open_connection_thread, (void *)s);

}
void wait_open(remoteserver *s) {
    pthread_mutex_lock(&(s->readymutex));
    pthread_mutex_unlock(&(s->readymutex));

}
char *strreplace(char *buffer,char *to_replace,char *by_replace)
{
    int count;

    count=0;

    char *temp;

    temp=strstr(buffer,to_replace);
    while (temp!=NULL)
    {
        count++;
        temp=strstr(temp+strlen(to_replace),to_replace);
    }


    char *sub=strstr(buffer,to_replace);
    char *old_sub;
    char *buffer2  = (char*)malloc(sizeof(char)*strlen(buffer)+1+(strlen(by_replace)-strlen(to_replace))*count);
    int j=0;
    int i;
    old_sub=buffer;
    while (sub!=NULL)
    {
        for (i=0;i<strlen(old_sub)-strlen(sub);i++)
        {
            buffer2[j++]=old_sub[i];
        }
        for (i=0;i<strlen(by_replace);i++)
            buffer2[j++]=by_replace[i];

        old_sub=sub+strlen(to_replace);
        sub=strstr(old_sub,to_replace);
    }
    for (i=0;i<strlen(old_sub);i++)
        buffer2[j++]=old_sub[i];
    buffer2[j]='\0';
    //printf("%s\n",buffer2);
    return buffer2;

}

int remote_server_get_int_var(remoteserver *s,char *var)
{
    char buffer[4096];
    char *ptr;
    int count=0;
    char tmp[128];
    sprintf(tmp,"$GET_VAR,%s\n",var);
    //printf("on envoie le getvar\n");
    send_command(s,tmp);
//printf("on fait le getvar\n");
    receiv_command(s,buffer,4096);
//printf("voici ce qu'on a récupéré: %s \n",buffer);
    ptr=buffer;
    while ((*ptr!='\n') && (*ptr!='\0') && (count<2))
    {
        if (*ptr==',')
            count++;
        ptr++;


    }
    if ((count==0) && (!strcmp(buffer,"$UNKNOW_VAR\n")))
    {

        return -1;
    }
    if (strlen(ptr)>0)
        return atoi(ptr);
    else
    {

        return -1;
    }
}
int remote_server_get_double_array(remoteserver *s,char *var,int *sizeptr,double **res)
{
    char buffer[4096];
    char tmpz[256];
    char *ptr;
    char *ptr2;
    int count=0;
    char tmp[128];
    int inc;
    sprintf(tmp,"$GET_VAR,%s\n",var);
    //printf("on envoie le getvar\n");
    send_command(s,tmp);
//printf("on fait le getvar\n");
    receiv_command(s,buffer,4096);
//printf("voici ce qu'on a récupéré: %s \n",buffer);
    ptr=buffer;
    ptr2=buffer;
    int z=0;
    while ((*ptr2!='\n') && (*ptr2!='\0'))
    {

        if (*ptr2==',')
            count++;

        ptr2++;


    }

    *res = (double*)malloc(sizeof(double)*count);
    *sizeptr=count-1;
    count=0;
    while ((*ptr!='\n') && (*ptr!='\0'))
    {

        if (*ptr==',')
        {
            if (count>1)
            {

                tmpz[z]='\0';
                (*res)[count-2]=atof(tmpz);
            }
            z=0;
            count++;
        }
        else
        {
            tmpz[z]=*ptr;
            z++;
        }
        ptr++;


    }

    if (count!=0)
    {
        tmpz[z]='\0';
        (*res)[count-2]=atof(tmpz);
    }
    if ((count==0) && (!strcmp(buffer,"$UNKNOW_VAR\n")))
    {

        return -1;
    }
    if (count-2>0)
        return (count-1);
    else
    {

        return -1;
    }
}
int remote_server_get_int_array(remoteserver *s,const char *var,int *sizeptr,int **res)
{
    char buffer[4096];
    char tmpz[256];
    char *ptr;
    char *ptr2;
    int count=0;
    char tmp[128];
    int inc;
    sprintf(tmp,"$GET_VAR,%s\n",var);
    //printf("on envoie le getvar\n");
    send_command(s,tmp);
//printf("on fait le getvar\n");
    receiv_command(s,buffer,4096);
//printf("voici ce qu'on a récupéré: %s \n",buffer);
    ptr=buffer;
    ptr2=buffer;
    int z=0;
    while ((*ptr2!='\n') && (*ptr2!='\0'))
    {

        if (*ptr2==',')
            count++;

        ptr2++;


    }

    *res = (int*)malloc(sizeof(int)*count);
    *sizeptr=count-1;
    count=0;
    while ((*ptr!='\n') && (*ptr!='\0'))
    {

        if (*ptr==',')
        {
            if (count>1)
            {

                tmpz[z]='\0';
                (*res)[count-2]=atoi(tmpz);
            }
            z=0;
            count++;
        }
        else
        {
            tmpz[z]=*ptr;
            z++;
        }
        ptr++;


    }

    if (count!=0)
    {
        tmpz[z]='\0';
        (*res)[count-2]=atoi(tmpz);
    }
    if ((count==0) && (!strcmp(buffer,"$UNKNOW_VAR\n")))
    {

        return -1;
    }
    if (count-2>0)
        return (count-1);
    else
    {

        return -1;
    }
}

double remote_server_get_double_var(remoteserver *s,char *var)
{
    char buffer[128];
    char *ptr;
    int count=0;
    char tmp[128];
    sprintf(tmp,"$GET_VAR,%s\n",var);

    send_command(s,tmp);

    receiv_command(s,buffer,128);

    ptr=buffer;
    while ((*ptr!='\n') && (*ptr!='\0') && (count<2))
    {
        if (*ptr==',')
            count++;
        ptr++;


    }
    if ((count==0) && (!strcmp(buffer,"UNKNOW_VAR\n")))
    {

        return -1.0;
    }
    if (strlen(ptr)>0)
        return atof(ptr);
    else
    {

        return -1.0;
    }
}

char* remote_server_get_str_var(remoteserver *s,char *var)
{
    char buffer[128];
    char *ptr;
    int count=0;
    char tmp[128];
    char *ret;
    sprintf(tmp,"$GET_VAR,%s\n",var);

    send_command(s,tmp);

    receiv_command(s,buffer,128);

    ptr=buffer;
    while ((*ptr!='\n') && (*ptr!='\0') && (count<2))
    {
        if (*ptr==',')
            count++;
        ptr++;


    }
    if ((count==0) && (!strcmp(buffer,"UNKNOW_VAR\n")))
    {

        return NULL;
    }
    if (strlen(ptr)>0)
    {
        ret = (char*)malloc(sizeof(char)*(strlen(ptr)+1));
        strcpy(ret,ptr);
        return ret;
    }
    else
    {

        return NULL;
    }
}
int receiv_command(remoteserver *s,char *buffer,int size)
{

///TODO FIXX NOT THREAD SAFE!!!!!!!!!!!!!!!!!
    //char *buffer = buff;
    int len,n;
    //int i=*z;
    fd_set fs;
    lock_rs(s);

    if (s->open)
    {

        //FD_ZERO(&fs);
        //FD_SET(0, &fs);
        //FD_SET(s->socket, &fs);
        memset(buffer,0,size);
        // Read and parse until there is nothing available any more
        //while (1) {
        //	memset(buffer,0,BUFFER_SIZE);
//unlock_rs(s);
        //	select(s->socket + 1, &fs, NULL, NULL, NULL); //on s'endore jusqu'a ce que des données arrive sur la socket comme read est en mode non bloquant.
//printf("Select\n\n\n");
//lock_rs(s);
//len=0;
//while(len<4000){
        len=read(s->socket, buffer, size);


//if(xx>0)
        //		len += xx;
//printf("len: %d\n",len);
//}
        tmp_buffer[0]='\0';
        int i=0;
        while (i<strlen(buffer) && (buffer[i]!='\n'))
            i++;

        if (buffer[i]=='\n')i++;
        int j=0;

        while (i<strlen(buffer))
        {
            tmp_buffer[j]=buffer[i];
            i++;
            j++;
        }
        tmp_buffer[j]='\0';



//printf("receiv : %s rest : %s\n",buffer,tmp_buffer);

        if (len < 1) {

            if (len==0)
            {
                //close(s->socket);
                unlock_rs(s);
                close_connection(s);

                //FD_CLR(tcp_nmea_socket[i],&readfs);
                //tcp_nmea_socket[i]=-1;
                //	free(num);
//return 0;
                //pthread_exit(NULL);
            }
            else
            {
                usleep(5000);

                unlock_rs(s);
                return -1;
            }

//free(buffer);
//printf("on sort de ce truc\n");
//FD_SET(tcp_nmea_server_filehandle, &readfs);
            //return 0;
        }
        else {

            //tcp_nmea_connection_filehandle=tcp_nmea_socket[i];
            //nmea_parser_process_data_(&parser, buffer, BUFFER_SIZE,i);z

            unlock_rs(s);
            if (!strcmp(buffer,"$PING\n"))
            {
                send_command(s,"$PONG\n");

            }
            return 0;
        }
        //}
    }
    else
    {
        unlock_rs(s);
        return -1;
    }
    unlock_rs(s);
    return -1;
}

int receiv_command_NONBLOCK(remoteserver *s,char *buffer,int size)
{

///TODO FIXX NOT THREAD SAFE!!!!!!!!!!!!!!!!!
    //char *buffer = buff;
    int len,n;
    //int i=*z;
    fd_set fs;
    lock_rs(s);

    if (s->open)
    {

        //FD_ZERO(&fs);
        //FD_SET(0, &fs);
        //FD_SET(s->socket, &fs);
        memset(buffer,0,size);
        // Read and parse until there is nothing available any more
        //while (1) {
        //	memset(buffer,0,BUFFER_SIZE);
//unlock_rs(s);
        //	select(s->socket + 1, &fs, NULL, NULL, NULL); //on s'endore jusqu'a ce que des données arrive sur la socket comme read est en mode non bloquant.
//printf("Select\n\n\n");
//lock_rs(s);
//len=0;
//while(len<4000){
        fcntl(s->socket, F_SETFL, O_NONBLOCK);
        len=read(s->socket, buffer, size);


//if(xx>0)
        //		len += xx;
//printf("len: %d\n",len);
//}
        tmp_buffer[0]='\0';
        int i=0;
        while (i<strlen(buffer) && (buffer[i]!='\n'))
            i++;

        if (buffer[i]=='\n')i++;
        int j=0;

        while (i<strlen(buffer))
        {
            tmp_buffer[j]=buffer[i];
            i++;
            j++;
        }
        tmp_buffer[j]='\0';



//printf("receiv : %s rest : %s\n",buffer,tmp_buffer);

        if (len < 1) {

            if (len==0)
            {
                //close(s->socket);
                unlock_rs(s);
                close_connection(s);

                //FD_CLR(tcp_nmea_socket[i],&readfs);
                //tcp_nmea_socket[i]=-1;
                //	free(num);
//return 0;
                //pthread_exit(NULL);
            }
            else
            {
                usleep(5000);

                unlock_rs(s);
                return -1;
            }

//free(buffer);
//printf("on sort de ce truc\n");
//FD_SET(tcp_nmea_server_filehandle, &readfs);
            //return 0;
        }
        else {

            //tcp_nmea_connection_filehandle=tcp_nmea_socket[i];
            //nmea_parser_process_data_(&parser, buffer, BUFFER_SIZE,i);z

            unlock_rs(s);
            if (!strcmp(buffer,"$PING\n"))
            {
                send_command(s,"$PONG\n");

            }
            return 0;
        }
        //}
    }
    else
    {
        unlock_rs(s);
        return -1;
    }
    unlock_rs(s);
    return -1;
}
int is_open(remoteserver *s) {

    int ret;
    lock_rs(s);
    ret = s->open;
    unlock_rs(s);
    return ret;
}
void remote_set_int_array(remoteserver *s,char *name ,int size,int *array)
{
    char *buffer;
    int i;

    buffer=(char*)malloc(sizeof(char)*10*size+32);
    sprintf(buffer,"$SET_VAR,4,%s,%d",name,size);
    for (i=0;i<size;i++)
    {
        sprintf(buffer,"%s,%d",buffer,array[i]);


    }
    sprintf(buffer,"%s\n",buffer);

    send_command(s,buffer);
    free(buffer);

}
void remote_set_double_array(remoteserver *s,char *name,int size,double *array)
{
    char *buffer;
    int i;
    buffer=(char*)malloc(sizeof(char)*32*size+32);
    sprintf(buffer,"$SET_VAR,5,%s,%d",name,size);
    for (i=0;i<size;i++)
    {
        sprintf(buffer,"%s,%f",buffer,array[i]);


    }
    sprintf(buffer,"%s\n",buffer);
    send_command(s,buffer);
    free(buffer);


}
void remote_broadcast(remoteserver *s,char *name)
{
    char buffer[128];
    sprintf(buffer,"$BROADCAST,%s\n",name);
    send_command(s,buffer);
}
void remote_set_int_var(remoteserver *s,char *name,int v)
{
    char buffer[128];
    sprintf(buffer,"$SET_VAR,%d,%s,%d\n",T_INT,name,v);
    send_command(s,buffer);
}

void remote_set_double_var(remoteserver *s,char *name,double v)
{
    char buffer[128];
    sprintf(buffer,"$SET_VAR,%d,%s,%f\n",T_DOUBLE,name,v);
    send_command(s,buffer);
}

void remote_set_char_var(remoteserver *s,char *name,char v)
{
    char buffer[128];
    sprintf(buffer,"$SET_VAR,%d,%s,%c\n",T_CHAR,name,v);
    send_command(s,buffer);
}

void remote_set_str_var(remoteserver *s,char *name,char* v)
{
    char buffer[128];
    sprintf(buffer,"$SET_VAR,%d,%s,%d\n",T_STR,name,v);
    send_command(s,buffer);
}

int send_command(remoteserver *s,const char *command)
{

    /*int count;

    	count=0;

    	char *temp;

    	temp=strstr(command,".arm");
    	while(temp!=NULL)
    	{
    	count++;
    	temp=strstr(temp+strlen(".arm"),".arm");
    }


    	char *sub=strstr(command,".arm");
    	char *old_sub;
    	char *command2  = (char*)malloc(sizeof(char)*strlen(command)+1+2*count);
    	int j=0;
    	int i;
    	old_sub=command;
    	while(sub!=NULL)
    	{
    	for(i=0;i<strlen(old_sub)-strlen(sub);i++)
    	{
    	command2[j++]=old_sub[i];
    }
    	old_sub=sub+strlen(".arm");
    	sub=strstr(old_sub,".arm");
    }
    	for(i=0;i<strlen(old_sub);i++)
    	command2[j++]=old_sub[i];
    	command2[j]='\0';
    	printf("%s\n",command2);*/

    char *buffer2;
    char *tmp;
    int ret=0;

    lock_rs(s);

    if (s->open)
    {

        /*tmp=strreplace(command,".armgs",".arm");
        tmp=command;*/
        /*if(!(s->type))
        {*/

        if ((ret=send(s->socket, command, strlen(command), MSG_NOSIGNAL)) < 0)
        {
            printf("%d %d \n",s->socket,s->port);
            unlock_rs(s);
            close_connection(s);
            perror("send()");
            // exit(-1);


            //free(tmp);
            return -1;
        }
        //}
        /*else
        {

        	buffer2=strreplace(tmp,".arm",".armgs");
        	if((ret=send(s->socket, buffer2, strlen(buffer2), MSG_NOSIGNAL)) < 0)
        	{
        		printf("%d %d \n",s->socket,s->port);
        		unlock_rs(s);
        		close_connection(s);
        		perror("send()");

        		free(buffer2);
        		free(tmp);
        		return -1;
        	}
        	free(buffer2);
        }
        free(tmp);*/

    }

    unlock_rs(s);
    usleep(500);
    return ret;
}

int send_command_all(char *command)
{
    int i;

    for (i=0;i<MAX_SERVEUR;i++)
    {


        if (remoteservertab[i]!=NULL)
        {

            if (send_command(remoteservertab[i], command) < 0)
            {
                printf("error\n");
                //printf("%d %d %d \n",i,remoteservertab[i]->socket,remoteservertab[i]->port);
                //	perror("send()");
                //  exit(-1);
            }

        }
    }
}

void send_int_array_all(char *name,int size,int *tab)
{
    int i;

    for (i=0;i<MAX_SERVEUR;i++)
    {


        if (remoteservertab[i]!=NULL)
        {

            remote_set_int_array(remoteservertab[i],name,size,tab);
        }
    }
}
void send_double_array_all(char *name,int size,double *tab)
{
    int i;

    for (i=0;i<MAX_SERVEUR;i++)
    {


        if (remoteservertab[i]!=NULL)
        {

            remote_set_double_array(remoteservertab[i],name,size,tab);
        }
    }
}
int load_file_server(char *file,remoteserver *origs)
{
    FILE *f;
    char buff[256];
    int n;
    int t;
    remoteserver *s;
    f=fopen(file,"r");
    if (!f)
    {
        printf("no file %s\n",file);
        return -1;
    }
    while (!feof(f))
    {
        fscanf(f,"%s %d %d\n",buff,&n,&t);
        if (strlen(buff)>3)
        {
            s = new_remoteserver(buff,n,1);
            s->type = t;
            if (!((s->port == origs->port) && !(strcmp(origs->location,s->location))))
            {
                open_connection(s);
                register_remoteserver(s);
            }

            //printf("%s %d\n",buff,n);
        }
    }
    fclose(f);

    return 1;


}
void print_rs(remoteserver *s)
{
    printf("s: %d open : %d location : %s \n port : %d id : %d type : %d \n",s->socket,s->open,s->location,s->port,s->id,s->type);

}
int close_connection(remoteserver *s)
{

    lock_rs(s);
    s->open=0;
    close(s->socket);
    s->socket=-1;
    unlock_rs(s);
    if (s->ptr_object!=NULL && s->ptr_deconnexion_func!=NULL)
    {
        s->ptr_deconnexion_func((void*)s->ptr_object);
    }



}

void init_remoteserver(remoteserver *s,char *location,int port,int id)

{

    pthread_mutex_init(&(s->mutex),NULL);
    unlock_rs(s);
    pthread_mutex_init(&(s->readymutex),NULL);
    lock_rs(s);
    s->open = 0;
    s->socket=-1;
    s->id = id;
    s->location = (char*)malloc(sizeof(char)*(strlen(location)+2));
    strcpy(s->location,location);
    s->port = port;
    s->type=0;
    unlock_rs(s);
}
remoteserver *new_remoteserver(char *location,int port,int type)
{
pthread_t t;
    remoteserver *ret = (remoteserver *)malloc(sizeof(remoteserver));
    ret->ptr_object=NULL;
    ret->ptr_connexion_func=NULL;
    ret->ptr_deconnexion_func=NULL;
    init_remoteserver(ret,location,port,current_id++);
    ret->type = type;
	pthread_create(&t,NULL,ping_thread,ret);
    return ret;
}
void init_remoteservertab()
{
    int i;
    for (i=0;i<MAX_SERVEUR;i++)
        remoteservertab[i]=NULL;

}

int register_remoteserver(remoteserver *s)
{

    int i;

    lock_rs(s);

    for (i=0;i<MAX_SERVEUR;i++)
    {

        if (remoteservertab[i] ==NULL)
        {

            remoteservertab[i]=s;
            unlock_rs(s);

            return 1;

        }
    }
    unlock_rs(s);

    return -1;
}

void unregister_remoteserver(int id)
{
    int i;
    for (i=0;i<MAX_SERVEUR;i++)
    {
        if (remoteservertab[i] !=NULL && remoteservertab[i]->id==id)
        {
            //free(remoteservertab[i]);
            remoteservertab[i]=NULL;
        }
    }

}

