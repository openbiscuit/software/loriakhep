/**
 * \file remote_variable.h
 * \brief store variable function
 * \author Nicolas Beaufort
 * \version 1.0
 *
 *
 *
 */
#ifndef REMOTE_VARIABLE_H
#define REMOTE_VARIABLE_H
#define MAX_VAR_NUM 4096
#define T_INT 0
#define T_DOUBLE 1
#define T_CHAR 2
#define T_CHARSTAR 3
#define T_INT_ARRAY 4
#define T_DOUBLE_ARRAY 5
#define T_STR T_CHARSTAR
#define T_FLOAT 6
#define T_FLOAT_ARRAY 7

#ifdef __cplusplus
extern "C" {
#endif
  
typedef struct remotevariable {
    int type;
    int val_int;
    double val_double;
    char val_char;
    char *val_charstar;
    int *val_intarray;
    float *val_floatarray;
	float val_float;
    double *val_doublearray;
    int size_array;
    char name[33];



}remotevariable;
//! Add a variable to database
/*!
	\param name the name of the variable
	\param type the type of the variable (T_INT for int, T_DOUBLE for double, T_CHAR for char, T_CHARSTAR == T_STR for char*)
	\param vi value int if type == T_INT , UNUSED IF NOT (set to 0 or -1)
	\param vd value doube if type == T_DOUBLE , UNUSED IF NOT (set to 0 or -1)
	\param vc value char if type == T_CHAR,  UNUSED IF NOT (set to 0 or -1)
	\param vvs value char* if type == T_CHARSTAR or T_STR, UNUSED IF NOT (set to NULL)
*/
void add_var(char *name,int type,int vi,double vd,char vc,char* vcs,float vf);

void add_array(char *name,int type,int size,int *vi,double *vd,float *vf);
//! get a variable from data base and return type in the type ptr, and the associate value in the right variable pointer (in *vi if type is integer, *vd if type is double etc...)
/*!
	\param name the name of the variable
	\param type the type of the returned variable (T_INT for int, T_DOUBLE for double, T_CHAR for char, T_CHARSTAR == T_STR for char*)
	\param vi value int if type == T_INT
	\param vd value doube if type == T_DOUBLE
	\param vc value char if type == T_CHAR
	\param vvs value char* if type == T_CHARSTAR or T_STR
*/
void get_var(char *name,int *type,int *vi,double *vd,char *vc,char** vcs,float *vf,int *arraysize,int **viarray,double **vdarray,float **vfarray);
int get_int_var(char *name);

#ifdef __cplusplus
}
#endif

#endif
