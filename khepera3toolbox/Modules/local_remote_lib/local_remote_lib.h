#ifndef _LOCAL_REMOTE_LIB_H
#define _LOCAL_REMOTE_LIB_H
#include "remote_serveur.h"
extern remoteserver *_local_rs;
void _sigint_cb(int sig) ;
//static void sig_tstp(int signo) ;
void *thread_ir_remote_update(void *param);
void local_remote_usage_init();
void active_ir_remote_update(int ms_period);
void local_remote_usage_init_with_comunication();
void unactive_ir_remote_update();
#endif
