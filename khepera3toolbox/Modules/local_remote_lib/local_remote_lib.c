#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include "khepera3.h"
//#include "remote_serveur.h"
#include <pthread.h>
#include "local_remote_lib.h"

remoteserver *_local_rs;
pthread_mutex_t _thread_on_mutex = PTHREAD_MUTEX_INITIALIZER;
int _thread_on;
void _sigint_cb(int sig)
{
    khepera3_unlock();
    khepera3_drive_set_speed(0,0);
    khepera3_drive_stop();
    khepera3_drive_idle();
    exit(-1);
}

static void sig_tstp(int signo)
{
    sigset_t    mask;
    khepera3_unlock();
    fflush(NULL);
    khepera3_drive_stop();
    //khepera3_drive_idle();
    sigemptyset(&mask);
    sigaddset(&mask, SIGTSTP);
    sigprocmask(SIG_UNBLOCK, &mask, NULL);


    signal(SIGTSTP, SIG_DFL);

    //kill(getpid(), SIGTSTP);
    kill(getpid(),SIGSTOP);

    signal(SIGTSTP, sig_tstp);
    fflush(NULL);
    khepera3_drive_start();

}



void *thread_ir_remote_update(void *param)
{
    char buffer[128];
    int ir[9];
    int i;
    int period = (int)(*(int*)param);
    int *irz;

    pthread_mutex_lock(&_thread_on_mutex);
    while (_thread_on)
    {
        pthread_mutex_unlock(&_thread_on_mutex);
        khepera3_infrared_proximity();

        //sprintf(buffer,"$SET_VAR,4,IR_PROXI,9");
        for (i=0;i<9;i++)
        {
            ir[i]=khepera3.infrared_proximity.sensor[i];
            //sprintf(buffer,"%s,%d",buffer,ir[i]);


        }

        remote_set_int_array(_local_rs,"IR_PROXI" ,9,ir);
        //	sprintf(buffer,"%s\n",buffer);
        //	send_command(_local_rs,buffer);

        usleep(period*1000);
        pthread_mutex_lock(&_thread_on_mutex);
    }

    pthread_mutex_unlock(&_thread_on_mutex);

    pthread_exit(NULL);
}

void local_remote_usage_init()
{

    _thread_on=0;
    signal(SIGINT,_sigint_cb);
    signal(SIGKILL,_sigint_cb);
    signal(SIGTSTP, sig_tstp);
    /*_local_rs = new_remoteserver("localhost",3000,0);
    open_connection(_local_rs);
    */

}
void local_remote_usage_init_with_comunication()
{

    _thread_on=0;
    signal(SIGINT,_sigint_cb);
    signal(SIGKILL,_sigint_cb);
    signal(SIGTSTP, sig_tstp);
    _local_rs = new_remoteserver("localhost",3000,0);
    open_connection(_local_rs);


}
void active_ir_remote_update(int ms_period)
{

    pthread_t thr;
    pthread_mutex_lock(&_thread_on_mutex);
    int *tmp=(int*)malloc(sizeof(int));
    *tmp=ms_period;
    if (!_thread_on)
    {

        _thread_on=1;
        pthread_create(&thr,NULL,thread_ir_remote_update,tmp);
        pthread_mutex_unlock(&_thread_on_mutex);
    }
    else {
        pthread_mutex_unlock(&_thread_on_mutex);


    }
}

void unactive_ir_remote_update()
{

    pthread_mutex_lock(&_thread_on_mutex);
    _thread_on=0;
    pthread_mutex_unlock(&_thread_on_mutex);

}
