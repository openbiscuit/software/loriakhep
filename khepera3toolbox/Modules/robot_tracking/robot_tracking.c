#include "robot_tracking.h"
void *robot_tracking_zmq_context;
pthread_mutex_t robot_tracking_zmq_fastmutex = PTHREAD_MUTEX_INITIALIZER;

robot_tracking_data *robot_tracking_data_create(char *name,char *serverip)
{
    robot_tracking_data *res = (robot_tracking_data *)malloc(sizeof(robot_tracking_data));
    res->name = name;
    res->serverip=serverip;
    res->x=-1.0;
    res->y=-1.0;
    res->time =-1.0;
    res->speed=-1.0;
    pthread_mutex_init(&(res->mutex), NULL);
    return res;
}
void robot_tracking_term()
{
    zmq_term (robot_tracking_zmq_context);
}
void *thread_pos(void *param)
{



    robot_tracking_data *r=(robot_tracking_data*)param;


    //  Socket to talk to server
    pthread_mutex_lock(&robot_tracking_zmq_fastmutex);
    void *subscriber = zmq_socket (robot_tracking_zmq_context, ZMQ_SUB);
    pthread_mutex_unlock(&robot_tracking_zmq_fastmutex);
    zmq_connect (subscriber, r->serverip);
    //  Subscribe to zipcode, default is NYC, 10001
    char *filter=(char*)malloc(sizeof(char)*(strlen(r->name)+strlen("_tracking")+1));
    sprintf(filter, "%s_tracking",r->name);
    zmq_setsockopt (subscriber, ZMQ_SUBSCRIBE, filter, strlen(filter));
//free(filter);
    //  Process 100 updates
    int update_nbr;
    long total_temp = 0;
    while (1)
    {
        char *string = s_recv (subscriber);
        double x,y,teta,timestamp,sp;
        sscanf(string+strlen(filter),"%lf %lf %lf %lf %lf",&x,&y,&teta,&sp,&timestamp);
        pthread_mutex_lock(&(r->mutex));
        r->x=x;
        r->y=y;
        r->teta=teta;
        r->speed=sp;
        r->time = timestamp;
        pthread_mutex_unlock(&(r->mutex));
        free (string);
        usleep(5000);
    }
    free(filter);

    zmq_close (subscriber);


}
void robot_tracking_data_subscrib(robot_tracking_data *r)
{
    pthread_t t1;
    pthread_create(&t1,NULL,thread_pos,(void*)r);
}


void robot_tracking_data_get(robot_tracking_data *r,double *x,double *y,double *teta,double *speed,double *time)
{
    pthread_mutex_lock(&(r->mutex));
    *x=r->x;
    *y=r->y;
    *teta=r->teta;
    *speed=r->speed;
    *time=r->time;
    pthread_mutex_unlock(&(r->mutex));

}

void robot_tracking_init()
{

    setlocale(LC_NUMERIC,"C");
    robot_tracking_zmq_context = zmq_init (1);
}

