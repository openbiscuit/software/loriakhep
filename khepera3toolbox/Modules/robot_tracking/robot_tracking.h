#ifndef robot_tracking_h
#define robot_tracking_h
#include "zhelpers.h"
#include <pthread.h>
#include <locale.h>
typedef struct robot_tracking_data {
    double x;
    double y;
    double teta;
    char* name;
    double speed;
    double time;
    pthread_mutex_t mutex;
    char *serverip;
} robot_tracking_data;
robot_tracking_data *robot_tracking_data_create(char *name,char *serverip);
void *thread_pos(void *param);
void robot_tracking_data_subscrib(robot_tracking_data *r);

void robot_tracking_data_get(robot_tracking_data *r, double *x, double *y, double *teta, double *speed, double *time);
void robot_tracking_init();
void robot_tracking_term();
#endif
