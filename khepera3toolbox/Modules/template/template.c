
/*!
 * (c) 2006 - 2008 EPFL, Lausanne, Switzerland
 * Thomas Lochmatter
 */

#include <stdio.h>
#include "template.h"

void template_init() {
}

void template_function1(struct sTemplate *templates) {
    templates->variable1 = 3;
    templates->variable2 = 10.5;
}

float template_function2(struct sTemplate *templates) {
    float sum;

    sum = (float)templates->variable1 + templates->variable2;
    return sum;
}
