#ifndef _REMOTE_VARIABLE_SERVER_HH
#define _REMOTE_VARIABLE_SERVER_HH
#define size_test 10000000/4
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include "remote_variable.h"
#define CMD_SET 0
#define CMD_BROADCAST 1
#define CMD_GET 2
typedef struct client_remote_var {
    int socket;
    pthread_mutex_t mutex;
    pthread_mutex_t ready;
    char *location;
    int port;
    int open;

}client_remote_var;


extern int listen_server_filehandle;
extern int listen_connection_filehandle;
extern fd_set readfs;
extern int max_socket;
int remote_variable_load_file_server(char *file,client_remote_var *origs);
//! init the librarie
void init_remote_var();
int register_client_remote_var(client_remote_var *c);
void _broadcast(char *name);
//! broadcast a variable
/*!
\param cli the client who whant broadcast variable
\param name the name of the variable to broadcast
 */
void remote_variable_server_broadcast(client_remote_var *cli,char *name);
//! get an char array
/*!
\param cli the client
\param var the name of the wanted variable
\return the
 */
int remote_server_variable_get_str_var(client_remote_var *s,char *var,int *size,char **res);
//! get an integer variable
/*!
\param cli the client 
\param var the name of the wanted variable
\return the integer value of var
 */
int remote_server_variable_get_int_var(client_remote_var *s,char *var);
//! get a double floating variable
/*!
\param cli the client 
\param var the name of the wanted variable
\return the double value of var
 */
double remote_server_variable_get_double_var(client_remote_var *s,char *var);
//! get a floating variable
/*!
\param cli the client 
\param var the name of the wanted variable
\return the float value of var
 */
float remote_server_variable_get_float_var(client_remote_var *s,char *var);


//! get an int array
/*!
\param cli the client 
\param var the name of the wanted array
\param size point on the variable to put the size of the returned array
\param res the returned array
\return 0 if ok -1 if ko
 */
int remote_server_variable_get_int_array(client_remote_var *s,char *var,int *size,int **res);
//! get a double array
/*!
\param cli the client 
\param var the name of the wanted array
\param size point on the variable to put the size of the returned array
\param res the returned array
\return 0 if ok -1 if ko
 */
int remote_server_variable_get_double_array(client_remote_var *s,char *var,int *size,double **res);
//! get a float array
/*!
\param cli the client 
\param var the name of the wanted array
\param size point on the variable to put the size of the returned array
\param res the returned array
\return 0 if ok -1 if ko (array not exist)
 */
int remote_server_variable_get_float_array(client_remote_var *s,char *var,int *size,float **res);
//! set an int array
/*!
\param cli the client 
\param var the name of the wanted array
\param size size of array
\param res array to set
 */
void remote_server_variable_set_int_array(client_remote_var *s,char *var ,int size,int *array);
//! set a float array
/*!
\param cli the client 
\param var the name of the wanted array
\param size size of array
\param res array to set
 */
void remote_server_variable_set_float_array(client_remote_var *s,char *var ,int size,float *array);
//! set a double array
/*!
\param cli the client 
\param var the name of the wanted array
\param size size of array
\param res array to set
 */
void remote_server_variable_set_double_array(client_remote_var *s,char *var,int size,double *array);
//! set an int var
/*!
\param cli the client 
\param var the name of the var to set
\param v value to set
 */
void remote_server_variable_set_int_var(client_remote_var *s,char *var,int v);
//! set a double var
/*!
\param cli the client 
\param var the name of the var to set
\param v value to set
 */
void remote_server_variable_set_double_var(client_remote_var *s,char *var,double v);
//! set a float var
/*!
\param cli the client 
\param var the name of the var to set
\param v value to set
 */
void remote_server_variable_set_float_var(client_remote_var *s,char *var,float v);
//! set a char var
/*!
\param cli the client 
\param var the name of the var to set
\param v value to set
 */
void remote_server_variable_set_char_var(client_remote_var *s,char *var,char v);
//! set a string(char*) var
/*!
\param cli the client 
\param var the name of the var to set
\param v value to set
 */
void remote_server_variable_set_str_var(client_remote_var *s,char *var,char* v);
//! create a new client to a server
/*!
\param location addr of the server
\param port port of the server
\return a client to a server
 */
client_remote_var *new_client_remote_var(char *location,int port);
//! launch a connection thread
/*!
\param client the client
 */
void client_launch_thread(client_remote_var *client);
//! wait the client connection open
/*!
\param cli the client
 */
void client_wait_ready(client_remote_var *cli);



//! internal fonction should not be use by user
char *pack_int_array(int *f,int n,char *name,int *size,int cmd) ;
//! internal fonction should not be use by user
char *pack_double_array(double *f,int n,char *name,int *size,int cmd);
//! internal fonction should not be use by user
char *pack_str(char *f,int n,char *name,int *size,int cmd) ;
//! internal fonction should not be use by user
char *pack_int(int v,char *name,int *size,int cmd) ;
//! internal fonction should not be use by user
char *pack_double(double v,char *name,int *size,int cmd) ;
//! internal fonction should not be use by user
char *pack_float(double v,char *name,int *size,int cmd) ;
//! internal fonction should not be use by user
char *pack_char(double v,char *name,int *size,int cmd) ;


//! internal fonction should not be use by user
int send_cmd(int socket,char *cmd,int size);
//! internal fonction should not be use by user
void client_lock(client_remote_var *client);
//! internal fonction should not be use by user
void client_unlock(client_remote_var *client);
//! internal fonction should not be use by user
char *receiv_cmd(int socket,char *name,int *size,int *cmd,int *type);
//! internal fonction should not be use by user
float *get_float_tab(client_remote_var *cli,char *name,int *size);
//! internal fonction should not be use by user
int *get_int_tab(client_remote_var *cli,char *name,int *size);
//! internal fonction should not be use by user
double *get_double_tab(client_remote_var *cli,char *name,int *size);
//! internal fonction should not be use by user

//! internal fonction should not be use by user
void thread_client_test(void *ptr);
//! internal fonction should not be use by user
int
tcp_connect( const char *address, int port, int *connect_filehandle);
//! internal fonction should not be use by user
void thread_socket(void *punter);

//! internal fonction should not be use by user
int listen_accept() ;

//! internal fonction should not be use by user
int listen_start(int port) ;
//! internal fonction should not be use by user
char *pack_float_array(float *f,int n,char *name,int *size,int cmd) ;
//! internal fonction should not be use by user
char *pack(int n,char *name,int *size,int cmd,int type,int vi,float vf,char vc,double vd,int *vi_array,float *vf_array,char *vstr,double *vd_array) ;
//! internal fonction should not be use by user
void unpack(char *tmp,int *n,int *type,int *vi,float *vf,char *vc,double *vd,int **vi_array,float **vf_array,char **vstr,double **vd_array) ;
//! internal fonction should not be use by user
float *unpack_float_array(char *tmp,int *n);
//! internal fonction should not be use by user
int unpack_int(char *tmp);
//! internal fonction should not be use by user
float unpack_float(char *tmp);
//! internal fonction should not be use by user
double unpack_double(char *tmp);
//! internal fonction should not be use by user
char unpack_char(char *tmp);
//! internal fonction should not be use by user
int *unpack_int_array(char *tmp,int *n);

//! internal fonction should not be use by user
double *unpack_double_array(char *tmp,int *n);
//! internal fonction should not be use by user
char *unpack_char_array(char *tmp,int *n);
//! internal fonction should not be use by user
int _get_int_var(client_remote_var *cli,char *name,int *size);
//! internal fonction should not be use by user
double _get_double_var(client_remote_var *cli,char *name,int *size);
//! internal fonction should not be use by user
float _get_float_var(client_remote_var *cli,char *name,int *size);
//! internal fonction should not be use by user
char _get_char_var(client_remote_var *cli,char *name,int *size);
//! internal fonction should not be use by user
char* _get_charstar_var(client_remote_var *cli,char *name,int *size);
#endif
