#define size_test 10000000/4
#include "remote_variable_server.h"
int listen_server_filehandle;
int listen_connection_filehandle;
fd_set readfs;
int max_socket;

#define MAX_CLI 4096
client_remote_var *_global_client[MAX_CLI];
int max_client;
void init_remote_var()
{
    int i;
    max_client=0;
    for (i=0;i<MAX_CLI;i++)
    {
        _global_client[i]=NULL;
    }
}

int register_client_remote_var(client_remote_var *c)
{
    int i;
    for (i=0;( i<MAX_CLI);i++)
    {
        if (_global_client[i]==NULL)
        {
            _global_client[i]=c;
            if (i>max_client)
                max_client=i;
            return 0;
        }
    }
    return -1;
}
void _broadcast(char *name)
{
    int i;
    client_remote_var *c;
    char *bf;
    int type,size;
    int vi;
    float vf;
    double vd;
    char vc;
    char *vstr;
    int arraysize;
    int *viarray;
    float *vfarray;
    double *vdarray;
    get_var( name,&type,&vi,&vd,&vc,&vstr,&vf,&arraysize,&viarray,&vdarray,&vfarray);
    if (type==-1)
    {
        printf("unknow var\n");
    }
    //printf("array size : %d\n",arraysize);
    else
    {
        switch (type)
        {
        case T_INT_ARRAY:
            bf=pack_int_array(viarray,arraysize,name,&size,CMD_SET);
            break;
        case T_DOUBLE_ARRAY:
            bf=pack_double_array(vdarray,arraysize,name,&size,CMD_SET);
            break;
        case T_STR:
            bf=pack_str( vstr,strlen(vstr),name,&size,CMD_SET);
            break;
        case T_FLOAT_ARRAY:
            bf=pack_float_array(vfarray,arraysize,name,&size,CMD_SET);
            break;
        case T_INT:
            bf=pack_int( vi,name,&size,CMD_SET);
            break;
        case T_FLOAT:
            bf=pack_int( vf,name,&size,CMD_SET);
            break;
        case T_DOUBLE:
            bf=pack_int( vd,name,&size,CMD_SET);
            break;
        case T_CHAR:
            bf=pack_int( vc,name,&size,CMD_SET);
            break;
        }
        for (i=0;i<=max_client;i++)
        {

            c=_global_client[i];
            if (c!=NULL)
            {
                pthread_mutex_lock(&(c->mutex));
                if (c->open)
                {
                    ///send
                    send_cmd(c->socket,bf,size);
                }
                pthread_mutex_unlock(&(c->mutex));
            }
        }
        free(bf);
    }
}

int remote_server_variable_get_str_var(client_remote_var *s,char *var,int *size,char **res) {
    *res = _get_charstar_var(s,var,size);
    return 0;
}

int remote_server_variable_get_int_array(client_remote_var *s,char *var,int *size,int **res) {
    *res = get_int_tab(s,var,size);
    return 0;
}
int remote_server_variable_get_double_array(client_remote_var *s,char *var,int *size,double **res) {
    *res = get_double_tab(s,var,size);
    return 0;
}
int remote_server_variable_get_float_array(client_remote_var *s,char *var,int *size,float **res) {
    *res = get_float_tab(s,var,size);
    return 0;
}

int remote_server_variable_get_int_var(client_remote_var *s,char *var) {
    int size;
    return _get_int_var(s,var,&size);
}

double remote_server_variable_get_double_var(client_remote_var *s,char *var) {
    int size;
    return _get_double_var (s,var,&size);
}
float remote_server_variable_get_float_var(client_remote_var *s,char *var) {
    int size;
    return _get_float_var(s,var,&size);
}
void remote_server_variable_set_int_array(client_remote_var *s,char *name ,int size,int *array) {
    int n;
    char *buffer=pack_int_array(array,size,name,&n,CMD_SET);
    pthread_mutex_lock(&(s->mutex));
    send_cmd(s->socket,buffer,n);
    char c;
    read(s->socket,&c,1); //wait ack
    pthread_mutex_unlock(&(s->mutex));
    free(buffer);
}
void remote_server_variable_set_float_array(client_remote_var *s,char *name ,int size,float *array) {
    int n;
    char *buffer=pack_float_array(array,size,name,&n,CMD_SET);
    pthread_mutex_lock(&(s->mutex));
    send_cmd(s->socket,buffer,n);
    char c;
    read(s->socket,&c,1); //wait ack
    pthread_mutex_unlock(&(s->mutex));
    free(buffer);
}
void remote_server_variable_set_double_array(client_remote_var *s,char *name,int size,double *array) {
    int n;
    char *buffer=pack_double_array(array,size,name,&n,CMD_SET);
    pthread_mutex_lock(&(s->mutex));
    send_cmd(s->socket,buffer,n);
    char c;
    read(s->socket,&c,1); //wait ack
    pthread_mutex_unlock(&(s->mutex));
    free(buffer);
}
void remote_server_variable_set_int_var(client_remote_var *s,char *name,int v) {
    int n;
    char *buffer=pack_int (v,name,&n,CMD_SET);
    pthread_mutex_lock(&(s->mutex));
    send_cmd(s->socket,buffer,n);
    char c;
    read(s->socket,&c,1); //wait ack
    pthread_mutex_unlock(&(s->mutex));
    free(buffer);
}
void remote_server_variable_set_double_var(client_remote_var *s,char *name,double v) {
    int n;
    char *buffer=pack_double (v,name,&n,CMD_SET);
    pthread_mutex_lock(&(s->mutex));
    send_cmd(s->socket,buffer,n);
    char c;
    read(s->socket,&c,1); //wait ack
    pthread_mutex_unlock(&(s->mutex));
    free(buffer);
}

void remote_server_variable_set_float_var(client_remote_var *s,char *name,float v) {
    int n;
    char *buffer=pack_float (v,name,&n,CMD_SET);
    pthread_mutex_lock(&(s->mutex));
    send_cmd(s->socket,buffer,n);
    char c;
    read(s->socket,&c,1); //wait ack
    pthread_mutex_unlock(&(s->mutex));
    free(buffer);
}

void remote_server_variable_set_char_var(client_remote_var *s,char *name,char v) {
    int n;
    char *buffer=pack_char (v,name,&n,CMD_SET);
    //pthread_mutex_lock(&(s->mutex));
    while(pthread_mutex_trylock(&(s->mutex))!=0){
        printf("WAITING TO SET CHAR ");
        usleep(300000);
    }
    send_cmd(s->socket,buffer,n);
    char c;
    read(s->socket,&c,1); //wait ack
    pthread_mutex_unlock(&(s->mutex));
    free(buffer);
}
void remote_server_variable_set_str_var(client_remote_var *s,char *name,char* v) {
    int n;
    char *buffer=pack_str (v,strlen(v),name,&n,CMD_SET);
    pthread_mutex_lock(&(s->mutex));
    send_cmd(s->socket,buffer,n);
    char c;
    read(s->socket,&c,1); //wait ack
    pthread_mutex_unlock(&(s->mutex));
    free(buffer);
}
client_remote_var *new_client_remote_var(char *location,int port){
    client_remote_var *res = (client_remote_var*)malloc(sizeof(client_remote_var));
    res->open=0;
    res->port=port;
    res->location = location;
    pthread_mutex_init(&(res->mutex),NULL);
    pthread_mutex_init(&(res->ready),NULL);
    pthread_mutex_lock(&(res->ready));
    return res;
}
void client_wait_ready(client_remote_var *cli)
{
    pthread_mutex_lock(&(cli->ready));
    pthread_mutex_unlock(&(cli->ready));
}
void client_launch_thread(client_remote_var *client)
{
    pthread_t t;
    pthread_create (&t, NULL,(void* (*)(void*))thread_client_test , client);
}
int send_cmd(int socket,char *cmd,int size)
{
    int n=0;
    int r;
    int i=0;
    while (n<size)
    {
        r=write(socket,cmd+n,size-n);
        if (r>0)
        {
            n+=r;
            i++;
        }
        if (r==0)
        {
            return -1;
        }
    }
    return 0;
}
void client_lock(client_remote_var *client)
{
    pthread_mutex_lock(&(client->mutex));
}
void client_unlock(client_remote_var *client)
{
    pthread_mutex_unlock(&(client->mutex));
}
char *receiv_cmd(int socket,char *name,int *size,int *cmd,int *type)
{
    int sock=socket;
    char tmp[256];
    char buffer[256];
    char *buffer2;
    int n,r,j;
    int state;
    // int size;
    int offset;
    //int cmd,type;
    //  char name[256];
    state=0;
    n=0;
    usleep(50000);
    while (state<4)
    {
        j=0;
        do {
            r=read(sock,buffer+n,1);
            if (r>=0)
            {
                tmp[j++]=buffer[n];
                if (r==0)
                {
                    *type=-2;
                    return NULL;
                }
                n+=r;
            }
        }
        while (buffer[n-r]!='\t');
        tmp[j-1]='\0';
        switch (state)
        {
        case 0:
            //        printf("cmd : %s\n",tmp);
            if (!strcmp(tmp,"GET"))
            {
                *cmd=CMD_GET;
            }
            if (!strcmp(tmp,"SET"))
            {
                *cmd=CMD_SET;
            }
            if (!strcmp(tmp,"BCAST"))
            {
                *cmd=CMD_BROADCAST;
            }
            if (!strcmp(tmp,"unknow var"))
            {
                *type=-1;
                return NULL;
            }
            // *cmd=atoi(tmp);
            break;
        case 1:
            //printf("name : %s %d\n",tmp,*cmd);
            memcpy(name,tmp,strlen(tmp)+1);
            if ((*cmd==CMD_GET) || (*cmd==CMD_BROADCAST))
            {
                state=3;
                *size=0;
            }
            break;
        case 2:
            //printf("type : %d\n",atoi(tmp));
            *type=atoi(tmp);
            break;
        case 3:
            //printf("syze : %d\n",atoi(tmp));
            *size=atoi(tmp);
            break;
        default:
            break;
        }
        state++;
    }
    offset=n;
    n=0;
    j=0;
    if (*size!=0)
    {
        switch (*type)
        {
        case T_INT:
            (*size)*=sizeof(int);
            break;
        case T_FLOAT:
            (*size)*=sizeof(float);
            break;
        case T_DOUBLE:
            (*size)*=sizeof(double);
            break;
        case T_CHAR:
            (*size)*=sizeof(char);
            break;
        case T_STR:
            (*size)*=sizeof(char);
            break;
        case T_INT_ARRAY:
            (*size)*=sizeof(int);
            break;
        case T_FLOAT_ARRAY:
            (*size)*=sizeof(float);
            break;
        case T_DOUBLE_ARRAY:
            (*size)*=sizeof(double);
            break;
        }
        buffer2=(char*)malloc(sizeof(char)*(*size+offset+256));
        memcpy( buffer2,buffer,offset);
        do {
            r=read(sock,buffer2+n+offset,128);
            n+=r;
            if (r==0)
            {
                *type=-2;
                free(buffer2);
                return NULL;
            }
        }
        while (n<*size);
        (*size)+=offset;
        buffer2[(*size)+1]='\0';
    }
    else
    {
        buffer2=(char*)malloc(sizeof(char)*(offset+20));
        memcpy(buffer2,buffer,offset);
        buffer2[offset]='\0';
    }
    return buffer2;
}
int _get_int_var(client_remote_var *cli,char *name,int *size)
{
    char buffer[128];
    char name2[256];
    int cmd;
    int type;
    char *result;
    int tab;
    sprintf(buffer,"GET\t%s\t",name);
    pthread_mutex_lock(&(cli->mutex));
    send_cmd(cli->socket,buffer,strlen(buffer));
    result=receiv_cmd(cli->socket,name2,size,&cmd,&type);
    pthread_mutex_unlock(&(cli->mutex));
    if (result!=NULL)
        tab=unpack_int(result);
    else
        tab=-1;
    return tab;
}
void remote_variable_server_broadcast(client_remote_var *cli,char *name)
{
    char buffer[128];
    sprintf(buffer,"BCAST\t%s\t",name);
    pthread_mutex_lock(&(cli->mutex));
    send_cmd(cli->socket,buffer,strlen(buffer));
    pthread_mutex_unlock(&(cli->mutex));
}
double _get_double_var(client_remote_var *cli,char *name,int *size)
{
    char buffer[128];
    char name2[256];
    int cmd;
    int type;
    char *result;
    double tab;
    sprintf(buffer,"GET\t%s\t",name);
    pthread_mutex_lock(&(cli->mutex));
    send_cmd(cli->socket,buffer,strlen(buffer));
    result=receiv_cmd(cli->socket,name2,size,&cmd,&type);
    pthread_mutex_unlock(&(cli->mutex));
    if (result!=NULL)
        tab=unpack_double(result);
    else
        tab=-1.0;
    return tab;
}
float _get_float_var(client_remote_var *cli,char *name,int *size)
{
    char buffer[128];
    char name2[256];
    int cmd;
    int type;
    char *result;
    float tab;
    sprintf(buffer,"GET\t%s\t",name);
    pthread_mutex_lock(&(cli->mutex));
    send_cmd(cli->socket,buffer,strlen(buffer));
    result=receiv_cmd(cli->socket,name2,size,&cmd,&type);
    pthread_mutex_unlock(&(cli->mutex));
    if (result!=NULL)
        tab=unpack_float(result);
    else
        tab=-1.0;
    return tab;
}
char _get_char_var(client_remote_var *cli,char *name,int *size)
{
    char buffer[128];
    char name2[256];
    int cmd;
    int type;
    char *result;
    char tab;
    sprintf(buffer,"GET\t%s\t",name);
    pthread_mutex_lock(&(cli->mutex));
    send_cmd(cli->socket,buffer,strlen(buffer));
    result=receiv_cmd(cli->socket,name2,size,&cmd,&type);
    pthread_mutex_unlock(&(cli->mutex));
    if (result!=NULL)
        tab=unpack_char(result);
    else
        tab=-1;
    return tab;
}
char* _get_charstar_var(client_remote_var *cli,char *name,int *size)
{
    char buffer[128];
    char name2[256];
    int cmd;
    int type;
    char *result;
    char *tab;
    sprintf(buffer,"GET\t%s\t",name);
    pthread_mutex_lock(&(cli->mutex));
    send_cmd(cli->socket,buffer,strlen(buffer));
    result=receiv_cmd(cli->socket,name2,size,&cmd,&type);
    pthread_mutex_unlock(&(cli->mutex));
    if (result!=NULL)
        tab=unpack_char_array(result,size);
    else
        tab=NULL;
    return tab;
}

float *get_float_tab(client_remote_var *cli,char *name,int *size)
{
    char buffer[128];
    char name2[256];
    int cmd;
    int type;
    char *result;
    float *tab;
    sprintf(buffer,"GET\t%s\t",name);
    pthread_mutex_lock(&(cli->mutex));
    send_cmd(cli->socket,buffer,strlen(buffer));
    result=receiv_cmd(cli->socket,name2,size,&cmd,&type);
    pthread_mutex_unlock(&(cli->mutex));
    if (result!=NULL)
        tab=unpack_float_array(result,size);
    else
        tab=NULL;
    return tab;
}

int *get_int_tab(client_remote_var *cli,char *name,int *size)
{
    char buffer[128];
    char name2[256];
    int cmd;
    int type;
    char *result;
    int *tab;
    sprintf(buffer,"GET\t%s\t",name);
    pthread_mutex_lock(&(cli->mutex));
    send_cmd(cli->socket,buffer,strlen(buffer));
    result=receiv_cmd(cli->socket,name2,size,&cmd,&type);
    pthread_mutex_unlock(&(cli->mutex));
    if (result!=NULL)
        tab=unpack_int_array(result,size);
    else
        tab=NULL;
    return tab;
}

double *get_double_tab(client_remote_var *cli,char *name,int *size)
{
    char buffer[128];
    char name2[256];
    int cmd;
    int type;
    char *result;
    double *tab;
    sprintf(buffer,"GET\t%s\t",name);
    pthread_mutex_lock(&(cli->mutex));
    send_cmd(cli->socket,buffer,strlen(buffer));
    result=receiv_cmd(cli->socket,name2,size,&cmd,&type);
    pthread_mutex_unlock(&(cli->mutex));
    if (result!=NULL)
        tab=unpack_double_array(result,size);
    else
        tab=NULL;
    return tab;
}
void thread_client_test(void *ptr)
{
    float *tab;
    client_remote_var *client = (client_remote_var *)ptr;
    tab=(float*)malloc(sizeof(float)*size_test);
    //sleep(1);
    while (1)
    {
        client_lock(client);
        if (client->open==0)
        {
            if(tcp_connect(client->location,client->port,&(client->socket)))
            {
                client->open=1;
            }
            client_unlock(client);
            pthread_mutex_unlock(&(client->ready));
        }
        else
        {
            client_unlock(client);
        }
        sleep(40);
    }
    pthread_exit(NULL);
}
int tcp_connect( const char *address, int port, int *connect_filehandle)
{
    struct sockaddr_in serv_address;
    struct hostent *serv_host;
    int connect_res;
    // Don't touch anything if the port is zero
    if (port == 0) {
        return 0;
    }
    // Look for host
    serv_host = NULL;
    serv_host = gethostbyname(address);
    if ( serv_host == NULL ) {
        fprintf(stderr, "Unable to get server host.");
        return 0;
    }
    // Create Socket handle
    // printf("Create Socket handle\n");
    *connect_filehandle = socket( AF_INET, SOCK_STREAM, 0);
    if (*connect_filehandle < 0) {
        fprintf(stderr, "Unable to create socket: Error %d.\n", *connect_filehandle);
        *connect_filehandle = -1;
        return 0;
    }
    // Connect
    memset( &serv_address, 0x00, sizeof(serv_address));
    serv_address.sin_family = serv_host->h_addrtype;
    serv_address.sin_port = htons(port);
    memcpy( &serv_address.sin_addr, serv_host->h_addr, serv_host->h_length);
    connect_res = connect( *connect_filehandle,
                           (struct sockaddr *) &serv_address, sizeof(serv_address));
    if ( connect_res < 0 ) {
        fprintf(stderr, "Connect failed: Error %d.\n", connect_res);
        *connect_filehandle = -1;
        return 0;
    }
    // IMPORTANT : set fd as non blocking.
    //fcntl(*connect_filehandle, F_SETFL, O_NONBLOCK);
    // Success!
    fprintf(stderr, "Connected on TCP port %d.\n", port);
    return 1;
}
void thread_socket(void *punter)
{
    int *ptr=(int*)punter;
    int sock=*ptr;
    char *buffer2;
    int n;
    int size;
    int cmd,type;
    char name[256];
    while (1)
    {
        buffer2=receiv_cmd( sock,name,&size,&cmd,&type);
        if (buffer2==NULL)
        {
            pthread_exit(NULL);
        }
        int *resulti;
        float *resultf;
        double *resultd;
        int vi;
        float vf;
        double vd;
        char vc;
        char *vstr;
        int arraysize;
        int *viarray;
        float *vfarray;
        double *vdarray;
        if (cmd==CMD_SET)
        {
            switch (type)
            {
            case T_INT:
                vi = unpack_int(buffer2);
                add_var(name,type,vi,0.0,0,NULL,0.0);
                break;
            case T_FLOAT:
                vf = unpack_float(buffer2);
                add_var(name,type,0,0.0,0,NULL,vf);
                break;
            case T_DOUBLE:
                vd = unpack_double(buffer2);
                add_var(name,type,0,vd,0,NULL,0.0);
                break;
            case T_CHAR:
                vc = unpack_char(buffer2);
                add_var(name,type,0,0.0,vc,NULL,0.0);
                break;
            case T_STR:
                vstr=unpack_char_array(buffer2,&n);
                add_var(name,type,0,0.0,0,vstr,0.0);
                break;
            case T_INT_ARRAY:
                resulti=unpack_int_array(buffer2,&n);
                add_array(name,type,n,resulti,NULL,NULL);
                break;
            case T_FLOAT_ARRAY:
                resultf=unpack_float_array(buffer2,&n);
                add_array(name,type,n,NULL,NULL,resultf);
                //free(resultf);
                break;
            case T_DOUBLE_ARRAY:
                resultd=unpack_double_array(buffer2,&n);
                add_array(name,type,n,NULL,resultd,NULL);
                break;
            }
            send_cmd(sock,"a",1);
        }
        else if ( cmd == CMD_GET)
        {
            char *bf;
            fflush(NULL);
            get_var( name,&type,&vi,&vd,&vc,&vstr,&vf,&arraysize,&viarray,&vdarray,&vfarray);
            if (type==-1)
            {
                bf=(char*)malloc(sizeof(char)*(strlen("unknow var\t")+1));
                sprintf(bf,"unknow var\t");
                size=strlen("unknow var\t");
            }
            //printf("array size : %d\n",arraysize);
            else
            {
                switch (type)
                {
                case T_INT_ARRAY:
                    bf=pack_int_array(viarray,arraysize,name,&size,CMD_SET);
                    break;
                case T_DOUBLE_ARRAY:
                    bf=pack_double_array(vdarray,arraysize,name,&size,CMD_SET);
                    break;
                case T_STR:
                    bf=pack_str( vstr,strlen(vstr),name,&size,CMD_SET);
                    break;
                case T_FLOAT_ARRAY:
                    bf=pack_float_array(vfarray,arraysize,name,&size,CMD_SET);
                    break;
                case T_INT:
                    bf=pack_int( vi,name,&size,CMD_SET);
                    break;
                case T_FLOAT:
                    bf=pack_int( vf,name,&size,CMD_SET);
                    break;
                case T_DOUBLE:
                    bf=pack_int( vd,name,&size,CMD_SET);
                    break;
                case T_CHAR:
                    bf=pack_int( vc,name,&size,CMD_SET);
                    break;
                }
            }
            send_cmd(sock,bf,size);
            free(bf);
        }
        else if (cmd==CMD_BROADCAST)
        {
            _broadcast(name);
        }
        free(buffer2);
        fflush(NULL);
    }
}
int listen_accept() {
    struct sockaddr_in sc;
    socklen_t sclen;
    int fh;
    int *ptr;
    pthread_t t;
    // If there is no server, do nothing
    if (listen_server_filehandle < 0) {
        return 0;
    }
    // Accept
    //printf("acceptation...\n");
    sclen = sizeof(sc);
    fh = accept(listen_server_filehandle, (struct sockaddr *) & sc, &sclen);
    //printf("accepted\n");
    if (fh < 0) {
        if ((errno != EINTR) && (errno != EAGAIN)) {
            fprintf(stderr, "Accept failed (Error %d): connection request ignored.\n", errno);
        }
        return 0;
    }
    if (fh>=max_socket)
    {
        max_socket=fh;
    }
    printf("Connection accepted.\n");
    fflush(NULL);
    ptr=(int*)malloc(sizeof(int));
    *ptr=fh;
    pthread_create (&t, NULL,(void* (*)(void*))thread_socket , (void*)ptr);
    listen_connection_filehandle = fh;
    return 1;
}
int listen_start(int port) {
    struct sockaddr_in sa;
    struct hostent *host;
    int bindres, listenres;
    // Don't touch anything if the port is zero
    if (port == 0) {
        return 0;
    }
    // Initialize
    listen_server_filehandle = -1;
    listen_connection_filehandle = -1;
    // Create socket handle
    listen_server_filehandle = socket(AF_INET, SOCK_STREAM, 0);
    if (listen_server_filehandle < 0) {
        fprintf(stderr, "Unable to create socket: Error %d.\n", listen_server_filehandle);
        listen_server_filehandle = -1;
        return 0;
    }
    int on=1;
    setsockopt(listen_server_filehandle, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));
    // Bind (on all local addresses)
    host = gethostbyname("0.0.0.0");
    memcpy((char *)&sa.sin_addr, (char *)host->h_addr, host->h_length);
    sa.sin_family = host->h_addrtype;
    sa.sin_port = htons(port);
    bindres = bind(listen_server_filehandle, (struct sockaddr *) & sa, sizeof(sa));
    if (bindres < 0) {
        if (bindres == EACCES) {
            fprintf(stderr, "Bind failed: You do not have enough access rights to listen on port %d.\n", port);
        } else {
            perror("bind");
            fprintf(stderr, "Bind failed: Error %d.\n", bindres);
        }
        listen_server_filehandle = -1;
        return 0;
    }
    // Listen
    listenres = listen(listen_server_filehandle, SOMAXCONN);
    if (listenres < 0) {
        if (listenres == EADDRINUSE) {
            fprintf(stderr, "Listen failed: Port %d is already in use.\n", port);
        } else {
            fprintf(stderr, "Listen failed: Error %d.\n", listenres);
        }
        listen_server_filehandle = -1;
        return 0;
    }
    max_socket=listen_server_filehandle;
    return 1;
}

char *pack_float_array(float *f,int n,char *name,int *size,int cmd) {
    return pack( n,name,size,cmd,T_FLOAT_ARRAY,0,0.0,0,0.0,NULL,f,NULL,NULL);
}
char *pack_int_array(int *f,int n,char *name,int *size,int cmd) {
    return pack(n,name,size,cmd,T_INT_ARRAY,0,0.0,0,0.0,f,NULL,NULL,NULL);
}
char *pack_double_array(double *f,int n,char *name,int *size,int cmd) {
    return pack(n,name,size,cmd,T_DOUBLE_ARRAY,0,0.0,0,0.0,NULL,NULL,NULL,f);
}
char *pack_str(char *f,int n,char *name,int *size,int cmd) {
    return pack(n,name,size,cmd,T_STR,0,0.0,0,0.0,NULL,NULL,f,NULL);
}
char *pack_int(int v,char *name,int *size,int cmd) {
    return pack(1,name,size,cmd,T_INT,v,0.0,0,0.0,NULL,NULL,NULL,NULL);
}
char *pack_double(double v,char *name,int *size,int cmd) {
    return pack(1,name,size,cmd,T_DOUBLE,0,0.0,0,v,NULL,NULL,NULL,NULL);
}
char *pack_float(double v,char *name,int *size,int cmd) {
    return pack(1,name,size,cmd,T_FLOAT,0,v,0,0.0,NULL,NULL,NULL,NULL);
}
char *pack_char(double v,char *name,int *size,int cmd) {
    return pack(1,name,size,cmd,T_CHAR,0,0.0,v,0.0,NULL,NULL,NULL,NULL);
}
char *pack(int n,char *name,int *size,int cmd,int type,int vi,float vf,char vc,double vd,int *vi_array,float *vf_array,char *vstr,double *vd_array) {
    char *result = (char*)malloc(256+sizeof(double)*(n+1));
    switch (cmd)
    {
    case CMD_SET:
        sprintf(result,"SET");
        break;
    case CMD_GET:
        sprintf(result,"GET");
        break;
    default:
        sprintf(result,"GET");
        break;
    }
    sprintf(result,"%s\t%s\t%d\t%d\t",result,name,type,n);
    switch (type)
    {
    case T_INT:
        *size=strlen(result)+sizeof(int);
        memcpy(result+strlen(result),&vi,sizeof(int));
        break;
    case T_FLOAT:
        *size=strlen(result)+sizeof(float);
        memcpy(result+strlen(result),&vf,sizeof(float));
        break;
    case T_CHAR:
        *size=strlen(result)+sizeof(char);
        memcpy(result+strlen(result),&vc,sizeof(char));
        break;
    case T_DOUBLE:
        *size=strlen(result)+sizeof(double);
        memcpy(result+strlen(result),&vd,sizeof(double));
        break;
    case T_INT_ARRAY:
        *size=strlen(result)+sizeof(int)*n;
        memcpy(result+strlen(result),vi_array,sizeof(int)*n);
        break;
    case T_FLOAT_ARRAY:
        *size=strlen(result)+sizeof(float)*n;
        memcpy(result+strlen(result),vf_array,sizeof(float)*n);
        break;
    case T_CHARSTAR:
        *size=strlen(result)+sizeof(char)*n;
        memcpy(result+strlen(result),vstr,sizeof(char)*n);
        break;
    case T_DOUBLE_ARRAY:
        *size=strlen(result)+sizeof(double)*n;
        memcpy(result+strlen(result),vd_array,sizeof(double)*n);
        break;
    }
    return result;
}

void unpack(char *tmp,int *n,int *type,int *vi,float *vf,char *vc,double *vd,
            int **vi_array,float **vf_array,char **vstr,double **vd_array) {
    char name[256];
    char temp[256];
    //int type;
    int size;
    int i;
    char *ptr=tmp;
    int cmd=-1;
    i=0;
    while (*ptr!='\t')
    {
        temp[i]=*ptr;
        i++;
        ptr++;
    }
    temp[i]='\0';
    if (!strcmp(temp,"SET"))
    {
        cmd=CMD_SET;
    }
    i=0;
    ptr++;
    while (*ptr!='\t')
    {
        name[i]=*ptr;
        i++;
        ptr++;
    }
    name[i]='\0';
    i=0;
    ptr++;
    while (*ptr!='\t')
    {
        temp[i]=*ptr;
        i++;
        ptr++;
    }
    temp[i]='\0';
    *type=atoi(temp);
    i=0;
    ptr++;
    while (*ptr!='\t')
    {
        temp[i]=*ptr;
        i++;
        ptr++;
    }
    temp[i]='\0';
    size=atoi(temp);
    ptr++;
    switch (*type)
    {
    case T_INT:
        memcpy(vi,ptr,sizeof(int));
        break;
    case T_FLOAT:
        memcpy(vf,ptr,sizeof(float));
        break;
    case T_DOUBLE:
        memcpy(vd,ptr,sizeof(double));
        break;
    case T_CHAR:
        memcpy(vc,ptr,sizeof(char));
        break;
    case T_INT_ARRAY:
        *vi_array=(int*)malloc(sizeof(int)*size);
        memcpy(*vi_array,ptr,sizeof(int)*size);
        break;
    case T_FLOAT_ARRAY:
        *vf_array=(float*)malloc(sizeof(float)*size);
        memcpy(*vf_array,ptr,sizeof(float)*size);
        break;
    case T_DOUBLE_ARRAY:
        *vd_array=(double*)malloc(sizeof(double)*size);
        memcpy(*vd_array,ptr,sizeof(double)*size);
        break;
    case T_CHARSTAR:
        *vstr=(char*)malloc(sizeof(char)*size);
        memcpy(*vstr,ptr,sizeof(char)*size);
        break;
    }
    *n=size;
}
float *unpack_float_array(char *tmp,int *n)
{
    float *resultfa;
    float resultf;
    double *resultda;
    double resultd;
    int *resultia;
    int resulti;
    char resultc;
    char *resultca;
    int type;
    unpack(tmp,n,&type,&resulti,&resultf,&resultc,&resultd,&resultia,&resultfa,&resultca,&resultda);
    switch (type)
    {
    case T_INT_ARRAY:
        free(resultia);
        break;
    case T_DOUBLE_ARRAY:
        free(resultda);
        break;
    case T_CHARSTAR:
        free(resultca);
        break;
    }
    if (type==T_FLOAT_ARRAY)
        return resultfa;
    else
        return NULL;
}

int unpack_int(char *tmp)
{
    float *resultfa;
    float resultf;
    double *resultda;
    double resultd;
    int *resultia;
    int resulti;
    char resultc;
    char *resultca;
    int type;
    int n;
    unpack(tmp,&n,&type,&resulti,&resultf,&resultc,&resultd,&resultia,&resultfa,&resultca,&resultda);
    switch (type)
    {
    case T_FLOAT_ARRAY:
        free(resultfa);
        break;
    case T_DOUBLE_ARRAY:
        free(resultda);
        break;
    case T_INT_ARRAY:
        free(resultia);
        break;
    case T_CHARSTAR:
        free(resultca);
        break;
    }
    if (type==T_INT)
        return resulti;
    else
        return 0;
}
float unpack_float(char *tmp)
{
    float *resultfa;
    float resultf;
    double *resultda;
    double resultd;
    int *resultia;
    int resulti;
    char resultc;
    char *resultca;
    int type;
    int n;
    unpack(tmp,&n,&type,&resulti,&resultf,&resultc,&resultd,&resultia,&resultfa,&resultca,&resultda);
    switch (type)
    {
    case T_FLOAT_ARRAY:
        free(resultfa);
        break;
    case T_DOUBLE_ARRAY:
        free(resultda);
        break;
    case T_INT_ARRAY:
        free(resultia);
        break;
    case T_CHARSTAR:
        free(resultca);
        break;
    }
    if (type==T_FLOAT)
        return resultf;
    else
        return 0;
}
double unpack_double(char *tmp)
{
    float *resultfa;
    float resultf;
    double *resultda;
    double resultd;
    int *resultia;
    int resulti;
    char resultc;
    char *resultca;
    int type;
    int n;
    unpack(tmp,&n,&type,&resulti,&resultf,&resultc,&resultd,&resultia,&resultfa,&resultca,&resultda);
    switch (type)
    {
    case T_FLOAT_ARRAY:
        free(resultfa);
        break;
    case T_DOUBLE_ARRAY:
        free(resultda);
        break;
    case T_INT_ARRAY:
        free(resultia);
        break;
    case T_CHARSTAR:
        free(resultca);
        break;
    }
    if (type==T_DOUBLE)
        return resultd;
    else
        return 0;
}
char unpack_char(char *tmp)
{
    float *resultfa;
    float resultf;
    double *resultda;
    double resultd;
    int *resultia;
    int resulti;
    char resultc;
    char *resultca;
    int type;
    int n;
    unpack(tmp,&n,&type,&resulti,&resultf,&resultc,&resultd,&resultia,&resultfa,&resultca,&resultda);
    switch (type)
    {
    case T_FLOAT_ARRAY:
        free(resultfa);
        break;
    case T_DOUBLE_ARRAY:
        free(resultda);
        break;
    case T_INT_ARRAY:
        free(resultia);
        break;
    case T_CHARSTAR:
        free(resultca);
        break;
    }
    if (type==T_CHAR)
        return resultc;
    else
        return 0;
}

int *unpack_int_array(char *tmp,int *n)
{
    float *resultfa;
    float resultf;
    double *resultda;
    double resultd;
    int *resultia;
    int resulti;
    char resultc;
    char *resultca;
    int type;
    unpack(tmp,n,&type,&resulti,&resultf,&resultc,&resultd,&resultia,&resultfa,&resultca,&resultda);
    switch (type)
    {
    case T_FLOAT_ARRAY:
        free(resultfa);
        break;
    case T_DOUBLE_ARRAY:
        free(resultda);
        break;
    case T_CHARSTAR:
        free(resultca);
        break;
    }
    if (type==T_INT_ARRAY)
        return resultia;
    else
        return NULL;
}


double *unpack_double_array(char *tmp,int *n)
{
    float *resultfa;
    float resultf;
    double *resultda;
    double resultd;
    int *resultia;
    int resulti;
    char resultc;
    char *resultca;
    int type;
    unpack(tmp,n,&type,&resulti,&resultf,&resultc,&resultd,&resultia,&resultfa,&resultca,&resultda);
    switch (type)
    {
    case T_FLOAT_ARRAY:
        free(resultfa);
        break;
    case T_INT_ARRAY:
        free(resultia);
        break;
    case T_CHARSTAR:
        free(resultca);
        break;
    }
    if (type==T_DOUBLE_ARRAY)
        return resultda;
    else
        return NULL;
}

char *unpack_char_array(char *tmp,int *n)
{
    float *resultfa;
    float resultf;
    double *resultda;
    double resultd;
    int *resultia;
    int resulti;
    char resultc;
    char *resultca;
    int type;
    unpack(tmp,n,&type,&resulti,&resultf,&resultc,&resultd,&resultia,&resultfa,&resultca,&resultda);
    switch (type)
    {
    case T_FLOAT_ARRAY:
        free(resultfa);
        break;
    case T_INT_ARRAY:
        free(resultia);
        break;
    case T_DOUBLE_ARRAY:
        free(resultda);
        break;

    }
    if (type==T_CHARSTAR)
        return resultca;
    else
        return NULL;
}

int remote_variable_load_file_server(char *file,client_remote_var *origs)
{
    FILE *f;
    char buff[256];
    int n;
    int t;
    client_remote_var *s;
    f=fopen(file,"r");
    if (!f)
    {
        printf("no file %s\n",file);
        return -1;
    }
    while (!feof(f))
    {
        fscanf(f,"%s %d %d\n",buff,&n,&t);
        if (strlen(buff)>3)
        {
            s = new_client_remote_var(buff,n);
            if (!((s->port == origs->port) && !(strcmp(origs->location,s->location))))
            {
                client_launch_thread(s);
                //open_connection(s);
                register_client_remote_var(s);
                //register_remoteserver(s);
            }
        }
    }
    fclose(f);
    return 1;
}
