/**
 * \file
 * ColorSensor library declarations.
 *
 * \ingroup colorsensor
 */


#ifndef _color_sensor_hh
#define _color_sensor_hh
#include <stdint.h>
#define COLOR_SENSOR_ON 1
#define COLOR_SENSOR_OFF 0
typedef struct color_sensor_value {
    int red;
    int green;
    int blue;
    int clear;
} color_sensor_value;


//!return -1 if ko, the serial port descriptor if ok
/*!
\param port serial port (example: "/dev/ttyUSB0")
 */
int color_sensor_init(char *port);


//!calibrate all sensor on red color, generally must be NOT USED (prefer use the manual calibration with the sensor_color_calibration tool)
void color_sensor_calibratered();
//!calibrate all sensor on green color, generally must be NOT USED (prefer use the manual calibration with the sensor_color_calibration tool)
void color_sensor_calibrategreen();
//!calibrate all sensor on blue color, generally must be NOT USED (prefer use the manual calibration with the sensor_color_calibration tool)
void color_sensor_calibrateblue();
//!calibrate all sensor on white color with HIGH brightness, generally must be NOT USED (prefer use the manual calibration with the sensor_color_calibration tool)
void color_sensor_calibrateclear();
//!active or desactive all the color sensor led
/*!
\param value : 0 desactivate,1 activate
 */
void color_sensor_led(int value);

//!set red gain of a sensor
/*!
\param sensor wanted sensor ID
\param value the gain value
 */
void color_sensor_set_red_gain(int sensor,int value);
//!set clear gain of a sensor
/*!
\param sensor wanted sensor ID
\param value the gain value
 */
void color_sensor_set_clear_gain(int sensor,int value);
//!set green gain of a sensor
/*!
\param sensor wanted sensor ID
\param value the gain value
 */
void color_sensor_set_green_gain(int sensor,int value);
//!set blue gain of a sensor
/*!
\param sensor wanted sensor ID
\param value the gain value
 */
void color_sensor_set_blue_gain(int sensor,int value);
//!deprecated NOT USE
void get_color_sensor_value(color_sensor_value *right,color_sensor_value *left);
//!get all sensor value
/*!
\param tab matrix for result, must be allocated
\param n in this parameter the fonction put the number of sensor found
 */
void get_color_sensor_value2(color_sensor_value *tab,int *n);

//!get all sensor gain
/*!
\param tab matrix for result, must be allocated
\param n in this parameter the fonction put the number of sensor found
 */
void get_color_sensor_gain(color_sensor_value *tab,int *n);

void set_filter_mask(int filter);

//!set clear capacitor number of a sensor
/*!
\param sensor wanted sensor ID
\param value the capacitor number 
 */
void color_sensor_set_clear_cap(int sensor,int value);
//!set red capacitor number of a sensor
/*!
\param sensor wanted sensor ID
\param value the capacitor number 
 */
void color_sensor_set_red_cap(int sensor,int value);
//!set green capacitor number of a sensor
/*!
\param sensor wanted sensor ID
\param value the capacitor number 
 */
void color_sensor_set_green_cap(int sensor,int value);
//!set blue capacitor number of a sensor
/*!
\param sensor wanted sensor ID
\param value the capacitor number 
 */
void color_sensor_set_blue_cap(int sensor,int value);

//!get all sensor capacitor number
/*!
\param tab matrix for result, must be allocated
\param n in this parameter the fonction put the number of sensor found
 */
void get_color_sensor_cap(color_sensor_value *tab,int *n);
//!set clear offset of a sensor
/*!
\param sensor wanted sensor ID
\param value offset 
 */
void color_sensor_set_clear_offset(int sensor,int value);
//!set red offset of a sensor
/*!
\param sensor wanted sensor ID
\param value offset 
 */
void color_sensor_set_red_offset(int sensor,int value);
//!set green offset of a sensor
/*!
\param sensor wanted sensor ID
\param value offset 
 */
void color_sensor_set_green_offset(int sensor,int value);
//!set blue offset of a sensor
/*!
\param sensor wanted sensor ID
\param value offset 
 */
void color_sensor_set_blue_offset(int sensor,int value);
//!get all sensor offset
/*!
\param tab matrix for result, must be allocated
\param n in this parameter the fonction put the number of sensor found
 */
void get_color_sensor_offset(color_sensor_value *tab,int *n);

int color_sensor_save(char *location,int n);
int color_sensor_load(char *location);

void color_sensor_set_red_factor(float f);


void color_sensor_set_green_factor(float f);

void color_sensor_set_blue_factor(float f);

///private
int serialport_init(const char* serialport, int baud);
int serialport_writebyte(int fd, uint8_t b);
int serialport_write(int fd, const char* str);
int serialport_read_until(int fd, char* buf, char until);
int clear_port(int fd);
void color_sensor_ledir1(int value) ;
void color_sensor_ledir2(int value);
#endif