#include <stdio.h>    /* Standard input/output definitions */
#include <stdlib.h>
#include <stdint.h>   /* Standard types */
#include <string.h>   /* String function definitions */
#include <unistd.h>   /* UNIX standard function definitions */
#include <fcntl.h>    /* File control definitions */
#include <errno.h>    /* Error number definitions */
#include <termios.h>  /* POSIX terminal control definitions */
#include <sys/ioctl.h>
#include <getopt.h>
#include <time.h>
#include <signal.h>
#include "color_sensor.h"





int global_fd;
int FILTER_MASK;
int color_sensor_save(char *location,int n)
{
int i;
int _n=n;
color_sensor_value *_cap;
color_sensor_value *_gain;
color_sensor_value *_offset;


FILE *f=fopen(location,"w");
if(!f)
return -1;
_cap = (color_sensor_value*)malloc(sizeof(color_sensor_value)*n);
_gain = (color_sensor_value*)malloc(sizeof(color_sensor_value)*n);
_offset = (color_sensor_value*)malloc(sizeof(color_sensor_value)*n);

get_color_sensor_cap(_cap,&_n);
fprintf(f,"%d",_n);
for(i=0;i<_n;i++)
fprintf(f," %d %d %d %d",_cap[i].clear,_cap[i].red,_cap[i].green,_cap[i].blue);
fprintf(f,"\n");
get_color_sensor_gain(_gain,&_n);
fprintf(f,"%d",_n);
for(i=0;i<_n;i++)
fprintf(f," %d %d %d %d",_gain[i].clear,_gain[i].red,_gain[i].green,_gain[i].blue);
fprintf(f,"\n");
get_color_sensor_offset(_offset,&_n);
fprintf(f,"%d",_n);
for(i=0;i<_n;i++)
fprintf(f," %d %d %d %d",_offset[i].clear,_offset[i].red,_offset[i].green,_offset[i].blue);
fprintf(f,"\n");


free(_cap);
free(_gain);
free(_offset);


fclose(f);
return 1;
}
int color_sensor_load(char *location)
{
int _n;
int i;
color_sensor_value *_cap;
color_sensor_value *_gain;
color_sensor_value *_offset;

FILE *f=fopen(location,"r");
if(!f)
return -1;

fscanf(f,"%d",&_n);
_cap = (color_sensor_value*)malloc(sizeof(color_sensor_value)*_n);
for(i=0;i<_n;i++)
{
fscanf(f,"%d %d %d %d",&(_cap[i].clear),&(_cap[i].red),&(_cap[i].green),&(_cap[i].blue));
color_sensor_set_clear_cap(i,_cap[i].clear);
usleep(10000);
color_sensor_set_red_cap(i,_cap[i].red);
usleep(10000);
color_sensor_set_green_cap(i,_cap[i].green);
usleep(10000);
color_sensor_set_blue_cap(i,_cap[i].blue);
usleep(10000);

}
fscanf(f,"%d",&_n);
printf("%d\n",_n);
_gain = (color_sensor_value*)malloc(sizeof(color_sensor_value)*_n);

for(i=0;i<_n;i++)
{
fscanf(f,"%d %d %d %d",&(_gain[i].clear),&(_gain[i].red),&(_gain[i].green),&(_gain[i].blue));
printf("read : %d %d %d %d\n",(_gain[i].clear),(_gain[i].red),(_gain[i].green),(_gain[i].blue));

color_sensor_set_clear_gain(i,_gain[i].clear);
usleep(10000);
color_sensor_set_red_gain(i,_gain[i].red);
usleep(10000);
color_sensor_set_green_gain(i,_gain[i].green);
usleep(10000);
color_sensor_set_blue_gain(i,_gain[i].blue);
usleep(10000);

}

fscanf(f,"%d",&_n);
printf("%d\n",_n);
_offset = (color_sensor_value*)malloc(sizeof(color_sensor_value)*_n);
for(i=0;i<_n;i++)
{
fscanf(f,"%d %d %d %d",&(_offset[i].clear),&(_offset[i].red),&(_offset[i].green),&(_offset[i].blue));
color_sensor_set_clear_offset(i,_offset[i].clear);
usleep(10000);
color_sensor_set_red_offset(i,_offset[i].red);
usleep(10000);
color_sensor_set_green_offset(i,_offset[i].green);
usleep(10000);
color_sensor_set_blue_offset(i,_offset[i].blue);
usleep(10000);

}

free(_offset);
free(_gain);
free(_cap);
fclose(f);
return 1;
}

void color_sensor_set_red_factor(float f){
    char tmp[128];
    sprintf(tmp,"zr %f\n",f);
    serialport_write(global_fd,tmp);
}

void color_sensor_set_green_factor(float f){
    char tmp[128];
    sprintf(tmp,"zg %f\n",f);
    serialport_write(global_fd,tmp);
}
void color_sensor_set_blue_factor(float f){
    char tmp[128];
    sprintf(tmp,"zb %f\n",f);
    serialport_write(global_fd,tmp);
}
int color_sensor_init(char *port) {
    global_fd = serialport_init(port,115200);
    FILTER_MASK = 0xFFFFFFFF;
//clear_port(global_fd);
    return global_fd;
}
void color_sensor_calibratered() {
    serialport_write(global_fd,"cr\n");
}
void color_sensor_calibrategreen() {
    serialport_write(global_fd,"cg\n");
    // clear_port(global_fd);
}
void color_sensor_calibrateblue() {
    serialport_write(global_fd,"cb\n");
}
void color_sensor_calibrateclear() {
    serialport_write(global_fd,"cc\n");

}
void set_filter_mask(int filter)
{
    FILTER_MASK = filter;

}
void color_sensor_led(int value) {
    if (value)
        serialport_write(global_fd,"lo\n");
    else
        serialport_write(global_fd,"lf\n");
}


void color_sensor_set_red_gain(int sensor,int value) {
    char tmp[128];
    sprintf(tmp,"mr %d %d\n",sensor,value);
    serialport_write(global_fd,tmp);
}
void color_sensor_set_clear_gain(int sensor,int value) {
    char tmp[128];
    sprintf(tmp,"mc %d %d\n",sensor,value);
    serialport_write(global_fd,tmp);
}
void color_sensor_set_green_gain(int sensor,int value) {
    char tmp[128];
    sprintf(tmp,"mg %d %d\n",sensor,value);
    serialport_write(global_fd,tmp);
}
void color_sensor_set_blue_gain(int sensor,int value) {
    char tmp[128];
    sprintf(tmp,"mb %d %d\n",sensor,value);
    serialport_write(global_fd,tmp);

}
void color_sensor_set_clear_cap(int sensor,int value) {
    char tmp[128];
    sprintf(tmp,"ec %d %d\n",sensor,value);
    serialport_write(global_fd,tmp);
}

void color_sensor_set_red_cap(int sensor,int value) {
    char tmp[128];
    sprintf(tmp,"er %d %d\n",sensor,value);
    serialport_write(global_fd,tmp);
}

void color_sensor_set_green_cap(int sensor,int value) {
    char tmp[128];
    sprintf(tmp,"eg %d %d\n",sensor,value);
    serialport_write(global_fd,tmp);
}

void color_sensor_set_blue_cap(int sensor,int value) {
    char tmp[128];
    sprintf(tmp,"eb %d %d\n",sensor,value);
    serialport_write(global_fd,tmp);
}



void color_sensor_set_clear_offset(int sensor,int value) {
    char tmp[128];
    printf("clear offset %d %d\n",sensor,value);
    sprintf(tmp,"oc %d %d\n",sensor,value);
    serialport_write(global_fd,tmp);
}

void color_sensor_set_red_offset(int sensor,int value) {
    char tmp[128];
    sprintf(tmp,"or %d %d\n",sensor,value);
    serialport_write(global_fd,tmp);
}

void color_sensor_set_green_offset(int sensor,int value) {
    char tmp[128];
    sprintf(tmp,"og %d %d\n",sensor,value);
    serialport_write(global_fd,tmp);
}

void color_sensor_set_blue_offset(int sensor,int value) {
    char tmp[128];
    sprintf(tmp,"ob %d %d\n",sensor,value);
    serialport_write(global_fd,tmp);
}



void color_sensor_ledir1(int value) {
    if (value)
        serialport_write(global_fd,"i1o\n");
    else
        serialport_write(global_fd,"i1f\n");
}
void color_sensor_ledir2(int value) {
    if (value)
        serialport_write(global_fd,"i2o\n");
    else
        serialport_write(global_fd,"i2f\n");
}
void get_color_sensor_value(color_sensor_value *right,color_sensor_value *left) {
    serialport_write(global_fd,"g\n");
    char buf[256];
    if (!serialport_read_until(global_fd, buf, '\n'))
    {
        sscanf(buf,"%d %d %d %d %d %d %d %d\n",&(right->clear), &(right->red),&(right->green),&(right->blue),&(left->clear), &(left->red),&(left->green),&(left->blue));
    }

}
void get_color_sensor_cap(color_sensor_value *tab,int *n)
{
    serialport_write(global_fd,"f\n");
    char buf[1024];
    int z;
    int i;
    z=0;
    int number;
//printf("getvaluz\n");
    if (!serialport_read_until(global_fd, buf, '\n'))
    {
//printf("%s\n",buf);
        sscanf(buf+z,"%d",n);

        for (i=0; i<*n; i++)
        {

            while (buf[z]!=' ')
            {
                z++;
            }
            z++;

            sscanf(buf+z,"%d %d %d %d %d", &number ,&(tab[i].clear),&(tab[i].red),&(tab[i].green),&(tab[i].blue));
            tab[i].clear&=FILTER_MASK;
            tab[i].red&=FILTER_MASK;
            tab[i].green&=FILTER_MASK;
            tab[i].blue&=FILTER_MASK;
            while (*(buf+z)!=' ' )
                z++;
            z++;

            while (*(buf+z)!=' ')
                z++;
            z++;

            while (*(buf+z)!=' ')
                z++;
            z++;

            while (*(buf+z)!=' ')
                z++;
            z++;
        }

    }

}
void get_color_sensor_offset(color_sensor_value *tab,int *n)
{
    serialport_write(global_fd,"u\n");
    char buf[1024];
    int z;
    int i;
    z=0;
    int number;
//printf("getvaluz\n");
    if (!serialport_read_until(global_fd, buf, '\n'))
    {
//printf("%s\n",buf);
        sscanf(buf+z,"%d",n);

        for (i=0; i<*n; i++)
        {

            while (buf[z]!=' ')
            {
                z++;
            }
            z++;

            sscanf(buf+z,"%d %d %d %d %d", &number ,&(tab[i].clear),&(tab[i].red),&(tab[i].green),&(tab[i].blue));
            tab[i].clear&=FILTER_MASK;
            tab[i].red&=FILTER_MASK;
            tab[i].green&=FILTER_MASK;
            tab[i].blue&=FILTER_MASK;
            while (*(buf+z)!=' ' )
                z++;
            z++;

            while (*(buf+z)!=' ')
                z++;
            z++;

            while (*(buf+z)!=' ')
                z++;
            z++;

            while (*(buf+z)!=' ')
                z++;
            z++;
        }

    }

}
void get_color_sensor_gain(color_sensor_value *tab,int *n)
{
    serialport_write(global_fd,"r\n");
    char buf[1024];
    int z;
    int i;
    z=0;
    int number;
//printf("getvaluz\n");
    if (!serialport_read_until(global_fd, buf, '\n'))
    {
//printf("%s\n",buf);
        sscanf(buf+z,"%d",n);

        for (i=0; i<*n; i++)
        {

            while (buf[z]!=' ')
            {
                z++;
            }
            z++;

            sscanf(buf+z,"%d %d %d %d %d", &number ,&(tab[i].clear),&(tab[i].red),&(tab[i].green),&(tab[i].blue));
            tab[i].clear&=FILTER_MASK;
            tab[i].red&=FILTER_MASK;
            tab[i].green&=FILTER_MASK;
            tab[i].blue&=FILTER_MASK;
            while (*(buf+z)!=' ' )
                z++;
            z++;

            while (*(buf+z)!=' ')
                z++;
            z++;

            while (*(buf+z)!=' ')
                z++;
            z++;

            while (*(buf+z)!=' ')
                z++;
            z++;
        }

    }

}
void get_color_sensor_value2(color_sensor_value *tab,int *n)
{
    serialport_write(global_fd,"g\n");
    char buf[1024];
    int z;
    int i;
    z=0;
    int number;
    if (!serialport_read_until(global_fd, buf, '\n'))
    {


        sscanf(buf+z,"%d",n);

        for (i=0; i<*n; i++)
        {

            while (buf[z]!=' ')
            {
                z++;
            }
            z++;

            sscanf(buf+z,"%d %d %d %d %d", &number ,&(tab[i].clear),&(tab[i].red),&(tab[i].green),&(tab[i].blue));
            tab[i].clear&=FILTER_MASK;
            tab[i].red&=FILTER_MASK;
            tab[i].green&=FILTER_MASK;
            tab[i].blue&=FILTER_MASK;
            while (*(buf+z)!=' ' )
                z++;
            z++;

            while (*(buf+z)!=' ')
                z++;
            z++;

            while (*(buf+z)!=' ')
                z++;
            z++;

            while (*(buf+z)!=' ')
                z++;
            z++;
        }

    }

}


int clear_port(int fd)
{
    int i=0;
    int n;
    char c;
    do {
        n = read(fd, &c, 1);  // read a char at a time
    } while ( n<1 );

    return 0;
}


int serialport_writebyte( int fd, uint8_t b)
{
    int n = write(fd,&b,1);
    if ( n!=1)
        return -1;
    return 0;
}

int serialport_write(int fd, const char* str)
{
    int len = strlen(str);
    int n;
    n=0;
    do
    {
        n+=write(fd, str+n, len-n);
    } while (n<len);
    if ( n!=len )
        return -1;
    return 0;
}

int serialport_read_until(int fd, char* buf, char until)
{
    char b;
    int i=0;
    do {
        int n = read(fd, &b, 1);  // read a char at a time
        //if( n==-1){ printf("MOINS UN\n");return -1;}    // couldn't read
        if ( n<1 ) {
            if (n==0)
            {
                printf("SERIAL CLOSED\n");
                return 0;

            }
            usleep(  /*5**/ 1000 ); // wait 10 msec try again
            continue;
        }
        buf[i] = b;
        i++;
    } while ( b != until );

    buf[i] = 0;  // null terminate the string
    return 0;
}

// takes the string name of the serial port (e.g. "/dev/tty.usbserial","COM1")
// and a baud rate (bps) and connects to that port at that speed and 8N1.
// opens the port in fully raw mode so you can send binary data.
// returns valid fd, or -1 on error
int serialport_init(const char* serialport, int baud)
{
    struct termios toptions;
    int fd;

    //fprintf(stderr,"init_serialport: opening port %s @ %d bps\n",
    //        serialport,baud);

    fd = open(serialport, O_RDWR | O_NOCTTY | O_NDELAY);
    if (fd == -1)  {
        perror("init_serialport: Unable to open port ");
        return -1;
    }

    if (tcgetattr(fd, &toptions) < 0) {
        perror("init_serialport: Couldn't get term attributes");
        return -1;
    }
    speed_t brate = baud; // let you override switch below if needed
    switch (baud) {
    case 4800:
        brate=B4800;
        break;
    case 9600:
        brate=B9600;
        break;
#ifdef B14400
    case 14400:
        brate=B14400;
        break;
#endif
    case 19200:
        brate=B19200;
        break;
#ifdef B28800
    case 28800:
        brate=B28800;
        break;
#endif
    case 38400:
        brate=B38400;
        break;
    case 57600:
        brate=B57600;
        break;
    case 115200:
        brate=B115200;
        break;
    }
    cfsetispeed(&toptions, brate);
    cfsetospeed(&toptions, brate);

    // 8N1
    toptions.c_cflag &= ~PARENB;
    toptions.c_cflag &= ~CSTOPB;
    toptions.c_cflag &= ~CSIZE;
    toptions.c_cflag |= CS8;
    // no flow control
    toptions.c_cflag &= ~CRTSCTS;

    toptions.c_cflag |= CREAD | CLOCAL;  // turn on READ & ignore ctrl lines
    toptions.c_iflag &= ~(IXON | IXOFF | IXANY); // turn off s/w flow ctrl

    toptions.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); // make raw
    toptions.c_oflag &= ~OPOST; // make raw

    // see: http://unixwiz.net/techtips/termios-vmin-vtime.html
    toptions.c_cc[VMIN]  = 0;
    toptions.c_cc[VTIME] = 20;
    // tcsetattr(fd,tcsetattr(fd, TCSAFLUSH, &options);


    if ( tcsetattr(fd, TCSAFLUSH, &toptions) < 0) {
        perror("init_serialport: Couldn't set term attributes");
        return -1;
    }

    return fd;
}

